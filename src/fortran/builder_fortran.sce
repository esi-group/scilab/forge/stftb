// This file is part of the stftb toolbox
// Copyright (C) 2018 - Serge Steer
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
function builder_fortran()
  src_c_path = get_absolute_file_path("builder_fortran.sce");
  src_files=["balanc.f" "d1mach.f" "d9b0mp.f" "d9b1mp.f" "d9knus.f" "d9lgmc.f" "dasyik.f" "dasyjy.f" "dbdiff.f" "dbesi0.f" "dbesi1.f" "dbesi.f" "dbesj0.f" "dbesj1.f" "dbesj.f" "dbesk0.f" "dbesk1.f" "dbesk.f" "dbesy0.f" "dbesy1.f" "dbesy.f" "dbkias.f" "dbkisr.f" "dbsi0e.f" "dbsi1e.f" "dbsk0e.f" "dbsk1e.f" "dbskes.f" "dbskin.f" "dbsknu.f" "dbsynu.f" "dcsevl.f" "dexint.f" "dgamlm.f" "dgamln.f" "dgamma.f" "dgamrn.f" "dhkseq.f" "djairy.f" "dlngam.f" "dpsixn.f" "dtensbs.f" "dxlegf.f" "dyairy.f" "fdump.f" "gamma.f" "i1mach.f" "initds.f" "j4save.f" "pchim.f" "xercnt.f" "xermsg.f" "xerprn.f" "xersve.f" "xgetua.f" "zabs.f" "zacai.f" "zacon.f" "zairy.f" "zasyi.f" "zbesh.f" "zbesi.f" "zbesj.f" "zbesk.f" "zbesy.f" "zbinu.f" "zbknu.f" "zbuni.f" "zbunk.f" "zdiv.f" "zexp.f" "zkscl.f" "zlog.f" "zmlri.f" "zmlt.f" "zrati.f" "zs1s2.f" "zseri.f" "zshch.f" "zsqrt.f" "zuchk.f" "zunhj.f" "zuni1.f" "zuni2.f" "zunik.f" "zunk1.f" "zunk2.f" "zuoik.f" "zwrsk.f"];
  
  inc_files=[]
  FFLAGS = ""
  if newest(src_c_path+['date_build','builder_c.sce',src_files])<>1 then
    curdir=pwd();
    try
      tbx_build_src("slatec", src_files,"c",src_c_path,[],"",FFLAGS);
      mputl(sci2exp(getdate()),src_c_path+'date_build');
    catch
      cd(curdir);
      mprintf("%s\n",lasterror());
    end           
  end
endfunction

builder_fortran();
clear builder_fortran; 
