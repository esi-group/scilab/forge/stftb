mode(-1)
// This file is part of the stftb toolbox
// Copyright (C) 2013 - Serge Steer
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
function builder_src()
  src_path = get_absolute_file_path("builder_src.sce");
  tbx_builder_src_lang("fortran", src_path);
endfunction
builder_src();
clear builder_src; // remove builder_src on stack


