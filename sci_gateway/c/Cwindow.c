/* Gateway for Scilab Cwindow function */

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
/* Interface program between MATLAB and language C
for the program CREATE_WINDOW.C */


#include <stdlib.h>
#include <stdio.h>
#include "tftb.h"

#include "api_scilab.h"
#include "Scierror.h"
#include "localization.h"
#include "sciprint.h"
#include "sci_malloc.h"


int int_Cwindow(scilabEnv env,
                 int nin, 
                 scilabVar *in,
                 int nopt, 
                 scilabOpt opt, 
                 int nout, 
                 scilabVar*out)



{
  char fname[] = "Cwindow";
  int       Window_length = 0, Window_name = 0, nb_param = 0;
  int       Window_name_length = 0;
  double   *Window = NULL;
  double    param[2];
  wchar_t*      Window_name_string=0;
  int mVar = 0, nVar = 0;
  double temp=0.0;
  
 /* tests the number of inputs and outputs */
  if ((nin < 2) || (nin > 4))
    {
      Scierror(999,_("%s: Wrong number of input argument: %d to %d expected."),fname,2,4);
      return STATUS_ERROR;
    }
  if  (nout != 1) 
    {
      Scierror(999,_("%s: Wrong number of output argument(s): %d expected."),fname,1);
      return STATUS_ERROR;
    }
 
   
  /* Recovery of N the length of the window*/
   if (scilab_isDouble(env, in[0]) == 0 || 
      scilab_isScalar(env, in[0]) == 0 ||
      scilab_isComplex(env, in[0]) == 1)
    {
      Scierror(999, _("%s: Wrong size for input argument #%d : A real scalar expected.\n"), fname, 1);
      return STATUS_ERROR;
    }
   scilab_getDouble(env, in[0], &temp);
   Window_length = (int) temp;
   if (Window_length <= 0)
     {
       Scierror(999,_("%s: Wrong type for input argument #%d: A positive integer expected.\n"),fname,1);
       return STATUS_ERROR;
     }

   /* Recovery of NAME the name of the window shape*/
   if (scilab_isString(env, in[1]) == 0 || scilab_isScalar(env, in[1]) == 0)
    {
        Scierror(999, _("Wrong type for input argument #%d: A String expected."), fname, 2);
        return STATUS_ERROR;
    }
   scilab_getString(env, in[1], &Window_name_string);
  
  /* window name recovery */
  Window_name = 0;
 
  if((!(wcscmp(Window_name_string,L"HAMMING"))) ||
     (!(wcscmp(Window_name_string,L"hamming"))) ||
     (!(wcscmp(Window_name_string,L"Hamming"))))
    {
      Window_name = HAMMING;
    }
  if((!(wcscmp(Window_name_string,L"HANNING"))) ||
     (!(wcscmp(Window_name_string,L"hanning"))) ||
     (!(wcscmp(Window_name_string,L"Hanning"))))
    {
      Window_name = HANNING;
    }
  if((!(wcscmp(Window_name_string,L"KAISER"))) ||
     (!(wcscmp(Window_name_string,L"kaiser"))) ||
     (!(wcscmp(Window_name_string,L"Kaiser"))))
    {
      Window_name = KAISER;
    }
  if((!(wcscmp(Window_name_string,L"NUTTALL"))) ||
     (!(wcscmp(Window_name_string,L"nuttall"))) ||
     (!(wcscmp(Window_name_string,L"Nuttall"))))
    {
      Window_name = NUTTALL;
    }
  if((!(wcscmp(Window_name_string,L"PAPOULIS"))) ||
     (!(wcscmp(Window_name_string,L"papoulis"))) ||
     (!(wcscmp(Window_name_string,L"Papoulis"))) ||
     (!(wcscmp(Window_name_string,L"SINE")))||
     (!(wcscmp(Window_name_string,L"sine")))||
     (!(wcscmp(Window_name_string,L"Sine"))))
    {
      Window_name = SINE;
    }
  if((!(wcscmp(Window_name_string,L"HARRIS"))) ||
     (!(wcscmp(Window_name_string,L"harris"))) ||
     (!(wcscmp(Window_name_string,L"Harris"))))
    {
      Window_name = HARRIS;
    }
  if((!(wcscmp(Window_name_string,L"RECT"))) ||
     (!(wcscmp(Window_name_string,L"rect"))) ||
     (!(wcscmp(Window_name_string,L"Rect"))))
    {
      Window_name = RECTANG;
    }
  if((!(wcscmp(Window_name_string,L"TRIANG"))) ||
     (!(wcscmp(Window_name_string,L"triang"))) ||
     (!(wcscmp(Window_name_string,L"Triang"))))
    {
      Window_name = TRIANG;
    }
  if((!(wcscmp(Window_name_string,L"BARTLETT"))) ||
     (!(wcscmp(Window_name_string,L"bartlett"))) ||
     (!(wcscmp(Window_name_string,L"Bartlett"))))
    {
      Window_name = BARTLETT;
    }
  if((!(wcscmp(Window_name_string,L"BARTHANN"))) ||
     (!(wcscmp(Window_name_string,L"barthann"))) ||
     (!(wcscmp(Window_name_string,L"Barthann"))))
    {
      Window_name = BARTHANN ;
    }
  if((!(wcscmp(Window_name_string,L"BLACKMAN"))) ||
     (!(wcscmp(Window_name_string,L"blackman"))) ||
     (!(wcscmp(Window_name_string,L"Blackman"))))
    {
      Window_name = BLACKMAN ;
    }
  if((!(wcscmp(Window_name_string,L"GAUSS"))) ||
     (!(wcscmp(Window_name_string,L"gauss"))) ||
     (!(wcscmp(Window_name_string,L"Gauss"))))
    {
      Window_name = GAUSS;
    }
  if((!(wcscmp(Window_name_string,L"PARZEN"))) ||
     (!(wcscmp(Window_name_string,L"parzen"))) ||
     (!(wcscmp(Window_name_string,L"Parzen"))))
    {
      Window_name = PARZEN;
    }
  if((!(wcscmp(Window_name_string,L"DOLPH"))) ||
     (!(wcscmp(Window_name_string,L"dolph"))) ||
     (!(wcscmp(Window_name_string,L"Dolph"))))
    {
      Window_name = DOLPH;
    }
  if((!(wcscmp(Window_name_string,L"DOLF"))) ||
     (!(wcscmp(Window_name_string,L"dolf"))) ||
     (!(wcscmp(Window_name_string,L"Dolf"))))
    {
      Window_name = DOLF;
    }
  if((!(wcscmp(Window_name_string,L"POWERSINE"))) ||
     (!(wcscmp(Window_name_string,L"powersine"))) ||
     (!(wcscmp(Window_name_string,L"Powersine"))))
    {
      Window_name =POWERSINE ;
    }
   if((!(wcscmp(Window_name_string,L"NUTBESS"))) ||
     (!(wcscmp(Window_name_string,L"nutbess"))) ||
     (!(wcscmp(Window_name_string,L"Nutbess"))))
    {
      Window_name = NUTBESS;
    }
  if((!(wcscmp(Window_name_string,L"SPLINE"))) ||
     (!(wcscmp(Window_name_string,L"spline"))) ||
     (!(wcscmp(Window_name_string,L"Spline"))))
    {
      Window_name = SPLINE;
    }

 if((!(wcscmp(Window_name_string,L"FLATTOP"))) ||
     (!(wcscmp(Window_name_string,L"flattop"))) ||
     (!(wcscmp(Window_name_string,L"Flattop"))))
    {
      Window_name = FLATTOP;
    }

 if((!(wcscmp(Window_name_string,L"FLATTOP_NI"))) ||
     (!(wcscmp(Window_name_string,L"flattop_ni"))) ||
     (!(wcscmp(Window_name_string,L"Flattop_ni"))))
    {
      Window_name = FLATTOP;
    }

 if((!(wcscmp(Window_name_string,L"FLATTOP_M"))) ||
     (!(wcscmp(Window_name_string,L"flattop_m"))) ||
     (!(wcscmp(Window_name_string,L"Flattop_m"))))
    {
      Window_name = FLATTOP_M;
    }

if((!(wcscmp(Window_name_string,L"FLATTOP_SRS"))) ||
     (!(wcscmp(Window_name_string,L"flattop_srs"))) ||
     (!(wcscmp(Window_name_string,L"Flattop_srs"))))
    {
      Window_name = FLATTOP_SRS;
    }


  if (Window_name == 0)
    { 
      //free(Window_name_string);
      Scierror (999,_("%s: Wrong value for input argument #%d: Unknown window type %s\n"),fname,2,Window_name_string);
      return STATUS_ERROR; 
    }

  nb_param = 0;
  
  if (nin >= 3)
    {
      nb_param = 1;
      if (scilab_isDouble(env, in[2]) == 0 || 
          scilab_isScalar(env, in[2]) == 0 ||
          scilab_isComplex(env, in[2]) == 1)
        {
          Scierror(999, _("%s: Wrong size for input argument #%d : A real scalar expected.\n"), fname, 3);
          return STATUS_ERROR;
        }
      scilab_getDouble(env, in[2], &(param[0]));
    }

  /* recovery of the parameters */ 
  if (nin == 4)
    {
      nb_param = 2;
      if (scilab_isDouble(env, in[3]) == 0 || 
          scilab_isScalar(env, in[3]) == 0 ||
          scilab_isComplex(env, in[3]) == 1)
        {
          Scierror(999, _("%s: Wrong size for input argument #%d : A real scalar expected.\n"), fname, 4);
          return STATUS_ERROR;
        }
      scilab_getDouble(env, in[3], &(param[1]));
    }
 
  Window = (double*) ALLOC(Window_length, sizeof(double) );


  create_window(Window_name, Window_length, param, nb_param, Window);


   out[0]=scilab_createDoubleMatrix2d(env, Window_length, 1,0);
   scilab_setDoubleArray(env, out[0], Window);

  return 0;
}

