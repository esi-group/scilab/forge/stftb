/* Gateway for Scilab Cambifunb function */

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA


#include <stdlib.h>
#include <stdio.h>
#include "tftb.h"
#include "api_scilab.h"
#include "Scierror.h"
#include "localization.h"
#include "sci_malloc.h"

int int_Cambifunb(scilabEnv env,
                  int nin, 
                  scilabVar *in,
                  int nopt, 
                  scilabOpt opt, 
                  int nout, 
                  scilabVar*out)
{
  char fname[] = "Cambifunb";
  type_signal    Signal;
  type_AF        AF;
  double        *ptr_delay_vect, *ptr_doppler_vect;
  int            i=0, rem=0,step=0;
  double         third=0.0;
  int retval;
  
  int mVar = 0, nVar = 0;

  if ((nin < 1) || (nin > 3))
    {
      Scierror(999,_("%s: Wrong number of input argument: %d to %d expected."),fname,1,3);
      return STATUS_ERROR;
    }
  if  ((nout < 1) || (nout > 3))
    {
      Scierror(999,_("%s: Wrong number of output argument(s): %d to %d expected."),fname,1,3);
      return STATUS_ERROR;
    }
  // Recovery of  X the Signal 
  if (scilab_isEmpty(env,in[0]) == 1)
    {
      Scierror (999,_("%s: Wrong size for input argument #%d: A non empty matrix expected.\n"), fname, 1);
      return STATUS_ERROR;
    }
  if (scilab_isMatrix2d(env, in[0]) == 0 || 
      scilab_isDouble(env, in[0]) == 0 || 
      scilab_isVector(env, in[0]) == 0 )
    {
      Scierror(999, _("%s: Wrong type for input argument #%d: A double 1D array expected.\n"), fname, 1);
      return STATUS_ERROR;
    }
  if (scilab_isComplex(env, in[0]) == 0)
    {
      scilab_getDoubleArray(env, in[0],&Signal.real_part);
      Signal.is_complex = FALSE;
    }
  else
    {
      scilab_getDoubleComplexArray(env, in[0],&Signal.real_part,&Signal.imag_part);
      Signal.is_complex = TRUE;
    }
  scilab_getDim2d(env, in[0], &mVar,&nVar);
  Signal.length = mVar*nVar;
 

  /* tests whether Signal.length is even or odd */
  rem = ISODD(Signal.length);


  /* recovery of TAU: the vector of delay values*/
  if (nin >= 2)		/* the vector of delays is given */
    {
      if (scilab_isEmpty(env,in[1]) == 1)
        {
          Scierror (999,_("%s: Wrong size for input argument #%d: A non empty matrix expected.\n"), fname, 2);
          return STATUS_ERROR;
        }
      if (scilab_isMatrix2d(env, in[0]) == 0 || 
          scilab_isDouble(env, in[1]) == 0 || 
          scilab_isVector(env, in[1]) == 0 ||
          scilab_isComplex(env, in[1]) == 1)
        {
          Scierror(999, _("%s: Wrong size for input argument #%d: A real vector expected.\n"), fname, 2);
          return STATUS_ERROR;
        }
      scilab_getDoubleArray(env, in[1],&ptr_delay_vect);
      scilab_getDim2d(env, in[1], &mVar,&nVar);
      AF.N_delay = mVar*nVar;
      step = (int)(ptr_delay_vect[1]-ptr_delay_vect[0]);
      if (step != ptr_delay_vect[1]-ptr_delay_vect[0])
        {
          Scierror(999, _("%s: Wrong value for input argument #%d: integer step between elements expected.\n"), fname, 2);
          return STATUS_ERROR;
        } 
      if ((int)ptr_delay_vect[0] != ptr_delay_vect[0]) 
        {
          Scierror(999, _("%s: Wrong value for input argument #%d: integer elements expected.\n"), fname, 2);
          return STATUS_ERROR;
        }
                                                         
      for (i=0; i<AF.N_delay-1; i++) {
        if ((int)(ptr_delay_vect[i+1] - ptr_delay_vect[i]) != step)
          {
            Scierror(999, _("%s: Wrong value for input argument #%d: regularily spaced vector expected.\n"), fname, 2);
            return STATUS_ERROR;
          }
      }
      if (ptr_delay_vect[0]<-Signal.length/2 || ptr_delay_vect[0]>Signal.length/2)
        {
          Scierror(999, _("%s: Wrong value for input argument #%d: Must be in [%g %g].\n"), fname, 2,-Signal.length/2,Signal.length/2);
          return STATUS_ERROR;
        }
    }
  else
    /* the vector of delays is not given */
    {
      AF.N_delay = Signal.length - 1 + rem;
      ptr_delay_vect = NULL;
    }


  /* recovery of the number of doppler bins N */
  if (nin >= 3)		/* the number of doppler bins is given */
    {
      if (scilab_isDouble(env, in[2]) == 0 || 
          scilab_isScalar(env, in[2]) == 0 ||
          scilab_isComplex(env, in[2]) == 1)
        {
          Scierror(999, _("%s: Wrong size for input argument #%d : A real scalar expected.\n"), fname, 3);
          return STATUS_ERROR;
        }
      scilab_getDouble(env, in[2], &third);
      AF.N_doppler = (int)third;
      if (AF.N_doppler > Signal.length)
        {
          Scierror(999,_("%s: Wrong value for input argument #%d: Must be in [%d %d].\n"),fname,3,Signal.length);
          return STATUS_ERROR;
        }
    }
  else
    /* the number of doppler bins is not given */
    {
      AF.N_doppler = Signal.length;
    }
  AF.is_complex = TRUE;

  /* Form output variables*/

  /* The AF matrix */
  /* allocation of memory for the AF matrix according to the pointers required */
  retval=mem_alloc_AF (&AF, NULL, ptr_delay_vect, NULL, NULL);
  if (retval == 4) {
    Scierror (999,_("%s : Memory allocation error.\n"),fname); 
    return STATUS_ERROR;
  }
  /* creation of the vector of delay values in case of no specification */
  if (nin < 2)			/* the vector of delays is not given */
    {
      for (i = 0; i < AF.N_delay; i++)
	{
	  AF.delay_bins[i] = -(AF.N_delay - 1.0) / 2.0 + i;
	}
    }
  /* ---------------------------------------------------- */
  /*             calls the computation function           */
  /* ---------------------------------------------------- */
  retval=af (Signal, AF);
  if (retval == 4) {
    if (ptr_delay_vect != NULL) AF.delay_bins=NULL;
    mem_free_AF(&AF);
    Scierror (999,_("%s : Memory allocation error.\n"),fname); 
    return STATUS_ERROR;
  }
  out[0]= scilab_createDoubleMatrix2d(env, AF.N_doppler, AF.N_delay, 1);
  scilab_setDoubleComplexArray(env, out[0],AF.real_part ,AF.imag_part );
  /* TAU the vector of delay bins */
  if (nout >= 2) 
    {
      out[1]=scilab_createDoubleMatrix2d(env, 1, AF.N_delay,0);
      scilab_setDoubleArray(env, out[1], AF.delay_bins);
    }
    
  /* XI the vector of doppler values */
  if (nout >= 3) 
    {
      out[2]= scilab_createDoubleMatrix2d(env, 1, AF.N_doppler, 0);
      scilab_setDoubleArray(env, out[2],AF.doppler_bins);
     }
  if (ptr_delay_vect != NULL) AF.delay_bins=NULL;
  mem_free_AF(&AF);
  return STATUS_OK;
}
