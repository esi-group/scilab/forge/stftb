/* Gateway for Scilab Ctfrdist function */

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

#include <stdlib.h>
#include <stdio.h>
#include "tftb.h"
#include "api_scilab.h"
#include "Scierror.h"
#include "localization.h"
#include "sciprint.h"
#include "sci_malloc.h"


int int_Ctfrdist(scilabEnv env,
                 int nin, 
                 scilabVar *in,
                 int nopt, 
                 scilabOpt opt, 
                 int nout, 
                 scilabVar*out)

/* at the moment , complex TFRs are not accepted */
{
  char fname[] = "Ctfrdist";
  type_TFR       first_TFR, second_TFR;
  double         coef = 0, dist = 0;
  int            dist_name_length = 0,dist_name=0;
  int            N_time = 0, N_freq = 0;
  wchar_t*       dist_name_string=0;

  int mVar = 0, nVar = 0;
  
  /* tests the number of inputs and outputs */
  if ((nin < 3) || (nin > 4))
    {
      Scierror(999,_("%s: Wrong number of input argument: %d to %d expected."),fname,3,4);
      return STATUS_ERROR;
    }
  if  (nout != 1)
    {
      Scierror(999,_("%s: Wrong number of output argument(s): %d expected."),fname,1);
      return STATUS_ERROR;
    }

  /* recover TFR1*/
  if (scilab_isEmpty(env,in[0]) == 1)
    {
      Scierror (999,_("%s: Wrong size for input argument #%d: A non empty matrix expected.\n"), fname, 1);
      return STATUS_ERROR;
    }
  if (scilab_isMatrix2d(env, in[0]) == 0 || 
      scilab_isDouble(env, in[0]) == 0 || 
      scilab_isComplex(env, in[0]) == 1)
    {
      Scierror(999, _("%s: Wrong type for input argument #%d: A real matrix expected.\n"), fname, 1);
      return STATUS_ERROR;
    }
  scilab_getDim2d(env, in[0], &first_TFR.N_time,&first_TFR.N_freq);
  scilab_getDoubleArray(env, in[0], &first_TFR.real_part);
  first_TFR.is_complex = FALSE;
 
  /* recover TFR2*/
  if (scilab_isEmpty(env,in[1]) == 1)
    {
      Scierror (999,_("%s: Wrong size for input argument #%d: A non empty matrix expected.\n"), fname, 2);
      return STATUS_ERROR;
    }
  if (scilab_isMatrix2d(env, in[1]) == 0 || 
      scilab_isDouble(env, in[1]) == 0 || 
      scilab_isComplex(env, in[1]) == 1)
    {
      Scierror(999, _("%s: Wrong type for input argument #%d: A real matrix expected.\n"), fname, 2);
      return STATUS_ERROR;
    }
  scilab_getDim2d(env, in[1], &second_TFR.N_time,&second_TFR.N_freq);
  scilab_getDoubleArray(env, in[1], &second_TFR.real_part);
  second_TFR.is_complex = FALSE;


  if ((first_TFR.N_time != second_TFR.N_time) ||   (first_TFR.N_freq != second_TFR.N_freq)){
    Scierror (999,_("%s: Wrong size for input arguments #%d and #%d: Same sizes expected.\n"),fname,1,2);
    return STATUS_ERROR;
  }

  /* recovery of the distance name */
  if (scilab_isString(env, in[2]) == 0 || scilab_isScalar(env, in[2]) == 0)
    {
      Scierror(999, _("%s: Wrong type for input argument #%d: A String expected.\n"), fname, 3);
      return STATUS_ERROR;
    }
  scilab_getString(env, in[2], &dist_name_string);
  
  /* distance name recovery */
  
 
  /* convert to distance code see tftb.h */
  if((!(wcscmp(dist_name_string,L"Lq"))) ||
     (!(wcscmp(dist_name_string,L"LQ"))) ||
     (!(wcscmp(dist_name_string,L"lq"))))
    {
      dist_name = LQ;
    }

  if((!(wcscmp(dist_name_string,L"Quadratic"))) ||
     (!(wcscmp(dist_name_string,L"QUADRATIC"))) ||
     (!(wcscmp(dist_name_string,L"quadratic"))))
    {
      dist_name = QUADRATIC;
    }

  if((!(wcscmp(dist_name_string,L"Correlation"))) ||
     (!(wcscmp(dist_name_string,L"CORRELATION"))) ||
     (!(wcscmp(dist_name_string,L"correlation"))))
    {
      dist_name = CORRELATION;
    }

  if((!(wcscmp(dist_name_string,L"Kolmogorov"))) ||
     (!(wcscmp(dist_name_string,L"KOLMOGOROV"))) ||
     (!(wcscmp(dist_name_string,L"kolmogorov"))))
    {
      dist_name = KOLMOGOROV;
    }
    
  if((!(wcscmp(dist_name_string,L"Kullback"))) ||
     (!(wcscmp(dist_name_string,L"KULLBACK"))) ||
     (!(wcscmp(dist_name_string,L"kullback"))))
    {
      dist_name = KULLBACK;
    }
  if((!(wcscmp(dist_name_string,L"Chernoff"))) ||
     (!(wcscmp(dist_name_string,L"CHERNOFF"))) ||
     (!(wcscmp(dist_name_string,L"chernoff"))))
    {
      dist_name = CHERNOFF;
    }
     
  if((!(wcscmp(dist_name_string,L"Matusita"))) ||
     (!(wcscmp(dist_name_string,L"MATUSITA"))) ||
     (!(wcscmp(dist_name_string,L"matusita"))))
    {
      dist_name = MATUSITA;
    }
 
 
  if((!(wcscmp(dist_name_string,L"NLq"))) ||
     (!(wcscmp(dist_name_string,L"NLQ"))) ||
     (!(wcscmp(dist_name_string,L"nlq"))))
    {
      dist_name = NLQ;
    }


  if((!(wcscmp(dist_name_string,L"Lsd"))) ||
     (!(wcscmp(dist_name_string,L"LSD"))) ||
     (!(wcscmp(dist_name_string,L"lsd"))))
    {
      dist_name = LSD;
    }
     
  if((!(wcscmp(dist_name_string,L"Jensen"))) ||
     (!(wcscmp(dist_name_string,L"JENSEN"))) ||
     (!(wcscmp(dist_name_string,L"jensen"))))
    {
      dist_name = JENSEN;
    }
 
  if (dist_name == 0)
    {
      Scierror (999,_("%s: Wrong value for input argument #%d: Must be in the set {%s}.\n"),fname,3,
                "\"Lq\", \"Quadratic\", \"Correlation\", \"Kolmogorov\", \"Kullback\", \"Chernoff\", \"Matusita\", \"NLq\", \"LSD\", \"Jensen\"");
      return STATUS_ERROR;
    }


  /* some tests about the distence coef */
  /* to have or not to have a coefficient ? */

  if ((dist_name == LQ)       || (dist_name == CHERNOFF) ||
      (dist_name == MATUSITA) ||(dist_name == NLQ) || 
      (dist_name == LSD)      || (dist_name == JENSEN))
    {
      if (nin == 3) /* cases where there should be a coef given */
	{
	  Scierror (999,_("%s: An argument  is required for this distance.\n"),fname);
	  return STATUS_ERROR;
	}
      else
        {
          if (scilab_isDouble(env, in[3]) == 0 || 
              scilab_isScalar(env, in[3]) == 0 ||
              scilab_isComplex(env, in[3]) == 1)
            {
              Scierror(999, _("%s: Wrong size for input argument #%d : A real scalar expected.\n"), fname, 4);
              return STATUS_ERROR;
            }
          scilab_getDouble(env, in[3], &coef);

          /* Error cases */
          if ((dist_name == LQ || dist_name == NLQ || dist_name == LSD || dist_name == JENSEN) && (coef <= 0)){
            Scierror (999, _("%s: Wrong value for input argument #%d: Must be > %d.\n"), fname,4,0);
            return STATUS_ERROR;
          }
          if ((dist_name == CHERNOFF) && ((coef > 1) || (coef < 0))){
            Scierror (999, _("%s: Wrong value for input argument #%d: Must be in the interval %s.\n"), fname,4,"[0 1]");
            return STATUS_ERROR;
          }
          if ((dist_name == MATUSITA) && (coef < 1)){
            Scierror (999, _("%s: Wrong value for input argument #%d: Must be >= %d.\n"), fname,4,1);
            return STATUS_ERROR;
          }
        }
    }
  else
    {
      if (nin == 4) /* cases without coefficient */
	{
          Scierror(999,_("%s: Wrong number of input argument: %d expected."),fname,3);
          return STATUS_ERROR;	
	}
    }
  /* computation of the distance */
  distance (first_TFR, second_TFR, dist_name, coef, &dist);
 
  /* Creation of the output variable */
  out[0]= scilab_createDouble(env, dist);

  return STATUS_OK;
}

