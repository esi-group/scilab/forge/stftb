//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
#ifndef TFTB_H
#define TFTB_H 1

#include <math.h>
#include <stdio.h>
#include "kiss_fft.h"


/* WARNING WARNING WARNING WARNING WARNING WARNING */
/* WARNING WARNING WARNING WARNING WARNING WARNING */
/* WARNING WARNING WARNING WARNING WARNING WARNING */
/* WARNING WARNING WARNING WARNING WARNING WARNING */
/* WARNING WARNING WARNING WARNING WARNING WARNING */

/*------------------------------------------------*/
/*          YOU SHOULD NOT EDIT THIS FILE         */
/*------------------------------------------------*/

/* WARNING WARNING WARNING WARNING WARNING WARNING */
/* WARNING WARNING WARNING WARNING WARNING WARNING */
/* WARNING WARNING WARNING WARNING WARNING WARNING */
/* WARNING WARNING WARNING WARNING WARNING WARNING */
/* WARNING WARNING WARNING WARNING WARNING WARNING */


//#if SYSTEME == UNIX
//   #define ALLOC(A,B)     calloc((A),(B))
//   #define FREE(A)        cfree((A)) 
//#else
  #define ALLOC(A,B)      malloc((A)*(B))
//  #define FREE(A)         free((A)) 
//#endif

/*   local functions   */
#define MAX(A, B)       ((A) > (B) ? (A) : (B))
#define MIN(A, B)       ((A) < (B) ? (A) : (B))
#define sgn(A)          ((A) > 0.0 ? 1.0 : -1.0)
#define ABS(a)          (((a) >= (0)) ? (a) : (-a))
#define SWAP(a,b)       {temp = (a); (a)=(b); (b)=temp;}
#define ROUND1(x)       (((((x)-(int)(x))>=0) &&(((x)-(int)(x))<0.5)) ? ((int)(x)) : ((int)(x+1)))
#define ROUND(x)        ((int)(sgn((x))*ROUND1((x)*sgn((x)))))
#define ISODD(x)        ((x/2.0)== ((int)(x/2)) ? 0 : 1)


/* local constants */
#define pi               3.141592653589793
#define EPS              0.0000000001

#define TRUE             1
#define FALSE            0

/* definition of the window identifiers */
#define RECTANG          1
#define HAMMING          2
#define HANNING          3
#define KAISER           4
#define NUTTALL          5
#define BLACKMAN         6
#define HARRIS           7
#define BARTLETT         8
#define TRIANG           8
#define BARTHANN         9
#define SINE             10
#define GAUSS            11
#define PARZEN           12
#define POWERSINE        13
#define DOLPH            14
#define DOLF             14
#define NUTBESS          15
#define SPLINE           16
#define FLATTOP          17
#define FLATTOP_M        18
#define FLATTOP_SRS      19


/* definition of the distance identifiers */
#define LQ               1
#define QUADRATIC        2
#define CORRELATION      3
#define KOLMOGOROV       4
#define KULLBACK         5
#define CHERNOFF         6
#define MATUSITA         7
#define NLQ              8
#define LSD              9
#define JENSEN           10


/* definition of the kernel shapes */
#define MTEK             1
#define RGK              2
#define GMCWK            3
#define WIGNER           4
#define SPECTRO          5


/* parametres for the MTEK */
#define NB_PARAM_MTEK    7
#define ALPHA            parameters[0]
#define BETA             parameters[1]
#define GAMMA            parameters[2]
#define R                parameters[3]
#define TAU_0            parameters[4]
#define NU_0             parameters[5]
#define LAMBDA           parameters[6]

/*-------------------------------------------*/
/*  definition of the structures and types   */
/*-------------------------------------------*/

/* Signal structure */
typedef struct SIG
  {
    int            length;	/* Length of the signal in points */
    double         sample_freq;	/* Sample frequency of the signal */
    double        *time_instants; /* instants of sample for the signal */
    unsigned char  is_complex;	/* TRUE if there exists an imag part */
    double        *real_part;	/* real part of the signal */
    double        *imag_part;	/* imaginary part of the signal */
  }
type_signal;

typedef struct Time_freq_rep
  {
    int            N_freq;	/* number of freq bins in the TFR matrix */
    int            N_time;	/* number of time_bins in the TFR matrix */
    double        *freq_bins;	/* fqs for each line of the matrix */
    double        *time_instants; /* instant for each column of the TFR */
    unsigned char  is_complex;	/* TRUE if there exists an imag part */
    double        *real_part;	/* real part of the TFR */
    double        *imag_part;	/* imaginary part of the TFR */
  }
type_TFR;

typedef struct Ambi_func
  {
    int            N_doppler;	/* number of doppler bins in the AF */
    int            N_delay;	/* number of delay bins in the AF matrix */
    double        *doppler_bins; /* doppler bin for each line of the AF */
    double        *delay_bins;	/* delay bin for each column of the AF */
    unsigned char  is_complex;	/* TRUE if there exists an imag part */
    double        *real_part;	/* real part of the AF */
    double        *imag_part;	/* imaginary part of the AF */
  }
type_AF;
int af (type_signal Signal, type_AF AF);
int af2tfr (type_AF ambif, type_AF kernel, type_TFR tfr);

int bj (type_signal Signal,  double *WindowT, int WindowT_Length,  double *WindowF, int WindowF_Length,   type_TFR tfr);
int bud (type_signal Signal,   double *WindowT, int WindowT_Length,   double *WindowF, int WindowF_Length,   double sigma,   type_TFR tfr);
int create_window (int Window_type, int Window_length, double* param,	int nb_param, double *Window);
int cw (type_signal Signal,    double *WindowT, int WindowT_Length,    double *WindowF, int WindowF_Length,    double sigma,    type_TFR tfr);
int Dwindow (double *h,int n,double *Dh);
double Renyi (type_TFR tfr, double alpha);

double Jensen_inter_index (type_TFR tfr_1, type_TFR tfr_2, double alpha);

int distance (type_TFR first_TFR, type_TFR second_TFR,  int name, double coef, double *dist);
double powof (double x, double alpha);
int fft (int Signal_Length, int Nfft, double *sig_real, double *sig_imag);
int po2 (int n);
double sinc (double x);
int irem( double x, double y);
int idx (int i_row, int j_col, int nb_row);
int ifft (int Signal_Length, int Nfft, double *sig_real, double *sig_imag);


double sqr (double x);
//int Recover_Signal (int class, int signal_number,		type_signal Signal);
int transpose (int N_line, int N_col, double *matrix);
int fftshift (double *vector_in, double *vector_out, int vector_length);
int mem_alloc_signal (type_signal *Signal, double *ptr_time_instant,		  double *ptr_real_part, double *ptr_imag_part);
int mem_free_signal (type_signal *Signal);
int mem_alloc_AF (type_AF *ambig_func,	      double *ptr_doppler_bins, double *ptr_delay_bins,	      double *ptr_real_part, double *ptr_imag_part);
int mem_free_AF (type_AF *ambig_func);
int mem_alloc_TFR (type_TFR *tfr,	       double *ptr_freq_bins, double *ptr_time_instants,	       double *ptr_real_part, double *ptr_imag_part);
int mem_free_TFR (type_TFR *tfr);


int gradient (double *matrix,	  int size_x, int size_y,	  double step_x, double step_y,	  double *grad_x, double *grad_y);
int grd (type_signal Signal,     double *WindowT, int WindowT_Length,     double *WindowF, int WindowF_Length,     double rs, double MoverN,     type_TFR tfr);
int hough (type_TFR tfr, double nb_theta,double  nb_rho,       double* transfo_hough, double* rho_vect, double* theta_vect);
int kernel (int type, double *parameters, int nb_param, type_AF ker);
int mh (type_signal Signal, type_TFR tfr);
int mhs (type_signal Signal,      double *WindowG, int WindowG_Length,      double *WindowH, int WindowH_Length,      type_TFR tfr );
int mmce (type_signal Signal,      double *Window, int Window_Length, int Window_col,      type_TFR tfr);
int page(type_signal Signal, type_TFR tfr);
int pmh (type_signal Signal,    double *Window, int Window_Length,    type_TFR tfr);
int ppage (type_signal Signal,       double *Window, int Window_Length,       type_TFR tfr);
int pwv (type_signal Signal,    double *Window, int Window_Length,    type_TFR tfr);
int reas_spectro (type_signal Signal,	      double *Window, int Window_Length, type_TFR TFR_reassigned, type_TFR TFR_not_reassigned, double *field_time, double *field_freq);
int reassign (type_TFR TFR_to_reassign, double *field_x, double *field_y,	  type_TFR TFR_reassigned,double threshold);
int ri (type_signal Signal, type_TFR tfr);
int ridb (type_signal Signal,     double *WindowT, int WindowT_Length,     double *WindowF, int WindowF_Length,     type_TFR tfr);
int ridbn (type_signal Signal,       double *WindowT, int WindowT_Length,       double *WindowF, int WindowF_Length,       type_TFR tfr);
int Binomial_Kernel(double *RIDBN, int Kernel_Length);
int ridh(type_signal Signal,     double *WindowT, int WindowT_Length,     double *WindowF, int WindowF_Length,     type_TFR tfr);
int ridt (type_signal Signal,     double *WindowT, int WindowT_Length,     double *WindowF, int WindowF_Length,     type_TFR tfr);
int sp (type_signal Signal,      double *Window, int Window_Length,      type_TFR tfr, double *norm_vector);
int spwv (type_signal Signal,    double *WindowT, int WindowT_Length,    double *WindowF, int WindowF_Length,    type_TFR tfr);
int stft (type_signal Signal,     double *Window, int Window_Length,    type_TFR tfr, double *norm_vector);
int wv (type_signal Signal, type_TFR tfr);
int zam (type_signal Signal,    double *WindowT, int WindowT_Length,    double *WindowF, int WindowF_Length,    type_TFR tfr);


#endif /* TFTB_H */
