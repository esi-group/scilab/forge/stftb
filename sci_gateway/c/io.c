//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
#include "tftb.h"

int
sciGetUIntScalar(void* _pvCtx,int varNr, SciErr* sciErr)
{
  
  int* piAddressVar     = NULL;
  int iType = 0;  
  int mVar = 0, nVar = 0;
  double *value;
  
  *sciErr = getVarAddressFromPosition(_pvCtx, varNr, &piAddressVar);
  if(sciErr->iErr)
  {
    printError(sciErr, 0);
    return 0;
  }
  
      /* check input type */
  *sciErr = getVarType(_pvCtx, piAddressVar, &iType);
  if(sciErr->iErr)
  {
    printError(sciErr, 0);
    return 0;
  }
     if ( iType != sci_matrix )
  {
    Scierror(999,"Parameter %d must be a  integer.\n",varNr);
    sciErr->iErr=999;
	 return 0;
  }
  
  *sciErr = getMatrixOfDouble(_pvCtx, piAddressVar,&mVar,&nVar,&value);
  if(sciErr->iErr)
  {
    printError(sciErr, 0);
    return 0;
  }
      if ( ( (mVar !=1) || (nVar !=1) )&& (mVar>0 && nVar>0))
  {
        Scierror(999,"Parameter %d must be a  integer.\n",varNr);
       sciErr->iErr=999;
	 return 0;
  }
  
    if (mVar>0 && nVar>0){
     if ((unsigned int)value[0] != value[0]){
        Scierror(999,"Parameter %d  must be a positive integer\n",varNr);
	sciErr->iErr=999;
	 return 0;
     }
     
   
   return value[0];
 
    } else  {
      Scierror(999,"Parameter %d  must be a positive integer\n",varNr);
	sciErr->iErr=999;
	 return 0;
      
    }
}


int
sciGetIntScalar(void* _pvCtx,int varNr, SciErr* sciErr)
{
  
  int* piAddressVar     = NULL;
  int iType = 0;  
  int mVar = 0, nVar = 0;
  double *value;
  
  *sciErr = getVarAddressFromPosition(_pvCtx, varNr, &piAddressVar);
  if(sciErr->iErr)
  {
    printError(sciErr, 0);
    return 0;
  }
  
      /* check input type */
  *sciErr = getVarType(_pvCtx, piAddressVar, &iType);
  if(sciErr->iErr)
  {
    printError(sciErr, 0);
    return 0;
  }
     if ( iType != sci_matrix )
  {
    Scierror(999,"Parameter %d must be a  integer.\n",varNr);
    sciErr->iErr=999;
	 return 0;
  }
  
  *sciErr = getMatrixOfDouble(_pvCtx, piAddressVar,&mVar,&nVar,&value);
  if(sciErr->iErr)
  {
    printError(sciErr, 0);
    return 0;
  }
      if ( ( (mVar !=1) || (nVar !=1) )&& (mVar>0 && nVar>0))
  {
        Scierror(999,"Parameter %d must be a  integer.\n",varNr);
       sciErr->iErr=999;
	 return 0;
  }
  
    if (mVar>0 && nVar>0){
     if (( int)value[0] != value[0]){
        Scierror(999,"Parameter %d  must be a  integer\n",varNr);
	sciErr->iErr=999;
	 return 0;
     }
     
   
   return value[0];
 
    } else  {
      Scierror(999,"Parameter %d  must be a positive integer\n",varNr);
	sciErr->iErr=999;
	 return 0;
      
    }
}


double
sciGetDoubleScalar(void* _pvCtx,int varNr, SciErr* sciErr)
{
  
  int* piAddressVar     = NULL;
  int iType = 0;  
  int mVar = 0, nVar = 0;
  double *value;
  
  *sciErr = getVarAddressFromPosition(_pvCtx, varNr, &piAddressVar);
  if(sciErr->iErr)
  {
    printError(sciErr, 0);
    return 0;
  }
  
      /* check input type */
  *sciErr = getVarType(_pvCtx, piAddressVar, &iType);
  if(sciErr->iErr)
  {
    printError(sciErr, 0);
    return 0;
  }
     if ( iType != sci_matrix )
  {
    Scierror(999,"Parameter %d must be a  double.\n",varNr);
    sciErr->iErr=999;
	 return 0;
  }
  
  *sciErr = getMatrixOfDouble(_pvCtx, piAddressVar,&mVar,&nVar,&value);
  if(sciErr->iErr)
  {
    printError(sciErr, 0);
    return 0;
  }
      if ( ( (mVar !=1) || (nVar !=1) )&& (mVar>0 && nVar>0))
  {
        Scierror(999,"Parameter %d must be a  double.\n",varNr);
       sciErr->iErr=999;
	 return 0;
  }
  
    if (mVar>0 && nVar>0){
   
   
   return value[0];
 
    } else  {
      Scierror(999,"Parameter %d  must be a double\n",varNr);
	sciErr->iErr=999;
	 return 0;
      
    }
}



int
sciGetStringLength(void* _pvCtx,int varNr, SciErr* sciErr)
{
  int* piAddressVar     = NULL;
  int iType = 0;  
  int mVar = 0, nVar = 0;
  double *value;
  int i=0;
  int* piLenVar = NULL;
  int len;
  
  /* get Address of inputs */
  *sciErr = getVarAddressFromPosition(_pvCtx, varNr, &piAddressVar);
  if(sciErr->iErr)
  {
    printError(sciErr, 0);
    return 0;
  }
  
  
  
  /* check input type */
  *sciErr = getVarType(_pvCtx, piAddressVar, &iType);
  if(sciErr->iErr)
  {
    printError(sciErr, 0);
    return 0;
  }
  if ( iType != sci_strings )
  {
    Scierror(999," Wrong type for input argument #%d: A string expected.\n",varNr);
    sciErr->iErr=999;
    return 0;
  }


  /* get string */
    *sciErr = getMatrixOfString(_pvCtx, piAddressVar,&mVar, &nVar, NULL, NULL);
  //sciErr = getMatrixOfDouble(pvApiCtx, piAddressVarOne,&m1,&n1,&pdVarOne);
  if(sciErr->iErr)
  {
    printError(sciErr, 0);
    return 0;
  }
      if ( ( (mVar !=1) || (nVar !=1) )&& (mVar>0 && nVar>0))
  {
        Scierror(999,"Parameter %d must be a  string.\n",varNr);
       sciErr->iErr=999;
	 return 0;
  }

    piLenVar = (int*)malloc(sizeof(int) * mVar * nVar);
   *sciErr = getMatrixOfString(_pvCtx, piAddressVar, &mVar, &nVar, piLenVar, NULL);
	  if(sciErr->iErr)
	  {
	    printError(sciErr, 0);
	    return 0;
	  }
	  
        len = piLenVar[0]+1 ;
	free (piLenVar);
	return len;
	  
}
int
sciGetString(void* _pvCtx,int varNr, SciErr* sciErr, char *string )
{
  int* piAddressVar     = NULL;
  int iType = 0;  
  int mVar = 0, nVar = 0;
  double *value;
  int i=0;
  int* piLenVar = NULL;
  char **stringData  = NULL;
  int len;
  
  /* get Address of inputs */
  *sciErr = getVarAddressFromPosition(_pvCtx, varNr, &piAddressVar);
  if(sciErr->iErr)
  {
    printError(sciErr, 0);
    return 0;
  }
  
  
  
  /* check input type */
  *sciErr = getVarType(_pvCtx, piAddressVar, &iType);
  if(sciErr->iErr)
  {
    printError(sciErr, 0);
    return 0;
  }
  if ( iType != sci_strings )
  {
    Scierror(999," Wrong type for input argument #%d: A string expected.\n",varNr);
    sciErr->iErr=999;
    return 0;
  }


  /* get string */
    *sciErr = getMatrixOfString(_pvCtx, piAddressVar,&mVar, &nVar, NULL, NULL);
  //sciErr = getMatrixOfDouble(pvApiCtx, piAddressVarOne,&m1,&n1,&pdVarOne);
  if(sciErr->iErr)
  {
    printError(sciErr, 0);
    return 0;
  }
      if ( ( (mVar !=1) || (nVar !=1) )&& (mVar>0 && nVar>0))
  {
        Scierror(999,"Parameter %d must be a  string.\n",varNr);
       sciErr->iErr=999;
	 return 0;
  }

    piLenVar = (int*)malloc(sizeof(int) * mVar * nVar);
   *sciErr = getMatrixOfString(_pvCtx, piAddressVar, &mVar, &nVar, piLenVar, NULL);
	  if(sciErr->iErr)
	  {
	    printError(sciErr, 0);
	    return 0;
	  }

		stringData = (char**)malloc(sizeof(char*) * mVar * nVar);
		for(i = 0 ; i < nVar * mVar ; i++)
		{
			stringData[i] = (char*)malloc(sizeof(char) * (piLenVar[i] + 1));//+ 1 for null termination
		}

		*sciErr = getMatrixOfString(_pvCtx, piAddressVar, &mVar, &nVar, piLenVar, stringData);
		if(sciErr->iErr)
		{
		  printError(sciErr, 0);
		  return 0;
		}
		//string=stringData[0];
		 strcpy(string,stringData[0]);
		
		free (stringData[0]);
		free (stringData);	
		len = piLenVar[0]+1 ;
		free (piLenVar);
		return len;
}




type_signal sciGetSignal(void* _pvCtx,int varNr, SciErr* sciErr)
{
type_signal Signal;
  int* piAddressVar     = NULL;
  int iType = 0;  
  int mVar = 0, nVar = 0;

    *sciErr = getVarAddressFromPosition(_pvCtx, varNr, &piAddressVar);
   if(sciErr->iErr)
	{
	 printError(sciErr, 0);
	 return Signal;
	}
    /* check input type */
  *sciErr = getVarType(_pvCtx, piAddressVar, &iType);
   if(sciErr->iErr)
	{
	 printError(sciErr, 0);
	 return Signal;
	}
 if ( iType != sci_matrix )
  {
    Scierror(999," The signal must  be an  matrix.\n");
    sciErr->iErr=999;
    return Signal;
  }
  if ( isVarComplex(_pvCtx, piAddressVar)){
  *sciErr = getComplexMatrixOfDouble(_pvCtx, piAddressVar, &mVar, &nVar, Signal.real_part, Signal.imag_part );
   if(sciErr->iErr)
	{
	 printError(sciErr, 0);
	 return Signal;
	}
  Signal.is_complex = TRUE;
  
  } else {
      *sciErr = getMatrixOfDouble(_pvCtx, piAddressVar, &mVar, &nVar, Signal.real_part);
   if(sciErr->iErr)
	{
	 printError(sciErr, 0);
	 return Signal;
	}
    
    Signal.is_complex = FALSE;
  }
  /* recovery of the signal */
  Signal.length = (int) MAX (mVar, nVar );//mxGetM (SIGNAL), mxGetN (SIGNAL));

  if (Signal.length == 0){
    Scierror(999,"The signal must not be an empty matrix");
     sciErr->iErr=999;
    return Signal;
  }
  return Signal;

}