/* Gateway for Scilab Ctfrpage function */

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
#include <stdlib.h>
#include <stdio.h>
#include "tftb.h"
#include "api_scilab.h"
#include "Scierror.h"
#include "localization.h"
#include "sciprint.h"
#include "sci_malloc.h"


int int_Ctfrpage(scilabEnv env,
                 int nin, 
                 scilabVar *in,
                 int nopt, 
                 scilabOpt opt, 
                 int nout, 
                 scilabVar*out)

{
  char fname[] = "Ctfrpage";
  type_signal    Signal;
  type_TFR       tfr;
  double        *ptr_time_inst=NULL, *ptr_freq_vect=NULL, *time_out=NULL;
  int            i = 0;
  int retval = 0;
  double temp = 0.0;  
  int mVar = 0, nVar = 0;

  /* tests the number of inputs and outputs */
  if ((nin < 1) || (nin > 3))
    {
      Scierror(999,_("%s: Wrong number of input argument: %d to %d expected."),fname,1,3);
      return STATUS_ERROR;
    }
  if  ((nout < 1) || (nout > 3))
    {
      Scierror(999,_("%s: Wrong number of output argument(s): %d to %d expected."),fname,1,3);
      return STATUS_ERROR;
    }
  /* recover X  the signal*/
  if (scilab_isEmpty(env,in[0]) == 1)
    {
      Scierror (999,_("%s: Wrong size for input argument #%d: A non empty matrix expected.\n"), fname, 1);
      return STATUS_ERROR;
    }
  if (scilab_isMatrix2d(env, in[0]) == 0 || 
      scilab_isDouble(env, in[0]) == 0 || 
      scilab_isVector(env, in[0]) == 0)
    {
      Scierror(999, _("%s: Wrong type for input argument #%d: A double 1D array expected.\n"), fname, 1);
      return STATUS_ERROR;
    }
  scilab_getDim2d(env, in[0], &mVar,&nVar);
  if (scilab_isComplex(env, in[0]) == 1)
    {
      scilab_getDoubleComplexArray(env, in[0], &Signal.real_part, &Signal.imag_part);
      Signal.is_complex = TRUE;
    }
  else
    {
      scilab_getDoubleArray(env, in[0], &Signal.real_part);
      Signal.is_complex = FALSE;
    }
  Signal.length = mVar*nVar;
 
  /* Recover T */
  if (nin >= 2)
    {
      if (scilab_isEmpty(env,in[1]) == 1)
        {
          Scierror (999,_("%s: Wrong size for input argument #%d: A non empty matrix expected.\n"), fname, 2);
          return STATUS_ERROR;
        }
      if (scilab_isMatrix2d(env, in[1]) == 0 || 
          scilab_isDouble(env, in[1]) == 0   || 
         (scilab_isVector(env, in[1]) == 0 && scilab_isScalar(env, in[1]) == 0) || 
          scilab_isComplex(env, in[1]) == 1)
        {
          Scierror(999, _("%s: Wrong size for input argument #%d: Real vector expected.\n"), fname, 2);
          return STATUS_ERROR;
        }
      scilab_getDoubleArray(env, in[1], &ptr_time_inst);
      scilab_getDim2d(env, in[1], &mVar,&nVar);
      tfr.N_time  = mVar*nVar;
   
    }
  else
    /* the time instants are no given */
    {
      /* default : Time_instants = 1:Signal_Length */
      tfr.N_time = Signal.length;
      ptr_time_inst = NULL;
    }

  /* Recover N the number of frequency bins */
  if (nin >= 3)	
    {
      if (scilab_isDouble(env, in[2]) == 0 || 
          scilab_isScalar(env, in[2]) == 0 ||
          scilab_isComplex(env, in[2]) == 1)
        {
          Scierror(999, _("%s: Wrong size for input argument #%d : A real scalar expected.\n"), fname, 3);
          return STATUS_ERROR;
        }
      scilab_getDouble(env, in[2], &temp);
      tfr.N_freq = (int) temp;
      if (tfr.N_freq <= 0)
        {
          Scierror(999,_("%s: Wrong type for input argument #%d: A positive integer expected.\n"),fname,3);
          return STATUS_ERROR;
        }
    }
  else
    /* the number of frequency bins is not given */
    {
      tfr.N_freq = Signal.length;
    }


  /* Call the computation function*/
  /* creation of the output real matrix */
  tfr.is_complex = FALSE;
  retval=mem_alloc_TFR (&tfr, NULL, ptr_time_inst, NULL,NULL);
  if (retval == 4) {
    Scierror (999,_("%s : Memory allocation error.\n"),fname); 
    return STATUS_ERROR;
  }
  
  /* creation of the time_instants vector if not given */
  if (ptr_time_inst == NULL)
    {
      for (i = 0; i < tfr.N_time; i++)
	{
	  tfr.time_instants[i] = i + 1.0;
	}
    }
  
  retval = page(Signal, tfr);
  if (retval == 6) {
    if (ptr_time_inst != NULL) tfr.time_instants=NULL;
    mem_free_TFR(&tfr);
    Scierror (999,_("%s : Memory allocation error.\n"),fname); 
    return STATUS_ERROR;
  }
  /* Form the output variables */

  /* TFR */
  out[0]= scilab_createDoubleMatrix2d(env, tfr.N_freq, tfr.N_time, 0);
  scilab_setDoubleArray(env, out[0],tfr.real_part);
 
  /* T */ 
  if (nout >= 2)
    {
      out[1]= scilab_createDoubleMatrix2d(env, 1, tfr.N_time, 0);
      scilab_setDoubleArray(env, out[1],tfr.time_instants);
    }
  
  /* F */ 
  if (nout >= 3)
    {
      out[2]= scilab_createDoubleMatrix2d(env, 1, tfr.N_freq, 0);
      scilab_setDoubleArray(env, out[2],tfr.freq_bins);
    }
  
  if (ptr_time_inst != NULL) tfr.time_instants=NULL;
  mem_free_TFR(&tfr);

  return STATUS_OK;  
}
