/*====================================================================*
 * Name of the function : window.c                                    *
 * Author               : Manuel DAVY                                 *
 * Date of creation     : 03 - 06 - 1999                              *
 *--------------------------------------------------------------------*
 * THE ALGORITHM                                                      *
 *                                                                    *
 * creates a window of given shapes and length.                       *
 * possible shapes and name :                                         *
 *     Rectangular -> RECTANG                                         *
 *     Hamming     -> HAMMING                                         *
 *     Hanning     -> HANNING                                         *
 *     Kaiser      -> KAISER            (1 optional parameter)        *
 *     Nuttal      -> NUTTALL                                          *
 *     Blackman    -> BLACKMAN                                       *
 *     Harris      -> HARRIS                                          *
 *     Triangular  -> BARTLETT, TRIANG                                *
 *     Barthann    -> BARTHANN                                        *
 *     Papoulis    -> PAPOULIS                                        *
 *     Gauss       -> GAUSS             (1 optional parameter)        *
 *     Parzen      -> PARZEN                                          *
 *     Hanna       -> HANNA             (1 optional parameter)        *
 *     Dolph (Dolf)-> DOLPH, DOLF                                     *
 *     Nutbess     -> NUTBESS           (2 optional parameters)       *
 *     Spline      -> SPLINE            (1 compulsary and 1 optional  *
 *                                       parameter)                   *
 *====================================================================*
 * INPUT VARIABLES                                                    *
 * Name           |                   role                            *
 * Window_type    | Type of window to compute (one of the choices     *
 *                | above)                                            *
 * Window_length  | Length of the window                              *
 * nb_param       | Number of parameters passed to compute the window *
 *                | used only for certain distances  (0<=nb_param<=2) *
 *--------------------------------------------------------------------*
 * OUTPUT VARIABLES                                                   *
 * Name           |                   role                            *
 * i              | index in the window                               *
 *--------------------------------------------------------------------*
 * INTERNAL VARIABLES                                                 *
 * Name           |                   role                            *
 * window         | vector of length 'Window_length' containing the   *
 *                | computed window                                   *
 *====================================================================*
 * SUBROUTINES USED HERE                                              *
 *--------------------------------------------------------------------*
 * Name   |                                                           *
 * Action |                                                           *
 * Place  |                                                           *
 *====================================================================*/

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
#include "machine.h" /* C2F */
#include "tftb.h"
extern void  C2F(zbesj) (double *xr, double *xi, double *alpha,int *kode, int *N, double *rr, double *ri, int *NZ, int *ierr);


                        int 
                        create_window (int Window_type, int Window_length, double* param,
                                       int nb_param, double *Window)
{
  /* variables */
  int i;
  double nu;
  double  beta,ind,K;
  double temp,L;
  double  nfreq,p;
  double  inter;
  /* tests the input variable */
  if (Window_length <= 0)
    {
      //Scierror (999,"create_window.c : Bad window length\n");
      return(1);
    }

  if ((nb_param != 0) && (nb_param != 1) && (nb_param != 2))
    {
      //Scierror (999,"create_window.c : Bad number of parameters\n");
      return(2);
    }



  /* computation according to the window type */
  switch (Window_type)
    {
      /* -----------------------------------------------------*/
    case RECTANG:
      /* -----------------------------------------------------*/
      if (nb_param >0)
        {
          sciprint("create_window.c : no param. required for RECTANG window\n");
        }
      for (i = 0; i < Window_length; i++)
        {
          Window[i] = 1;
        }
      break;

      /* -----------------------------------------------------*/
    case HAMMING:
      /* -----------------------------------------------------*/
      if (nb_param >0)
        {
          sciprint("create_window.c : no param. required for HAMMING window\n");
        }

      for (i = 0; i < Window_length; i++)
        {
          Window[i] = 0.54 - 0.46 * cos (2.0*pi*i/(Window_length - 1.0));
        }
      break;
      /* -----------------------------------------------------*/
    case HANNING:
      /* -----------------------------------------------------*/
      if (nb_param >0)
        {
          sciprint("create_window.c : no param. required for HANNING window\n");
        }

      for (i = 0; i < Window_length; i++)
        {
          Window[i] = 0.5*(1.0 - cos (2.0*pi*i /(Window_length - 1.0)));
        }
      break;

      /* -----------------------------------------------------*/
    case KAISER:
      /* -----------------------------------------------------*/
      /* local variable */
      {
        double xr=0.0,xi=0.0,alpha=0.0,wr,wi;
        int un=1,zero=0,IERR,nz;
     

        if (nb_param >1)
          {
            sciprint("create_window.c : maximum one param. required for KAISER window\n");
          }

        if (nb_param == 1)
          {
            beta =  param[0];
          }
        else
          {
            beta= 3.0 * pi ;
          }
        for (i = 0; i < Window_length; i++)
          {
            ind=2.0*i/(Window_length-1) - 1.0;
            xi=beta*sqrt(1.0-ind*ind);
            C2F(zbesj) (&xr, &xi, &alpha, &un, &un, &wr,&wi,&nz,&IERR);
            C2F(zbesj) (&xr, &beta, &alpha, &un,&un, &xi,&wi,&nz,&IERR);
            Window[i]=wr/xi;
          }
     
      }
      break;

      /* -----------------------------------------------------*/
    case NUTTALL:
      /* -----------------------------------------------------*/
      /* local variable */
      {
        // double   ind;

        if (nb_param >0)
          {
            sciprint("create_window.c : no param. required for NUTTAL window\n");
          }

        for (i = 0; i < Window_length; i++)
          {
            ind = 2.0*pi*i/(Window_length - 1.0);
            Window[i] = 0.3635819 - 0.4891775*cos(ind) + 0.1363995*cos(2.0*ind) -0.0106411*cos(3.0*ind) ;
          }
      }
      break;

      /* -----------------------------------------------------*/
    case BLACKMAN:
      /* -----------------------------------------------------*/
      /* local variable */
      {
        if (nb_param >0)
          {
            sciprint("create_window.c : no param. required for BLACKMANN window\n");
          }

        for (i = 0; i < Window_length; i++)
          {
            ind = 2.0*pi*i/(Window_length - 1.0);
            Window[i] = 0.42 - 0.5 * cos(ind) +0.08 * cos (2.0 * ind);
          }
      }
      break;

      /* -----------------------------------------------------*/
    case HARRIS:
      /* -----------------------------------------------------*/
      /* local variable */
      {
        if (nb_param >0)
          {
            sciprint("create_window.c : no param. required for HARRIS window\n");
          }

        for (i = 0; i < Window_length; i++)
          {
            ind = 2.0*pi*i/(Window_length - 1.0);
            Window[i] = 0.35875-0.48829 * cos(ind)+0.14128 * cos(2.0*ind)-0.01168 *cos(3.0*ind);
          }
      }
      break;

      /* -----------------------------------------------------*/
    case BARTLETT: /* or case TRIANG */
      /* -----------------------------------------------------*/
      /* local variable */
      {
        if (nb_param >0)
          {
            sciprint("create_window.c : no param. required for BARTLETT window\n");
          }

        for (i = 0; i < Window_length; i++)
          {
            Window[i] = 1.0 - fabs(2.0*i/(Window_length-1) - 1.0);
          }
      }
      break;


      /* -----------------------------------------------------*/
    case BARTHANN:
      /* -----------------------------------------------------*/
      /* local variable */
      {
        if (nb_param >0)
          {
            sciprint("create_window.c : no param. required for BARTHANN window\n");
          }

        for (i = 0; i < Window_length; i++)
          {
            ind = i/(Window_length - 1.0) - 0.5;
            Window[i] = 0.62-0.48*fabs(ind)+0.38*cos(2*pi*ind);
          }
      }
      break;

      /* -----------------------------------------------------*/
    case SINE:
      /* -----------------------------------------------------*/
      /* local variable */
      {
        if (nb_param >0)
          {
            sciprint("create_window.c : no param. required for PAPOULIS window\n");
          }

        for (i = 0; i < Window_length; i++)
          {
            Window[i] = sin (i*pi/(Window_length - 1.0));
          }
      }
      break;

      /* -----------------------------------------------------*/
    case GAUSS:
      /* -----------------------------------------------------*/
      /* local variable */
      {
        if (nb_param >1)
          {
            sciprint("create_window.c : maximum one param. required for GAUSS window\n");
          }
     
        if (nb_param == 1)
          {
            K = param[0];
          }
        else
          {
            K = 0.005;
          }

        for (i = 0; i < Window_length; i++)
          {
            ind = -1.0 + (2.0*i) / (Window_length - 1.0);
            Window[i] = exp((ind * ind) * log(K));
          }
      }
      break;

      /* -----------------------------------------------------*/
    case PARZEN:
      /* -----------------------------------------------------*/
      /* local variable */
      {
        // double   ind , temp;

        if (nb_param >0)
          {
            sciprint("create_window.c : no param. required for PARZEN window\n");
          }

        for (i = 0; i < Window_length; i++)
          {
            ind = ABS(((-(Window_length - 1.0)/2.0 + i)) * 2.0 / Window_length);
            temp = 2.0 * powof(1.0 - ind,3);
            Window[i] = MIN(temp - (1.0 - 2.0*ind) * (1.0 - 2.0*ind) * (1.0 - 2.0*ind) , temp );
          }
      }
      break;

      /* -----------------------------------------------------*/
    case POWERSINE:
      /* -----------------------------------------------------*/
      /* local variable */
      {
        if (nb_param >1)
          {
            sciprint("create_window.c : maximum one param. required for HANNA window\n");
          }

        if (nb_param == 1)
          {
            L = param[0];
          }
        else
          {
            L = 1;
          }

        for (i = 0; i < Window_length; i++)
          {
            Window[i] = powof(
                              sin(i * pi / (Window_length-1.0)),
                              2.0 * L);
          }
      }
      break;

      /* -----------------------------------------------------*/
    case DOLPH: /* or case DOLF */
      /* -----------------------------------------------------*/
      /* local variable */
      {     
        sciprint("create_window : window DOLPH not implemented yet\n");
      }
      break;


      /* -----------------------------------------------------*/
    case NUTBESS:
      /* -----------------------------------------------------*/
      /* local variable */

      {
        double xr=0.0,xi=0.0,alpha=0.0,wr,wi;
        int un=1,zero=0,IERR,nz;
        beta= 3.0 * pi;
        nu = 0.5 ;

        if (nb_param >2)
          {
            sciprint("create_window.c : maximum two param. required for NUTBESS window\n");
          }
     
        if (nb_param == 1)
          {
            beta = param[0];
          }
        if (nb_param == 2)
          {
            nu = param[1];
          }

        for (i = 0; i < Window_length; i++)
          {
            ind= (2.0*i-(Window_length-1))/Window_length;
            xi=beta*sqrt(1.0-ind*ind);
            C2F(zbesj) (&xr, &xi, &nu, &un, &un, &wr,&wi,&nz,&IERR);
            C2F(zbesj) (&xr, &beta, &nu, &un,&un, &xi,&wi,&nz,&IERR);
            Window[i]=pow(sqrt(1.0-ind*ind),nu)*wr/xi;
          }
    
      }
      break;


      /* -----------------------------------------------------*/
    case SPLINE:
      /* -----------------------------------------------------*/
      /* local variable */
      {
        // double     nfreq,p;
        // double ind, inter;

        if ((nb_param != 1) && (nb_param != 2))
          {
            sciprint("create_window.c : One/two parameter required for SPLINE window\n");
            return(3);
          }
        else
          {
            nfreq = param[0];
          }
        if (nb_param == 2)
          {
            p = param[1];
          }
        else
          {
            p = pi * Window_length * nfreq / 10.0;
          }


        for (i = 0; i < Window_length; i++)
          {
            ind = -(Window_length - 1.0)/2.0 + i;
            inter = (0.5 * nfreq / p) * ind ;

            if (inter != 0.0)
              {
                inter = sin(inter * pi)/(inter * pi);
                Window[i] = powof(inter,p);
              }
            else
              {
                Window[i] = 1.0;
              }
          }
      }
      break;
      /* -----------------------------------------------------*/
    case FLATTOP:
      /* -----------------------------------------------------*/
      if (nb_param >0)
        {
          sciprint("create_window.c : no param. required for PARZEN window\n");
        }

      for (i = 0; i < Window_length; i++){
        ind = 2.0*pi*i/(Window_length - 1.0);
        Window[i]=0.2810639 -0.5208972*cos(ind)+0.1980399*cos(2.0*ind) ;
      }
      break;

    case FLATTOP_M:
      /* -----------------------------------------------------*/
      if (nb_param >0)
        {
          sciprint("create_window.c : no param. required for PARZEN window\n");
        }

      for (i = 0; i < Window_length; i++){
        ind = 2.0*pi*i/(Window_length - 1.0);
        Window[i]=0.21557895-0.41663158*cos(ind)+0.277263158*cos(2*ind)-0.083578947*cos(3*ind)+0.006947368*cos(4*ind);
      }
      break;

    case FLATTOP_SRS:
      /* -----------------------------------------------------*/
      if (nb_param >0)
        {
          //sciprint("create_window.c : no param. required for PARZEN window\n");
        }

      for (i = 0; i < Window_length; i++){
        ind = 2.0*pi*i/(Window_length - 1.0);
        Window[i]=1.0-1.93*cos(ind)+1.29*cos(2*ind)-0.388*cos(3*ind)+0.028*cos(4*ind);
      }
      break;



    default :
      {
        //Scierror (999,"create_window.c : Unknowm window type\n");
        return(4);
      }
      break;

    }
  return 0;

}
