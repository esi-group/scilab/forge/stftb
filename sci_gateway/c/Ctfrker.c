/* Gateway for Scilab Ctfrker function 
SEE kernel.c for comments */

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
#include <stdlib.h>
#include <stdio.h>
#include "tftb.h"
#include "api_scilab.h"
#include "Scierror.h"
#include "localization.h"
#include "sciprint.h"
#include "sci_malloc.h"


int int_Ctfrker(scilabEnv env,
                int nin, 
                scilabVar *in,
                int nopt, 
                scilabOpt opt, 
                int nout, 
                scilabVar*out)


{
  char fname[] = "Ctfrker";
  double        *parameters=NULL;
  type_AF        ker;
  int            kernel_name = 0, nb_param = 0, kernel_name_length = 0;
  wchar_t*       kernel_name_string=0;
  double         temp=0.0;
  int retval = 0;
  
  int mVar = 0, nVar = 0;

  if ((nin < 3) || (nin > 4))
    {
      Scierror(999,_("%s: Wrong number of input argument: %d to %d expected."),fname,3,4);
      return STATUS_ERROR;
    }
  if  (nout != 1) 
    {
      Scierror(999,_("%s: Wrong number of output argument(s): %d expected."),fname,1);
      return STATUS_ERROR;
    }

  /* Recovery of the kernel dimensions */
 //attention N_doppler et N_delay order changed according to the help page
  if (scilab_isMatrix2d(env, in[0]) == 0 || 
      scilab_isDouble(env, in[0]) == 0 || 
      scilab_isScalar(env, in[0]) == 0 || 
      scilab_isComplex(env, in[0]) == 1)
    {
      Scierror(999, _("%s: Wrong type for argument %d: Real scalar expected.\n"), fname, 1);
      return STATUS_ERROR;
    }
  scilab_getDouble(env, in[0], &temp);
 
  ker.N_doppler=(int)temp;
  if (ker.N_doppler <= 0)
    {
      Scierror(999, _("%s: Wrong type for input argument #%d: A positive integer expected.\n"), fname, 1);
      return STATUS_ERROR;
    }


  if (scilab_isMatrix2d(env, in[1]) == 0 || 
      scilab_isDouble(env, in[1]) == 0 || 
      scilab_isScalar(env, in[1]) == 0 || 
      scilab_isComplex(env, in[1]) == 1)
    {
      Scierror(999, _("%s: Wrong type for argument %d: Real scalar expected.\n"), fname, 2);
      return STATUS_ERROR;
    }
  scilab_getDouble(env, in[1], &temp);
 
  ker.N_delay=(int)temp;
  if (ker.N_delay <= 0)
    {
      Scierror(999, _("%s: Wrong type for input argument #%d: A positive integer expected.\n"), fname, 2);
      return STATUS_ERROR;
    }

  ker.is_complex = FALSE;
 
 /* kernel name recovery */
  if (scilab_isString(env, in[2]) == 0 || scilab_isScalar(env, in[2]) == 0)
    {
        Scierror(999, _("Wrong type for input argument #%d: A String expected."), fname, 3);
        return STATUS_ERROR;
    }
   scilab_getString(env, in[2], &kernel_name_string);

 
  kernel_name = 0;
 
  if((!(wcscmp(kernel_name_string,L"MTEK"))) ||
     (!(wcscmp(kernel_name_string,L"mtek"))) ||
     (!(wcscmp(kernel_name_string,L"Mtek"))))
    {
      kernel_name = MTEK;
    }
  if((!(wcscmp(kernel_name_string,L"GMCWK"))) ||
     (!(wcscmp(kernel_name_string,L"gmcwk"))) ||
     (!(wcscmp(kernel_name_string,L"Gmcwk"))))
    {
      kernel_name = GMCWK;
    }
  if((!(wcscmp(kernel_name_string,L"RGK"))) ||
     (!(wcscmp(kernel_name_string,L"rgk"))) ||
     (!(wcscmp(kernel_name_string,L"Rgk"))))
    {
      kernel_name = RGK;
    }
  if((!(wcscmp(kernel_name_string,L"WV"))) ||
     (!(wcscmp(kernel_name_string,L"Wv"))) ||
     (!(wcscmp(kernel_name_string,L"wv"))))
    {
      kernel_name = WIGNER;
    }
  if((!(wcscmp(kernel_name_string,L"SPECTRO"))) ||
     (!(wcscmp(kernel_name_string,L"spectro"))) ||
     (!(wcscmp(kernel_name_string,L"Spectro"))))
    {
      kernel_name = SPECTRO;
    }

  if (kernel_name == 0)
    {
      Scierror (999,_("%s: Wrong value for input argument #%d: Must be in the set {%s}.\n"),fname,3,
                "\"MTEK\", \"RGK\", \"GMCWK\", \"WV\", \"SPECTRO\"");
      return STATUS_ERROR;
    }



  /* recovers the parameters from inputs */
  //   parameters = mxGetPr (PARAMETERS);
  //   nb_param = (int) MAX (mxGetM (PARAMETERS), mxGetN (PARAMETERS));
  if (kernel_name!=WIGNER) {
    if (scilab_isDouble(env, in[3]) == 0 || 
              scilab_isVector(env, in[3]) == 0 ||
              scilab_isComplex(env, in[3]) == 1)
            {
              Scierror(999, _("%s: Wrong size for input argument #%d : A real vector expected.\n"), fname, 4);
              return STATUS_ERROR;
            }
    scilab_getDoubleArray(env, in[3], &parameters);
    scilab_getDim2d(env, in[3], &mVar,&nVar);
    nb_param  = mVar*nVar;

    /* error cases */
    if (kernel_name == MTEK)
    {
      if (nb_param != NB_PARAM_MTEK){
	Scierror(999,_("%s: Wrong size for input argument #%d: A %d-element vector expected.\n"),fname,4,7);
	return STATUS_ERROR;
      }
      if (!(((BETA == 1) && (GAMMA == 1)) || ((BETA == 2) && (GAMMA == 0.5)))){
	Scierror(999,_("%s: Wrong value for input argument #%d: Incorrect value MTEK: beta=gamma=1 or beta=2 and gamma=0.5"),fname,4);
	return STATUS_ERROR;
      }
    }
    if (kernel_name == GMCWK)
      {
        if (nb_param < 2){
          Scierror(999,_("%s: Wrong size for input argument #%d: At least %d elements expected.\n"),fname,4,2);
          return STATUS_ERROR;
        }
      }
    if (kernel_name == RGK)
    {
      if (nb_param<3) {
	Scierror(999,_("%s: Wrong size for input argument #%d: At least %d elements expected.\n"),fname,4,3);
        return STATUS_ERROR;
      }
      if (nb_param%2 == 0)
        {
          Scierror(999,_("%s: Wrong size for input argument #%d: An odd number of elements expected.\n"),fname,4);
          return STATUS_ERROR;
        } 
    }
    if (kernel_name == SPECTRO)
    {
      if (nb_param%2 == 1){
	Scierror(999,_("%s: Wrong size for input argument #%d: An even number of elements expected.\n"),fname,4);
        return STATUS_ERROR;
      }
      if (nb_param > ker.N_delay){
	Scierror(999,_("%s: Wrong size for input argument #%d: It must be less than input argument #%d\n"),fname,4,2);
        return STATUS_ERROR;
      }
    }
  } else {
    nb_param=0;
    parameters=NULL;
  }

  /* pointer on the results matrix */
  ker.real_part = (double*)malloc(sizeof(double) * ker.N_doppler*ker.N_delay );
  if ( ker.real_part ==NULL)
  {
    Scierror (999,_("%s : Memory allocation error.\n"),fname); 
    return STATUS_ERROR;
  }

  /* computation of the kernel - call to kernel.c */
  retval = kernel (kernel_name, parameters, nb_param, ker);
  if (retval == 5)
    {
      free(ker.real_part);ker.real_part=NULL;
      Scierror (999,_("%s : Memory allocation error.\n"),fname); 
      return STATUS_ERROR;
    }
  out[0]=scilab_createDoubleMatrix2d(env, ker.N_doppler, ker.N_delay, 0);
  scilab_setDoubleArray(env, out[0], ker.real_part);
  free(ker.real_part);ker.real_part=NULL;

  return STATUS_OK;
}
