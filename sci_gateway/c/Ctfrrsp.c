/* Gateway for Scilab Ctfrrsp function */

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

#include <stdlib.h>
#include <stdio.h>
#include "tftb.h"
#include "api_scilab.h"
#include "Scierror.h"
#include "localization.h"
#include "sciprint.h"
#include "sci_malloc.h"


int int_Ctfrrsp(scilabEnv env,
                 int nin, 
                 scilabVar *in,
                 int nopt, 
                 scilabOpt opt, 
                 int nout, 
                 scilabVar*out)
{
  char fname[] = "Ctfrrsp";
  type_signal    Signal;
  type_TFR       TFR_reassigned, TFR_not_reassigned;
  double        *ptr_time_inst=NULL, *ptr_tfr_not_reas=NULL, *time_out=NULL;
  double        *field_time=NULL, *field_freq=NULL;
  double        *Window=NULL;
  double        step_time;
  int           Window_Length;
  int           time=0;

  int retval = 0, i =0;  
  int mVar = 0, nVar = 0;
  double temp = 0.0;

  /* tests the number of inputs and outputs */
  if ((nin < 1) || (nin > 4))
    {
      Scierror(999,_("%s: Wrong number of input argument: %d to %d expected."),fname,1,4);
      return STATUS_ERROR;
    }
  if  ((nout < 1) || (nout > 3))
    {
      Scierror(999,_("%s: Wrong number of output argument(s): %d to %d expected."),fname,1,3);
      return STATUS_ERROR;
    }

  /* recover X  the signal*/
  if (scilab_isEmpty(env,in[0]) == 1)
    {
      Scierror (999,_("%s: Wrong size for input argument #%d: A non empty matrix expected.\n"), fname, 1);
      return STATUS_ERROR;
    }
  if (scilab_isMatrix2d(env, in[0]) == 0 || 
      scilab_isDouble(env, in[0]) == 0 || 
      scilab_isVector(env, in[0]) == 0)
    {
      Scierror(999, _("%s: Wrong type for input argument #%d: A double 1D array expected.\n"), fname, 1);
      return STATUS_ERROR;
    }
  scilab_getDim2d(env, in[0], &mVar,&nVar);
  if (scilab_isComplex(env, in[0]) == 1)
    {
      scilab_getDoubleComplexArray(env, in[0], &Signal.real_part, &Signal.imag_part);
      Signal.is_complex = TRUE;
    }
  else
    {
      scilab_getDoubleArray(env, in[0], &Signal.real_part);
      Signal.is_complex = FALSE;
    }
  Signal.length = mVar*nVar;

  /* Recover T */
  if (nin >= 2)
    {
      if (scilab_isEmpty(env,in[1]) == 1)
        {
          Scierror (999,_("%s: Wrong size for input argument #%d: A non empty matrix expected.\n"), fname, 2);
          return STATUS_ERROR;
        }
      if (scilab_isMatrix2d(env, in[1]) == 0 || 
         (scilab_isVector(env, in[1]) == 0 && scilab_isScalar(env, in[1]) == 0) ||
          scilab_isComplex(env, in[1]) == 1)
        {
          Scierror(999, _("%s: Wrong size for input argument #%d: Real vector expected.\n"), fname, 2);
          return STATUS_ERROR;
        }
      scilab_getDoubleArray(env, in[1], &ptr_time_inst);
      scilab_getDim2d(env, in[1], &mVar,&nVar);
      TFR_reassigned.N_time  = mVar*nVar;
      if (TFR_reassigned.N_time>1)
        {
          step_time = ptr_time_inst[1] - ptr_time_inst[0];
          for (time=1 ; time <(TFR_reassigned.N_time-1) ; time++)
            if ((ptr_time_inst[time+1]-ptr_time_inst[time])!= step_time)
              {
                Scierror(999, _("%s: Wrong value for input argument #%d: regularily spaced vector expected.\n"), fname, 2);
                return STATUS_ERROR;
              }
        }
    }
  else
    /* the time instants are no given */
    {
      /* default : Time_instants = 1:Signal_Length */
      TFR_reassigned.N_time = Signal.length;
      ptr_time_inst = NULL;
    }

  /* Recover N the number of frequency bins */
  if (nin >= 3)	
    {
      if (scilab_isDouble(env, in[2]) == 0 || 
          scilab_isScalar(env, in[2]) == 0 ||
          scilab_isComplex(env, in[2]) == 1)
        {
          Scierror(999, _("%s: Wrong size for input argument #%d : A real scalar expected.\n"), fname, 3);
          return STATUS_ERROR;
        }
      scilab_getDouble(env, in[2], &temp);
      TFR_reassigned.N_freq = (int) temp;
      if (TFR_reassigned.N_freq <= 0)
        {
          Scierror(999,_("%s: Wrong type for input argument #%d: A positive integer expected.\n"),fname,3);
          return STATUS_ERROR;
        }
    }
  else
    /* the number of frequency bins is not given */
    {
      /* default : Nfft = length of the signal */
      TFR_reassigned.N_freq = Signal.length;
    }

 
  /* Recover H the frequency smoothing window*/
  if (nin >= 4)	
    {
      if (scilab_isEmpty(env,in[3]) == 1)
        {
          Scierror (999,_("%s: Wrong size for input argument #%d: A non empty matrix expected.\n"), fname, 4);
          return STATUS_ERROR;
        }
      if (scilab_isMatrix2d(env, in[3]) == 0 || 
          scilab_isDouble(env, in[3]) == 0 || 
          (scilab_isVector(env, in[3]) == 0 && scilab_isScalar(env, in[3]) == 0) || 
          scilab_isComplex(env, in[3]) == 1)
        {
          Scierror(999, _("%s: Wrong size for input argument #%d: Real vector expected.\n"), fname, 4);
          return STATUS_ERROR;
        }
      scilab_getDoubleArray(env, in[3], &Window);
      scilab_getDim2d(env, in[3], &mVar,&nVar);
      Window_Length  = mVar*nVar; 

      if (Window_Length%2 == 0) {
        Scierror (999,_("%s: Wrong size for input argument #%d: An odd number is expected.\n"), fname, 4);
        return STATUS_ERROR;
      } 
    }
  else
    /* the window is not given */
    {
      /* default : gaussian window of length : the next odd
         number after Signal_Length/4 */
      Window_Length = (int) (Signal.length / 4.0);
      if (Window_Length%2 == 0)
	{
	  Window_Length = Window_Length + 1;
	}
      Window = (double *) ALLOC (Window_Length, sizeof (double));
      if (Window == NULL) {
        Scierror (999,_("%s : Memory allocation error.\n"),fname); 
        return STATUS_ERROR;
      }
      create_window(HAMMING,Window_Length,NULL,0,Window);
    }
 
/* Call the computation function*/

  TFR_reassigned.is_complex = FALSE;
  retval = mem_alloc_TFR (&TFR_reassigned, NULL, ptr_time_inst, NULL,NULL);
  if (retval == 4) {
    if (nin < 4) {free(Window);Window=NULL;}
    Scierror (999,_("%s : Memory allocation error.\n"),fname);
    return STATUS_ERROR;
  }
  /* creation of the time_instants vector if not given */
  if (ptr_time_inst == NULL)
    {
      for (i = 0; i < TFR_reassigned.N_time; i++)
        {
          TFR_reassigned.time_instants[i] = i + 1.0;
        }
    }
  TFR_not_reassigned.N_freq = TFR_reassigned.N_freq;
  TFR_not_reassigned.N_time = TFR_reassigned.N_time;
  TFR_not_reassigned.is_complex = FALSE;

  retval = mem_alloc_TFR (&TFR_not_reassigned, NULL, NULL, NULL,NULL);
  if (retval == 4) {
    if (nin < 4) {free(Window);Window=NULL;}
    if (ptr_time_inst != NULL) TFR_reassigned.time_instants=NULL;
    mem_free_TFR(&TFR_reassigned);
    Scierror (999,_("%s : Memory allocation error.\n"),fname);
    return STATUS_ERROR;
  }
  for (i = 0; i < TFR_reassigned.N_time; i++)
    {
      TFR_not_reassigned.time_instants[i] = TFR_reassigned.time_instants[i];
    }

  field_time = (double *) ALLOC (TFR_reassigned.N_time * TFR_reassigned.N_freq, sizeof (double));
  if (field_time == NULL)
    {
      if (nin < 4) {free(Window);Window=NULL;}
      if (ptr_time_inst != NULL) TFR_reassigned.time_instants=NULL;
      mem_free_TFR(&TFR_reassigned);
      mem_free_TFR(&TFR_not_reassigned);
      Scierror (999,_("%s : Memory allocation error.\n"),fname);
      return STATUS_ERROR;
    }
  field_freq = (double *) ALLOC (TFR_reassigned.N_time * TFR_reassigned.N_freq, sizeof (double));
  if (field_freq == NULL)
    {      
      if (nin < 4) {free(Window);Window=NULL;}
      if (ptr_time_inst != NULL) TFR_reassigned.time_instants=NULL;
      mem_free_TFR(&TFR_reassigned);
      mem_free_TFR(&TFR_not_reassigned);
      free(field_time);field_time=NULL;
      Scierror (999,_("%s : Memory allocation error.\n"),fname);
      return STATUS_ERROR;
    }
  retval = reas_spectro(Signal, Window, Window_Length, TFR_reassigned, TFR_not_reassigned,field_time,field_freq);
  if (nin < 4) {free(Window);Window=NULL;}
  if (retval == 6) {
    if (ptr_time_inst != NULL) TFR_reassigned.time_instants=NULL;
    mem_free_TFR(&TFR_reassigned);
    mem_free_TFR(&TFR_not_reassigned);
    free(field_time);field_time=NULL;
    free(field_freq);field_freq=NULL;
    Scierror (999,_("%s : Memory allocation error.\n"),fname); 
    return STATUS_ERROR;
  }
  
  /* Form the output variables */
 
  /* TFR_not_reassigned */
  out[0]= scilab_createDoubleMatrix2d(env, TFR_not_reassigned.N_freq, TFR_not_reassigned.N_time, 0);
  scilab_setDoubleArray(env, out[0],TFR_not_reassigned.real_part);
 
 
  /* TFR_reassigned */ 
  if (nout >= 2)
    {
      out[1]= scilab_createDoubleMatrix2d(env, TFR_reassigned.N_freq, TFR_reassigned.N_time, 0);
      scilab_setDoubleArray(env, out[1],TFR_reassigned.real_part);
    }

 
  if (nout == 3)
    {
      out[2]= scilab_createDoubleMatrix2d(env, TFR_reassigned.N_freq, TFR_reassigned.N_time, 1);
      scilab_setDoubleComplexArray(env, out[2],field_time,field_freq);
    }
  if (ptr_time_inst != NULL) TFR_reassigned.time_instants=NULL;
  mem_free_TFR(&TFR_reassigned);
  mem_free_TFR(&TFR_not_reassigned);
  free(field_time);field_time=NULL;
  free(field_freq);field_freq=NULL;
 
  return STATUS_OK;
}
