/* Gateway for Scilab Ctfrreas function */

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

// 

#include <stdlib.h>
#include <stdio.h>
#include "tftb.h"
#include "api_scilab.h"
#include "Scierror.h"
#include "localization.h"
#include "sciprint.h"
#include "sci_malloc.h"


int int_Ctfrreas(scilabEnv env,
                 int nin, 
                 scilabVar *in,
                 int nopt, 
                 scilabOpt opt, 
                 int nout, 
                 scilabVar*out)

{
  char fname[] = "Ctfrreas";
  double        *field_x = NULL, *field_y = NULL;
  type_TFR       TFR_to_reassign, TFR_reassigned;

  int mVar = 0, nVar = 0;
  
  if ((nin < 1) || (nin > 4))
    {
      Scierror(999,_("%s: Wrong number of input argument: %d expected."),fname,1,3);
      return STATUS_ERROR;
    }
  if  ((nout < 1) || (nout > 3))
    {
      Scierror(999,_("%s: Wrong number of output argument(s): %d expected."),fname,1);
      return STATUS_ERROR;
    }

  /* Recovery of the tfr parameter */
  if (scilab_isEmpty(env,in[0]) == 1)
    {
      Scierror (999,_("%s: Wrong size for input argument #%d: A non empty matrix expected.\n"), fname, 1);
      return STATUS_ERROR;
    }
  if (scilab_isMatrix2d(env, in[0]) == 0 || 
      scilab_isDouble(env, in[0]) == 0 || 
      scilab_isComplex(env, in[0]) == 1)
    {
      Scierror(999, _("%s: Wrong type for argument %d: Real matrix expected.\n"), fname, 1);
      return STATUS_ERROR;
    }
  scilab_getDoubleArray(env, in[0], &TFR_to_reassign.real_part);
  scilab_getDim2d(env, in[0], &TFR_to_reassign.N_freq, &TFR_to_reassign.N_time);
  TFR_to_reassign.is_complex = FALSE;

  /* Recover field_time */
  if (scilab_isEmpty(env,in[1]) == 1)
    {
      Scierror (999,_("%s: Wrong size for input argument #%d: A non empty matrix expected.\n"), fname, 2);
      return STATUS_ERROR;
    }
  if (scilab_isMatrix2d(env, in[1]) == 0 || 
      scilab_isDouble(env, in[1]) == 0 || 
      scilab_isComplex(env, in[1]) == 1)
    {
      Scierror(999, _("%s: Wrong type for argument %d: Real matrix expected.\n"), fname, 2);
      return STATUS_ERROR;
    }
  scilab_getDoubleArray(env, in[1], &field_x);
  scilab_getDim2d(env, in[1], &mVar, &nVar);
 
  if (( TFR_to_reassign.N_freq != mVar) || (TFR_to_reassign.N_time != nVar ) ){
    Scierror(999,_("%s: Incompatible input arguments #%d and #%d: Same sizes expected.\n"),fname,1,2);
    return STATUS_ERROR;
  }

/* Recover field_freq */
  if (scilab_isEmpty(env,in[2]) == 1)
    {
      Scierror (999,_("%s: Wrong size for input argument #%d: A non empty matrix expected.\n"), fname, 3);
      return STATUS_ERROR;
    }
  if (scilab_isMatrix2d(env, in[2]) == 0 || 
      scilab_isDouble(env, in[2]) == 0 || 
      scilab_isComplex(env, in[2]) == 1)
    {
      Scierror(999, _("%s: Wrong type for argument %d: Real matrix expected.\n"), fname, 3);
      return STATUS_ERROR;
    }
  scilab_getDoubleArray(env, in[2], &field_y);
  scilab_getDim2d(env, in[2], &mVar, &nVar);
 
  if (( TFR_to_reassign.N_freq != mVar) || (TFR_to_reassign.N_time != nVar ) ){
    Scierror(999,_("%s: Incompatible input arguments #%d and #%d: Same sizes expected.\n"),fname,1,3);
    return STATUS_ERROR;
  }


 
  /* Creation of the output variable */
  TFR_reassigned.N_time = TFR_to_reassign.N_time;
  TFR_reassigned.N_freq = TFR_to_reassign.N_freq;
  TFR_reassigned.real_part=  (double*)malloc(sizeof(double)*TFR_reassigned.N_freq*TFR_reassigned.N_time); 
  if (TFR_reassigned.real_part==NULL)
  {
    Scierror (999,_("%s : Memory allocation error.\n"),fname); 
    return STATUS_ERROR;
  }

  TFR_reassigned.is_complex = FALSE;

  /* Reassignement of the TFR matrix according to the field of vectors */

  reassign (TFR_to_reassign, field_x, field_y, TFR_reassigned,1e-10);

  out[0]=scilab_createDoubleMatrix2d(env, TFR_reassigned.N_freq, TFR_reassigned.N_time,0);
  scilab_setDoubleArray(env, out[0], TFR_reassigned.real_part);
 
  //free(&TFR_reassigned.real_part);
  return STATUS_OK;
}
