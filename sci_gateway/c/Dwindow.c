/* EXISTS AN INTERFACCE PROGRAM WITH MATLAB : GRADMEX.C               *
 *====================================================================*
 * Name of the function : dwindow (void)                              *
 * Author               : Serge Steer                                 *
 * Date of creation     : 2018                                        *
 *--------------------------------------------------------------------*
 * THE ALGORITHM                                                      *
 * Computes the estimated derivative of a window                      *
 * The algorithm consists in computing, the central slope :           * 
 * slope :                                                            *
 *                                                                    *
 *                    h(i+1) - h(i-1)                                 *
 * Dh(i) =            -----------------                               *
 *                             2                                      *
 *                                                                    *
 * On the edge, the forward difference is computed                    *
 *====================================================================*
 * INPUT VARIABLES                                                    *
 * Name           |                   role                            *
 * h              | the window coefficients                           *
 * n              | the window size (assumed to be odd)               *
 *--------------------------------------------------------------------*
 * OUTPUT VARIABLES                                                   *
 * Name           |                   role                            *
 * Dh             | the coefficient of the estimated derivative       *
 *====================================================================*/
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

#include "tftb.h"
int
Dwindow (double *h,int n,double *Dh)
/* n is assumed to be odd */
{
  int i,Lh;
  double ramp,step;

  if (n <= 1)
    Dh[0] = 0;
  else {
    Lh=(n-1)/2;
    ramp=(h[n-1]-h[0])/(n-1);
    step=(h[n-1]+h[0])/2.0;
    Dh[0]=(h[1]+step+ramp*(Lh+1))/2.0;
    Dh[n-1]=(-h[n-2]-step+ramp*(Lh+1))/2.0;
    for (i=1;i<n-1;i++)
      Dh[i]=(h[i+1]-h[i-1])/2.0;
  }
  return 0;
}
