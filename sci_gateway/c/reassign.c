/* EXISTS AN INTERFACE PROGRAM TO MATLAB : CTFRREAS.C                */
/*====================================================================*
 * Name of the function : reassign (double)                           *
 * Author               : Manuel DAVY                                 * 
 * Date of creation     : 26 - 01 - 1999                              *
 *--------------------------------------------------------------------*
 * THE ALGORITHM                                                      *
 * Given a TFR and a field of vectors  , reassigns the TFR pixels     *
 * according to the field along the directions 'time' (x) and 'freq'  *
 * (y). sometimes, the pixel should be reassigned outside the TFR     *
 * matrix. In time, such pixels are left along the edges (time=0  or  *
 * time = max_time). In frequency, a circular rotation is done in     *
 * order to reassign the pixels somewhere.                            *
 *    perso.ens-lyon.fr/patrick.flandrin/0065-Ch05.pdf                *
 *====================================================================*
 * INPUT VARIABLES                                                    *
 * Name              |                role                            *
 * TFR_to_reassign : | the TFR to be reassigned                       *
 *   .N_time         | Number of time bins i.e number of columns      *
 *   .N_time         | Number of frequency bins i.e. number of rows   *
 *   .is_complex     | must be initialized to the same value as       *
 *                   | 'TFR_reassigned.is_complex'                    *
 *   .real_part      | real part of the tfr to reassign               *
 *   .imag_part      | imag. part of the tfr (only if the field       *
 *                   | 'TFR_to_reassign.is_complex==TRUE'             *
 *                   |                                                *
 * TFR_reassigned    | result of the reassignment. The previous fileds*
 *                   | '.N_time', '.N_freq' and '.is_complex' in      *
 *                   | TFR_to_reassign) must be set to the same values*
 *                   |                                                *
 * field_x           | component of the reassignment along the axis   *
 *                   | 'time'                                         *
 * field_y           | component of the reassignment along the axis   *
 *                   | 'frequency'                                    *
 *--------------------------------------------------------------------*
 * OUTPUT VARIABLES                                                   *
 * Name              |                role                            *
 * TFR_reassigned    | Result of the reassignment                     *
 *     .real_part    | real part of the reassigned TFR                *
 *     .imag_part    | imag part of the reassigned TFR                *
 *--------------------------------------------------------------------*
 * LOCAL VARIABLES                                                    *
 * Name              |               role                             *
 * time, freq        |  row  and column index in the matrix TFR       *
 * index             |  index in the matrix seen as a vector          *
 * index_time        |new position of the TFR pixel located in 'time' *
 * index_freq        |new position of the TFR pixel located in 'freq' *
 *====================================================================*
 * SUBROUTINES USED HERE                                              *
 *--------------------------------------------------------------------*
 * Name   | int idx(int line, int row, int nb_row)                    *
 * Action | computes the vector index for an element in a matrix given* 
 *        | the line and column indices and the number of columns     *
 * Place  | divers.c                                                  *
 *====================================================================*/
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

#include "tftb.h"

//INFINITY undefined under Visual C
#ifndef INFINITY
#define INFINITY HUGE_VAL
#endif

int
reassign (type_TFR TFR_to_reassign, double *field_x, double *field_y,
	  type_TFR TFR_reassigned, double Treshold)
{
  int            index_time, index_freq;
  int            time, freq, index, index1;
  int            Ntf;
  /*--------------------------------------------------------------------*/
  /*                      Test the input variables                      */
  /*--------------------------------------------------------------------*/

  if ((TFR_to_reassign.is_complex != TFR_reassigned.is_complex) ||
      (TFR_to_reassign.N_time != TFR_reassigned.N_time) || 
      (TFR_to_reassign.N_freq != TFR_reassigned.N_freq))
    {
      //Scierror (999,"reassign.c :  TFR_reassigned and TFR_to_reassign are not compatible\n");
      return(1);
    }


  /*--------------------------------------------------------------------*/
  /*               initialization of the reassigned matrix              */
  /*--------------------------------------------------------------------*/
  Ntf=TFR_to_reassign.N_time * TFR_to_reassign.N_freq;
  for (index = 0;index < Ntf;index++)
    {
      TFR_reassigned.real_part[index] = 0.0;
    }

  if (TFR_reassigned.is_complex == TRUE)
    {
      for (index = 0;index < Ntf;index++)
	{
	  TFR_reassigned.imag_part[index] = 0.0;
	}
    }
  /*--------------------------------------------------------------------*/
  /*                    reassignement of the matrix                     */
  /*--------------------------------------------------------------------*/
 
  for (time = 0; time < TFR_to_reassign.N_time; time++)
    for (freq = 0; freq < TFR_to_reassign.N_freq; freq++)
      {
	index1 = idx (freq, time, TFR_to_reassign.N_freq);
        if (ABS(TFR_to_reassign.real_part[index1])>Treshold)
          {
            /* computation of the final position of the pixel located */
            /* in (time, freq) */
            index_time = time + ROUND (field_x[index1]);
            index_freq = freq + ROUND (field_y[index1]);

            /* in time, the pixels are reassigned along the edges */
            /* case of the negative indices            */
            if (index_time < 0) index_time = 0;
            /* case of too large indices */
            if  (index_time >= TFR_to_reassign.N_time) index_time = TFR_to_reassign.N_time - 1;

            /* in frequency, the pixels are reassigned  according to a rotation */
            while (index_freq < 0) {
              index_freq = index_freq  + TFR_to_reassign.N_freq;
            }
            while (index_freq >= TFR_to_reassign.N_freq) {
              index_freq = index_freq  - TFR_to_reassign.N_freq;
            }
            /* reassignement */
            index = idx (index_freq, index_time, TFR_to_reassign.N_freq);
            TFR_reassigned.real_part[index] = TFR_reassigned.real_part[index]
              + TFR_to_reassign.real_part[index1];
            
            if (TFR_to_reassign.is_complex == TRUE)
              {
                TFR_reassigned.imag_part[index] = TFR_reassigned.imag_part[index]
                  + TFR_to_reassign.imag_part[index1];
              }
            field_x[index1]=index_freq+1;
            field_y[index1]=index_time+1;
          }
        else
          {
            TFR_reassigned.real_part[index1] = TFR_reassigned.real_part[index1]
              + TFR_to_reassign.real_part[index1];
            if (TFR_reassigned.is_complex == TRUE)
              {
                TFR_reassigned.imag_part[index1] = TFR_reassigned.imag_part[index1]
                  + TFR_to_reassign.imag_part[index1];
              }
            field_x[index1]=INFINITY;
            field_y[index1]=INFINITY;
          }
      }
  return 0;
}
