/* Gateway for the Scilab function Caf2tfr */

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

#include <stdlib.h>
#include <stdio.h>
#include "tftb.h"
#include "api_scilab.h"
#include "Scierror.h"
#include "localization.h"
#include "sciprint.h"
#include "sci_malloc.h"



int int_Caf2tfr(scilabEnv env,
                int nin, 
                scilabVar *in,
                int nopt, 
                scilabOpt opt, 
                int nout, 
                scilabVar*out)

{
  char fname[] = "Caf2tfr";
  type_AF        ambif, kernel;
  type_TFR       tfr;
  int retval=0;

  if (nin != 2)
    {
      Scierror(999, _("%s: Wrong number of input arguments: %d expected.\n"), fname, 2);
      return STATUS_ERROR;
    }

  if (nout != 1)
    {
      Scierror(999, _("%s: Wrong number of output arguments: %d expected.\n"), fname, 1);
      return STATUS_ERROR;
    }
  /* AF */
  if (scilab_isMatrix2d(env, in[0]) == 0 || 
      scilab_isDouble(env, in[0]) == 0 || 
      scilab_isMatrix2d(env, in[0]) == 0 || 
      scilab_isComplex(env, in[0]) == 0)
    {
      Scierror(999, _("%s: Wrong type for input argument #%d: A complex matrix expected.\n"), fname, 1);
      return STATUS_ERROR;
    }
  scilab_getDoubleComplexArray(env, in[0],&ambif.real_part,&ambif.imag_part);
  scilab_getDim2d(env, in[0], &ambif.N_delay,&ambif.N_doppler);
  ambif.is_complex = TRUE;
  if ((ambif.N_doppler != ambif.N_delay) )
    {
      Scierror(999,_("%s: Wrong size for argument #%d: Square matrix expected.\n"),fname,1);
      return STATUS_ERROR;
    }
  /* kernel */
  if (scilab_isMatrix2d(env, in[1]) == 0 || 
      scilab_isDouble(env, in[1]) == 0 || 
      scilab_isMatrix2d(env, in[1]) == 0 || 
      scilab_isComplex(env, in[1]) == 1)
    {
      Scierror(999, _("%s: Wrong type for input argument #%d: A real matrix expected.\n"), fname, 2);
      return STATUS_ERROR;
    }
  scilab_getDoubleArray(env, in[1],&kernel.real_part);
  scilab_getDim2d(env, in[1], &kernel.N_delay,&kernel.N_doppler);
  kernel.is_complex = FALSE;
  if ((kernel.N_doppler != ambif.N_doppler) || (kernel.N_delay != ambif.N_delay))
    {
      Scierror(999,_("%s: Arguments #%d and #%d must have the same sizes.\n"),fname,1,2);
      return STATUS_ERROR;
    }  
  tfr.is_complex = FALSE;
  tfr.N_time = ambif.N_doppler;
  tfr.N_freq = ambif.N_delay;
  tfr.real_part =  (double*)malloc(sizeof(double) * tfr.N_time*tfr.N_freq );
  if (tfr.real_part == NULL) 
    {
      Scierror (999,_("%s : Memory allocation error.\n"),fname); 
      return STATUS_ERROR;
    }

  memset(tfr.real_part,0,sizeof(double) * tfr.N_time*tfr.N_freq);
 
  /* computation of the distance */
  retval= af2tfr (ambif, kernel, tfr);
  if (retval==3) {
    free(tfr.real_part);tfr.real_part=NULL;
    Scierror (999,_("%s : Memory allocation error.\n"),fname); 
    return STATUS_ERROR;
  }
  out[0]=scilab_createDoubleMatrix2d(env, tfr.N_freq, tfr.N_time, 0);
  scilab_setDoubleArray(env, out[0], tfr.real_part);
  free(tfr.real_part);tfr.real_part=NULL;
  
  return STATUS_OK;
}
