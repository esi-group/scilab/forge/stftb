/* Gateway for Scilab Chtl function */

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA



#include <stdlib.h>
#include <stdio.h>
#include "tftb.h"
#include "api_scilab.h"
#include "Scierror.h"
#include "localization.h"
#include "sciprint.h"
#include "sci_malloc.h"


int int_Chtl(scilabEnv env,
             int nin, 
             scilabVar *in,
             int nopt, 
             scilabOpt opt, 
             int nout, 
             scilabVar*out)
{
  char fname[] = "Chtl";
  int            nb_theta, nb_rho;
  type_TFR       tfr;
  double         *transfo_hough=NULL, *rho_vect=NULL, *theta_vect=NULL;
  double         temp=0;

  /* checks the number of inputs */
  if ((nin < 1) || (nin > 3))
    {
      Scierror(999,_("%s: Wrong number of input argument: %d to %d expected."),fname,1,3);
      return STATUS_ERROR;
    }
  if  ((nout < 1) || (nout > 3))
    {
      Scierror(999,_("%s: Wrong number of output argument(s): %d to %d expected."),fname,1,3);
      return STATUS_ERROR;
    }

  /* Recovery of the tfr parameter */
  if (scilab_isEmpty(env,in[0]) == 1)
    {
      Scierror (999,_("%s: Wrong size for input argument #%d: A non empty matrix expected.\n"), fname, 1);
      return STATUS_ERROR;
    }
  if (scilab_isMatrix2d(env, in[0]) == 0 || 
      scilab_isDouble(env, in[0]) == 0 || 
      scilab_isComplex(env, in[0]) == 1)
    {
      Scierror(999, _("%s: Wrong type for argument %d: Real matrix expected.\n"), fname, 1);
      return STATUS_ERROR;
    }
  scilab_getDoubleArray(env, in[0], &tfr.real_part);
  scilab_getDim2d(env, in[0], &tfr.N_time,&tfr.N_freq);
  tfr.is_complex = FALSE;

  /* recovery of the other inputs */
  if(nin>1)
    {
      if (scilab_isMatrix2d(env, in[1]) == 0 || 
          scilab_isDouble(env, in[1]) == 0 || 
          scilab_isScalar(env, in[1]) == 0 || 
          scilab_isComplex(env, in[1]) == 1)
        {
          Scierror(999, _("%s: Wrong type for argument %d: Real scalar expected.\n"), fname, 2);
          return STATUS_ERROR;
        }
      scilab_getDouble(env, in[1], &temp);
      nb_rho=(int)temp;
      if (nb_rho <= 0)
        {
          Scierror(999, _("%s: Wrong type for input argument #%d: A positive integer expected.\n"), fname, 2);
          return STATUS_ERROR;
        }
    }
  else
    {
      nb_rho = tfr.N_time;
    }

  if(nin > 2)
    {
      if (scilab_isMatrix2d(env, in[2]) == 0 || 
          scilab_isDouble(env, in[2]) == 0 || 
          scilab_isScalar(env, in[2]) == 0 || 
          scilab_isComplex(env, in[2]) == 1)
        {
          Scierror(999, _("%s: Wrong type for argument %d: Real scalar expected.\n"), fname, 3);
          return STATUS_ERROR;
        }
      scilab_getDouble(env, in[2], &temp);
      nb_theta=(int)temp;
      if (nb_theta <= 0)
        {
          Scierror(999, _("%s: Wrong type for input argument #%d: A positive integer expected.\n"), fname, 3);
          return STATUS_ERROR;
        }
    }
  else
    {
      nb_theta = tfr.N_freq;
    }
  
  /* Memory allocation for results */
  transfo_hough = (double*)ALLOC(nb_rho*nb_theta ,sizeof(double) );
  if (transfo_hough == NULL) {
    Scierror (999,_("%s : Memory allocation error.\n"),fname); 
    return STATUS_ERROR;
  }

  rho_vect = ALLOC(nb_rho,sizeof(double));
  if (rho_vect == NULL) {
    free(transfo_hough);transfo_hough = NULL;
    Scierror (999,_("%s : Memory allocation error.\n"),fname); 
    return STATUS_ERROR;
  }
  
  theta_vect = ALLOC(nb_theta,sizeof(double));
  if (theta_vect == NULL) {
    free(transfo_hough);transfo_hough = NULL;
    free(rho_vect);rho_vect = NULL;
    Scierror (999,_("%s : Memory allocation error.\n"),fname); 
    return STATUS_ERROR;
  }
 

  hough (tfr,nb_theta,nb_rho,transfo_hough,rho_vect,theta_vect);
 
  /* Form output variables */

  /* HT*/
  out[0]=scilab_createDoubleMatrix2d(env, nb_rho, nb_theta,0);
  scilab_setDoubleArray(env, out[0], transfo_hough);
  /* RHO */
  if (nout >= 2)
    {
      out[1]=scilab_createDoubleMatrix2d(env, 1,nb_rho, 0);
      scilab_setDoubleArray(env, out[1], rho_vect);
    }

  /*THETa*/
  if (nout >= 3)
    {
      out[2]=scilab_createDoubleMatrix2d(env, 1,nb_theta, 0);
      scilab_setDoubleArray(env, out[2], theta_vect);
    }

  free(theta_vect);theta_vect=NULL;
  free(rho_vect);rho_vect=NULL;
  free(transfo_hough);transfo_hough=NULL;
  return STATUS_OK;
}
