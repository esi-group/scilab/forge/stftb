mode(-1)
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
//function ambifuwt
//AMBIFUWT Unit test for the function AMBIFUWB.

//	O. Lemoine - April 1996.
// H. Nahrstaedt - 2012

N=128;
t0=45;
t=1:N;
ind=zeros(1,82);
// Ambiguity function of a pulse : an hyperbola
sig=sum(t'==t0,'c'); 
[amb,tau,theta]=ambifuwb(sig,0.005,0.5,3*N);
for ti=1:82,
 [Max,ind(ti)]=max(abs(amb(:,ti).^2)); 
end
hyp1=theta(ind);
taup=1:82;
hyp2=log(20.23./(-taup+85));

assert_checkalmostequal(hyp1,hyp2,%eps,0.1);



// Ambiguity function of a sine wave : non zero only for a=1 ie theta=0.
f0=0.25;
sig=fmconst(N,f0);
ind=zeros(1,112);
[amb,tau,theta]=ambifuwb(sig,0.1,0.4,N);
for ti=1:112,
 [Max ind(ti)]=max(abs(amb(:,ti).^2)); 
end
assert_checkfalse(or(ind~=N/2+1));


// Energy 
sig=fmlin(N).*amgauss(N);
Es=norm(sig)^2; 
[amb,tau,theta]=ambifuwb(sig,0.05,0.45,N);
Eamb=abs(amb(N/2,N/2));
assert_checkalmostequal(Eamb,Es,%eps,sqrt(%eps));


ind=zeros(1,74);
N=121;
t=1:N;
t0=45;
		     
// Ambiguity function of a pulse : an hyperbola
sig=sum(t'==t0,'c'); 
[amb,tau,theta]=ambifuwb(sig,0.005,0.5,3*N);
for ti=1:74,
 [Max ind(ti)]=mtlb_max(abs(amb(:,ti).^2)); 
end
hyp1=theta(ind);
taup=1:74;
hyp2=log(17 ./(-taup+79));

assert_checkalmostequal(hyp1,hyp2,%eps,5e-2);


ind=zeros(1,105);
// Ambiguity function of a sine wave : non zero only for a=1 ie theta=0.
f0=0.25;
sig=fmconst(N,f0);
[amb,tau,theta]=ambifuwb(sig,0.1,0.4,N);
for ti=1:105,
 [Max ind(ti)]=max(abs(amb(:,ti).^2)); 
end
assert_checkfalse(or(ind~=round(N/2)));



// Energy 
sig=fmlin(N).*amgauss(N);
Es=norm(sig)^2; 
[amb,tau,theta]=ambifuwb(sig,0.05,0.45,N);
Eamb=abs(amb(round(N/2),round(N/2)));
assert_checkalmostequal(Eamb,Es,%eps,sqrt(%eps));



			     
