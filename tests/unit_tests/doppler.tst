mode(-1)
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
//function dopplert
//DOPPLERT Unit test for the function DOPPLER.

//	O. Lemoine - February 1996.

N=128; Fs=100; D=12; 

// Pure tone for a fixed target
F0=25; V=0;
[fm,am,iflaw]=doppler(N,Fs,F0,D,V);
d1=abs(fft(fm));
d2=abs(am-am(1));
d3=abs(iflaw-iflaw(1));
assert_checkfalse(sum(or(d1>sqrt(%eps)))~=1);
assert_checkfalse(sum(or(d2>sqrt(%eps)))~=0);
assert_checkfalse(sum(or(d3>sqrt(%eps)))~=0);

// Null signal for a non-emitting target
F0=0; V=50;
[fm,am,iflaw]=doppler(N,Fs,F0,D,V);
d1=abs(fm-fm(1));
d2=abs(am-am(1));
d3=abs(iflaw-iflaw(1));
assert_checkfalse( sum(or(d1>sqrt(%eps)))~=0);
assert_checkfalse(sum(or(d2>sqrt(%eps)))~=0);
assert_checkfalse(sum(or(d3>sqrt(%eps)))~=0);


// Symmetry of the amplitude modulation
F0=15.6; V=32.3; T0=52;
[fm,am,iflaw]=doppler(N,Fs,F0,D,V,T0);
dist=1:min([N-T0,T0-1]);
assert_checkfalse(or(abs(am(T0-dist)-am(T0+dist))>sqrt(%eps)));


N=123; Fs=61; D=7; 

// Pure tone for a fixed target
F0=26; V=0;
[fm,am,iflaw]=doppler(N,Fs,F0,D,V);
d1=abs(fft(fm));
d2=abs(am-am(1));
d3=abs(iflaw-iflaw(1));
assert_checkfalse(sum(or(d1>sqrt(%eps)))~=1);
assert_checkfalse(sum(or(d2>sqrt(%eps)))~=0);
assert_checkfalse(sum(or(d3>sqrt(%eps)))~=0);

// Null signal for a non-emitting target
F0=0; V=47;
[fm,am,iflaw]=doppler(N,Fs,F0,D,V);
d1=abs(fm-fm(1));
d2=abs(am-am(1));
d3=abs(iflaw-iflaw(1));
assert_checkfalse(sum(or(d1>sqrt(%eps)))~=0);
assert_checkfalse(sum(or(d2>sqrt(%eps)))~=0);
assert_checkfalse(sum(or(d3>sqrt(%eps)))~=0);

// Symmetry of the amplitude modulation
F0=15.6; V=32.3; T0=51;
[fm,am,iflaw]=doppler(N,Fs,F0,D,V,T0);
dist=1:min([N-T0,T0-1]);
assert_checkfalse(or(abs(am(T0-dist)-am(T0+dist))>sqrt(%eps)));

