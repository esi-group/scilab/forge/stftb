mode(-1)
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
//       O. Lemoine - March 1996. 
rtol=100*%eps;
atol=1000*%eps;

//check optionnal arguments
N=32;
sig=ones(1,N);
tfr=Ctfrpage(sig);
assert_checkequal(size(tfr),[32,32]);
assert_checkequal(Ctfrpage(sig,1:N),tfr);
assert_checkequal(Ctfrpage(sig,1:N,32),tfr);

[t,T]=Ctfrpage(sig);
assert_checkequal(T,1:N);assert_checkequal(t,tfr);
[t,T]=Ctfrpage(sig,1:N);
assert_checkequal(T,1:N);assert_checkequal(t,tfr);
[t,T]=Ctfrpage(sig,1:N,32);
assert_checkequal(T,1:N);assert_checkequal(t,tfr);

[t,T,F]=Ctfrpage(sig);
assert_checkequal(T,1:N);assert_checkequal(t,tfr);
[t,T,f]=Ctfrpage(sig,1:N);
assert_checkequal(T,1:N);assert_checkequal(t,tfr);assert_checkequal(f,F);
[t,T,f]=Ctfrpage(sig,1:N,32);
assert_checkequal(T,1:N);assert_checkequal(t,tfr);assert_checkequal(f,F);


// Check against the tfrpage Scilab function
N=128;

t1=60; t2=70; f=0.3;
sig=amgauss(N,t1).*fmconst(N,f,t1); 
assert_checkalmostequal(Ctfrpage(sig),tfrpage(sig),[],1e-12);  
assert_checkalmostequal(Ctfrpage(sig,1:N,N),...
                        tfrpage(sig,1:N,N),...
                        rtol,atol);  
assert_checkalmostequal(Ctfrpage(sig,1:N,N/2),...
                        tfrpage(sig,1:N,N/2),...
                        rtol,atol);

f0=10;
sig=fmconst(N+6,f0/N);
assert_checkalmostequal(Ctfrpage(sig,round(N/2)+2,N),...
                        tfrpage(sig,round(N/2)+2,N),...
                        rtol,atol);  



N=131;
assert_checkalmostequal(Ctfrpage(sig),tfrpage(sig));  
assert_checkalmostequal(Ctfrpage(sig,1:N,N),...
                        tfrpage(sig,1:N,N),...
                        rtol,atol);  
assert_checkalmostequal(Ctfrpage(sig,1:N,round(N/2)),...
                        tfrpage(sig,1:N,round(N/2)),...
                        rtol,atol);

f0=10;
sig=fmconst(N+6,f0/N);
assert_checkalmostequal(Ctfrpage(sig,round(N/2)+2,N),...
                        tfrpage(sig,round(N/2)+2,N),...
                        rtol,atol);  

