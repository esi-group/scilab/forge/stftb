mode(-1)
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
//
//Unit test for the function Ctfrdist 
//	S. Steer - April 2018
N=128;
AF1 = Cambifunb(hilbert(sin(2*%pi*0.25*(1:N))),-N/2+1:N/2,N);
AF2 = Cambifunb(hilbert(sin(2*%pi*0.15*(1:N))),-N/2+1:N/2,N);
ker = Ctfrker(N,N,'WV');
TFR1 = Caf2tfr(AF1,ker);
TFR2 = Caf2tfr(AF2,ker);

assert_checkalmostequal(Ctfrdist(TFR1,TFR2,'lq',100),127.04080690256);
assert_checkalmostequal(Ctfrdist(TFR1,TFR2,'quadratic'),2107397.704111);
assert_checkalmostequal(Ctfrdist(TFR1,TFR2,'correlation'),0.9998916131605);
assert_checkalmostequal(Ctfrdist(TFR1,TFR2,'kolmogorov'),1.4522808431803);
assert_checkalmostequal(Ctfrdist(TFR1,TFR2,'kullback'),4.7672312575022);
assert_checkalmostequal(Ctfrdist(TFR1,TFR2,'Chernoff',0.1),0.2046968924049);
assert_checkalmostequal(Ctfrdist(TFR1,TFR2,'matusita',2), 0.951465804289);
assert_checkalmostequal(Ctfrdist(TFR1,TFR2,'nlq',2),0.0332945427738);
assert_checkalmostequal(Ctfrdist(TFR1,TFR2,'lsd',2),242.46850868556);
assert_checkalmostequal(Ctfrdist(TFR1,TFR2,'jensen',2),0.4713312729391);


assert_checkerror("Ctfrdist(TFR1,TFR2,''lq'')",msprintf(_("%s: An argument  is required for this distance.\n"),"Ctfrdist"));
assert_checkerror("Ctfrdist(TFR1,TFR2,''lq'',-1)",msprintf(_("%s: Wrong value for input argument #%d: Must be > %d.\n"), "Ctfrdist",4,0));

assert_checkerror("Ctfrdist(TFR1,TFR2,''quadratic'',1)",msprintf(_("%s: Wrong number of input argument: %d expected."),"Ctfrdist",3));
assert_checkerror("Ctfrdist(TFR1,TFR2,''chernoff'')",msprintf(_("%s: An argument  is required for this distance.\n"),"Ctfrdist"));
assert_checkerror("Ctfrdist(TFR1,TFR2,''chernoff'',-1)",msprintf(_("%s: Wrong value for input argument #%d: Must be in the interval %s.\n"), "Ctfrdist",4,"[0 1]"));
assert_checkerror("Ctfrdist(TFR1,TFR2,''matusita'')",msprintf(_("%s: An argument  is required for this distance.\n"),"Ctfrdist"));
assert_checkerror("Ctfrdist(TFR1,TFR2,''matusita'',-1)",msprintf(_("%s: Wrong value for input argument #%d: Must be >= %d.\n"), "Ctfrdist",4,1));
assert_checkerror("Ctfrdist(TFR1,TFR2,''nlq'')",msprintf(_("%s: An argument  is required for this distance.\n"),"Ctfrdist"));
assert_checkerror("Ctfrdist(TFR1,TFR2,''nlq'',-1)",msprintf(_("%s: Wrong value for input argument #%d: Must be > %d.\n"), "Ctfrdist",4,0));
assert_checkerror("Ctfrdist(TFR1,TFR2,''lsd'')",msprintf(_("%s: An argument  is required for this distance.\n"),"Ctfrdist"));
assert_checkerror("Ctfrdist(TFR1,TFR2,''lsd'',-1)",msprintf(_("%s: Wrong value for input argument #%d: Must be > %d.\n"), "Ctfrdist",4,0));
assert_checkerror("Ctfrdist(TFR1,TFR2,''jensen'')",msprintf(_("%s: An argument  is required for this distance.\n"),"Ctfrdist"));
assert_checkerror("Ctfrdist(TFR1,TFR2,''jensen'',-1)",msprintf(_("%s: Wrong value for input argument #%d: Must be > %d.\n"), "Ctfrdist",4,0));
