mode(-1)
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
//function noisecgt
//NOISECGT Unit test for the function NOISECG.

//	O. Lemoine - February 1996.





function [h,t] = hist_tmp(x,n)

// hist - compute and display histogram
//
//  [h,t] = hist(x,n);
//
//  if h is ommitted, then display the histogram, 
//  otherwise, compute it.
//
//  Copyright (c) 2008 Gabriel Peyre


  if argn(2)<2
    n = 100;
  end


  x = x(:);
  if length(n)>1
    t = n;
    n = length(n);
    // closest point histograms
    h = zeros(n,1);
    for i=1:length(x)
      [tmp,j] = min( abs(x(i)-t(:)), 'r' );
      h(j) = h(j)+1;
    end
  else
    // equispaced histograms
    a = min(x); b = max(x);
    tau = (b-a)/n;
    a1 = a+tau/2; b1 = b-tau/2;
    t = a1:tau:b1;
    x1 = (x-a1)/(b1-a1)*(n-1)+1;
    x1 = round(x1);
    h = zeros(n,1);
    for i=1:n
      h(i) = sum(x1==i);
    end
  end
  h = h/sum(h);
endfunction
    


N=32768;
A1=0.2; A2=0.7;
sig=noisecg(N,A1,A2);


// Mean
Mean=mean(sig);
assert_checkfalse(abs(Mean)>10/sqrt(N));


// Variance
Var=mean((sig-Mean).^2);
assert_checkfalse(abs(Var)>10/sqrt(N));


// histogram
Nh=100;
[h,m]=hist_tmp(real(sig),Nh); h=h/max(h);
Nc=find(abs(m)==min(abs(m)));
pdf=amgauss(Nh,Nc,Nh/(2*sqrt(%pi)))';
assert_checkfalse(or(abs(h-pdf').^2>10/sqrt(N)));


// whiteness
sig=abs(noisecg(N));
autocor=convol(sig,sig($:-1:1));
Max=mtlb_max(autocor);
L=length(find(abs(autocor/Max)>0.8));//5e-2));
assert_checkfalse( L/N>5e-4);

N=1; Np=10000;sig=zeros(1,Np);
for k=1:Np,
 sig(k)=noisecg(N);
end
Mean=mean(sig);
assert_checkfalse(abs(Mean)>10/sqrt(Np));
Var=mean((sig-Mean).^2);
assert_checkfalse(abs(Var)>10/sqrt(Np));

[h,m]=hist_tmp(real(sig),Nh); h=h/max(h);
Nc=find(abs(m)==min(abs(m)));
pdf=amgauss(Nh,Nc,Nh/(2*sqrt(%pi)));
assert_checkfalse(or(abs(h-pdf).^2>10/sqrt(Np)));


// For N=2
N=2;sig=zeros(1,Np);
for k=1:2:(Np-1),
 noise=noisecg(N);
 sig(k)=noise(1);
 sig(k+1)=noise(2);
end
Mean=mean(sig);
assert_checkfalse(abs(Mean)>10/sqrt(Np));

Var=mean((sig-Mean).^2);
assert_checkfalse(abs(Var)>10/sqrt(Np));

[h,m]=hist_tmp(real(sig),Nh); h=h/max(h);
Nc=find(abs(m)==min(abs(m)));
pdf=amgauss(Nh,Nc,Nh/(2*sqrt(%pi)));
assert_checkfalse(or(abs(h-pdf).^2>10/sqrt(Np)));

clear hist_tmp;
