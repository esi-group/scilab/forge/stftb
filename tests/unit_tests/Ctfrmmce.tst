mode(-1)
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
// function tfrmmcet
//TFRMMCET Unit test for the time frequency representation TFRMMCE.

//       O. Lemoine - March 1996. 

// We test each property of the corresponding TFR :

N=128;
h=zeros(19,3);
h(10+(-5:5),1)=tftb_window(11,'Hamming'); 
h(10+(-7:7),2)=tftb_window(15,'Hamming');  
h(10+(-9:9),3)=tftb_window(19,'Hamming');

// Covariance by translation in time 
t1=55; f=0.3;
sig=amgauss(N,t1).*fmconst(N,f,t1); 

[tfr,t,f]=Ctfrmmce(sig,h);
assert_checkequal(t,1:N);
assert_checkequal(f,(0:N-1)/N);

assert_checkequal(size(tfr),[N,N]);
assert_checkalmostequal(Ctfrmmce(sig,h,1:N),tfr);
assert_checkalmostequal(Ctfrmmce(sig,h,1:N,N),tfr);


assert_checkalmostequal(tfr,tfrmmce(sig,h));
assert_checkalmostequal(Ctfrmmce(sig,h,N),tfrmmce(sig,h,N));
assert_checkalmostequal(Ctfrmmce(sig,h,1:N,1),tfrmmce(sig,h,1:N,1));

N=121;
h=zeros(19,3);
h(10+(-6:6),1)=tftb_window(13,'gauss'); 
h(10+(-8:8),2)=tftb_window(17,'kaiser');  
h(10+(-7:7),3)=tftb_window(15,'parzen');
t1=55; f=0.3;
sig=amgauss(N,t1).*fmconst(N,f,t1); 

[tfr,t,f]=Ctfrmmce(sig,h);
assert_checkequal(t,1:N);
assert_checkequal(f,(0:N-1)/N);

assert_checkequal(size(tfr),[N,N]);
assert_checkalmostequal(Ctfrmmce(sig,h,1:N),tfr);
assert_checkalmostequal(Ctfrmmce(sig,h,1:N,N),tfr);


assert_checkalmostequal(tfr,tfrmmce(sig,h));
assert_checkalmostequal(Ctfrmmce(sig,h,N),tfrmmce(sig,h,N));
assert_checkalmostequal(Ctfrmmce(sig,h,1:N,1),tfrmmce(sig,h,1:N,1));
