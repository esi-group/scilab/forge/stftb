mode(-1)
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

//	O. Lemoine - June 1996. 

// We test each property of the corresponding TFR :

N=128;

// Covariance by translation in time 
t1=60; t2=70; f=0.3; W=0; 
sig1=amgauss(N,t1).*fmconst(N,f,t1); 
sig2=amgauss(N,t2).*fmconst(N,f,t2); 
tfr1=tfrscalo(sig1,1:N,W,0.1,0.4,128);  
tfr2=tfrscalo(sig2,1:N,W,0.1,0.4,128);        
assert_checkalmostequal(tfr1,tfr2(:,modulo((0:N-1)-t1+t2,N)+1),sqrt(%eps),[],"matrix");


// Covariance by dilation
t=N/2; f=0.2; T=2*sqrt(N); a=2; W=8; 
sig1=amgauss(N,t,T).*fmconst(N,f,t);
sig2=amgauss(a*N,a*t,T*a).*fmconst(a*N,f/a,a*t);
[tfr1,t1,f1]=tfrscalo(sig1,1:N  ,W,0.01,0.49,N);  
[tfr2,t2,f2]=tfrscalo(sig2,1:a*N,W,0.01,0.49,N);        
Max1=max(real(tfr1));
Max2=max(real(tfr2));
[I1,J1]=find(tfr1==Max1);
[I2,J2]=find(tfr2==Max2);
assert_checkequal(J2,a*J1);
assert_checkalmostequal(f1(I1),a*f2(I2),[],1e-2);

// Reality of the TFR
sig=noisecg(N); W=5;
tfr=tfrscalo(sig,1:N,W,0.01,0.5,N);
assert_checktrue(isreal(tfr));

// Energy conservation
sig=fmsin(N,.1,.4); W=6; Nf=2*N ;
[tfr,t,f]=tfrscalo(sig,1:N,W,0.01,0.49,Nf);
Es=norm(sig)^2/Nf;
Etfr=integ2d(tfr,t,f)/N;
assert_checkalmostequal(Es,Etfr);
assert_checktrue(real(tfr)>=0);

// Same energy in the time-scale plane for 2 gaussian atoms at different scales
sig=amgauss(256).*(fmconst(256,.15)+fmconst(256,.35));
[tfr,t,f]=tfrscalo(sig,1:256,12,.01,.49,512);
int1=integ2d(tfr(1:430,:),t,f(1:430));
int2=integ2d(tfr(431:512,:),t,f(431:512));
assert_checkalmostequal(int1,int2,[],1e-4);


N=127;

// Covariance by dilation
t=round(N/2); f=0.2; T=2*sqrt(N); a=2; W=8; 
//t=ceil(N/2); f=0.2; T=2*sqrt(N); a=2; W=8;
sig1=amgauss(N,t,T).*fmconst(N,f,t);
sig2=amgauss(a*N,a*t,T*a).*fmconst(a*N,f/a,a*t);
[tfr1,t1,f1]=tfrscalo(sig1,1:N  ,W,0.01,0.49,N);  
[tfr2,t2,f2]=tfrscalo(sig2,1:a*N,W,0.01,0.49,N);        
Max1=max(real(tfr1));
Max2=max(real(tfr2));
[I1,J1]=find(tfr1==Max1);
[I2,J2]=find(tfr2==Max2);  
assert_checkequal(J2,a*J1);
assert_checkalmostequal(f1(I1),a*f2(I2),[],1e-2);


// Reality of the TFR
sig=noisecg(N); W=5;
tfr=tfrscalo(sig,1:N,W,0.01,0.5,N);
assert_checktrue(tfr>=0);

// Energy conservation
sig=fmsin(N,.1,.4); W=6; Nf=2*N+1 ;
[tfr,t,f]=tfrscalo(sig,1:N,W,0.01,0.49,Nf);
//SP = fft(hilbert(sig)); 
SP = fft(hilbert(real(sig)));
indmin = 1+round(0.01*(N-2));
indmax = 1+round(0.49*(N-2));
SPana = SP(indmin:indmax);
Es=SPana'*SPana/Nf;
Etfr=integ2d(tfr,t,f);
assert_checkalmostequal(Es,Etfr);
assert_checktrue(tfr>=0);



// Same energy in the time-scale plane for 2 gaussian atoms at different scales
sig=amgauss(N).*(fmconst(N,.15)+fmconst(N,.35));
[tfr,t,f]=tfrscalo(sig,1:N,12,.01,.49,2*N+1);
int1=integ2d(tfr(1:210,:),t,f(1:210));
int2=integ2d(tfr(211:255,:),t,f(211:255));
assert_checkalmostequal(int1,int2,1e-4,1e-3);
