mode(-1)
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
//function ifestart
//IFESTART Unit test for the function IFESTAR2.

//	O. Lemoine - August 1996.

N=235; 

// Constant frequency modulation
[sig,ifl]=fmconst(N);
[iflaw,t]=ifestar2(real(sig));
assert_checkalmostequal(ifl(t),iflaw)


[sig,ifl]=fmconst(N,0.01);
[iflaw,t]=ifestar2(real(sig));
assert_checkalmostequal(ifl(t),iflaw)

// Linear frequency modulation
[sig,ifl]=fmlin(N,.05,.45);
[iflaw,t]=ifestar2(real(sig));
assert_checkalmostequal(ifl(t),iflaw,[],1e-2);

// Sinusoidal frequency modulation
[sig,ifl]=fmsin(N);
[iflaw,t]=ifestar2(real(sig));
assert_checkalmostequal(ifl(t),iflaw,[],0.1);

[sig,ifl]=fmsin(N,0.03,0.35,2*N);
[iflaw,t]=ifestar2(real(sig));
assert_checkalmostequal(ifl(t),iflaw,[],0.005);
