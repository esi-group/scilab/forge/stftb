mode(-1)
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

//       F. Auger, Dec. 1995 - O. Lemoine, March 1996.

//Check optional arguments

N=32;
sig=ones(1,N);
tfr=Ctfrridh(sig);
assert_checkequal(size(tfr),[32,32]);
assert_checkequal(Ctfrridh(sig,1:N),tfr);
assert_checkequal(Ctfrridh(sig,1:N,32),tfr);
assert_checkequal(Ctfrridh(sig,1:N,32,tftb_window(int(N/10),"hamming")),tfr);
assert_checkequal(Ctfrridh(sig,1:N,32,tftb_window(int(N/10),"hamming"),tftb_window(N/4+1,"hamming")),tfr);

[t,T]=Ctfrridh(sig);
assert_checkequal(T,1:N);assert_checkequal(t,tfr);
[t,T]=Ctfrridh(sig,1:N);
assert_checkequal(T,1:N);assert_checkequal(t,tfr);
[t,T]=Ctfrridh(sig,1:N,32);
assert_checkequal(T,1:N);assert_checkequal(t,tfr);
[t,T]=Ctfrridh(sig,1:N,32,tftb_window(int(N/10),"hamming"));
assert_checkequal(T,1:N);assert_checkequal(t,tfr);
[t,T]=Ctfrridh(sig,1:N,32,tftb_window(int(N/10),"hamming"),tftb_window(N/4+1,"hamming"));
assert_checkequal(T,1:N);assert_checkequal(t,tfr);

[t,T,F]=Ctfrridh(sig);
assert_checkequal(T,1:N);assert_checkequal(t,tfr);
[t,T,f]=Ctfrridh(sig,1:N);
assert_checkequal(T,1:N);assert_checkequal(t,tfr);assert_checkequal(f,F);
[t,T,f]=Ctfrridh(sig,1:N,32);
assert_checkequal(T,1:N);assert_checkequal(t,tfr);assert_checkequal(f,F);
[t,T,f]=Ctfrridh(sig,1:N,32,tftb_window(int(N/10),"hamming"));
assert_checkequal(T,1:N);assert_checkequal(t,tfr);assert_checkequal(f,F);
[t,T,f]=Ctfrridh(sig,1:N,32,tftb_window(int(N/10),"hamming"),tftb_window(N/4+1,"hamming"));
assert_checkequal(T,1:N);assert_checkequal(t,tfr);assert_checkequal(f,F);

// Check against the tfrridh Scilab function
rtol=100*%eps;
atol=1000*%eps;
N=128;
t1=55;  f=0.3;
sig=amgauss(N,t1).*fmconst(N,f,t1); 
assert_checkalmostequal(Ctfrridh(sig),tfrridh(sig));  
assert_checkalmostequal(Ctfrridh(sig,1:N,N,1,ones(N-1,1)),...
                        tfrridh(sig,1:N,N,1,ones(N-1,1)),...
                        rtol,atol);  
assert_checkalmostequal(Ctfrridh(sig,1:N,N/2,window("re",11),window("re",N/2+1)),...
                        tfrridh(sig,1:N,N/2,window("re",11),window("re",N/2+1)),...
                        rtol,atol);
f0=10;
sig=fmconst(N+6,f0/N);
assert_checkalmostequal(Ctfrridh(sig,round(N/2)+2,N,tftb_window(11,'rect'),tftb_window(N+1,'rect')),...
                        tfrridh(sig,round(N/2)+2,N,tftb_window(11,'rect'),tftb_window(N+1,'rect')),...
                        rtol,atol);  

N=111;
sig=amgauss(N,t1).*fmconst(N,f,t1); 
assert_checkalmostequal(Ctfrridh(sig),tfrridh(sig));  
assert_checkalmostequal(Ctfrridh(sig,1:N,N,1,ones(N,1)),...
                        tfrridh(sig,1:N,N,1,ones(N,1)),...
                        rtol,atol);  
assert_checkalmostequal(Ctfrridh(sig,1:N,round(N/2),window("re",11),window("re",round(N/2)+1)),...
                        tfrridh(sig,1:N,round(N/2),window("re",11),window("re",round(N/2)+1)),...
                        rtol,atol);
f0=10;
sig=fmconst(N+6,f0/N);
assert_checkalmostequal(Ctfrridh(sig,round(N/2)+2,N,tftb_window(11,'rect'),tftb_window(N,'rect')),...
                        tfrridh(sig,round(N/2)+2,N,tftb_window(11,'rect'),tftb_window(N,'rect')),...
                        rtol,atol);
