mode(-1)
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
//function dwindowt
//DWINDOWT Unit test for the function DWINDOW.

//	O. Lemoine - March 1996.


//      S. Steer   - July 2018

N=11;

//Rectangle
assert_checkequal(ones(N,1),Cwindow(N,"rect"));

//Hamming
href=[0.08;0.1678521825875;0.3978521825875;0.6821478174125;0.9121478174125;1;0.9121478174125;0.6821478174125;0.3978521825875;0.1678521825875;0.08] ;
assert_checkalmostequal(href,Cwindow(N,"hamming"));

//Hann
href=[0;0.0954915028125;0.3454915028125;0.6545084971875;0.9045084971875;1;0.9045084971875;0.6545084971875;0.3454915028125;0.0954915028125;0];
assert_checkalmostequal(href,Cwindow(N,"hanning"));
//revoir, c'est la def de hann de matlab pas de hanning

//Kaiser
href=[0.0006123359278;0.0300779177577;0.1704063285494;0.4762250780923;0.8353500729547;1;0.8353500729547;0.4762250780924;0.1704063285494;0.0300779177577;0.0006123359278];
assert_checkalmostequal(href,Cwindow(N,"kaiser",3*%pi));

//Blackman Nuttall
href=[0.0001628;0.0132670334972;0.1106770564487;0.3957877165028;0.7981962935513;0.9998;0.7981962935513;0.3957877165028;0.1106770564487;0.0132670334972;0.0001628];
assert_checkalmostequal(href,Cwindow(N,"nuttall"));
//it is in fact the Backman Nuttall window

//Blackman
href= [0;0.0402128623625;0.2007701432625;0.5097871376375;0.8492298567375;1;0.8492298567375;0.5097871376375;0.2007701432625;0.0402128623625;0];
assert_checkalmostequal(href,Cwindow(N,"blackman"),[],%eps);

//Blackman Harris
href=[0.00006;0.0109823312762;0.1030114893457;0.3858926687238;0.7938335106543;1;0.7938335106543;0.3858926687238;0.1030114893457;0.0109823312762;0.00006];
assert_checkalmostequal(href,Cwindow(N,"harris"));


//Bartlett
href=[0;0.2;0.4;0.6;0.8;1;0.8;0.6;0.4;0.2;0];
assert_checkalmostequal(href,Cwindow(N,"bartlett"));

//Barthann
href=[0;0.1205735421375;0.3585735421375;0.6414264578625;0.8794264578625;1;0.8794264578625;0.6414264578625;0.3585735421375;0.1205735421375;0];
assert_checkalmostequal(href,Cwindow(N,"barthann"));

//Papoulis
href=[0;0.3090169943749;0.5877852522925;0.8090169943749;0.9510565162952;1;0.9510565162952;0.8090169943749;0.5877852522925;0.3090169943749;1.224646799D-16];
assert_checkalmostequal(href,Cwindow(N,"papoulis"));

//Gauss
href=[0.005;0.0336775742859;0.1484667499371;0.4283859323293;0.8090191470413;1;0.8090191470413;0.4283859323293;0.1484667499371;0.0336775742859;0.005];
assert_checkalmostequal(href,Cwindow(N,"gauss",0.005));//K=0.005 is the default
//the Matlab parameter a corresponds to sqrt(-2*log(K))

//Parzen
href=[0.0015026296018;0.0405709992487;0.1878287002254;0.4951164537941;0.8377160030053;1;0.8377160030053;0.4951164537941;0.1878287002254;0.0405709992487;0.0015026296018];
assert_checkalmostequal(href,Cwindow(N,"parzen"));

//Power Sine
href=[0;0.0954915028125;0.3454915028125;0.6545084971875;0.9045084971875;1;0.9045084971875;0.6545084971875;0.3454915028125;0.0954915028125;1.499759783D-32];
assert_checkalmostequal(href,Cwindow(N,"powersine"),[],%eps);

//Nutbess (no matlab equivalent found)
href=[0.0040915759577;0.0520215437589;0.2175142588079;0.5245518016788;0.8546265948540;1;0.8546265948540;0.5245518016788;0.2175142588079;0.0520215437589;0.0040915759577];
assert_checkalmostequal(href,Cwindow(N,"nutbess"));

//Spline

//Flattop Flattop_ni
href=[-0.0417934;-0.0791530924580;-0.0401198317866;0.2818123424580;0.7636762817866;1.000001;0.7636762817866;0.2818123424580;-0.0401198317866;-0.0791530924580;-0.0417934];
assert_checkalmostequal(href,Cwindow(N,"flattop"));
assert_checkalmostequal(href,Cwindow(N,"flattop_ni"));

//Flattop_m (matlab equivalent)
href=[-0.000421051;-0.0155972746604;-0.0677142520762;0.0545446481604;0.6068721525762;1.000000003;0.6068721525762;0.0545446481604;-0.0677142520762;-0.0155972746604;-0.000421051];
assert_checkalmostequal(href,Cwindow(N,"flattop_m"));

//Flattop_srs
href=[8.673617380D-17;-0.0655247584250;-0.3174836522274;0.2475247584250;2.8174836522274;4.636;2.8174836522274;0.2475247584250;-0.3174836522274;-0.0655247584250;8.673617380D-17];
assert_checkalmostequal(href,Cwindow(N,"flattop_srs"));



