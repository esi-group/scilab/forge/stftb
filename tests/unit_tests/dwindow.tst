mode(-1)
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
//  S. Steer august 2018

//  approximate derivation is compared with the symbolic derivation of the
//  window expression

F=['Hamming','Hanning','Nuttall','Papoulis','Sine','Harris','Rect', ...
   'Blackman','Gauss','Parzen','Kaiser','Dolph','Powersine','spline', ...
   'Flattop','Flattop_ni','Flattop_m','Flattop_srs','Nutbess'];

F1=['Triang','Barthann'];


N=10000;//we use a lot of point for good derivative estimate
for f=F
  mprintf("%s\n",f)
  Dh=dwindow(N,f);
  Dhe=dwindow(tftb_window(N,f));
  assert_checkalmostequal(Dh(2:$-1),Dhe(2:$-1),1e-4,1e-6);
end

for f=F1
  mprintf("%s\n",f)
  Dh=dwindow(N,f);
  Dhe=dwindow(tftb_window(N,f));
  assert_checkalmostequal(Dh(2:$/2-1),Dhe(2:$/2-1),1e-4,1e-9);
  assert_checkalmostequal(Dh($/2+2:$-1),Dhe($/2+2:$-1),1e-4,1e-9);
end
