mode(-1)
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

rtol=100*%eps;
atol=1000*%eps;

//Optionnal arguments tests
N=32;
sig=ones(1,N);
tfr=Ctfrsp(sig);
assert_checkequal(size(tfr),[32,32]);
assert_checkequal(Ctfrsp(sig,1:N),tfr);
assert_checkequal(Ctfrsp(sig,1:N,32),tfr);
assert_checkequal(Ctfrsp(sig,1:N,32,tftb_window(int(N/4)+1,"hamming")),tfr);

[t,T]=Ctfrsp(sig);
assert_checkequal(T,1:N);assert_checkequal(t,tfr);
[t,T]=Ctfrsp(sig,1:N);
assert_checkequal(T,1:N);assert_checkequal(t,tfr);
[t,T]=Ctfrsp(sig,1:N,32);
assert_checkequal(T,1:N);assert_checkequal(t,tfr);
[t,T]=Ctfrsp(sig,1:N,32,tftb_window(int(N/4)+1,"hamming"));
assert_checkequal(T,1:N);assert_checkequal(t,tfr);

[t,T,F]=Ctfrsp(sig);
assert_checkequal(T,1:N);assert_checkequal(t,tfr);
[t,T,f]=Ctfrsp(sig,1:N);
assert_checkequal(T,1:N);assert_checkequal(t,tfr);assert_checkequal(f,F);
[t,T,f]=Ctfrsp(sig,1:N,32);
assert_checkequal(T,1:N);assert_checkequal(t,tfr);assert_checkequal(f,F);
[t,T,f]=Ctfrsp(sig,1:N,32,tftb_window(int(N/4)+1,"hamming"));
assert_checkequal(T,1:N);assert_checkequal(t,tfr);assert_checkequal(f,F);

//tests against tfrsp
t1=55; f=0.3;
N=128; 

sig=amgauss(N,t1).*fmconst(N,f,t1); 
Ctfr=Ctfrsp(sig,1:N,N,window("hm",33));
tfr=tfrsp(sig,1:N,N,window("hm",33));
assert_checkalmostequal(tfr,Ctfr,rtol,atol); 
assert_checkalmostequal(Ctfrsp(sig),tfrsp(sig),rtol,atol);  
assert_checkalmostequal(Ctfrsp(sig,1:N,N,ones(N-1,1)),...
                        tfrsp(sig,1:N,N,ones(N-1,1)),...
                        rtol,atol);  
assert_checkalmostequal(Ctfrsp(sig,1:N,N,1),...
                        tfrsp(sig,1:N,N,1),...
                        rtol,atol);  
assert_checkalmostequal(Ctfrsp(sig,1:N,N/2,window("re",N/2+1)),...
                        tfrsp(sig,1:N,N/2,window("re",N/2+1)),...
                        rtol,atol);
f0=10;
sig=fmconst(N+6,f0/N);
assert_checkalmostequal(Ctfrsp(sig,round(N/2)+2,N,tftb_window(N+1,'rect')),...
                        tfrsp(sig,round(N/2)+2,N,tftb_window(N+1,'rect')),...
                        rtol,atol);  


N=123;


sig=amgauss(N,t1).*fmconst(N,f,t1); 
assert_checkalmostequal(Ctfrsp(sig),tfrsp(sig));  
assert_checkalmostequal(Ctfrsp(sig,1:N,N,ones(N,1)),...
                        tfrsp(sig,1:N,N,ones(N,1)),...
                        rtol,atol);  
assert_checkalmostequal(Ctfrsp(sig,1:N,round(N/2),window("re",round(N/2)+1)),...
                        tfrsp(sig,1:N,round(N/2),window("re",round(N/2)+1)),...
                        rtol,atol);
f0=10;
sig=fmconst(N+6,f0/N);
assert_checkalmostequal(Ctfrsp(sig,ceil(N/2)+2,N,tftb_window(N,'rect')),...
                         tfrsp(sig,ceil(N/2)+2,N,tftb_window(N,'rect')),...
                        rtol,atol);


