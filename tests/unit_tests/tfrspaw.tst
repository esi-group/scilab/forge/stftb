mode(-1)
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
//function tfrspawt
//TFRSPAWT Unit test for the time-frequency representation TFRSPAW.

//	O. Lemoine - June 1996. 

// We test each property of the corresponding TFR :

N=64;

K=-1;
NG0=15;
NH0=14;

// Covariance by translation in time 
t1=30; t2=40; f=0.25; 
sig1=amgauss(N,t1).*fmconst(N,f,t1); 
sig2=amgauss(N,t2).*fmconst(N,f,t2); 
tfr1=tfrspaw(sig1,1:N,K,NH0,NG0,0.05,0.45,N);  
tfr2=tfrspaw(sig2,1:N,K,NH0,NG0,0.05,0.45,N);        
[tr,tc]=size(tfr1);
tfr=tfr1-tfr2(:,vecmodulo((1:tc)-t1+t2,tc));
assert_checktrue( max(abs(tfr))<1e-1);

// Reality of the TFR
sig=noisecg(N); 
tfr=tfrspaw(sig,1:N,K,NH0,NG0,0.05,0.45,N);
assert_checktrue(isreal(tfr));

// Energy conservation
sig=fmconst(N); 
[tfr,t,f]=tfrspaw(sig,1:N,K,NH0,NG0,0.1,0.4,N);
Es=norm(sig)^2;
Etfr=integ2d(tfr,t,f);
assert_checkalmostequal(Es,Etfr);


K=0;
NG0=2;
NH0=11;

// Covariance by translation in time 
t1=30; t2=40; f=0.25; 
sig1=amgauss(N,t1).*fmconst(N,f,t1); 
sig2=amgauss(N,t2).*fmconst(N,f,t2); 
tfr1=tfrspaw(sig1,1:N,K,NH0,NG0,0.05,0.45,N);  
tfr2=tfrspaw(sig2,1:N,K,NH0,NG0,0.05,0.45,N);        
[tr,tc]=size(tfr1);
tfr=tfr1-tfr2(:,vecmodulo((1:tc)-t1+t2,tc));
assert_checktrue( max(abs(tfr))<1e-1);

// Reality of the TFR
sig=noisecg(N); 
tfr=tfrspaw(sig,1:N,K,NH0,NG0,0.05,0.45,N);
assert_checktrue(isreal(tfr));


// Energy conservation
sig=fmconst(N); 
[tfr,t,f]=tfrspaw(sig,1:N,K,NH0,NG0,0.1,0.4,N);
Es=norm(sig)^2;
Etfr=integ2d(tfr,t,f);
assert_checkalmostequal(Es,Etfr);

K=1/2;
NG0=5;
NH0=12;

// Covariance by translation in time 
t1=30; t2=40; f=0.25; 
sig1=amgauss(N,t1).*fmconst(N,f,t1); 
sig2=amgauss(N,t2).*fmconst(N,f,t2); 
tfr1=tfrspaw(sig1,1:N,K,NH0,NG0,0.05,0.45,N);  
tfr2=tfrspaw(sig2,1:N,K,NH0,NG0,0.05,0.45,N);        
[tr,tc]=size(tfr1);
tfr=tfr1-tfr2(:,vecmodulo((1:tc)-t1+t2,tc));
assert_checktrue( max(abs(tfr))<1e-1);

// Reality of the TFR
sig=noisecg(N); 
tfr=tfrspaw(sig,1:N,K,NH0,NG0,0.05,0.45,N);
assert_checktrue(isreal(tfr));

// Energy conservation
sig=fmconst(N); 
[tfr,t,f]=tfrspaw(sig,1:N,K,NH0,NG0,0.1,0.4,N);
Es=norm(sig)^2;
Etfr=integ2d(tfr,t,f);
assert_checkalmostequal(Es,Etfr);

K=2;
NG0=7;
NH0=17;

// Covariance by translation in time 
t1=30; t2=40; f=0.25; 
sig1=amgauss(N,t1).*fmconst(N,f,t1); 
sig2=amgauss(N,t2).*fmconst(N,f,t2); 
tfr1=tfrspaw(sig1,1:N,K,NH0,NG0,0.05,0.45,2*N);  
tfr2=tfrspaw(sig2,1:N,K,NH0,NG0,0.05,0.45,2*N);        
[tr,tc]=size(tfr1);
tfr=tfr1-tfr2(:,vecmodulo((1:tc)-t1+t2,tc));
assert_checktrue( max(abs(tfr))<2e-1);


// Reality of the TFR
sig=noisecg(N); 
tfr=tfrspaw(sig,1:N,K,NH0,NG0,0.05,0.45,2*N);
assert_checktrue(isreal(tfr));

// Energy conservation
sig=fmconst(N); 
[tfr,t,f]=tfrspaw(sig,1:N,K,NH0,NG0,0.1,0.4,N);
Es=norm(sig)^2;
Etfr=integ2d(tfr,t,f);
assert_checkalmostequal(Es,Etfr);


N=63;

K=-1;
NG0=15;
NH0=14;

// Covariance by translation in time 
t1=30; t2=40; f=0.25; 
sig1=amgauss(N,t1).*fmconst(N,f,t1); 
sig2=amgauss(N,t2).*fmconst(N,f,t2); 
tfr1=tfrspaw(sig1,1:N,K,NH0,NG0,0.05,0.45,N);  
tfr2=tfrspaw(sig2,1:N,K,NH0,NG0,0.05,0.45,N);        
[tr,tc]=size(tfr1);
nu=round(f*(tc-1)*2)+1;
tfr=tfr1-tfr2(:,vecmodulo((1:tc)-t1+t2,tc));
assert_checktrue( max(abs(tfr))<2e-1);

// Reality of the TFR
sig=noisecg(N); 
tfr=tfrspaw(sig,1:N,K,NH0,NG0,0.05,0.45,N);
assert_checktrue(isreal(tfr))

// Energy conservation
sig=fmconst(N); 
[tfr,t,f]=tfrspaw(sig,1:N,K,NH0,NG0,0.1,0.4,N);
SP = fft(hilbert(real(sig))); 
indmin = 1+round(.1*(N-2));
indmax = 1+round(.4*(N-2));
SPana = SP(indmin:indmax);
Es=SPana'*SPana/N;
Etfr=integ2d(tfr,t,f);
assert_checkalmostequal(Es,Etfr);

K=0;
NG0=2;
NH0=11;

// Covariance by translation in time 
t1=30; t2=40; f=0.25; 
sig1=amgauss(N,t1).*fmconst(N,f,t1); 
sig2=amgauss(N,t2).*fmconst(N,f,t2); 
tfr1=tfrspaw(sig1,1:N,K,NH0,NG0,0.05,0.45,N);  
tfr2=tfrspaw(sig2,1:N,K,NH0,NG0,0.05,0.45,N);        
[tr,tc]=size(tfr1);
tfr=tfr1-tfr2(:,vecmodulo((1:tc)-t1+t2,tc));
assert_checktrue( max(abs(tfr))<1e-1);


// Reality of the TFR
sig=noisecg(N); 
tfr=tfrspaw(sig,1:N,K,NH0,NG0,0.05,0.45,N);
assert_checktrue(isreal(tfr));

// Energy conservation
sig=fmconst(N); 
[tfr,t,f]=tfrspaw(sig,1:N,K,NH0,NG0,0.1,0.4,N);
SP = fft(hilbert(real(sig))); 
indmin = 1+round(.1*(N-2));
indmax = 1+round(.4*(N-2));
SPana = SP(indmin:indmax);
Es=SPana'*SPana/N;
Etfr=integ2d(tfr,t,f);
assert_checkalmostequal(Es,Etfr);

