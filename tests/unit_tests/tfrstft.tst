mode(-1)
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
//function tfrstftt
//TFRSTFTT Unit test for the function TFRSTFT.

//	O. Lemoine - April 1996. 

N=128;

// Covariance by translation in time 
t1=60; t2=70; f=0.3;
sig1=amgauss(N,t1).*fmconst(N,f,t1); 
sig2=amgauss(N,t2).*fmconst(N,f,t2); 
tfr1=abs(tfrstft(sig1)).^2;  
tfr2=abs(tfrstft(sig2)).^2;        
[tr,tc]=size(tfr1);
assert_checkalmostequal(tfr1,tfr2(:,vecmodulo((1:tc)-t1+t2,tc)),1e-10,1e-8);

// Unitarity
x1=amgauss(N).*fmlin(N);
x2=amexpo2s(N);
tfr1=tfrstft(x1);
tfr2=tfrstft(x2);
cor1=x1'*x2;
cor2=sum(conj(tfr1).*tfr2)/N;
assert_checkalmostequal(cor1,cor2);

// Energy conservation
sig=noisecg(N);
tfr=abs(tfrstft(sig,1:N,N,[1])).^2;
Es=norm(sig)^2;
Etfr=sum(mean(tfr,1));
assert_checkalmostequal(Es,Etfr);

// Comparison with the spectrogram
sig=amgauss(N).*fmlin(N);
t=1:N; Nf=64; Lh=8; 
h=tftb_window(2*Lh+1,'Kaiser'); h=h/norm(h);
tfr1=abs(tfrstft(sig,t,Nf,h)).^2;
tfr2=tfrsp(sig,t,Nf,h);
assert_checkalmostequal(tfr1,tfr2,1e-12,1e-10);


// Synthesis
t=1:N; Lh=8; 
h=tftb_window(2*Lh+1,'Kaiser'); h=h/norm(h);
sig=fmlin(N,0.1,0.4); 
stft=tfrstft(sig,t,N,h); 
timerep=mtlb_ifft(stft); sig2=zeros(N,1);
for ti=1:N,
  tau=-mtlb_min([N/2-1,Lh,N-ti]):mtlb_min([N/2-1,Lh,ti-1]);
  indices= modulo(N+tau,N) + 1 + N*(ti-tau-1);
  timerep=timerep(:).';
  sig2(ti)=timerep(indices)*h(Lh+1+tau)/norm(h(Lh+1+tau));
end;
assert_checkalmostequal(sig2(Lh+1:N-Lh),sig(Lh+1:N-Lh),1e-10,1e-5);


// time localization
t0=30; sig=((1:N)'==t0)+0;
tfr=abs(tfrstft(sig,1:N,N,[1])).^2;
[ik,jk]=find(tfr~=0.0);
assert_checkequal(jk,t0*ones(jk));
assert_checkequal(ik,1:N);


N=127;

// Covariance by translation in time 
t1=61; t2=70; f=0.3;
sig1=amgauss(N,t1).*fmconst(N,f,t1); 
sig2=amgauss(N,t2).*fmconst(N,f,t2); 
tfr1=abs(tfrstft(sig1)).^2;  
tfr2=abs(tfrstft(sig2)).^2;        
[tr,tc]=size(tfr1);
assert_checkalmostequal(tfr1,tfr2(:,vecmodulo((1:tc)-t1+t2,tc)),1e-10,1e-8);

// Unitarity
x1=amgauss(N).*fmlin(N);
x2=amexpo2s(N);
tfr1=tfrstft(x1);
tfr2=tfrstft(x2);
cor1=x1'*x2;
cor2=sum(sum(conj(tfr1).*tfr2))/N;
assert_checkalmostequal(cor1,cor2);

// Energy conservation
sig=noisecg(N);
tfr=abs(tfrstft(sig,1:N,N,[1])).^2;
Es=norm(sig)^2;
Etfr=sum(mean(tfr,1));
assert_checkalmostequal(Es,Etfr);

// Comparison with the spectrogram
sig=amgauss(N).*fmlin(N);
t=1:N; Nf=64; Lh=8; 
h=tftb_window(2*Lh+1,'Kaiser'); h=h/norm(h);
tfr1=abs(tfrstft(sig,t,Nf,h)).^2;
tfr2=tfrsp(sig,t,Nf,h);
assert_checkalmostequal(tfr1,tfr2,1e-12,1e-10);


// Synthesis
t=1:N; Lh=8; 
h=tftb_window(2*Lh+1,'Kaiser'); h=h/norm(h);
sig=fmlin(N,0.1,0.4); 
stft=tfrstft(sig,t,N,h); 
timerep=mtlb_ifft(stft); sig2=zeros(N,1);

for ti=1:N,
  tau=-min([round(N/2)-1,Lh,N-ti]):min([round(N/2)-1,Lh,ti-1]);
  indices= modulo(N+tau,N) + 1 + N*(ti-tau-1);
  timerep=timerep(:).';
  sig2(ti)=timerep(indices)*h(Lh+1+tau)/norm(h(Lh+1+tau));
end;
assert_checkalmostequal(sig2(Lh+1:N-Lh),sig(Lh+1:N-Lh),1e-12,1e-10);



// time localization
t0=30; sig=((1:N)'==t0)+0;
tfr=abs(tfrstft(sig,1:N,N,[1])).^2;
[ik,jk]=find(tfr~=0.0);
assert_checkequal(jk,t0*ones(jk));
assert_checkequal(ik,1:N);


