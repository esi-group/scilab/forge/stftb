mode(-1)
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

//       O. Lemoine - March 1996. 

// We test each property of the corresponding TFR :
N=128;

// Covariance by translation in time 
t1=60; t2=70; f=0.3;
sig1=amgauss(N,t1).*fmconst(N,f,t1); 
sig2=amgauss(N,t2).*fmconst(N,f,t2); 
tfr1=tfrpage(sig1);  
tfr2=tfrpage(sig2);        
[tr,tc]=size(tfr1);
assert_checkalmostequal(tfr1,tfr2(:,vecmodulo((1:tc)-t1+t2,tc)),[],1e-8);


// Reality of the TFR
sig=noisecg(N);
tfr=tfrpage(sig);
assert_checktrue(isreal(tfr));


// Energy conservation
sig=noisecg(N);
tfr=tfrpage(sig);
Es=norm(sig)^2;
Etfr=sum(mean(tfr,1));
assert_checkalmostequal(Es,Etfr);


// Time-marginal
sig=noisecg(N);
tfr=tfrpage(sig);
assert_checkalmostequal(abs(sig).^2,mean(tfr,1)');


// Frequency-marginal
sig=noisecg(N);
tfr=tfrpage(sig);
FFT=fft(sig);
psd1=abs(FFT).^2/N;
psd2=mean(tfr',1)';
assert_checkalmostequal(psd1,psd2,[],[],"matrix");


// Conservation of the time support (wide-sense)
sig=[zeros(N/4,1);noisecg(N/2);zeros(N/4,1)];
tfr=tfrpage(sig);
assert_checkfalse(or(abs(tfr(:,1:round(N/4)-1))>sqrt(%eps)));
assert_checkfalse(or(abs(tfr(:,(round(3*N/4)+1):N))>sqrt(%eps)));

// time localization
t0=30; sig=((1:N)'==t0)+0;
tfr=tfrpage(sig);
[ik,jk]=find(tfr~=0.0);
assert_checktrue(and(jk==t0));
assert_checkequal(ik,1:N);


N=131;

// Covariance by translation in time 
t1=60; t2=70; f=0.3;
sig1=amgauss(N,t1).*fmconst(N,f,t1); 
sig2=amgauss(N,t2).*fmconst(N,f,t2); 
tfr1=tfrpage(sig1);  
tfr2=tfrpage(sig2);        
[tr,tc]=size(tfr1);
assert_checkalmostequal(tfr1,tfr2(:,vecmodulo((1:tc)-t1+t2,tc)),[],1e-8);


// Reality of the TFR
sig=noisecg(N);
tfr=tfrpage(sig);
assert_checktrue(isreal(tfr));


// Energy conservation
sig=noisecg(N);
tfr=tfrpage(sig);
Es=norm(sig)^2;
Etfr=sum(mean(tfr,1));
assert_checkalmostequal(Es,Etfr);


// Time-marginal
sig=noisecg(N);
tfr=tfrpage(sig);
assert_checkalmostequal(abs(sig).^2,mean(tfr,1)');


// Frequency-marginal
sig=noisecg(N);
tfr=tfrpage(sig);
FFT=fft(sig);
psd1=abs(FFT).^2/N;
psd2=mean(tfr,2);
assert_checkalmostequal(psd1,psd2);


// Conservation of the time support (wide-sense)
sig=[zeros(round(N/4),1);noisecg(round(N/2));zeros(round(N/4),1)];
tfr=tfrpage(sig);
assert_checkfalse(or(abs(tfr(:,1:round(N/4)-1))>sqrt(%eps)));
assert_checkfalse(or(abs(tfr(:,(round(3*N/4)+2):N))>sqrt(%eps)));


// time localization
t0=30; sig=((1:N)'==t0)+0;
tfr=tfrpage(sig);
[ik,jk]=find(tfr~=0.0);
assert_checktrue(and(jk==t0));
assert_checkequal(ik,1:N);
