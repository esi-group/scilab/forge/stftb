mode(-1)
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
//
//Unit test for the function Caf2tfr 
//	S. Steer - April 2018

ref=[6.2360679774998,-5.4721359549996,3.4721359549996,-5.4721359549996,6.2360679774998;
     1.2360679774998,4.1631189606246,1.9270509831248,4.1631189606246,1.2360679774998;
     -4.3541019662497,-1.4270509831248,-3.6631189606246,-1.4270509831248,-4.3541019662497;
     -4.3541019662497,-1.4270509831248,-3.6631189606246,-1.4270509831248,-4.3541019662497;
     1.2360679774998,4.1631189606246,1.9270509831248,4.1631189606246,1.2360679774998];
 
sig=[-ones(1,2),0,ones(1,2)];
NAF=Cambifunb(sig,-2:2);
K=Ctfrker(5,5,'WV');
TFR=Caf2tfr(NAF,K);
assert_checkalmostequal(TFR,ref); 
assert_checkerror ("Caf2tfr(NAF,Ctfrker(6,6,''WV''))" , msprintf(_("%s: Arguments #%d and #%d must have the same sizes.\n"),"Caf2tfr",1,2));
assert_checkerror ("Caf2tfr(NAF(:,1:4),K)" , msprintf(_("%s: Wrong size for argument #%d: Square matrix expected.\n"),"Caf2tfr",1,2));
