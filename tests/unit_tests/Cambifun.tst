mode(-1)
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
//
//Unit test for the function Cambifunb assuming ambifunb is ok.
//	S. Steer - April 2018


sig=[zeros(1,2),ones(1,4),zeros(1,2)];


[NAF]=ambifunb(sig);
[CNAF]=Cambifunb(sig);
assert_checkalmostequal(NAF,CNAF);

[NAF,TAU]=ambifunb(sig);
[CNAF,CTAU]=Cambifunb(sig);
assert_checkalmostequal(NAF,CNAF);
assert_checkalmostequal(TAU,CTAU);

[NAF,TAU,XI]=ambifunb(sig);
[CNAF,CTAU,CXI]=Cambifunb(sig);
assert_checkalmostequal(NAF,CNAF);
assert_checkalmostequal(TAU,CTAU);
assert_checkalmostequal(XI,CXI);

tau=-2:3;
[NAF,TAU,XI]=ambifunb(sig,tau);
[CNAF,CTAU,CXI]=Cambifunb(sig,tau);
assert_checkalmostequal(NAF,CNAF);
assert_checkalmostequal(TAU,CTAU);
assert_checkalmostequal(XI,CXI);

tau=-3:3;
[NAF,TAU,XI]=ambifunb(sig,tau,6);
[CNAF,CTAU,CXI]=Cambifunb(sig,tau,6);
assert_checkalmostequal(NAF,CNAF); 
assert_checkalmostequal(TAU,CTAU);
assert_checkalmostequal(XI,CXI);

tau=-3:3;
[NAF,TAU,XI]=ambifunb(sig,tau,5);
[CNAF,CTAU,CXI]=Cambifunb(sig,tau,5);
assert_checkalmostequal(NAF,CNAF); 
assert_checkalmostequal(TAU,CTAU);
assert_checkalmostequal(XI,CXI);

