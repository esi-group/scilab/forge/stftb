mode(-1)
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
//function anaaskt
//ANAASKT Unit test for the function ANAASK.

//	O. Lemoine - February 1996.
//	H. Nahrstaedt - 2012
N=256;

// Output frequency law
f0=0.02;
[signal,am]=anaask(N,32,f0);
iflaw=instfreq(signal);

assert_checkalmostequal(iflaw,f0*ones(N-2,1),%eps,sqrt(%eps));


f0=0.25;
[signal,am]=anaask(N,41,f0);
iflaw=instfreq(signal);
assert_checkalmostequal(iflaw,f0*ones(N-2,1),%eps,sqrt(%eps));

f0=0.49;
[signal,am]=anaask(N,57,f0);
iflaw=instfreq(signal);
assert_checkalmostequal(iflaw,f0*ones(N-2,1),%eps,sqrt(%eps));

// Output amplitude
f0=0.01;
[signal,am]=anaask(N,26,f0);
assert_checkfalse(or(am>1)|or(am<0));


f0=0.17;
[signal,am]=anaask(N,37,f0);
assert_checkfalse(or(am>1)|or(am<0));

f0=0.43;
[signal,am]=anaask(N,12,f0);
assert_checkfalse(or(am>1)|or(am<0));

N=221; 

// Output frequency law
f0=0.02;
[signal,am]=anaask(N,32,f0);
iflaw=instfreq(signal);
assert_checkalmostequal(iflaw,f0*ones(N-2,1),%eps,sqrt(%eps));

f0=0.25;
[signal,am]=anaask(N,41,f0);
iflaw=instfreq(signal);
assert_checkalmostequal(iflaw,f0*ones(N-2,1),%eps,sqrt(%eps));

f0=0.49;
[signal,am]=anaask(N,57,f0);
iflaw=instfreq(signal);
assert_checkalmostequal(iflaw,f0*ones(N-2,1),%eps,sqrt(%eps));

// Output amplitude
f0=0.01;
[signal,am]=anaask(N,26,f0);
assert_checkfalse(or(am>1)|or(am<0));

f0=0.17;
[signal,am]=anaask(N,37,f0);
assert_checkfalse(or(am>1)|or(am<0));

f0=0.43;
[signal,am]=anaask(N,12,f0);
assert_checkfalse(or(am>1)|or(am<0));
 
