mode(-1)
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
//function tfrwvt
//TFRWVT	Unit test for the function TFRWV.

//	F. Auger, December 1995 - O. Lemoine, March 1996.

N=128; 

// Covariance by translation in time 
t1=60; t2=70; f=0.3;
sig1=amgauss(N,t1).*fmconst(N,f,t1); 
sig2=amgauss(N,t2).*fmconst(N,f,t2); 
tfr1=Ctfrwv(sig1);  
tfr2=Ctfrwv(sig2);
assert_checkalmostequal(tfr1,tfr2(:,modulo((0:N-1)-t1+t2,N)+1),sqrt(%eps),[],"matrix");

// Reality of the TFR
sig=noisecg(N);
tfr=Ctfrwv(sig);
assert_checktrue(isreal(tfr));

// Energy conservation
sig=noisecg(N);
tfr=Ctfrwv(sig);
Es=norm(sig)^2;
Etfr=sum(mean(tfr,1));
assert_checkalmostequal(Es,Etfr);

// Time-marginal
sig=noisecg(N);
tfr=Ctfrwv(sig);
assert_checkalmostequal(abs(sig).^2,mean(tfr,1)');

// Frequency-marginal
sig=noisecg(N);
tfr=Ctfrwv(sig);
FFT=fft(sig);
psd1=abs(FFT(2:N/2)).^2/(2*N);
psd2=mean(tfr(1:2:N,:)',1)';
assert_checkalmostequal(psd1,psd2(2:$));

// Compatibility with filtering
h=amgauss(N,N/2,sqrt(N)).*fmlin(N);
x=amgauss(N).*fmconst(N);
y=convol(x,h); y=y(:);  y=y((N/2+1):3*N/2);
tfrx=Ctfrwv(x);
tfry=Ctfrwv(y)/(2*N);
tfrh=Ctfrwv(h);
tfr=zeros(N,N);
for f=1:N,
 tmp=convol(tfrx(f,:),tfrh(f,:));
 tfr(f,:)=tmp((N/2+1):3*N/2)/N;
end
assert_checkalmostequal(tfr,tfry,[],%eps,"matrix");

// Unitarity
x1=amgauss(N).*fmlin(N);
x2=amgauss(N).*fmsin(N);
tfr1=Ctfrwv(x1);
tfr2=Ctfrwv(x2);
cor1=abs(x1'*x2)^2/2;
cor2=sum(sum(tfr1.*conj(tfr2)))/N;
assert_checkalmostequal(cor1,cor2);

// Conservation of the time support (wide-sense)
sig=[zeros(N/4,1);fmlin(N/2);zeros(N/4,1)];
tfr=Ctfrwv(sig);
assert_checkfalse(or(abs(tfr(:,1:round(N/4)-1))>sqrt(%eps)));
assert_checkfalse(or(abs(tfr(:,(3*round(N/4)+2):N))>sqrt(%eps)));

// time localization
t0=30; sig= ((1:N)'==t0)+0;
tfr=Ctfrwv(sig);
[ik,jk]=find(tfr~=0.0);
assert_checktrue(and(jk==t0));
assert_checkequal(ik,1:N);

// frequency localization
f0=10;
sig=fmconst(N+6,f0/N);
tfr=Ctfrwv(sig,N/2+2,N);
assert_checkfalse(or(find(tfr>1/N)~=2*f0+1));
assert_checkalmostequal(mean(tfr),1);


N=127; 

// Covariance by translation in time 
t1=60; t2=70; f=0.3;
sig1=amgauss(N,t1).*fmconst(N,f,t1); 
sig2=amgauss(N,t2).*fmconst(N,f,t2); 
tfr1=Ctfrwv(sig1);  
tfr2=Ctfrwv(sig2);  
assert_checkalmostequal(tfr1,tfr2(:,modulo((0:N-1)-t1+t2,N)+1),sqrt(%eps),[],"matrix");

// Reality of the TFR
sig=noisecg(N);
tfr=Ctfrwv(sig);
assert_checktrue(isreal(tfr));

// Energy conservation
sig=noisecg(N);
tfr=Ctfrwv(sig);
Es=norm(sig)^2;
Etfr=sum(mean(tfr,1));
assert_checkalmostequal(Es,Etfr);

// Time-marginal
sig=noisecg(N);
tfr=Ctfrwv(sig);
assert_checkalmostequal(abs(sig).^2,mean(tfr,1)');


// Frequency-marginal
sig=noisecg(N);
tfr=Ctfrwv(sig);
FFT=fft(sig);
psd1=abs(FFT(2:fix(N/2)-3)).^2/(2*N);
psd2=mean(tfr(1:2:N,:)',1)';
assert_checkalmostequal(psd1,psd2(2:fix(N/2)-3),[],0.1);

// Compatibility with filtering
h=amgauss(N,N/2,sqrt(N)).*fmlin(N);
x=amgauss(N).*fmconst(N);
y=convol(x,h);  y=y(:); y=y((round(N/2)+1):round(3*N/2));
tfrx=Ctfrwv(x);
tfry=Ctfrwv(y)/(2*N);
tfrh=Ctfrwv(h);
tfr=zeros(N,N);
for f=1:N,
 tmp=convol(tfrx(f,:),tfrh(f,:));
 tfr(f,:)=tmp((round(N/2)+1):round(3*N/2))/N;
end
assert_checkalmostequal(tfr,tfry,[],%eps,"matrix");

// Unitarity
x1=amgauss(N).*fmlin(N);
x2=amgauss(N).*fmsin(N);
tfr1=Ctfrwv(x1);
tfr2=Ctfrwv(x2);
cor1=abs(x1'*x2)^2/2;
cor2=sum(sum(tfr1.*conj(tfr2)))/N;
assert_checkalmostequal(cor1,cor2);

// Conservation of the time support (wide-sense)
sig=[zeros(round(N/4),1);fmlin(round(N/2));zeros(round(N/4),1)];
tfr=Ctfrwv(sig);
assert_checkfalse(or(abs(tfr(:,1:round(N/4)-1))>sqrt(%eps)));
assert_checkfalse(or(abs(tfr(:,(round(3*N/4)+2):N))>sqrt(%eps)));

// time localization
t0=30; sig= ((1:N)'==t0)+0;
tfr=Ctfrwv(sig);
[ik,jk]=find(tfr~=0.0);
assert_checktrue(and(jk==t0));
assert_checkequal(ik,1:N);

// frequency localization
f0=10;
sig=fmconst(N+6,f0/N);
tfr=Ctfrwv(sig,round(N/2)+2,N);
//assert_checkfalse(or(find(tfr>1/N)~=2*f0+1));
assert_checkalmostequal(mean(tfr),1);
