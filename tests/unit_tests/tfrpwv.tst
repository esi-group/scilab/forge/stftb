mode(-1)
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

//       F. Auger, Dec. 1995 - O. Lemoine, March 1996.

// We test each property of the corresponding TFR :

N=128;

// Covariance by translation in time 
t1=55; t2=70; f=0.3;
sig1=amgauss(N,t1).*fmconst(N,f,t1); 
sig2=amgauss(N,t2).*fmconst(N,f,t2); 
tfr1=tfrpwv(sig1);  
tfr2=tfrpwv(sig2);        
[tr,tc]=size(tfr1);
assert_checkalmostequal(tfr1,tfr2(:,vecmodulo((1:tc)-t1+t2,tc)),1e-10,1e-8);

// Reality of the TFR
sig=noisecg(N);
tfr=tfrpwv(sig);
assert_checktrue(isreal(tfr));

// Energy conservation
sig=noisecg(N);
tfr=tfrpwv(sig);
assert_checkalmostequal(norm(sig)^2,sum(mean(tfr,1)));

// Time-marginal
sig=noisecg(N);
tfr=tfrpwv(sig);
assert_checkalmostequal(abs(sig).^2,mean(tfr,1)');

// Frequency-marginal
sig=noisecg(N);
tfr=tfrpwv(sig,1:N,N,ones(N-1,1));
FFT=fft(sig);
psd1=abs(FFT(2:round(N/2))).^2/(2*N);
psd2=mean(tfr(1:2:N,:),2);
assert_checkalmostequal(psd1,psd2(2:$  ));

// Conservation of the time support (wide-sense)
sig=[zeros(N/4,1);noisecg(N/2);zeros(N/4,1)];
tfr=tfrpwv(sig);
assert_checktrue(or(abs(tfr(:,1:N/4-1))<sqrt(%eps)));
assert_checktrue(or(abs(tfr(:,(3*N/4+1):N))<sqrt(%eps)));

// frequency localization
f0=10;
sig=fmconst(N+6,f0/N);
tfr=tfrpwv(sig,N/2+2,N,tftb_window(N+1,'rect'));
assert_checkequal(find(tfr>1/N),2*f0+1);
assert_checkalmostequal(mean(tfr,1),1);

// time localization
t0=30; sig=((1:N)'==t0)+0;
tfr=tfrpwv(sig);
[ik,jk]=find(tfr~=0.0);
assert_checkequal(jk,t0*ones(jk));
assert_checkequal(ik,1:N);

// A PWVD with a large rectangular window is a WVD
sig=noisecg(N);
tfr1=tfrpwv(sig,1:N,N,ones(2*N+1,1));
tfr2=tfrwv(sig);
assert_checkalmostequal(tfr1,tfr2,[],1e-5)


N=127;

// Covariance by translation in time 
t1=55; t2=70; f=0.3;
sig1=amgauss(N,t1).*fmconst(N,f,t1); 
sig2=amgauss(N,t2).*fmconst(N,f,t2); 
tfr1=tfrpwv(sig1);  
tfr2=tfrpwv(sig2);        
[tr,tc]=size(tfr1);
assert_checkalmostequal(tfr1,tfr2(:,vecmodulo((1:tc)-t1+t2,tc)),1e-10,1e-8);

// Reality of the TFR
sig=noisecg(N);
tfr=tfrpwv(sig);
assert_checktrue(isreal(tfr));

// Energy conservation
sig=noisecg(N);
tfr=tfrpwv(sig);
assert_checkalmostequal(norm(sig)^2,sum(mean(tfr,1)));

// Time-marginal
sig=noisecg(N);
tfr=tfrpwv(sig);
assert_checkalmostequal(abs(sig).^2,mean(tfr,1)');


// Conservation of the time support (wide-sense)
sig=[zeros(round(N/4),1);noisecg(round(N/2));zeros(round(N/4),1)];
tfr=tfrpwv(sig);
assert_checktrue(or(abs(tfr(:,1:N/4-1))<sqrt(%eps)));
assert_checktrue(or(abs(tfr(:,(3*N/4+1):N))<sqrt(%eps)));


// time localization
t0=30; sig=((1:N)'==t0)+0;
tfr=tfrpwv(sig);
[ik,jk]=find(tfr~=0.0);
assert_checkequal(jk,t0*ones(jk));
assert_checkequal(ik,1:N);

// frequency localization
f0=10;
sig=fmconst(N+6,f0/N);
tfr=tfrpwv(sig,round(N/2)+2,N,tftb_window(N,'rect'));
assert_checkequal(find(tfr>1/N),2*f0+1);
assert_checkalmostequal(mean(tfr,1),1);


// A PWVD with a large rectangular window is a WVD
sig=noisecg(N);
tfr1=tfrpwv(sig,1:N,N,ones(2*N+1,1));
tfr2=tfrwv(sig);
assert_checkalmostequal(tfr1,tfr2,[],1e-5)
