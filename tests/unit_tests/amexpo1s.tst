mode(-1)
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
//function amexpo1t
//AMEXPO1T Unit test for the function AMEXPO1S.

//	O. Lemoine - February 1996.
//	H. Nahrstaedt - 2012


N=256; t0=32; T=50; 
sig=amexpo1s(N,t0,T);
assert_checkalmostequal(sig(t0),1,%eps,sqrt(%eps));

[tm,T1]=loctime(sig);
assert_checkalmostequal(T,T1,1/(N-t0));
assert_checkalmostequal(sig(1:t0-1),zeros(sig(1:t0-1)),%eps,sqrt(%eps));



N=133; t0=28; T=15; 
sig=amexpo1s(N,t0,T);
assert_checkalmostequal(sig(t0),1,%eps,sqrt(%eps));

[tm,T1]=loctime(sig);
assert_checkalmostequal(T,T1,1/(N-t0));
assert_checkalmostequal(sig(1:t0-1),zeros(sig(1:t0-1)),%eps,sqrt(%eps));


N=529; t0=409; T=31; 
sig=amexpo1s(N,t0,T);
assert_checkalmostequal(sig(t0),1,%eps,sqrt(%eps));

[tm,T1]=loctime(sig);
assert_checkalmostequal(T,T1,1/(N-t0));
assert_checkalmostequal(sig(1:t0-1),zeros(sig(1:t0-1)),%eps,sqrt(%eps));


