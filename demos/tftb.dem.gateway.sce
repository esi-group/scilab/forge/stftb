
demopath = get_absolute_file_path("tftb.dem.gateway.sce");

subdemolist=['Introduction', 'tfdemo1.sce';..
'Non stationary signals', 'tfdemo2.sce';..
'Linear time-frequency representations', 'tfdemo3.sce';..
 'Cohen''s class time-frequency distributions', 'tfdemo4.sce';..
'Affine class time-frequency distributions', 'tfdemo5.sce';..
'Reassigned time-frequency distributions', 'tfdemo6.sce';..
 'Post-processing ', 'tfdemo7.sce'; ..
'ANSI C Time Frequency demo', 'Ctftbdemo.sce';..
'Analysis of some real signals', 'Ctftbdemo2.sce';..
];


subdemolist(:,2) = demopath + subdemolist(:,2);
// ====================================================================
