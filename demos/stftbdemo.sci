//function Ctftbdemo();
//-----------------------------------
// ANSI C Time Frequency Toolbox
//-----------------------------------
//        Scilab demo file
//-----------------------------------
//close all
//mode(1)
clc;  f=scf(); fig_id=get(gcf(),"figure_id"); clf; 
f.color_map = jetcolormap(64);
mode(1);lines(0);
disp('---------------------------------------------')
disp(' Welcome to the ANSI C Time frequency toolbox')
disp(' demonstration file')
disp('---------------------------------------------')

disp('--------------------------------------------')
disp(' First of all, we create FM a signal')
disp(' with additive noise')
disp('---------------------------------------------')
t=1:128;
sig=sin(2*%pi*(0.3+0.2*t+0.001*t.^2))+0.5*rand(1,128,'normal');
sig=hilbert(sig);sig=sig(:);

disp('--------------------------------------------')
disp(' this signal looks like this')
disp('---------------------------------------------')
scf(fig_id);clf(fig_id);
plot(1:128,real(sig),'b',1:128,imag(sig),'r');
xlabel('Time (points)')
ylabel('Amplitude')
title('Analyzed signal (blue=real part, red=imag part)')
a=gca();a.data_bounds=([0 128 -2.5 2.5]);
a.tight_limits ='on';

halt('Press return to continue');

disp('--------------------------------------------')
disp(' Let''s have a look to a spectrogram')
disp('---------------------------------------------')
[TFR,T,F]=tfrsp(sig,1:128,128,tftb_window(31,'hamming'));
scf(fig_id);clf(fig_id);
grayplot(T,F,TFR');

//axis xy
xlabel('Time (points)');
ylabel('Normalized frequency');
title('Spectrogram');


halt('Press return to continue');


disp('--------------------------------------------')
disp(' What about a  Reassigned spectrogram ?')
disp('---------------------------------------------')
TFR=tfrrsp(sig,1:128,128,tftb_window(31,'hamming'));
scf(fig_id);clf(fig_id);
grayplot(T,F,TFR');
//axis xy
xlabel('Time (points)');
ylabel('Normalized frequency');
title('Reassigned Spectrogram');
a=gca();a.tight_limits ='on';


halt('Press return to continue');
scf(fig_id);clf(fig_id);
disp('--------------------------------------------')
disp(' Or a Choi-Williams Representation ?')
disp('--------------------------------------------')
[TFR,T,F]=tfrcw(sig);

grayplot(T,F,TFR');
//axis xy
xlabel('Time (points)');
ylabel('Normalized frequency');
title('Choi-Williams representation');
a=gca();a.tight_limits ='on';


halt('Press return to continue');

disp('--------------------------------------------')
disp(' We could try a lot more TFR kernels, ')
disp(' such as the following')
disp('--------------------------------------------')
mode(-1)
scf(fig_id);clf(fig_id);;
[TFR,T,F]=tfrbj(sig);
subplot(2,3,1);
grayplot(T,F,TFR');
//axis xy
xlabel('Time (points)');
ylabel('Normalized frequency');
title('Born-Jordan')
a=gca();a.tight_limits ='on';

[TFR,T,F]=tfrbud(sig);
subplot(2,3,2);
grayplot(T,F,TFR');
//axis xy
xlabel('Time (points)');
ylabel('Normalized frequency');
title('Butterworth')
a=gca();a.tight_limits ='on';

[TFR,T,F]=tfrgrd(sig);
subplot(2,3,3);
grayplot(T,F,TFR');
//axis xy
xlabel('Time (points)');
ylabel('Normalized frequency');
title('Generalized rectangular')
a=gca();a.tight_limits ='on';

[TFR,T,F]=tfrmh(sig);
subplot(2,3,4);
grayplot(T,F,TFR');
//axis xy
xlabel('Time (points)');
ylabel('Normalized frequency');
title('Generalized Rectangular')
[TFR,T,F]=tfrmh(sig);
a=gca();a.tight_limits ='on';

[TFR,T,F]=tfrwv(sig);
subplot(2,3,5);
grayplot(T,F,TFR');
//axis xy
xlabel('Time (points)');
ylabel('Normalized frequency');
title('Wigner-Ville')
a=gca();a.tight_limits ='on';

[TFR,T,F]=tfrpwv(sig);
subplot(2,3,6);
grayplot(T,F,TFR');
//axis xy
xlabel('Time (points)');
ylabel('Normalized frequency');
title('Pseudo Wigner-Ville')
a=gca();a.tight_limits ='on';
mode(1)

halt('Press return to continue');

