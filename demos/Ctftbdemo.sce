//function Ctftbdemo();
//-----------------------------------
// ANSI C Time Frequency Toolbox
//-----------------------------------
//        Scilab demo file
//-----------------------------------
//close all
//mode(1)

function imagesc(x,y,z)
// set figure_style new; 
// a=gca();
// a.data_bounds=[x(1),y(1);x($),y($)]; 
// a.axes_visible="on"; 
// Matplot1(z($:-1:1,:),[x(1),y(1),x($),y($)])
  grayplot(x,y,z');

endfunction

clc;  f=scf(); fig_id=get(gcf(),"figure_id"); clf; 
f.color_map = jetcolormap(64);
mode(1);lines(0);
disp('---------------------------------------------')
disp(' Welcome to the ANSI C Time frequency toolbox')
disp(' demonstration file')
disp('---------------------------------------------')



disp('--------------------------------------------')
disp(' First of all, we create FM a signal')
disp(' with additive noise')
disp('---------------------------------------------')
t=1:128;
sig=sin(2*%pi*(0.3+0.2*t+0.001*t.^2))+0.5*rand(1,128,'normal');
sig=hilbert(sig);


disp('--------------------------------------------')
disp(' this signal looks like this')
disp('---------------------------------------------')
scf(fig_id);clf(fig_id);
plot(t,real(sig),'b',t,imag(sig),'r');
xlabel('Time (points)')
ylabel('Amplitude')
title('Analyzed signal (blue=real part, red=imag part)')
//a=gca();a.data_bounds=([0 t($) -2.5 2.5]);
a=gca();a.tight_limits ='on';


halt('Press return to continue');

disp('--------------------------------------------')
disp(' Let''s have a look to a spectrogram')
disp('---------------------------------------------')
[TFR,T,F]=Ctfrsp(sig,t,128,Cwindow(31,'hamming'));
scf(fig_id);clf(fig_id);
//grayplot(T,F,TFR');
imagesc(T,F,TFR);
a=gca();a.tight_limits ='on';
//axis xy
xlabel('Time (points)');
ylabel('Normalized frequency');
title('Spectrogram');


halt('Press return to continue');


disp('--------------------------------------------')
disp(' What about a  Reassigned spectrogram ?')
disp('---------------------------------------------')
TFR=Ctfrrsp(sig,t,128,Cwindow(31,'hamming'));
scf(fig_id);clf(fig_id);
//grayplot(T,F,TFR');
imagesc(T,F,TFR);
//axis xy
a=gca();a.tight_limits ='on';
xlabel('Time (points)');
ylabel('Normalized frequency');
title('Reassigned Spectrogram');



halt('Press return to continue');

scf(fig_id);clf(fig_id);
disp('--------------------------------------------')
disp(' Or a Choi-Williams Representation ?')
disp('--------------------------------------------')
[TFR,T,F]=Ctfrcw(sig);

//grayplot(T,F,TFR');
imagesc(T,F,TFR);
//axis xy
a=gca();a.tight_limits ='on';
xlabel('Time (points)');
ylabel('Normalized frequency');
title('Choi-Williams representation');



halt('Press return to continue');

disp('--------------------------------------------')
disp(' We could try a lot more TFR kernels, ')
disp(' such as the following')
disp('--------------------------------------------')
mode(-1)
scf(fig_id);clf(fig_id);;
[TFR,T,F]=Ctfrbj(sig);
subplot(2,3,1);
//grayplot(T,F,TFR');
imagesc(T,F,TFR);
//axis xy
a=gca();a.tight_limits ='on';
xlabel('Time (points)');
ylabel('Normalized frequency');
title('Born-Jordan')

[TFR,T,F]=Ctfrbud(sig);
subplot(2,3,2);
//grayplot(T,F,TFR');
imagesc(T,F,TFR);
//axis xy
a=gca();a.tight_limits ='on';
xlabel('Time (points)');
ylabel('Normalized frequency');
title('Butterworth')

[TFR,T,F]=Ctfrgrd(sig);
subplot(2,3,3);
//grayplot(T,F,TFR');
imagesc(T,F,TFR);
//axis xy
xlabel('Time (points)');
ylabel('Normalized frequency');
title('Generalized rectangular')

[TFR,T,F]=Ctfrmh(sig);
subplot(2,3,4);
//grayplot(T,F,TFR');
imagesc(T,F,TFR);
//axis xy
a=gca();a.tight_limits ='on';
xlabel('Time (points)');
ylabel('Normalized frequency');
title('Margenau-Hill')
[TFR,T,F]=Ctfrmh(sig);

[TFR,T,F]=Ctfrwv(sig);
subplot(2,3,5);
//grayplot(T,F,TFR');
imagesc(T,F,TFR);
//axis xy
a=gca();a.tight_limits ='on';
xlabel('Time (points)');
ylabel('Normalized frequency');
title('Wigner-Ville')

[TFR,T,F]=Ctfrpwv(sig);
subplot(2,3,6);
//grayplot(T,F,TFR');
imagesc(T,F,TFR);
//axis xy

a=gca();a.tight_limits ='on';
xlabel('Time (points)');
ylabel('Normalized frequency');
title('Pseudo Wigner-Ville')
mode(1)

halt('Press return to continue');

disp(' Now, why not try do adapt the TFR kernel')
disp(' to the signals. This is done in 4 steps')
disp('--------------------------------------------')
disp(' 1 - Compute the ambiguity function of the signal')
disp('--------------------------------------------')
scf(fig_id);clf(fig_id);
[AF,TAU,XI]=Cambifunb(sig,-63:64,128);
//imagesc(TAU,XI,abs(AF));
//grayplot(TAU,XI,abs(AF)');
imagesc(TAU,XI,abs(AF));
//axis xy
a=gca();a.tight_limits ='on';
xlabel('Time lag (points)');
ylabel('Normalized frequency lag');
title('Ambiguity function (module)')


halt('Press return to continue');
scf(fig_id);clf(fig_id);
disp('--------------------------------------------')
disp(' 2 - Choose a kernel shape among the possible')
disp(' choices (see Ctfrker). We choose a Generalized')
disp(' Marginals Choi-Williams kernel with one branch')
disp('--------------------------------------------')

Kernel=Ctfrker(128,128,'gmcwk',[1 0.2]);
imagesc(TAU,XI,Kernel);
//grayplot(TAU,XI,Kernel');

//axis xy
a=gca();a.tight_limits ='on';
xlabel('Time lag (points)');
ylabel('Normalized frequency lag');
title('Initial one branch kernel')


halt('Press return to continue');
scf(fig_id);clf(fig_id);
disp('---------------------------------------------')
disp(' 3 - Optimize the parameters of this kernel')
disp(' to fit the analyzed signal')
disp('---------------------------------------------')
d_min=1e100;
sigma=0.1;
theta_min=0;
for theta=linspace(0,2*%pi,30);
  Kernel=Ctfrker(128,128,'gmcwk',[sigma theta]);
  d=Ctfrdist(abs(AF),Kernel,'kullback');
  if (d<d_min),
    d_min=d;
    theta_min=theta;
  end
end

disp('---------------------------------------------')
disp(' 4 - Display the optimal kernel')
disp('---------------------------------------------')
Kernel=Ctfrker(128,128,'gmcwk',[sigma theta_min]);
imagesc(TAU,XI,Kernel);
//grayplot(TAU,XI,Kernel');
//axis xy
a=gca();a.tight_limits ='on';
xlabel('Time lag (points)');
ylabel('Normalized frequency lag');
title('One branch kernel')


halt('Press return to continue');
scf(fig_id);clf(fig_id);
disp('---------------------------------------------')
disp(' 4 - here is the optimal TFR for this signal')
disp('---------------------------------------------')

TFR=Caf2tfr(AF,Kernel)';
imagesc(1:128,linspace(0,0.5,128),TFR);
//grayplot(1:128,linspace(0,0.5,128),TFR');
//axis xy
a=gca();a.tight_limits ='on';
xlabel('Time(points)');
ylabel('Normalized frequency');
title('Optimal One branch kernel TFR')

disp('---------------------------------------------')
disp('Kernel optimization is simple to implement !!')
disp('---------------------------------------------')

