<?xml version="1.0" encoding="UTF-8"?>
<!--
This file is part of the Scilab time frequency toolbox.
This file must be used under the terms of the  terms of the GNU General Public License 
as published by the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

Copyright (c) 1996 by CNRS (France).
-->


<refentry xml:id="fmconst" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:scilab="http://www.scilab.org"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">


  <refsynopsisdiv>
    <title>Calling Sequence</title>
    <synopsis>
      [Y,IFLAW] = fmconst(N,FNORM,T0)
    </synopsis>
  </refsynopsisdiv>

  <refsection>
    <title>Parameters</title>
    <variablelist>
      <varlistentry>
        <term>N     :</term>
        <listitem><para>a positive integer: the number of points.</para></listitem>
      </varlistentry>
      <varlistentry>
        <term>FNORM :</term>
        <listitem>
          <para>a real scalar in [-0.5 0.5]: the normalized frequency (default: 0.25)</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>T0    :</term>
        <listitem>
          <para>an  integer in [1 N]: the time center (default: round(N/2)).</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>Y     :</term>
        <listitem>
          <para>a complex column vector of size N: the signal.</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>IFLAW :</term>
        <listitem>
          <para>a real column vector of size N: the instantaneous frequency law (optional).</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>

  <refsection>
    <title>Description</title>
    <para>
      fmconst generates a frequency modulation  with a constant frequency fnorm.
      The phase of this modulation is such that y(t0)=1.
    </para>
  </refsection>

  <refsection>
    <title>Examples</title>
    <programlisting role="example"><![CDATA[
    [X,IFLAW]=fmconst(128,0.05,50); 
    clf;
    subplot(211); plot(real(X));xtitle(_("Signal real part"))
    subplot(212); plot(IFLAW);xtitle(_("Instantaneous frequency law"))
    ]]></programlisting>
    <scilab:image><![CDATA[
    [X,IFLAW]=fmconst(128,0.05,50); 
    clf;
    subplot(211); plot(real(X));xtitle(_("Signal real part"))
    subplot(212); plot(IFLAW);xtitle(_("Instantaneous frequency law"))
    ]]></scilab:image>
  </refsection>

  <refsection>
    <title>See also</title>
    <simplelist type="inline">
      <member><link linkend="fmlin">fmlin</link></member>
      <member><link linkend="fmsin">fmsin</link></member>
      <member><link linkend="fmodany">fmodany</link></member>
      <member><link linkend="fmhyp">fmhyp</link></member>
      <member><link linkend="fmpar">fmpar</link></member>
      <member><link linkend="fmpower">fmpower</link></member>
    </simplelist>
  </refsection>

  <refsection>
    <title>Authors</title>
    <simplelist type="vert">
      <member>H. Nahrstaedt - Aug 2010</member>
      <member>F. Auger, July 1995.</member>
    </simplelist>
  </refsection>
</refentry>
