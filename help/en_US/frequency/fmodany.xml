<?xml version="1.0" encoding="UTF-8"?>
<!--
This file is part of the Scilab time frequency toolbox.
This file must be used under the terms of the  terms of the GNU General Public License 
as published by the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

Copyright (c) 1996 by CNRS (France).
-->


<refentry xml:id="fmodany" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:scilab="http://www.scilab.org"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">


  <refnamediv>
    <refname>fmodany</refname><refpurpose>Signal with arbitrary frequency modulation.</refpurpose>
  </refnamediv>



  <refsynopsisdiv>
    <title>Calling Sequence</title>
    <synopsis>
      [Y,IFLAW]=fmodany(IFLAW)
      [Y,IFLAW]=fmodany(IFLAW,T0)
    </synopsis>
  </refsynopsisdiv>

  <refsection>
    <title>Parameters</title>
    <variablelist>
      <varlistentry>
        <term>IFLAW :</term>
        <listitem>
          <para>real vector of length N: the instantaneous frequency law samples.</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>T0    :</term>
        <listitem>
          <para>an integer in [1 N]: the time reference (default: 1).</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>Y     :</term>
        <listitem>
          <para>a complex column vector of size N: the output signal</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>

  <refsection>
    <title>Description</title>
    <para>
      fmodany generates a frequency modulated
      signal whose instantaneous frequency law is approximately given by
      the vector IFLAW (the integral is approximated by CUMSUM).
      The phase of this modulation is such that y(t0)=1.
    </para>
  </refsection>

  <refsection>
    <title>Examples</title>
    <programlisting role="example"><![CDATA[
    [y1,ifl1]=fmlin(100); 
    [y2,ifl2]=fmsin(100);
    iflaw=[ifl1;ifl2]; sig=fmodany(iflaw);
    clf;
    subplot(211); plot(real(sig));xtitle(_("Signal real part"))
    subplot(212); plot(iflaw);xtitle(_("Instantaneous frequency law"))
    ]]></programlisting>
    <scilab:image><![CDATA[
    [y1,ifl1]=fmlin(100); 
    [y2,ifl2]=fmsin(100);
    iflaw=[ifl1;ifl2]; sig=fmodany(iflaw);
    clf;
    subplot(211); plot(real(sig));xtitle(_("Signal real part"))
    subplot(212); plot(iflaw);xtitle(_("Instantaneous frequency law"))
    ]]></scilab:image>
  </refsection>

  <refsection>
    <title>See also</title>
    <simplelist type="inline">
      <member><link linkend="fmhyp">fmhyp</link></member>
      <member><link linkend="fmsin">fmsin</link></member>
      <member><link linkend="fmpar">fmpar</link></member>
      <member><link linkend="fmconst">fmconst</link></member>
      <member><link linkend="fmlin">fmlin</link></member>
      <member><link linkend="fmpower">fmpower</link></member>
    </simplelist>
  </refsection>

  <refsection>
    <title>Authors</title>
    <simplelist type="vert">
      <member>H. Nahrstaedt - Aug 2010</member>
      <member>F. Auger, August 1995.</member>
    </simplelist>
  </refsection>
</refentry>
