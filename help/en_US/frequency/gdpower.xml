<?xml version="1.0" encoding="UTF-8"?>
<!--
This file is part of the Scilab time frequency toolbox.
This file must be used under the terms of the  terms of the GNU General Public License 
as published by the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

Copyright (c) 1996 by CNRS (France).
-->


<refentry xml:id="gdpower" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:scilab="http://www.scilab.org"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">


  <refnamediv>
    <refname>gdpower</refname><refpurpose>Signal with power-law group delay.</refpurpose>
  </refnamediv>



  <refsynopsisdiv>
    <title>Calling Sequence</title>
    <synopsis>
      [X,GPD,F]=gspower(N,K,C)
    </synopsis>
  </refsynopsisdiv>

  <refsection>
    <title>Parameters</title>
    <variablelist>
      <varlistentry>
        <term>N  :</term>
        <listitem>
          <para>an even positive integer:  number of points in time</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>K  :</term>
        <listitem>
          <para>a real scalar: the degree of the power-law (default : 0)</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>C  :</term>
        <listitem>
          <para>a real non zero scalar: the  rate-coefficient of the power-law group delay. (default : 1)</para>
        </listitem>
      </varlistentry>
      
      
      <varlistentry>
        <term>X   :</term>
        <listitem>
          <para>a real column vector: the modulated signal samples</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>GPD :</term>
        <listitem><para>a real row vector (length : round(N/2)-1): the group delay (length : round(N/2)-1)</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>F   :</term>
        <listitem>
          <para>a real row vector (length : round(N/2)-1): the frequency bins</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>

  <refsection>
    <title>Description</title>
    <para>
      generates a signal with a power-law group delay of the form tx(f) = T0 + C*f^(K-1).
      The output signal is of unit energy.
    </para>
  </refsection>

  <refsection>
    <title>Examples</title>
    <programlisting role="example"><![CDATA[
    [sig,gpd,f]=gdpower(256,1/2); 
    clf;
    subplot(211);plot(sig);
    subplot(212);plot(gpd,f);
    ]]></programlisting>
    <scilab:image><![CDATA[
    [sig,gpd,f]=gdpower(256,1/2); 
    clf;
    subplot(211);plot(sig);
    subplot(212);plot(gpd,f);
    ]]></scilab:image>
  </refsection>

  <refsection>
    <title>See also</title>
    <simplelist type="inline">
      <member><link linkend="fmpower">fmpower</link></member>
    </simplelist>
  </refsection>

  <refsection>
    <title>Authors</title>
    <simplelist type="vert">
      <member>H. Nahrstaedt - Aug 2010</member>
      <member>O. Lemoine - April, July 1996</member>
    </simplelist>
  </refsection>
</refentry>
