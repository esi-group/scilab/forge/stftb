<?xml version="1.0" encoding="UTF-8"?>
<!--
This file is part of the Scilab time frequency toolbox.
This file must be used under the terms of the  terms of the GNU General Public License 
as published by the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

Copyright (c) 1996 by CNRS (France).
-->

<refentry xml:id="midpoint" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:scilab="http://www.scilab.org"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">

  <refnamediv>
    <refname>midpoint</refname><refpurpose>Mid-point construction used in the interference diagram.</refpurpose>
  </refnamediv>



  <refsynopsisdiv>
    <title>Calling Sequence</title>
    <synopsis>
      [TI,FI]=midpoint(T1,F1,T2,F2,K)
    </synopsis>
  </refsynopsisdiv>

  <refsection>
    <title>Parameters</title>
    <variablelist>
      <varlistentry>
        <term>	T1 :</term>
        <listitem>
          <para>a real vector of size N: the time-coordinate(s) of the first point(s)</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>	F1 :</term>
        <listitem>
          <para>a real vector of size N with positive elements: the frequency-coordinate(s) of the first point(s)</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>	T2 :</term>
        <listitem>
          <para>a real vector of size N: the  time-coordinate(s) of the second point(s)</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>	F2 :</term>
        <listitem>
          <para>a real vector of size N with positive elements: the frequency-coordinate(s) of the second point(s)</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>	K  :</term>
        <listitem>
          <para>a real scalar: the power of the group-delay law</para>
          <itemizedlist>
            <listitem>
              <para>K = 2    :Wigner-Ville</para>
            </listitem>
            <listitem>
              <para>K = 1/2  :D-Flandrin</para>
            </listitem>
            <listitem>
              <para>K = 0:Bertrand (unitary)</para>
            </listitem>
            <listitem>
              <para>K = -1   :Unterberger (active)</para>
            </listitem>
            <listitem>
              <para>K = %inf :Margenau-Hill-Rihaczek</para>
            </listitem>
          </itemizedlist>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>	TI :</term>
        <listitem>
          <para>a real row vector of size N: the time-coordinate(s) of the interference term(s)</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>	FI :</term>
        <listitem>
          <para>a real row vector of size N:  frequency-coordinate(s) of the interference term(s)</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>

  <refsection>
    <title>Description</title>
    <para>
      midpoint  gives the coordinates in the
      time-frequency plane of the interference-term corresponding to
      the points (T1,F1) and (T2,F2), for a distribution in the
      affine class perfectly localized on power-law group-delays of
      the form : tx(nu)=t0+c nu^(K-1).
    </para>
  </refsection>

  <refsection>
    <title>See also</title>
    <simplelist type="inline">
      <member><link linkend="plotsid">plotsid</link></member>
    </simplelist>
  </refsection>

  <refsection>
    <title>Authors</title>
    <simplelist type="vert">
      <member>H. Nahrstaedt - Aug 2010</member>
      <member>	P. Flandrin, September 1995 - F. Auger, April 1996.</member>
    </simplelist>
  </refsection>
</refentry>
