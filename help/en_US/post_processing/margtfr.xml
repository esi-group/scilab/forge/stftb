<?xml version="1.0" encoding="UTF-8"?>
<!--
This file is part of the Scilab time frequency toolbox.
This file must be used under the terms of the  terms of the GNU General Public License 
as published by the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

Copyright (c) 1996 by CNRS (France).
-->
<refentry xml:id="margtfr" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:scilab="http://www.scilab.org"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">

  <refnamediv>
    <refname>margtfr</refname><refpurpose>Marginals and energy of a time-frequency representation.</refpurpose>
  </refnamediv>



  <refsynopsisdiv>
    <title>Calling Sequence</title>
    <synopsis>
      [MARGT,MARGF,E]=margtfr(TFR,T,F)
    </synopsis>
  </refsynopsisdiv>

  <refsection>
    <title>Parameters</title>
    <variablelist>
      <varlistentry>
        <term>	TFR :</term>
        <listitem>
          <para>a M by N complex array: the  time-frequency representation.</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>	T   :</term>
        <listitem>
          <para>a real vector of size N: the time samples in sec.    (default : (1:N))</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>	F   :</term>
        <listitem><para>a real  vector of size M: the frequency samples in Hz, not   necessary uniformly sampled. (default : (1:M))</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>	MARGT :</term>
        <listitem>
          <para>a real column vector of size N: then time marginal</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>	MARGF :</term>
        <listitem>
          <para>a real colum vectorof size M: the frequency marginal</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>	E     :</term>
        <listitem>
          <para>a positive scalar: the energy of TFR</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>

  <refsection>
    <title>Description</title>
    <para>
      margtfr calculates the time and frequency marginals and the
      energy of a time-frequency representation.
    </para>
  </refsection>

  <refsection>
    <title>Examples</title>
    <programlisting role="example"><![CDATA[
    S=altes(128,0.05,0.45); 
    [TFR,T,F]=tfrscalo(S,1:128,8,0.05,0.3,128);
    [MARGT,MARGF,E] = margtfr(TFR);
    clf
    subplot(211); plot(T,MARGT); 
    subplot(212); plot(F,MARGF);
    ]]></programlisting>
    <scilab:image><![CDATA[ ]]>
    S=altes(128,0.05,0.45); 
    [TFR,T,F]=tfrscalo(S,1:128,8,0.05,0.3,128);
    [MARGT,MARGF,E] = margtfr(TFR);
    clf
    subplot(211); plot(T,MARGT); 
    subplot(212); plot(F,MARGF);
    </scilab:image>
  </refsection>

  <refsection>
    <title>See also</title>
    <simplelist type="inline">
      <member><link linkend="momttfr">momttfr</link></member>
    </simplelist>
  </refsection>

  <refsection>
    <title>Authors</title>
    <simplelist type="vert">
      <member>H. Nahrstaedt - Aug 2010</member>
      <member>	P. Goncalves, October 95</member>
      <member>	Copyright (c) 1995 Rice University</member>
    </simplelist>
  </refsection>
</refentry>
