<?xml version="1.0" encoding="UTF-8"?>
<!--
This file is part of the Scilab time frequency toolbox.
This file must be used under the terms of the  terms of the GNU General Public License 
as published by the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

Copyright (c) 1996 by CNRS (France).
-->


<refentry xml:id="tfrrstan" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:scilab="http://www.scilab.org"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">

  <refnamediv>
    <refname>tfrrstan</refname><refpurpose>Reassigned Stankovic distribution.</refpurpose>
  </refnamediv>



  <refsynopsisdiv>
    <title>Calling Sequence</title>
    <synopsis>
      [TFR,RTFR,HAT] = tfrrstan(X)
      [TFR,RTFR,HAT] = tfrrstan(X,T)
      [TFR,RTFR,HAT] = tfrrstan(X,T,N)
      [TFR,RTFR,HAT] = tfrrstan(X,T,N,G)

      [TFR,RTFR,HAT] = tfrrstan(X,T,N,G,H)
      [TFR,RTFR,HAT] = tfrrstan(X,T,N,G,H,TRACE)
      [TFR,RTFR,HAT] = tfrrstan(...,'plot')
    </synopsis>
  </refsynopsisdiv>

  <refsection>
    <title>Parameters</title>
    <variablelist>
      <varlistentry>
        <term>X :</term>
        <listitem>
          <para> A Nx elements vector: the analyzed signal .</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term> T:</term> 
        <listitem>
          <para> a real Nt vector with elements regularly spaced in [1 Nx] : time instant(s)
          (default: 1:NX).</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term> N:</term> 
        <listitem><para> a positive integer: the number of frequency bins
        (default:NX). For faster computation N should be a power of
        2.</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term> G:</term> 
        <listitem>
          <para> a real vector with odd length: the frequency averaging window (default :[0.25 0.5 0.25]).</para>
          
        </listitem>
      </varlistentry>
      
      <varlistentry>
        <term> H :</term> 
        <listitem>
          <para> real vector with odd length: the stft smoothing window,(default: Hamming(N/4)).</para>
          
        </listitem>
      </varlistentry>
      <varlistentry>
        <term> TRACE :</term> 
        <listitem>
          <para>A boolean (or a real scalar) if true (or nonzero),the
          progression of the algorithm is shown (default : %f).</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>'plot':</term> 
        <listitem>
          <para> if one input parameter is 'plot', <link
          linkend="tfrqview">tfrqview</link> is called and the
          time-frequency representation will be plotted.</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term> TFR :</term> 
        <listitem>
          <para> A real N by Nt array: the time-frequency representation.</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term> RTFR :</term> 
        <listitem>
          <para> A real N by Nt array: the reassigned time-frequency
          representation.</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>HAT   :</term>
        <listitem>
          <para> A complex N by Nt array: the reassignment
          vectors.</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>

  <refsection>
    <title>Description</title>
    <para>
      tfrrstan computes the Stankovic distribution and its reassigned
      version.
    </para>
  </refsection>

  <refsection>
    <title>Examples</title>
    <programlisting role="example"><![CDATA[
    N=128;
    sig=fmlin(N,0.1,0.4);
    g=window('hn',9); 
    h=window("hn",61); 
    t=1:2:128;
    tfr=tfrrstan(sig,t,N,g,h);
    f=(0.5*(0:N-1)/N)';
    clf;gcf().color_map= jetcolormap(128);
    grayplot(t,f,tfr');
    ]]></programlisting>
    <scilab:image><![CDATA[
    N=128;
    sig=fmlin(N,0.1,0.4);
    g=window('hn',9); 
    h=window("hn",61); 
    t=1:2:128;
    tfr=tfrrstan(sig,t,N,g,h);
    f=(0.5*(0:N-1)/N)';
    clf;gcf().color_map= jetcolormap(128);
    grayplot(t,f,tfr');
    ]]></scilab:image>
  </refsection>

  <refsection>
    <title>Authors</title>
    <simplelist type="vert">
      <member>H. Nahrstaedt - Aug 2010</member>
      <member>	F. Auger, August, September 1997.</member>
    </simplelist>
  </refsection>
</refentry>
