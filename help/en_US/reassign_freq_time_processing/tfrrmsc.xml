<?xml version="1.0" encoding="UTF-8"?>
<!--
This file is part of the Scilab time frequency toolbox.
This file must be used under the terms of the  terms of the GNU General Public License 
as published by the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

Copyright (c) 1996 by CNRS (France).
-->


<refentry xml:id="tfrrmsc" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:scilab="http://www.scilab.org"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">

  <refnamediv>
    <refname>tfrrmsc</refname><refpurpose>Reassigned Morlet Scalogram time-frequency distribution.</refpurpose>
  </refnamediv>



  <refsynopsisdiv>
    <title>Calling Sequence</title>
    <synopsis>
      [TFR,RTFR,HAT] = tfrrmsc(X)
      [TFR,RTFR,HAT] = tfrrmsc(X,T)
      [TFR,RTFR,HAT] = tfrrmsc(X,T,N)
      [TFR,RTFR,HAT] = tfrrmsc(X,T,N,F0T)
      [TFR,RTFR,HAT] = tfrrmsc(X,T,N,F0T,TRACE)
      [TFR,RTFR,HAT] = tfrrmsc(...,'plot')
    </synopsis>
  </refsynopsisdiv>

  <refsection>
    <title>Parameters</title>
    <variablelist>
      <varlistentry>
        <term>	X     :</term>
        <listitem>
          <para> A Nx elements vector or a Nx by 2 array signal.</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>	T     :</term>
        <listitem>
          <para> the time instant(s)  with elements in [1 Nx] (default : 1:length(X))</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>	N     :</term>
        <listitem>
          <para> number of frequency bins (default : length(X)). For
          faster computation N should be a power of 2.</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>	F0T   :</term>
        <listitem>
          <para>a positive scalar: the time-bandwidth product of the
          mother wavelet (default : 2.5))</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>	TRACE :</term>
        <listitem>
          <para>A boolean (or a real scalar) if true (or nonzero),the
          progression of the algorithm is shown (default : %f).</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>'plot':</term>
        <listitem>
          <para> if one input parameter is 'plot', <link
       linkend="tfrqview">tfrqview</link> is called and the
       time-frequency representation will be plotted.</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>	TFR  :</term>
        <listitem>
          <para>a N by Nt array: the time-frequency representation.</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>	RTFR  :</term>
        <listitem>
          <para>A real N by Nt array: the reassigned time-frequency representation.</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>	HAT   :</term>
        <listitem>
          <para>a N by Nt complex matrix: the reassignment vectors.</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>

  <refsection>
    <title>Description</title>
    <para>
      tfrrmsc computes the Morlet scalogram and its reassigned version.
    </para>
  </refsection>

  <refsection>
    <title>Examples</title>
    <programlisting role="example"><![CDATA[
    N=128;
    sig=fmlin(N,0.1,0.4);
    t=1:128;
   
    [tfr,rtfr,hat]=tfrrmsc(sig,t,N,2.1);
    clf;gcf().color_map= jetcolormap(128);
    subplot(121)
    grayplot(t,linspace(0,0.25,N/2),tfr(1:N/2,:)')
    title("TFR")
    subplot(122)
    grayplot(t,linspace(0,0.25,N/2),rtfr(1:N/2,:)')
    title("RTFR")
    ]]></programlisting>
    <scilab:image><![CDATA[
    N=128;
    sig=fmlin(N,0.1,0.4);
    t=1:128;
   
    [tfr,rtfr,hat]=tfrrmsc(sig,t,N,2.1);
    clf;gcf().color_map= jetcolormap(128);
    subplot(121)
    grayplot(t,linspace(0,0.25,N/2),tfr(1:N/2,:)')
    title("TFR")
    subplot(122)
    grayplot(t,linspace(0,0.25,N/2),tfr(1:N/2,:)')
    title("RTFR")
    ]]></scilab:image>
  </refsection>

  <refsection>
    <title>Authors</title>
    <simplelist type="vert">
      <member>H. Nahrstaedt - Aug 2010</member>
      <member>	F. Auger, January, April 1996.</member>
      <member>	Copyright (c) 1996 by CNRS (France).</member>
    </simplelist>
  </refsection>
</refentry>
