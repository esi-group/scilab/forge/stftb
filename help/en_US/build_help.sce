function builder_help()
  smd=getscilabmode()
  if and(smd <> ["STD" "NW"]) then
    error(msprintf(gettext("%s: documentation cannot be built in this scilab mode: %s.\n"),...
                   "build_help.sce",smd));
  end

  path=get_absolute_file_path("build_help.sce");
  srcfiles=[path+"date_build"
            ls(path+"*.xml")
            path+"build_help.sce"];
  for d=["ambiguity/" "amplitude/" "bilin_freq_time_processing_affine/" ..
         "bilin_freq_time_processing_cohen/" "C_ambiguity/" "C_miscellanaeous/" ...
         "C_time_freq/" "freq_domain_processing/" "frequency/" ..
         "lin_freq_time_processing/" "modification/" "noise/" "other/" ..
         "post_processing/" "reassign_freq_time_processing/" "signals/" ..
         "time_domain_processing/" "visualization/" ]
     srcfiles=[srcfiles;
            ls(path+d+"*.xml")];
  end
  if newest(srcfiles)==1 then return;end
  xmltojar(path,TOOLBOX_TITLE,"en_US");
  mputl(sci2exp(getdate()),path+"date_build")  
endfunction
builder_help()
clear builder_help;
