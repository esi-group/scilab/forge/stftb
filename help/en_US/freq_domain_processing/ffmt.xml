<?xml version="1.0" encoding="UTF-8"?>
<!--
This file is part of the Scilab time frequency toolbox.
This file must be used under the terms of the  terms of the GNU General Public License 
as published by the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

Copyright (c) 1995 Rice University - CNRS (France) 1996.
-->


<refentry xml:id="ffmt" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:scilab="http://www.scilab.org"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">

  <refnamediv>
    <refname>ffmt</refname><refpurpose>Fast Fourier Mellin Transform.</refpurpose>
  </refnamediv>



  <refsynopsisdiv>
    <title>Calling Sequence</title>
    <synopsis>
      [MELLIN,BETA]=ffmt(X)
      [MELLIN,BETA]=ffmt(X,FMIN,FMAX)
      [MELLIN,BETA]=ffmt(X,FMIN,FMAX,N)
    </synopsis>
  </refsynopsisdiv>

  <refsection>
    <title>Parameters</title>
    <variablelist>
      <varlistentry>
        <term>X :</term>
        <listitem>
          <para>a real vector of size Nx: the signal in time.</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term> FMIN:</term> 
        <listitem>
          <para>
            a positive scalar in ]0 0.5], the normalized lower
            frequency bound in (Hz) of the analyzed signal. When
            unspecified, you have to enter it at the command line from
          the plot of the spectrum. </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term> FMAX :</term> 
        <listitem>
          <para> a positive scalar in ]0 0.5], the normalized upper
          frequency bound (in Hz) of the analyzed signal.  When
          unspecified, you have to enter it at the command line from
          the plot of the spectrum.
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>N :</term> 
        <listitem>
          <para>a positive integer: number of analyzed voices.  When
          unspecified, you have to enter it at the command line.</para>
        </listitem>
      </varlistentry>


      <varlistentry>
        <term>MELLIN :</term>
        <listitem>
          <para>a complex row vector of size N: the N-points Mellin transform of signal X.</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>BETA :</term>
        <listitem>
          <para>a real row vector of size N: the N-points Mellin variable.</para>
          <para>The increment between two consecutive elements is constant.</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>

  <refsection>
    <title>Description</title>
    <para>
      ffmt computes the Fast Mellin  Transform of signal X.
    </para>
  </refsection>

  <refsection>
    <title>Examples</title>
    <programlisting role="example"><![CDATA[
    sig=altes(128,0.05,0.45);
    [MELLIN,BETA]=ffmt(sig,0.05,0.5,148);
    clf;
    subplot(211); plot(sig)
    subplot(212);plot(BETA,real(MELLIN));
    ]]></programlisting>
    <scilab:image><![CDATA[
    sig=altes(128,0.05,0.45);
    [MELLIN,BETA]=ffmt(sig,0.05,0.5,148);
    clf;
    subplot(211); plot(sig)
    subplot(212);plot(BETA,real(MELLIN));
    ]]></scilab:image>
  </refsection>

  <refsection>
    <title>See also</title>
    <simplelist type="inline">
      <member><link linkend="iffmt">iffmt</link></member>
    </simplelist>
  </refsection>

  <refsection>
    <title>Authors</title>
    <simplelist type="vert">
      <member>H. Nahrstaedt - Aug 2010</member>
      <member>P. Goncalves 9-95 - O. Lemoine, June 1996.</member>
    </simplelist>
  </refsection>
</refentry>
