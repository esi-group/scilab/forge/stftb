<?xml version="1.0" encoding="UTF-8"?>
<!--
This file is part of the Scilab time frequency toolbox.
This file must be used under the terms of the  terms of the GNU General Public License 
as published by the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

Copyright (c) 1996 by CNRS (France).
-->



<refentry xml:id="tfrbj" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:scilab="http://www.scilab.org"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">


  <refnamediv>
    <refname>tfrbj</refname><refpurpose>Born-Jordan time-frequency distribution.</refpurpose>
  </refnamediv>



<refsynopsisdiv>
   <title>Calling Sequence</title>
   <synopsis>
   	[TFR,T,F]=tfrbj(X)
   	[TFR,T,F]=tfrbj(X,T)
   	[TFR,T,F]=tfrbj(X,T,N)
   	[TFR,T,F]=tfrbj(X,T,N,G)
   	[TFR,T,F]=tfrbj(X,T,N,G,H)
   	[TFR,T,F]=tfrbj(X,T,N,G,H,TRACE)
   	[TFR,T,F]=tfrbj(...,'plot')
   </synopsis>
</refsynopsisdiv>
<refsection>
   <title>Parameters</title>
   <variablelist>
   <varlistentry>
     <term>X :</term>
     <listitem>
       <para> A Nx elements vector (auto-BJ)  or a Nx by 2 array signal (cross-BJ).</para>
     </listitem>
   </varlistentry>

   <varlistentry>
     <term> T:</term> 
     <listitem>
       <para> a real Nt vector with elements in [1 Nx] : time instant(s)
        (default: 1:NX).</para>
     </listitem>
   </varlistentry>

   <varlistentry>
     <term> N:</term> 
     <listitem><para> a positive integer: the number of frequency bins
     (default:NX). For faster computation N should be a power of
     2.</para>
   </listitem>
   </varlistentry>

   <varlistentry>
     <term> G:</term> 
     <listitem>
       <para> a real vector with odd length: the time smoothing window,  (default :Hamming(N/10)).</para>
       <para>It will be normalized such as the middle point equals 1 to preserve signal energy .</para>
   </listitem>
   </varlistentry>

   <varlistentry>
     <term> H :</term> 
     <listitem>
       <para> real vector with odd length: the frequency smoothing window,(default: Hamming(N/4)).</para>
       <para>It will be normalized such as the middle point equals 1 to preserve signal energy.</para>
   </listitem>
   </varlistentry>

   <varlistentry>
     <term> TRACE :</term> 
     <listitem>
        <para>A boolean (or a real scalar) if true (or nonzero),the
        progression of the algorithm is shown (default : %f).</para>
     </listitem>
   </varlistentry>

   <varlistentry>
     <term>'plot':</term> 
     <listitem>
       <para> if one input parameter is 'plot', <link
       linkend="tfrqview">tfrqview</link> is called and the
       time-frequency representation will be plotted.</para>
     </listitem>
   </varlistentry>

   <varlistentry>
     <term> TFR :</term> 
     <listitem>
       <para> A real N by Nt array: the time-frequency representation.</para>
     </listitem>
   </varlistentry>

   <varlistentry>
     <term> F :</term> 
     <listitem>
       <para> A N vector of normalized frequencies.</para>
     </listitem>
   </varlistentry>
   </variablelist>
</refsection>


<refsection>
   <title>Description</title>
   <para>
tfrbj computes the Born-Jordan distribution of a discrete-time signal
X, or the cross Born-Jordan representation between two signals.
</para>
</refsection>

<refsection>
   <title>Examples</title>
<para> Interactive use </para>
   <programlisting role="example"><![CDATA[
N=128;
sig=fmlin(N,0.05,0.3)+fmlin(N,0.15,0.4);
g=window("kr",9,3*%pi); h=window("kr",27,3*%pi);
t=1:N; 
tfrbj(sig,t,N,g',h',1,'plot');
   ]]></programlisting>
 <para>Non interactive use</para>

   <programlisting role="example"><![CDATA[
N=128;
sig=fmlin(N,0.05,0.3)+fmlin(N,0.15,0.4);
g=window("kr",9,3*%pi); h=window("kr",27,3*%pi);
t=1:N; 
[TFR,T,F]=tfrbj(sig,t,N,g,h);
clf;gcf().color_map= jetcolormap(128);
grayplot(T,F,TFR');
   ]]></programlisting>
  <scilab:image><![CDATA[
N=128;
sig=fmlin(N,0.05,0.3)+fmlin(N,0.15,0.4);
g=window("kr",9,3*%pi); h=window("kr",27,3*%pi);
t=1:N; 
[TFR,T,F]=tfrbj(sig,t,N,g,h);
clf;gcf().color_map= jetcolormap(128);
grayplot(T,F,TFR');
    ]]></scilab:image>
</refsection>

<refsection>
   <title>Authors</title>
   <simplelist type="vert">
   <member>H. Nahrstaedt - Aug 2010	</member>
   <member>	F. Auger, May-August 1994, July 1995.</member>
   <member>	</member>
   </simplelist>
</refsection>
</refentry>
