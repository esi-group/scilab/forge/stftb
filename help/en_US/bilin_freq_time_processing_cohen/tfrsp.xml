<?xml version="1.0" encoding="UTF-8"?>
<!--
This file is part of the Scilab time frequency toolbox
This file must be used under the terms of the CeCILL.
This source file is licensed as described in the file COPYING, which
you should have received as part of this distribution.  The terms
are also available at
http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
-->


<refentry xml:id="tfrsp" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:scilab="http://www.scilab.org"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">

  <refnamediv>
    <refname>tfrsp</refname><refpurpose>Spectrogram time-frequency
    distribution.</refpurpose>
  </refnamediv>



<refsynopsisdiv>
   <title>Calling Sequence</title>
   <synopsis>
   	[TFR,T,F]=tfrsp(X)
   	[TFR,T,F]=tfrsp(X,T)
   	[TFR,T,F]=tfrsp(X,T,N)
   	[TFR,T,F]=tfrsp(X,T,N,H)
   	[TFR,T,F]=tfrsp(X,T,N,H,TRACE)
   	[TFR,T,F]=tfrsp(...,'plot')
   </synopsis>
</refsynopsisdiv>

<refsection>
   <title>Description</title>
   <para>
tfrsp computes the Spectrogram distribution of a discrete-time signal
X.
</para>
</refsection>
<refsection>
   <title>Parameters</title>
   <variablelist>
   <varlistentry>
     <term>X :</term>
     <listitem>
       <para> A Nx elements vector (auto-SP)  or a Nx by 2 array signal (cross-SP).</para>
     </listitem>
   </varlistentry>

   <varlistentry>
     <term> T:</term> 
     <listitem>
        <para>a real Nt vector with elements in [1 Nx] : time instant(s)
        (default: 1:NX).</para>
     </listitem>
   </varlistentry>

   <varlistentry>
     <term> N:</term> 
     <listitem><para> a positive integer: the number of frequency bins
     (default:NX). For faster computation N should be a power of
     2.</para>
   </listitem>
   </varlistentry>

   <varlistentry>
     <term> H:</term> 
     <listitem>
       <para> a real vector with odd length: the analysis window, H
       being normalized so as to be of unit energy.  (default :
       Hamming(N/4)).</para>
   </listitem>
   </varlistentry>

 <varlistentry>
     <term> TRACE :</term> 
     <listitem>
       <para> if nonzero,the progression of the algorithm is shown
       (default : 0).</para>
     </listitem>
   </varlistentry>

   <varlistentry>
     <term>'plot':</term> <listitem><para> if the last input parameter value is
     'plot', <link linkend="tfrqview">tfrqview</link> is called and
     the time-frequency representation will be plotted.</para>
     </listitem>
   </varlistentry>
   <varlistentry>
     <term> TFR :</term> 
     <listitem>
       <para> A real N by Nt array: the time-frequency representation.</para>
     </listitem>
   </varlistentry>
   <varlistentry>
     <term> F :</term> 
     <listitem>
       <para> A N vector of normalized frequencies.</para>
     </listitem>
   </varlistentry>
   </variablelist>
</refsection>


<refsection>
   <title>Examples</title>
   <programlisting role="example"><![CDATA[
N=128;
sig=fmlin(N,0.1,0.4);
h=window("kr",17,3*%pi); 

[TFR,T,F]=tfrsp(sig,1:N,N/2,h);
clf;gcf().color_map= jetcolormap(128);
subplot(121);Sgrayplot(T,F(1:$/2),TFR(1:$/2,:)');
subplot(122);plot(fftshift(F'),fftshift(TFR(:,100)))
  ]]></programlisting>
  <scilab:image><![CDATA[
N=128;
sig=fmlin(N,0.1,0.4);
h=window("kr",17,3*%pi); 

[TFR,T,F]=tfrsp(sig,1:N,N/2,h);
clf;gcf().color_map= jetcolormap(128);
subplot(121);Sgrayplot(T,F(1:$/2),TFR(1:$/2,:)');
subplot(122);plot(fftshift(F'),fftshift(TFR(:,100)))
    ]]></scilab:image>
</refsection>

<refsection>
   <title>Authors</title>
   <simplelist type="vert">
   <member>H. Nahrstaedt - Aug 2010</member>
   <member>	F. Auger, May-August 1994, July 1995.</member>
   <member>	Copyright (c) 1996 by CNRS (France).</member>
   </simplelist>
</refsection>
</refentry>
