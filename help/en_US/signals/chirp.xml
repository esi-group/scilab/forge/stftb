<?xml version="1.0" encoding="UTF-8"?>
<!--
This file is part of the Scilab time frequency toolbox.
This file must be used under the terms of the  terms of the GNU General Public License 
as published by the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.


-->

<refentry  xml:id="chirp" xml:lang="en"
           xmlns="http://docbook.org/ns/docbook"
           xmlns:xlink="http://www.w3.org/1999/xlink"
           xmlns:svg="http://www.w3.org/2000/svg"
           xmlns:scilab="http://www.scilab.org"
           xmlns:ns3="http://www.w3.org/1999/xhtml"
           xmlns:mml="http://www.w3.org/1998/Math/MathML"
           xmlns:db="http://docbook.org/ns/docbook">

  <refnamediv>
    <refname>chirp</refname><refpurpose>Evaluate a chirp signal at time t.  A chirp signal is a frequency swept cosine wave.</refpurpose>
  </refnamediv>



  <refsynopsisdiv>
    <title>Calling Sequence</title>
    <synopsis>
      y = chirp(t)
      y = chirp(t,f0)
      y = chirp(t,f0,t1)
      y = chirp(t,f0,t1,f1)
      y = chirp(t,f0,t1,f1,form)
      y = chirp(t,f0,t1,f1,form,phase)
    </synopsis>
  </refsynopsisdiv>

  <refsection>
    <title>Parameters</title>
    <variablelist>
      <varlistentry>
        <term>t:</term>
        <listitem>
          <para>real vector of times (in seconds) to evaluate the chirp signal</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>f0:</term>
        <listitem>
          <para>a positive scalar: the frequency (in Hz) at time t=0 (default: 0). If set to [] the default value is used.</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>t1:</term>
        <listitem>
          <para>a positive scalar: the  time (in seconds) t1 (default: 1). If set to [] the default value is used.</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>f1:</term>
        <listitem>
          <para>a positive scalar:  frequency (in Hz) at time t=t1 (default: 100). If set to [] the default value is used.</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>form:</term>
        <listitem>
          <para>a character string: the shape of frequency sweep;</para>
          <itemizedlist>
            <listitem>
              <para>'linear' :     f(t) = (f1-f0)*(t/t1) + f0,</para>
            </listitem>
            <listitem>
              <para>'quadratic':   f(t) = (f1-f0)*(t/t1)^2 + f0,</para>
            </listitem>
            <listitem>
              <para>'logarithmic': f(t) = (f1-f0)^(t/t1) + f0</para>
            </listitem>
          </itemizedlist>
          <para>The default value is "linear". If set to [] the default value is used.</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>phase:</term>
        <listitem>
          <para>a real scalar: the phase shift (in degree) at t=0 (default:
          0). If set to [] the default value is used.</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>

  <refsection>
    <title>Description</title>
    <para>
      If you want a different sweep shape f(t), use the following:
      y = cos(2*%pi*integral(f(t)) + 2*%pi*f0*t + phase);
    </para>
  </refsection>

  <refsection>
    <title>Examples</title>
    <programlisting role="example"><![CDATA[
    clf;gcf().color_map= jetcolormap(128);
    //Linear
    subplot(221);
    [TFR,T,F]=tfrsp(chirp([0:0.002:5])',1:2:2501,128);
	subTFR = linspace(1, size(TFR, 'c'), 100);
    grayplot(T(subTFR),F(1:$/2),TFR(1:$/2,subTFR)');
    xtitle("linear, 0-100Hz in 1 sec")

    //Quadratic
    subplot(222);
    [TFR,T,F]=tfrsp(chirp([-2:0.001:15], 400, 10, 100, 'quadratic')',1:2:5001,128);
	subTFR = linspace(1, size(TFR, 'c'), 100);
    grayplot(T(subTFR),F(1:$/2),TFR(1:$/2,subTFR)');
    xtitle("quadratic, 400 Hz at t=0 and 100 Hz at t=10")

    //Logarithmic
    subplot(223);
    [TFR,T,F]=tfrsp(chirp([0:1/8000:5], 200, 2, 500, "logarithmic")',1:20:40001,128);
	subTFR = linspace(1, size(TFR, 'c'), 100);
    grayplot(T(subTFR),F(1:$/2),TFR(1:$/2,subTFR)');
    xtitle("logaritmic, 200 Hz at t=0 and 500 Hz at t=2")

    ]]></programlisting>
    <scilab:image><![CDATA[
    clf;gcf().color_map= jetcolormap(128);
    //Linear
    subplot(221);
    [TFR,T,F]=tfrsp(chirp([0:0.002:5])',1:2:2501,128);
	subTFR = linspace(1, size(TFR, 'c'), 100);
    grayplot(T(subTFR),F(1:$/2),TFR(1:$/2,subTFR)');
    xtitle("linear, 0-100Hz in 1 sec")

    //Quadratic
    subplot(222);
    [TFR,T,F]=tfrsp(chirp([-2:0.001:15], 400, 10, 100, 'quadratic')',1:2:5001,128);
	subTFR = linspace(1, size(TFR, 'c'), 100);
    grayplot(T(subTFR),F(1:$/2),TFR(1:$/2,subTFR)');
    xtitle("quadratic, 400 Hz at t=0 and 100 Hz at t=10")

    //Logarithmic
    subplot(223);
    [TFR,T,F]=tfrsp(chirp([0:1/8000:5], 200, 2, 500, "logarithmic")',1:20:40001,128);
	subTFR = linspace(1, size(TFR, 'c'), 100);
    grayplot(T(subTFR),F(1:$/2),TFR(1:$/2,subTFR)');
    xtitle("logaritmic, 200 Hz at t=0 and 500 Hz at t=2")

   ]]></scilab:image>
  </refsection>

  <refsection>
    <title>Authors</title>
    <simplelist type="vert">
      <member>2001-08-31 Paul Kienzle pkienzle@users.sf.net</member>
    </simplelist>
  </refsection>
</refentry>
