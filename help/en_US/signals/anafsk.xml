<?xml version="1.0" encoding="UTF-8"?>
<!--
This file is part of the Scilab time frequency toolbox.
This file must be used under the terms of the  terms of the GNU General Public License 
as published by the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.
Copyright (c) 1996 by CNRS (France).
-->

<refentry xml:id="anafsk" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:scilab="http://www.scilab.org"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">


  <refnamediv>
    <refname>anafsk</refname><refpurpose>Frequency Shift Keying (FSK) signal.</refpurpose>
  </refnamediv>



<refsynopsisdiv>
   <title>Calling Sequence</title>
   <synopsis>
   	[Y,IFLAW]=ANAFSK(N,NCOMP,NBF)
   </synopsis>
</refsynopsisdiv>
 <refsection>
    <title>Parameters</title>
    <variablelist>
      <varlistentry>
        <term>	N: </term>
        <listitem>
          <para> a positive integer: the signal length.</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>	NCOMP :</term>
        <listitem>
          <para> number of points of each component (default: N/5)</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>NBF:</term>
        <listitem>
          <para>a positive integer: the number of distinct frequencies (default: 4).</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>	Y:</term>
        <listitem>
          <para>a complex column vector of length N: the signal</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>IFLAW:</term>
        <listitem>
          <para>a real column vector of length N: the resulting
          instantaneous frequency.</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>

<refsection>
   <title>Description</title>
   <para>
simulates a phase coherent
	Frequency Shift Keying (FSK) signal. This signal is a succession
	of complex sinusoids of NCOMP points each and with a normalized
	frequency uniformly chosen between NBF distinct values between
	0.0 and 0.5.
	Such signal is only 'quasi'-analytic.
</para>
</refsection>


<refsection>
   <title>Examples</title>
   <programlisting role="example"><![CDATA[
   [signal,ifl]=anafsk(512,64,5); 
   clf; 
   subplot(211); plot(real(signal)); 
   subplot(212); plot(ifl);
   ]]></programlisting>
   <scilab:image><![CDATA[
   rand("seed",0);
   [signal,ifl]=anafsk(512,64,5); 
   clf; 
   subplot(211); plot(real(signal)); xtitle(_("Signal real part"))
   subplot(212); plot(ifl);xtitle(_("Signal instantaneous frequency"))
    ]]></scilab:image>
</refsection>

<refsection>
   <title>See also</title>
   <simplelist type="inline">
   <member><link linkend="anabpsk">anabpsk</link></member>
   <member><link linkend="anaqpsk">anaqpsk</link></member>
   <member><link linkend="anaask">anaask</link></member>
   </simplelist>
</refsection>

<refsection>
   <title>Authors</title>
   <simplelist type="vert">
   <member>	O. Lemoine - June 1995, F. Auger - August 1995.</member>
   <member>	Copyright (c) 1996 by CNRS (France).</member>
   </simplelist>
</refsection>
</refentry>
