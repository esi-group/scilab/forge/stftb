<?xml version="1.0" encoding="UTF-8"?>
<!--
This file is part of the Scilab time frequency toolbox.
This file must be used under the terms of the  terms of the GNU General Public License 
as published by the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

Copyright (c) 1996 by CNRS (France).
-->


<refentry xml:id="tffilter" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:scilab="http://www.scilab.org"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">

  <refnamediv>
    <refname>tffilter</refname><refpurpose>Time frequency filtering of a signal.</refpurpose>
  </refnamediv>



  <refsynopsisdiv>
    <title>Calling Sequence</title>
    <synopsis>
      Y=tffilter(TFR,X,T)
      Y=tffilter(TFR,X,T,TRACE)
    </synopsis>
  </refsynopsisdiv>

  <refsection>
    <title>Parameters</title>
    <variablelist>
      <varlistentry>
        <term>	TFR   :</term>
        <listitem>
          <para>a M by N array: the Wigner-Ville distribution of the
          filter frequency axis is graduated from 0.0 to 0.5.</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>	X     :</term>
        <listitem>
          <para>a complex vector of size N: the input signal (must be analytic).</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>	T     :</term>
        <listitem>
          <para>a real vector with elements in [1 N]: the time instant(s)(default : 1:length(X)).</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>	TRACE :</term>
        <listitem>
          <para> if nonzero, the progression of the algorithm is shown  (default : 0).</para>
        </listitem>
      </varlistentry>
       <varlistentry>
        <term>Y:</term>
        <listitem>
          <para>a vector with same sizes as X: the filtered signal.</para>
        </listitem>
      </varlistentry>
      
    </variablelist>
  </refsection>

  <refsection>
    <title>Description</title>
    <para>
      tffilter filters the signal X with a non stationary filter.
    </para>
  </refsection>

  <refsection>
    <title>Examples</title>
    <programlisting role="example"><![CDATA[
    clf;gcf().color_map= jetcolormap(128);

    Nt=128; 
    t=1:Nt; 
    sig=fmlin(Nt,0.05,0.3)+fmlin(Nt,0.2,0.45);
    sig(Nt/2)=sig(Nt/2)+8; 
    [TFR,T,F]=tfrwv(sig,t);
    subplot(221);grayplot(T,F,TFR');

    Nf=128;freqs=0.5*(0:Nf-1).'/Nf;
    H=[];    
    for tloop=1:Nt,
      rate=0.2*(tloop-1)/(Nt-1);
      H(:,tloop)=(0+rate<freqs).*(freqs<0.1+rate);
    end;
    y=tffilter(H,sig,t); 
    [TFR,T,F]=tfrwv(y,t);
    subplot(222);grayplot(T,F,TFR');


    Nt=128; 
    t=1:Nt; 
    sig=atoms(128,[64 0.25 round(sqrt(128)) 1],0);
    [TFR,T,F]=tfrwv(sig,t);
    subplot(223);grayplot(T,F,TFR');

    Nf=64;
    H=zeros(Nf,Nt);H(Nf/4+(-15:15),Nt/2+(-15:15))=ones(31);
    y=tffilter(H,sig,t);
    [TFR,T,F]=tfrwv(y,t);
    subplot(224);grayplot(T,F,TFR');
    ]]></programlisting>
    <scilab:image><![CDATA[
    clf;gcf().color_map= jetcolormap(128);

    Nt=128; 
    t=1:Nt; 
    sig=fmlin(Nt,0.05,0.3)+fmlin(Nt,0.2,0.45);
    sig(Nt/2)=sig(Nt/2)+8; 
    [TFR,T,F]=tfrwv(sig,t);
    subplot(221);grayplot(T,F,TFR');

    Nf=128;freqs=0.5*(0:Nf-1).'/Nf;
    H=[];
    for tloop=1:Nt,
      rate=0.2*(tloop-1)/(Nt-1);
      H(:,tloop)=(0+rate<freqs).*(freqs<0.1+rate);
    end;
    y=tffilter(H,sig,t); 
    [TFR,T,F]=tfrwv(y,t);
    subplot(222);grayplot(T,F,TFR');


    Nt=128; 
    t=1:Nt; 
    sig=atoms(128,[64 0.25 round(sqrt(128)) 1],0);
    [TFR,T,F]=tfrwv(sig,t);
    subplot(223);grayplot(T,F,TFR');

    Nf=64;
    H=zeros(Nf,Nt);H(Nf/4+(-15:15),Nt/2+(-15:15))=ones(31);
    y=tffilter(H,sig,t);
    [TFR,T,F]=tfrwv(y,t);
    subplot(224);grayplot(T,F,TFR');
    ]]></scilab:image>
  </refsection>
  <refsection>
    <title>See also:</title>
    <link linkend="tfristft">tfristft</link>
  </refsection>
  <refsection>
    <title>Authors</title>
    <simplelist type="vert">
      <member>H. Nahrstaedt - Aug 2010</member>
      <member>F. Auger, Jan 1997.</member>
    </simplelist>
  </refsection>
</refentry>
