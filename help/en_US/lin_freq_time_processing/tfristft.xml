<?xml version="1.0" encoding="UTF-8"?>
<!--
This file is part of the Scilab time frequency toolbox.
This file must be used under the terms of the  terms of the GNU General Public License 
as published by the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

Copyright (c) 1996 by CNRS (France)
-->


<refentry xml:id="tfristft" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:scilab="http://www.scilab.org"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">

  <refnamediv>
    <refname>tfristft</refname><refpurpose>Inverse Short time Fourier transform.</refpurpose>
  </refnamediv>



  <refsynopsisdiv>
    <title>Calling Sequence</title>
    <synopsis>
      [X,T]=tfristft(tfr,T,H)
      [X,T]=tfristft(tfr,T,H,TRACE)
    </synopsis>
  </refsynopsisdiv>

  <refsection>
    <title>Parameters</title>
    <variablelist>
      <varlistentry>
        <term>tfr     :</term>
        <listitem>
          <para>>a N by M complex matrix: the time-frequency representation.</para>
        </listitem>
      </varlistentry>
   
      <varlistentry>
        <term>	T:</term>
        <listitem>
          <para>a vector with integer values and increments between
          elements equal to 1: the time instant(s)(default :
          1:length(X)).</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>	H     :</term>
        <listitem>
          <para>a real vector with odd size: the frequency smoothing window, H being normalized so as to be of unit energy.</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>	TRACE :</term>
        <listitem>
          <para> if nonzero of %t, the progression of the algorithm is shown      (default : %f).</para>
        </listitem>
      </varlistentry>
   <varlistentry>
        <term>	X     :</term>
        <listitem>
          <para>a column vector of size length(t): the signal with the specified time frequency representation.</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>

  <refsection>
    <title>Description</title>
    <para>
      tfristft computes the inverse short-time Fourier transform of a
      discrete-time signal X. This function may be used for
      time-frequency synthesis of signals.
    </para>
  </refsection>

  <refsection>
    <title>Examples</title>
    <programlisting role="example"><![CDATA[
    t=200+(-128:127); 
    sig=[fmconst(200,0.2);fmconst(200,0.4)];
    h=window("hm",57); 
    tfr=tfrstft(sig,t,256,h);
    sigsyn=tfristft(tfr,t,h);
    plot(t',abs(sigsyn-sig(t)))
    ]]></programlisting>
    <scilab:image><![CDATA[
    t=200+(-128:127); 
    sig=[fmconst(200,0.2);fmconst(200,0.4)];
    h=window("hm",57); 
    tfr=tfrstft(sig,t,256,h);
    sigsyn=tfristft(tfr,t,h);
    plot(t',abs(sigsyn-sig(t)))
    ]]></scilab:image>
  </refsection>

  <refsection>
    <title>Authors</title>
    <simplelist type="vert">
      <member>H. Nahrstaedt - Aug 2010</member>
      <member>	F. Auger, November 1996.</member>
    </simplelist>
  </refsection>
</refentry>
