<?xml version="1.0" encoding="UTF-8"?>
<!--
This file is part of the Scilab time frequency toolbox.
This file must be used under the terms of the  terms of the GNU General Public License 
as published by the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

Copyright (c) CNRS - France 1999.
-->


<refentry xml:id="imextrac" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:scilab="http://www.scilab.org"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">

  <refnamediv>
    <refname>imextrac</refname><refpurpose>imextrac(Image) extract and isolate dots in a binary image</refpurpose>
  </refnamediv>



  <refsynopsisdiv>
    <title>Calling Sequence</title>
    <synopsis>
      I=imextrac(Image)
      I=imextrac(Image,trace)
    </synopsis>
  </refsynopsisdiv>
  <refsection>
    <title>Parameters</title>
    <variablelist>
      <varlistentry>
        <term>Image :</term>
        <listitem>
          <para>a boolean array: then black and white image.</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term> TRACE :</term> 
        <listitem>
          <para>A boolean (or a real scalar) if true (or nonzero),the
          progression of the algorithm is shown (default : %f).</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>I:</term> 
        <listitem>
          <para>A matrix with same sizes as Image with integer elements: the resulting image with cluster indexed.</para>
        </listitem>
      </varlistentry>
     
    </variablelist>
  </refsection>
  <refsection>
    <title>Description</title>
    <para>
     imextrac extracts and isolates dots in a binary image.  it finds
     and indexes the cluster of true value. a cluster is a set of true
     values image(i,j) such image(i-1,j)==%t or image(i,j-1)==%t.
    </para>
  </refsection>

  <refsection>
    <title>Examples</title>
    <para>Small example</para>
    <programlisting role="example"><![CDATA[
    Image=[5 0 0 0 0 0 5 0 0 0 5 0 ;
           5 5 0 0 0 5 5 0 0 0 5 5 ;
           5 0 0 0 5 0 5 0 0 0 0 5 ;
           0 0 0 0 5 5 5 0 0 0 0 0 ;
           0 0 0 0 0 0 0 0 0 0 0 0 ];
           clf;
           subplot(211);Matplot(Image)
           I=imextrac(Image>0);
           subplot(212);Matplot(I)
           c_ind=unique(I(I>0)) //the cluster indices
    ]]></programlisting>
    <scilab:image><![CDATA[
    Image=[5 0 0 0 0 0 5 0 0 0 5 0 ;
           5 5 0 0 0 5 5 0 0 0 5 5 ;
           5 0 0 0 5 0 5 0 0 0 0 5 ;
           0 0 0 0 5 5 5 0 0 0 0 0 ;
           0 0 0 0 0 0 0 0 0 0 0 0 ];
           clf;
           subplot(211);Matplot(Image)
           I=imextrac(Image>0);
           subplot(212);Matplot(I)
           c_ind=unique(I(I>0)) //the cluster indices
     ]]></scilab:image>

    <para>Big ones</para>
    <programlisting role="example"><![CDATA[
    rand("seed",0)
    N=32;
    Image=10*rand(N,N);Image(Image<5)=0;
    clf;gcf().color_map=jetcolormap(150);
    Image(Image>0)=color("red");
    subplot(211);Matplot(Image)
    I=imextrac(Image>0);
    subplot(212);Matplot(I)
    ]]></programlisting>
    <scilab:image><![CDATA[
    rand("seed",0)
    N=32;
    Image=10*rand(N,N);Image(Image<5)=0;
    clf;gcf().color_map=jetcolormap(150);
    Image(Image>0)=color("red");
    subplot(211);grayplot(1:N,1:N,bool2s(Image))
    I=imextrac(Image>0);
    subplot(212);grayplot(1:N,1:N,I)
    ]]></scilab:image>
  </refsection>

  <refsection>
    <title>See also</title>
    <simplelist type="inline">
      <member><link linkend="tfrsurf">tfrsurf</link></member>
    </simplelist>
  </refsection>

  <refsection>
    <title>Authors</title>
    <simplelist type="vert">
      <member>H. Nahrstaedt - Aug 2010</member>
      <member>	F. Auger, oct 1999</member>
    </simplelist>
  </refsection>
</refentry>
