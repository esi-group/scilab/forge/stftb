function y = tffilter(tfr,x,t,ptrace);
// Time frequency filtering of a signal.
// Calling Sequence
//       Y=tffilter(TFR,X,T) 
//       Y=tffilter(TFR,X,T,TRACE) 
// Parameters
//	X     : input signal (must be analytic).
//	T     : time instant(s)          (default : 1:length(X)).
//	TRACE : if nonzero, the progression of the algorithm is shown  (default : 0).
//	TFR   : Wigner-Ville distribution of the filter  frequency axis is graduated from 0.0 to 0.5.
//    Description
//    tffilter filters the signal X with a non stationary filter. 
//    Examples
//        Nt=128; t=1:Nt; sig=fmlin(Nt,0.05,0.3)+fmlin(Nt,0.2,0.45); 
//        sig(Nt/2)=sig(Nt/2)+8; figure(1);tfrwv(sig,t);
//        Nf=128;freqs=0.5*(0:Nf-1).'/Nf; 
//        for tloop=1:Nt, 
//        rate=0.2*(tloop-1)/(Nt-1);
//        H(:,tloop)=(0+rate<freqs).*(freqs<0.1+rate); 
//        end;
//        y=tffilter(H,sig,t,1);figure(2); tfrwv(y,t);
//
//
//
//        Nt=128; t=1:Nt; sig=atoms(128,[64 0.25 sqrt(128) 1]);
//        figure(1);tfrwv(sig,t);
//        Nf=64;
//        H=zeros(Nf,Nt);H(Nf/4+(-15:15),Nt/2+(-15:15))=ones(31);
//        y=tffilter(H,sig,t,1);figure(2); tfrwv(y,t);
//    See also: 
//       tfristft
//   Authors
//      H. Nahrstaedt - Aug 2010
//	F. Auger, Jan 1997.

// this function is based on the following reference :
// W. Kozek, F. Hlawatsch, A comparative study of linear and non-linear
// time-frequency filters, proc IEEE int symp on time-frequency and 
// time-scale analysis, pp 163-166, victoria, canada, 1992.

//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  [nargout,nargin]=argn(0);
  fname="tffilter";
  if nargin <2 then
    error(msprintf(_("%s: Wrong number of input argument: %d to %d expected.\n"),fname,2,4));
  end;

  //ptrace
  if nargin==4 then 
    if and(type(ptrace)<>[1 4])|size(ptrace,"*")<>1 then
      error(msprintf(_("%s: Wrong type for argument #%d: A boolean or real scalar expected.\n"),fname,4));
    end
  else
    ptrace=%f;
  end

  //tfr
  if type(tfr)<>1 then
    error(msprintf(_("%s: Wrong type for input argument #%d: A matrix of double expected.\n"),fname,1));
  end
  [N,NbPoints]=size(tfr);
  
  
  //x
  if type(x)<>1|and(size(x)<>1) then
    error(msprintf(_("%s: Wrong type for input argument #%d: A vector of double expected.\n"),fname,2));
  end
  szx=size(x)
  x=x(:);
  xrow =size(x,1);
  if xrow<>NbPoints then
    error(msprintf(_("%s: Incompatible input arguments #%d and #%d.\n"),fname,1,2));
  end
  
  //t
  if nargin>=3 then
    if type(t)<>1|and(size(t)<>1)|~isreal(t) then
      error(msprintf(_("%s: Wrong type for input argument #%d: A real vector expected.\n"),fname,2));
    end
    if min(t)<1|max(t)>NbPoints then
      error(msprintf(_("%s: Wrong value for input argument #%d: Elements must be in the interval [%d, %d].\n"),fname,2,1 ,NbPoints))
    end
  else
    t=1:xrow
  end
  tcol=size(t,2);

  if ptrace, disp('non stationary signal filtering'); end;

  tfr=tftb_ifft(tfr);
  y=zeros(tcol,1);

  for icol=1:tcol,
    if ptrace, disprog(icol,tcol,10); end;
    ti=t(icol); 
    valuestj=max([1 2-ti ti-N/2+1]):min([NbPoints 2*NbPoints-ti ti+N/2]);
    

    for tj=valuestj,
      tmid = fix(0.5*(ti+tj)); 
      tdiff= fix(ti-tj); indice= rem(N+tdiff,N)+1;
      
      y(icol,1)=y(icol,1)+tfr(indice,tmid)*x(tj);
    end;
  end;
  y=matrix(y,szx)
  if ptrace, printf('\n'); end;
endfunction
