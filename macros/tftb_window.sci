function h=tftb_window(N,name,param,param2,opt_plot);
//	Window generation.
// Calling Sequence
//	H=tftb_window(N)
//	H=tftb_window(N,NAME)
//	H=tftb_window(N,NAME,PARAM)
//	H=tftb_window(N,NAME,PARAM,PARAM2)
//	H=tftb_window(...,'plot') 
//  Parameters
//	N      : length of the window
//	NAME   : name of the window shape (default : Hamming)
//	PARAM  : optional parameter
//	PARAM2 : second optional parameters
//      'plot':	if one input parameter is 'plot', the window will be plotted
//  Description
//	yields a window of length N with a given shape.
//
//	Possible names are :
//
//	'Hamming', 'Hanning', 'Nuttall',  'Papoulis', 'Harris',
//
//	'Rect',    'Triang',  'Bartlett', 'BartHann', 'Blackman'
//
//	'Gauss',   'Parzen',  'Kaiser',   'Dolph',    'Hanna'.
//
//	'Nutbess', 'spline',  'Flattop'
//
//	For the gaussian window, an optionnal parameter K
//	sets the value at both extremities. The default value is 0.005
//
//	For the Kaiser-Bessel window, an optionnal parameter
//	sets the scale. The default value is 3*pi.
//
//	For the Spline windows, h=tftb_window(N,'spline',nfreq,p)
//	yields a spline weighting function of order p and frequency
//	bandwidth proportional to nfreq.
//   Examples
//        h=tftb_window(256,'Gauss',0.005); 
//        plot(0:255, h); a=gca();a.data_bounds=([0,255,-0.1,1.1]); xgrid
//
//   See also
//        dwindow
//   Authors
//      H. Nahrstaedt - Aug 2010
//	F. Auger, June 1994 - November 1995.
//	S. Steer July 2018

//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
//
//	References : 
//	- F.J. Harris, "On the use of windows for harmonic
//	analysis with the discrete Fourier transform",
//	Proceedings of the IEEE, Vol 66, No 1, pp 51-83, 1978.
//	- A.H. Nuttal, "A two-parameter class of Bessel weighting 
//	functions for spectral analysis or array processing", 
//	IEEE Trans on ASSP, Vol 31, pp 1309-1311, Oct 1983.
//	- Y. Ho Ha, J.A. Pearce, "A New window and comparison to
//	standard windows", Trans IEEE ASSP, Vol 37, No 2, 
//	pp 298-300, February 1989.
//	- C.S. Burrus, Multiband Least Squares FIR Filter Design,
//	Trans IEEE SP, Vol 43, No 2, pp 412-421, February 1995.
//     - https://en.wikipedia.org/wiki/Window_function#Hann_and_Hamming_windows
  [nargout,nargin]=argn(0);
  if (nargin==0), error ( 'at least 1 parameter is required' ); end;

  in_par=['N','name','param','param2','opt_plot'];
  in_par_min=1;
  plotting=%f;
  if (nargin>in_par_min)
    if (type(evstr(in_par(nargin)))==10)
      if convstr(evstr(in_par(nargin)))=="plot" then
        plotting=%t; 
        nargin=nargin-1;
      end; 
    end;
  end;
  clear in_par;



  if (N<=0), error('N should be strictly positive.'); end;
  if (nargin==1), name= 'Hamming'; end ;
  name=convstr(name,"u");
  T=N-1;t=(0:T)';
  if (name=='RECTANG') | (name=='RECT'), 
    h=ones(t);
  elseif (name=='HAMMING'),
    h=0.54 - 0.46*cos(2*%pi*t/T);
  elseif (name=='HANNING') | (name=='HANN'),
    h=0.5*(1 - cos(2*%pi*t/T));
  elseif (name=='KAISER'),
    if (nargin==3), beta0=param; else beta0=3*%pi; end;
    ind=2*t/T-1
    h=besselj(0,%i*beta0*sqrt(1-ind.^2))/real(besselj(0,%i*beta0));
  elseif (name=='NUTTALL'), //Blackman Nuttall
    ind=2*%pi*t/T;
    h=+0.3635819-0.4891775*cos(ind)+0.1363995*cos(2*ind)-0.0106411*cos(3*ind) ;
  elseif (name=='BLACKMAN'),
    alpha=0.16;
    ind=2*%pi*t/T;
    h= (1-alpha)/2 - 1/2*cos(ind) + alpha/2*cos(2*ind) ;
  elseif (name=='HARRIS'),//Blackman Harris
    ind=2*%pi*t/T;
    h=+0.35875-0.48829 *cos(ind)+0.14128*cos(2.0*ind)-0.01168 *cos(3.0*ind);
  elseif (name=='BARTLETT') | (name=='TRIANG'),
    h=1-abs(2*t/T-1)
  elseif (name=='BARTHANN'),
    a=t/T-1/2
    h=0.62-0.48*abs(a)+0.38*cos(2*%pi*a)
  elseif (name=='PAPOULIS')|name=='SINE' then
    h=sin(%pi*t/T);
  elseif (name=='GAUSS'),
    if (nargin==3), K=param; else K=0.005; end;
    h= exp(log(K) * linspace(-1,1,N)'.^2 );
  elseif (name=='PARZEN')
    //https://en.wikipedia.org/wiki/Window_function#Parzen_window
    ind=abs((0:N-1)'-(N-1)/2)*2/N;
    temp=2*(1.0-ind).^3;
    h= min(temp-(1-2.0*ind).^3,temp);
  elseif (name=='POWERSINE'),
    if (nargin==3), L=param; else L=1; end;
    h=sin(%pi*t/T).^(2*L)
  elseif (name=='DOLPH') | (name=='DOLF'),
    if (rem(N,2)==0) then
      oddN=1; N=2*N+1; 
    else 
      oddN=0; 
    end;
    if (nargin==3) then
      A=10^(param/20); 
    else 
      A=1e-3; 
    end;
    K=N-1; 
    Z0=cosh(acosh(1.0/A)/K); 
    x0=acos(1/Z0)/%pi; 
    x=(0:K)/N; 
    indices1=find((x<x0)|(x>1-x0));
    indices2=find((x>=x0)&(x<=1-x0));
    h(indices1)= cosh(K*acosh(Z0*cos(%pi*x(indices1))));
    h(indices2)= cos(K*acos(Z0*cos(%pi*x(indices2))));
    h=fftshift(real(ifft(A*real(h))));h=h'/h(K/2+1);
    if oddN, h=h(2:2:K); end;
  elseif (name=='NUTBESS'),
    if (nargin==3) then
      beta0=param; nu=0.5; 
    elseif (nargin==4) then
      beta0=param; nu=param2;
    else 
      beta0=3*%pi; nu=0.5;
    end;
    ind=(-(N-1)/2:(N-1)/2)' *2/N; 
    h=sqrt(1-ind.^2).^nu .* ...
      real(besselj(nu,%i*beta0*sqrt(1.0-ind.^2)))/real(besselj(nu,%i*beta0));
  elseif (name=='SPLINE'),
    if (nargin < 3),
      nfreq=1
      p=%pi*N*nfreq/10.0;
    elseif (nargin==3),
      nfreq=param; 
      p=%pi*N*nfreq/10.0;
    else 
      nfreq=param; 
      p=param2;
    end;
    ind=(-(N-1)/2:(N-1)/2)'; 
    h=sinc((0.5*nfreq/p)*ind) .^ p;
  elseif name=='FLATTOP'|name=='FLATTOP_NI',//National instrument (http://edoc.mpg.de/395068)
    ind=2*%pi*t/T;
    h=+0.2810639-0.5208972*cos(ind)+0.1980399*cos(2*ind) ;
  elseif (name=='FLATTOP_M'), //Matlab equivalent
    ind=2*%pi*t/T;
    h=0.21557895-0.41663158*cos(ind)+0.277263158*cos(2*ind)-0.083578947*cos(3*ind)+0.006947368*cos(4*ind);
  elseif (name=='FLATTOP_SRS'), //Standford Research (http://edoc.mpg.de/395068)
    ind=2*%pi*t/T;                            
    h=1-1.93*cos(ind)+1.29*cos(2*ind)-0.388*cos(3*ind)+0.028*cos(4*ind);
  else 
    mess=[" unknown window name. choose one of:"
    "	''Hamming'',   ''Hanning'', ''Nuttall'',  ''Papoulis'',  ''Sine'', "
    "	''Harris'',    ''Rect'',    ''Triang'',   ''Bartlett'',  ''BartHann'', "
    "	''Blackman'',  ''Gauss'',   ''Parzen'',   ''Kaiser'',    ''Dolph'', "
    "	''Powersine'', ''Nutbess'', ''spline'',   ''Flattop'',   ''Flattop_ni'', "
    "	''Flattop_m'', ''Flattop_srs''."];
    error(mess);
  end;

  if and(abs(imag(h))<%eps)
    h=real(h);
  end

  if (plotting),
    subplot(121);plot2d(1:N,h,style=color('blue'))
    xlabel("Samples");ylabel("Amplitude");title("time domain");
    set(gca(),'grid',[1 1]*color('gray'))
    subplot(122)
    [W,fr]=frmag(h,256);
    plot2d(fr(1:$-1),20*log10(W(1:$-1)),style=color('blue'))
    xlabel("Normalized Frequency"); ylabel("Magnitude (dB)");title("frequency domain");
    set(gca(),'grid',[1 1]*color('gray'))
  end

endfunction

