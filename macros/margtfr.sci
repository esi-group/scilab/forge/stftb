function [margt,margf,E]=margtfr(tfr,t,f) 
// Marginals and energy of a time-frequency representation.
// Calling Sequence
//	[MARGT,MARGF,E]=margtfr(TFR,T,F)
// Parameters
//	TFR : time-frequency representation (M,N)
//	T   : vector containing the time samples in sec.    (default : (1:N))
//	F   : vector containing the frequency samples in Hz, not   necessary uniformly sampled. (default : (1:M))
//	MARGT : time marginal
//	MARGF : frequency marginal
//	E     : energy of TFR
//  Description
//   margtfr  calculates the time and	frequency marginals and the energy of a time-frequency	representation. 
//  Examples   
//        S=altes(128,0.05,0.45); TFR=tfrscalo(S,1:128,8,'auto');
//       [MARGT,MARGF,E] = margtfr(TFR); 
//       subplot(211); plot(T,MARGT); subplot(212); plot(F,MARGF);
//  See also 
//       momttfr
//   Authors
//      H. Nahrstaedt - Aug 2010
//	P. Goncalves, October 95
//	Copyright (c) 1995 Rice University

//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  [nargout,nargin]=argn(0);  
  fname="margtfr"
  if nargin == 0 then
    error(msprintf(_("%s: Wrong number of input argument: %d to %d expected.\n"),fname,1,3));
  end;
  
  //tfr
  if type(tfr)<>1 then
    error(msprintf(_("%s: Wrong type for input argument #%d: A matrix of double expected.\n"),fname,1));
  end
  [M,N] = size(tfr) ;

  //t
  if nargin>=2 then
    if type(t)<>1|and(size(t)<>1)|~isreal(t)then
      error(msprintf(_("%s: Wrong type for input argument #%d: A real vector expected.\n"),fname,2));
    end
    if size(t,"*")<>N then
      error(msprintf(_("%s: Wrong size for input arguments #%d: %d expected.\n"),fname,2,N));
    end
  else
    t = (1:N);
  end
  
  //f
  if nargin==3 then  
    if type(f)<>1|and(size(f)<>1)|~isreal(f)then
      error(msprintf(_("%s: Wrong type for input argument #%d: A real vector expected.\n"),fname,3));
    end
    if size(f,"*")<>M then
      error(msprintf(_("%s: Wrong size for input arguments #%d: %d expected.\n"),fname,3,M));
    end
    if or(f<0) then
      error(msprintf(_("%s: Wrong value for input argument #%d: The elements must be >= %d\n"),fname,2,0))
    end
    f=f(:);
  else
    f = (1:M)';
  end
  
  E     = real(integ2d(tfr,t,f)/M) ;
  margt = real(integ(tfr.',f')/M) ;
  margf = real(integ(tfr,t)/N) ;
endfunction
