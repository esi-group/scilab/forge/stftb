function [tfr2,OrderedSurfaces]=tfrsurf(tfr,threshold,keep,ptrace);
// extract from a time-frequency representation the biggest energy dots
// Calling Sequence
//      [tfr2,OrderedSurfaces]=tfrsurf(tfr);
//      [tfr2,OrderedSurfaces]=tfrsurf(tfr,threshold);
//      [tfr2,OrderedSurfaces]=tfrsurf(tfr,threshold,keep);
//      [tfr2,OrderedSurfaces]=tfrsurf(tfr,threshold,keep,trace);
// Parameters
//        TFR       : time-frequency representation.
//        THRESHOLD : the energy threshold, in % 
//        KEEP      : number of dots to keep
//        TRACE     : if nonzero, the progression of the algorithm is shown     (default : 0).
// Examples
// N=256; 
// sig=fmlin(N,0.1,0.3)+fmlin(N,0.3,0.4)+2*fmlin(N,0.05,0.2).*amgauss(N,190,70);
// tfr=tfrwv(sig,1:N,128);
// [tfr2,OrderedSurfaces]=tfrsurf(tfr,5,3,1);
// scf(1);tfrview(tfr,sig,1:N,'tfrwv',[2 1 5 10 128 2 1 0 0 .5])
// title('original tfr');
// scf(2);tfrview(tfr2,sig,1:N,'tfrwv',[2 1 5 10 128 2 1 0 0 .5]);
// title('modified tfr');
// scf(3);plot(1:10,OrderedSurfaces(1:10),'-',1:10,OrderedSurfaces(1:10),'o'); //semilogy
// title('number of points of the 10 biggest dots');
//	See also 
//        imextract.
//     Authors
//      H. Nahrstaedt - Aug 2010
//      F. Auger, oct 1999
//	Copyright (c) CNRS - France 1999. 

//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  [nargout,nargin]=argn(0);
  fname="tfrsurf";
  if nargin == 0 then
    error(msprintf(_("%s: Wrong number of input argument: %d to %d expected.\n"),fname,1,4));
  end;
  
  //tfr
  if type(tfr)<>1 then
    error(msprintf(_("%s: Wrong type for input argument #%d: A matrix of double expected.\n"),fname,1));
  end
  
  //ptrace
  if nargin==4 then
      if and(type(ptrace)<>[1 4])|size(ptrace,"*")<>1 then
      error(msprintf(_("%s: Wrong type for argument #%d: A boolean or real scalar expected.\n"),fname,4));
    end
  else
    ptrace=%f
  end
  
  //keep
  if nargin>=3 then
    if type(keep)<>1|size(keep,"*")>1|~isreal(keep)|int(keep)<>keep then
      error(msprintf(_("%s: Wrong type for input argument #%d: A integer value expected.\n"),fname,3));
    end 
    if keep<=0 then
      error(msprintf(_("%s: Wrong value for input argument #%d: A positive integer expected.\n"),fname,3));
    end
  else
    keep=10
  end
  
  //threshold
  if nargin>=2 then
    if type(threshold)<>1|size(threshold,"*")>1|~isreal(threshold)| ...
          threshold<=0 then
      error(msprintf(_("%s: Wrong type for input argument #%d: A positive real value expected.\n"),fname,3));
    end 
  else
    threshold=5
  end
 

  [Nbrow,Nbcol]=size(tfr);
  TheMax=max(tfr);
  [EnergyDots,NbDots]=imextrac(tfr>=threshold*TheMax*0.01,ptrace);
  Surfaces=zeros(1,NbDots+1);
  for i=0:NbDots,
    Surfaces(i+1)=length(find(EnergyDots==i));
  end;
  [OrderedSurfaces,Indices]=gsort(Surfaces(2:NbDots+1),'g','i');
  OrderedSurfaces=OrderedSurfaces(:,$:-1:1);
  Indices=Indices(:,$:-1:1);

  Binary=zeros(Nbrow,Nbcol);
  for i=1:keep,
    DotIndice=find(EnergyDots==Indices(i));
    Binary(DotIndice)=1;
  end;

  tfr2=tfr.*Binary;

endfunction

