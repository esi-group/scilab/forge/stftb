function H=kaytth(flength);
//	 Kay-Tretter filter computation. 
// Calling Sequence
//	H=kaytth(length)
//    See also 
//        instfreq
//   Authors
//      H. Nahrstaedt - Aug 2010
//	F. Auger, March 1994.
//	Copyright (c) 1996 by CNRS (France).

//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301
//  USA
  fname="kaytth"
  if nargin == 0 then
    error(msprintf(_("%s: Wrong number of input argument: %d expected.\n"),fname,1));
  end;
  if type(flength)<>1|size(flength,"*")<>1|~isreal(flength)|int(flength)<>flength then
    error(msprintf(_("%s: Wrong type for input argument #%d: An integer value expected.\n"),fname,1));
  end
  if flength<=0 then
      error(msprintf(_("%s: Wrong value for input argument #%d: A value >= %d expected.\n"),fname,1,1));
  end
  pp1=flength*(flength+1);
  den=2.0*flength*(flength+1)*(2.0*flength+1.0)/3.0;
  i=1:flength; H=pp1-i.*(i-1);

  H=H ./ den;

endfunction
