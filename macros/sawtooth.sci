function y = sawtooth (t,width)
//Generates a sawtooth wave 
// Calling Sequence
// y = sawtooth (t)
// y = sawtooth (t,width)
// Description
// Generates a sawtooth wave of period 2 * pi with limits +1/-1
//  for the elements of t
//
// width is a real number between 0 and 1 which specifies
// the point between 0and 2 * pi where the maximum is. The
// function increases linearly from -1 to 1  in  [0, 2 * 
// pi * width] interval, and decreases linearly from 1 to 
// -1 in the interval[2 * pi * width, 2 * pi].
//
// If width is 0.5, the function generates a standard triangular wave.
//
// If width is not specified, it takes a value of 1, which is a standard
// sawtooth function.
// Authors
// Copyright (C) 2007   Juan Aguado  

//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; If not, see <http://www.gnu.org/licenses/>.


  [nargout,nargin]=argn(0);
  fname="sawtooth";
  if nargin < 1 then
    error(msprintf(_("%s: Wrong number of input argument: %d to %d expected.\n"),fname,1, 2));
  end
 
  if nargin==2 then
    if type(width)<>1|~isreal(width)|or(size(width)<>1) then
      error(msprintf(_("%s: Wrong type for input argument #%d: A real scalar expected.\n"),fname,2));
    end
    if width<0|width>1 then
      error(msprintf(_("%s: Wrong value for input argument #%d: The elements must be in the interval %s.\n"),fname,2,"[0 1]"));

    end
  else
    width = 1;
  end
    if type(t)<>1|~isreal(t)|and(size(width)<>1) then
      error(msprintf(_("%s: Wrong type for input argument #%d: A real vector expected.\n"),fname,1));
    end

  t = pmodulo (t / (2 * %pi), 1);
  y = zeros(t);

  if (width ~= 0)
    y (t < width) = 2 * t (t < width) / width - 1;
  end

  if (width ~= 1)
    y( t >= width) = -2 * (t (t >= width) - width) / (1 - width) + 1;
  end

endfunction;

