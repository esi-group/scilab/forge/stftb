function [tfr,rtfr,rhat] = tfrrmsc(x,t,N,f0T,ptrace,K,opt_plot);
// Reassigned Morlet Scalogram time-frequency distribution.
// Calling Sequence
//	[TFR,RTFR,HAT] = tfrrmsc(X) 
//	[TFR,RTFR,HAT] = tfrrmsc(X,T) 
//	[TFR,RTFR,HAT] = tfrrmsc(X,T,N) 
//	[TFR,RTFR,HAT] = tfrrmsc(X,T,N,F0T) 
//	[TFR,RTFR,HAT] = tfrrmsc(X,T,N,F0T,K) 
//	[TFR,RTFR,HAT] = tfrrmsc(X,T,N,F0T,K,TRACE) 
//	[TFR,RTFR,HAT] = tfrrmsc(...,'plot') 
//    Parameters
//	X     : analysed signal
//	T     : the time instant(s)           (default : 1:length(X))
//	N     : number of frequency bins      (default : length(X))
//	F0T   : time-bandwidth product of the mother wavelet (default : 2.5)) 
//	TRACE : if nonzero, the progression of the algorithm is shown    (default : 0).
//      'plot':	if one input parameter is 'plot',  tfrrmsc runs tfrqview. and TFR will be plotted
//	TFR,  : time-frequency representation and its reassigned
//	RTFR    version.
//	HAT   : Complex matrix of the reassignment vectors.
// Description
//	tfrrmsc computes the Morlet scalogram and its reassigned version.
// Examples
//       sig=fmlin(64,0.1,0.4); tfrrmsc(sig,1:64,64,2.1,1,'plot');
// Authors
//      H. Nahrstaedt - Aug 2010
//	F. Auger, January, April 1996.
//	Copyright (c) 1996 by CNRS (France).

//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  [nargout,nargin]=argn(0);
  fname="tfrrmsc"
 

  in_par=['x','t','N','f0T','ptrace','K','opt_plot'];
  in_par_min=1;
  plotting=%f;
  if (nargin>in_par_min)
    if (type(evstr(in_par(nargin)))==10)
      if convstr(evstr(in_par(nargin)))=="plot" then
        plotting=%t; 
        nargin=nargin-1;
      end; 
    end;
  end;
  clear in_par;
  
  if nargin == 0 then
    error(msprintf(_("%s: Wrong number of input argument: %d to %d expected.\n"),fname,1,7));
  end
 
  //x
  if type(x)<>1 then
    error(msprintf(_("%s: Wrong type for input argument #%d: A matrix of double expected.\n"),fname,1));
  end
  if or(size(x)==1) then
    x=x(:);
  else
    error(msprintf(_("%s: Wrong size for input argument #%d: A  vector  expected.\n"),fname,1));
  end
  Nx=size(x,1);
  
  //K
  if nargin==6 then
    if type(K)<>1|~isreal(K)|or(size(K)<>1)|K<=0 then
     error(msprintf(_("%s: Wrong type for input argument #%d: A positive scalar expected.\n"),fname,6)); 
    end
  else
   K= 0.001; 
  end
  
  //ptrace
  if nargin==5 then 
    if and(type(ptrace)<>[1 4])|size(ptrace,"*")<>1 then
      error(msprintf(_("%s: Wrong type for argument #%d: A boolean or real scalar expected.\n"),fname,5));
    end
  else
    ptrace=%f;
  end
  
  //f0T
  if nargin==4 then 
    if type(f0T)<>1|~isreal(f0T)|or(size(f0T)<>1)|f0T<=0 then
      error(msprintf(_("%s: Wrong type for input argument #%d: A positive scalar expected.\n"),fname,4));
    end
  else
    f0T=2.5;
  end
   //N
  if nargin>=3 then
    if type(N)<>1|size(N,"*")>1|~isreal(N)|int(N)<>N then
      error(msprintf(_("%s: Wrong type for input argument #%d: A integer value expected.\n"),fname,3));
    end 
    if N<=0 then
      error(msprintf(_("%s: Wrong value for input argument #%d: A positive integer expected.\n"),fname,3));
    end
  else
    N=Nx
  end
    
  //T
  if nargin>=2 then
    if type(t)<>1|and(size(t)>1)|~isreal(t) then
      error(msprintf(_("%s: Wrong type for input argument #%d: A real vector expected.\n"),fname,2));
    end
    if max(t)>Nx|min(t)<1 then
      error(msprintf(_("%s: Wrong value for input argument #%d: The elements must be in the interval %s.\n"),fname,2,"[1 "+string(Nx)+"]"));
    end
    Deltat=diff(t);
    if or(Deltat<>Deltat(1)) then
      error(msprintf(_("%s: Wrong value for input argument #%d: The elements must be regularly spaced.\n"),fname,2));
    end
    Dt=Deltat(1);
  else
    t=1:Nx;
    Dt=1
  end
  Nt=size(t,2);

 
  tfr= zeros(N,Nt); tf2= zeros(N,Nt);
  if ptrace, disp('Morlet Scalogram'); end;

  M=ceil(f0T*N*sqrt(2.0*log(1/K))); 
  tau = 0:M+round(N/2); 
  pi2 = 2.0*%pi;
  hstar = exp(-(tau/(N*f0T)).^2 /2.0) .* exp(-%i*pi2*tau/N);
  Thstar = tau.*hstar;

  for m=1:round(N/2)-1
    if ptrace, disprog(m,N/2,10); end;
    Factor=sqrt(m/(f0T*N));
    for icol=1:Nt,
      ti= t(icol);
      tauneg=1:min([ceil(M/m),ti-1]);
      taupos=0:min([ceil(M/m),Nx-ti]);
      // positive frequencies
      tfr(  1+m,icol)= hstar(1+taupos*m)*x(ti+taupos);
      tf2(  1+m,icol)=Thstar(1+taupos*m)*x(ti+taupos);
      if length(tauneg) > 0,
        tfr(1+m,icol)=tfr(1+m,icol) + conj( hstar(1+tauneg*m))*x(ti-tauneg);
        tf2(1+m,icol)=tf2(1+m,icol) - conj(Thstar(1+tauneg*m))*x(ti-tauneg);
      end;
      // negative frequencies
      tfr(N+1-m,icol)=conj( hstar(1+taupos*m))*x(ti+taupos);
      tf2(N+1-m,icol)=conj(Thstar(1+taupos*m))*x(ti+taupos);
      if length(tauneg) > 0,
        tfr(N+1-m,icol)=tfr(N+1-m,icol) +  hstar(1+tauneg*m)*x(ti-tauneg);
        tf2(N+1-m,icol)=tf2(N+1-m,icol) - Thstar(1+tauneg*m)*x(ti-tauneg);
      end;
    end;
    tfr(  1+m,:)=Factor*tfr(  1+m,:); tf2(  1+m,:)=Factor*tf2(  1+m,:)/m;
    tfr(N+1-m,:)=Factor*tfr(N+1-m,:); tf2(N+1-m,:)=Factor*tf2(N+1-m,:)/m;
  end;

  m=round(N/2); 
  Factor=sqrt(m/(f0T*N));
  if ptrace, disprog(m,N/2,10); end
  for icol=1:Nt,
    ti= t(icol);
    tauneg=1:min([ceil(M/m),ti-1]);
    taupos=0:min([ceil(M/m),Nx-ti]);
    tfr(  1+m,icol)= hstar(1+taupos*m)*x(ti+taupos);
    tf2(  1+m,icol)=Thstar(1+taupos*m)*x(ti+taupos);
    if length(tauneg) > 0,
      tfr(1+m,icol)=tfr(1+m,icol) + conj( hstar(1+tauneg*m))*x(ti-tauneg);
      tf2(1+m,icol)=tf2(1+m,icol) - conj(Thstar(1+tauneg*m))*x(ti-tauneg);
    end;
  end;

  tfr(1+m,:)=Factor*tfr(1+m,:); 
  tf2(1+m,:)=Factor*tf2(1+m,:)/m;
  tfr=tfr(:); tf2=tf2(:);

  avoid_warn=find(tfr~=0.0);
  tf2(avoid_warn)=tf2(avoid_warn)./tfr(avoid_warn); 
  tfr=abs(tfr).^2;

  if ptrace, disp('reassignment :'); end;
  tfr=matrix(tfr,N,Nt);
  tf2=matrix(tf2,N,Nt);

  rtfr= zeros(N,Nt); 
  Ex=mean(abs(x(min(t):max(t))).^2); Threshold=1.0e-6*Ex;
  Factor=2.0*%pi*N*f0T*f0T;
  for icol=1:Nt,
    if ptrace, disprog(icol,Nt,10); end;
    for jcol=1:N,
      if tfr(jcol,icol)>Threshold,
        icolhat= icol + round(real(tf2(jcol,icol)/Dt));
        icolhat=min(max(real(icolhat),1),Nt);
        m=rem(jcol+round(N/2)-2,N)-round(N/2)+1;
        jcolhat= jcol + round(imag(m*m*tf2(jcol,icol)/Factor));
        jcolhat=rem(rem(jcolhat-1,N)+N,N)+1;
        rtfr(jcolhat,icolhat)= rtfr(jcolhat,icolhat)+tfr(jcol,icol);
        tf2(jcol,icol)= jcolhat + %i * icolhat;
      else
        tf2(jcol,icol)=(1+%i)*%inf;
        rtfr(jcol,icol)=rtfr(jcol,icol)+tfr(jcol,icol);
      end;
    end;
  end;

  if (plotting),
    TFTBcontinue=1;
    while (TFTBcontinue==1),
      choice=x_choose(['Morlet scalogram',...
                    'reassigned Morlet scalogram'],'Choose the representation:','stop');
      if (choice==0), TFTBcontinue=0;
      elseif (choice==1), 
        tfrqview(tfr,x,t,'tfrmsc',f0T);
      elseif (choice==2),
        tfrqview(rtfr,x,t,'tfrrmsc',f0T);
      end;
    end;
  end
  if (nargout>2),
    rhat=tf2;
  end;
endfunction


