function tifd=friedman(tfr,rhat,t,method,ptrace,opt_plot);
//FRIEDMAN Instantaneous frequency density.
// Calling Sequence
//	TIFD = friedman(TFR,HAT 
//	TIFD = friedman(TFR,HAT,T 
//	TIFD = friedman(TFR,HAT,T,METHOD) 
//	TIFD = friedman(TFR,HAT,T,METHOD,TRACE) 
//	TIFD = friedman(...,'plot') 
//  Parameters
//	TFR   : time-frequency representation, (N,M) matrix.
//	HAT   : complex matrix of the reassignment vectors.
//	T     : the time instant(s)	(default : (1:M)).
//	METHOD: chosen representation	(default : 'tfrrsp').  
//      'plot':	when called with the additional string 'plot',  friedman runs tfrqview. and TIFD will be plotted
//	TRACE : if nonzero, the progression of the algorithm is shown	(default : 0).
//	TIFD  : time instantaneous-frequency density.
// Description
//      computes the	time-instantaneous frequency density (defined by Friedman [1])
//	of a reassigned time-frequency representation.
//
//	WARNING : TIFD is not an energy distribution, but an estimated 
//	-------        probability distribution !
//   Examples
//       sig=fmlin(128,0.1,0.4); h=tftb_window(47,'Kaiser');
//       t=1:2:127; [tfr,rtfr,hat]=tfrrpwv(sig,t,128,h);
//       friedman(tfr,hat,t,'tfrrpwv',1,'plot'); 
//    See also
//        ridges
//   Authors
//      H. Nahrstaedt - Aug 2010
//	F. Auger, August 1994, Decembre 1995.
//	Copyright (c) 1996 by CNRS (France).

//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
//
//	[1] : D. H. Friedman, "Instantaneous Frequency vs Time : An
//	      Interpretation of the Phase Structure of Speech", Proc. IEEE
//	      ICASSP, pp. 29.10.1-4, Tampa, 1985.	
  [nargout,nargin]=argn(0);
  fname="friedman";
  


  in_par=['tfr','rhat','t','method','ptrace','opt_plot'];
  in_par_min=2;
  plotting=%f;
  if (nargin>in_par_min)
    if (type(evstr(in_par(nargin)))==10)
      if convstr(evstr(in_par(nargin)))=="plot" then
        plotting=%t; 
        nargin=nargin-1;
      end; 
    end;
  end;
  clear in_par;

  if nargin <3 then
    error(msprintf(_("%s: Wrong number of input argument: %d to %d expected.\n"),fname,2,6));
  end;

  //tfr
  if type(tfr)<>1 then
    error(msprintf(_("%s: Wrong type for input argument #%d: A matrix of double expected.\n"),fname,1));
  end
  [tfrrow,tfrcol]=size(tfr);
  
  //rhat
  if type(rhat)<>1 then
    error(msprintf(_("%s: Wrong type for input argument #%d: A matrix of double expected.\n"),fname,2));
  end
  [hatrow,hatcol]=size(rhat);
  
  if (tfrrow~=hatrow)|(tfrcol~=hatcol) then
    error(msprintf(_("%s: Arguments #%d and #%d must have the same sizes.\n"),fname,1,2));
  end;

   //ptrace
  if nargin==5 then 
    if and(type(ptrace)<>[1 4])|size(ptrace,"*")<>1 then
      error(msprintf(_("%s: Wrong type for argument #%d: A boolean or real scalar expected.\n"),fname,5));
    end
  else
    ptrace=%f;
  end
  
  //method
   if nargin>=4 then
     if type(method)<>10|size(method,"*")<>1 then
        error(msprintf(_("%s: Wrong type for input argument #%d: A character string expected.\n"),fname,4));
     end
     method=convstr(method,"U")
     //see tfrqview for valid values...
   else
     method='TFRRSP'
   end
  
  //t
  if nargin>=3 then
    if type(t)<>1|and(size(t)>1)|~isreal(t) then
      error(msprintf(_("%s: Wrong type for input argument #%d: A real vector expected.\n"),fname,3));
    end
    if min(t)<1 then
      error(msprintf(_("%s: Wrong value for input argument #%d: The elements must be >=.\n"),fname,3,1));
    end
    t=matrix(t,1,-1);
  else
    t=1:tfrcol;
  end
  tcol = size(t,2);

  tifd=zeros(tfrrow,tfrcol);
  bins=0.5+(0:tfrrow-1);
  threshold=sum(tfr)*0.5/(tfrrow*tfrcol);

  if ptrace, printf ('\nFriedman distribution: \n'); end;

  for j=1:tfrcol,
    if ptrace, disprog(j,tfrcol,10); end;
    indices=find(tfr(:,j)>threshold);
    if (length(indices)>=1),
      [occurences,trash]=hist(real(rhat(indices,j)),bins);
      tifd(:,j)=occurences;
    end;
  end; 
  tifd=tifd/sum(tifd);

  method=convstr(method,"u");
  if plotting then
    tfrqview(tifd,[],t,method);
  end
endfunction



function [h,t] = hist(x,n)

// hist - compute and display histogram
//
//  [h,t] = hist(x,n);
//
//  if h is ommitted, then display the histogram, 
//  otherwise, compute it.
//
//  Copyright (c) 2008 Gabriel Peyre


  if argn(2)<2
    n = 100;
  end


  x = x(:);
  if length(n)>1
    t = n;
    n = length(n);
    // closest point histograms
    h = zeros(n,1);
    for i=1:length(x)
      [tmp,j] = min( abs(x(i)-t(:)), 'r' );
      h(j) = h(j)+1;
    end
  else
    // equispaced histograms
    a = min(x); b = max(x);
    tau = (b-a)/n;
    a1 = a+tau/2; b1 = b-tau/2;
    t = a1:tau:b1;
    x1 = (x-a1)/(b1-a1)*(n-1)+1;
    x1 = round(x1);
    h = zeros(n,1);
    for i=1:n
      h(i) = sum(x1==i);
    end
  end
  h = h/sum(h);



endfunction





