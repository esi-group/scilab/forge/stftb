function [tfr,t,f] = tfrmh(x,t,N,ptrace,opt_plot);
//	Margenau-Hill time-frequency distribution.
// Calling Sequence
//	[TFR,T,F]=tfrmh(X)
//	[TFR,T,F]=tfrmh(X,T)
//	[TFR,T,F]=tfrmh(X,T,N)
//	[TFR,T,F]=tfrmh(X,T,N,TRACE)
//	[TFR,T,F]=tfrmh(...,'plot')
//    Parameters
//	X     : signal if auto-MH, or [X1,X2] if cross-MH.
//	T     : time instant(s)          (default : 1:length(X)).
//	N     : number of frequency bins (default : length(X)).
//	TRACE : if nonzero, the progression of the algorithm is shown  (default : 0).
//      'plot':	if one input parameter is 'plot',  tfrmh runs tfrqview. and TFR will be plotted
//	TFR   : time-frequency representation.
//	F     : vector of normalized frequencies.
//     Description
//      tfrmh computes the Margenau-Hill distribution of a discrete-time signal X, 
//	or the cross Margenau-Hill representation between two signals. 
//     Examples
//	 sig=fmlin(128,0.1,0.4); tfrmh(sig,1:128,128,1,'plot');
//     Authors
//      H. Nahrstaedt - Aug 2010
//	F. Auger, May-August 1994, July 1995.
//	Copyright (c) 1996 by CNRS (France).

//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  [nargout,nargin]=argn(0);
  fname="tfrmh";

  in_par=['x','t','N','ptrace','opt_plot'];
  in_par_min=1;
  plotting=%f;
  if (nargin>in_par_min)
    if (type(evstr(in_par(nargin)))==10)
      if convstr(evstr(in_par(nargin)))=="plot" then
        plotting=%t; 
        nargin=nargin-1;
      end; 
    end;
  end;
  clear in_par;
  
  if nargin == 0 then
    error(msprintf(_("%s: Wrong number of input argument: %d to %d expected.\n"),fname,1,5));
  end
 
  //x
  if type(x)<>1 then
    error(msprintf(_("%s: Wrong type for input argument #%d: A matrix of double expected.\n"),fname,1));
  end
  if or(size(x)==1) then
    x=x(:);
  end
  if size(x,2)>2 then
     error(msprintf(_("%s: Wrong size for input argument #%d: A a vector or a two columns array expected.\n"),fname,1));
  end
  [Nx,xcol]=size(x);

  //ptrace
  if nargin==4 then 
    if and(type(ptrace)<>[1 4])|size(ptrace,"*")<>1 then
      error(msprintf(_("%s: Wrong type for argument #%d: A boolean or real scalar expected.\n"),fname,4));
    end
  else
    ptrace=%f;
  end
 
  //N
  if nargin>=3 then
    if type(N)<>1|size(N,"*")>1|~isreal(N)|int(N)<>N then
      error(msprintf(_("%s: Wrong type for input argument #%d: A integer value expected.\n"),fname,3));
    end 
    if N<=0 then
      error(msprintf(_("%s: Wrong value for input argument #%d: A positive integer expected.\n"),fname,3));
    end
    if 2^nextpow2(N)~=N then
      //printf(msprintf(_("%s: For a faster computation, N should be a power of two\n"),fname,3));
    end
  else
    N=Nx
  end
    
  //t
  if nargin>=2 then
    if type(t)<>1|and(size(t)>1)|~isreal(t) then
      error(msprintf(_("%s: Wrong type for input argument #%d: A real vector expected.\n"),fname,2));
    end
    if max(t)>Nx|min(t)<1 then
      error(msprintf(_("%s: Wrong value for input argument #%d: The elements must be in the interval %s.\n"),fname,2,"[1 "+string(Nx)+"]"));
    end
  else
    t=1:Nx;
  end
  Nt=size(t,2);
  
  
  
  tfr= zeros (N,Nt) ;  
  if ptrace, disp('Margenau-Hill distribution'); end;
  for icol=1:Nt,
    ti= t(icol); tau=-min([N-ti,Nx-ti]):(ti-1);
    indices= rem(N+tau,N)+1; 
    if ptrace, disprog(icol,Nt,10); end;
    tfr(indices,icol)=x(ti,1)*conj(x(ti-tau,xcol));
  end; 

  tfr=real(fft(tfr,-1,1)); 


  if (plotting),
    tfrqview(tfr,x,t,'type1');
  end
  if (nargout==3),
    //  if rem(N,2)==0, 
    //   f=[0:N/2-1 -N/2:-1]'/N;
    //  else
    //   f=[0:(N-1)/2 -(N-1)/2:-1]'/N;  
    //  end;
    // f=(0.5*(0:N-1)/N)';
    f=(0:N-1)/N;
  end;

endfunction
