function plotwindow(w,wname)
//  This file is made available under the Creative Commons CC0 1.0 Universal
// Public Domain Dedication.  The person who associated a work with this
// deed has dedicated the work to the public domain by waiving all of his or
// her rights to the work worldwide under copyright law, including all
// related and neighboring rights, to the extent allowed by law. You can
// copy, modify, distribute and perform the work, even for commercial
// purposes, all without asking permission.
// Authors:   	Bob K (original version), Olli Niemitalo, BobQQ
//see https://commons.wikimedia.org/wiki/File:Window_function_and_frequency_response_-_Rectangular.svg
  M = 32;   // Fourier transform size as multiple of window length
  Q = 512;  // Number of samples in time domain plot
  P = 40;   // Maximum bin index drawn
  dr = 130; // (dynamic range) Maximum attenuation (dB) drawn in frequency domain plot
  N = length(w);
  B = N*sum(w.^2)/sum(w)^2 // noise bandwidth (bins)
  if M/N < Q then Q = M/N;end
 
  clf
  subplot(121),plot(w),xgrid()
  gca().tight_limits="on"
  ylabel("amplitude");xlabel("samples");
  xtitle(wname+" window")
  //add zeros to increase frequency resolution
  H = abs(fft([w(:);zeros((M-1)*N,1)])); 
  H = fftshift(H);
  H = H/max(H);
  H = 20*log10(H);
  H = max(-dr,H);
  k = (-M*N/2:(M*N/2-1))/M;
  k2 = -P:1/M:P;
  H2 = interp1 (k, H, k2);

  subplot(122),plot(k2,H2),xgrid()
  gca().tight_limits="on"
  ylabel("dB");xlabel("bins");title("Fourier transform")
endfunction
