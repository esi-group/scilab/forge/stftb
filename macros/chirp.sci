function y = chirp(t, f0, t1, f1, form, phase)
//Evaluate a chirp signal at time t.  A chirp signal is a frequency swept cosine wave.
// Calling Sequence
//  y = chirp(t)
//  y = chirp(t,f0)
//  y = chirp(t,f0,t1)
//  y = chirp(t,f0,t1,f1)
//  y = chirp(t,f0,t1,f1,form)
//  y = chirp(t,f0,t1,f1,form,phase)
// Parameters
// t: vector of times to evaluate the chirp signal
// f0: frequency at time t=0 [0 Hz]
// t1: time t1 [1 sec]
// f1: frequency at time t=t1 [100 Hz]
// form: shape of frequency sweep;   'linear' :     f(t) = (f1-f0)*(t/t1) + f0, ,    'quadratic':   f(t) = (f1-f0)*(t/t1)^2 + f0,    'logarithmic': f(t) = (f1-f0)^(t/t1) + f0
// phase: phase shift at t=0
// Description
// If you want a different sweep shape f(t), use the following:
//    y = cos(2*%pi*integral(f(t)) + 2*%pi*f0*t + phase);
// Examples
//     tfrsp(chirp([0:0.001:5])',1:5001,128,'plot'); // linear, 0-100Hz in 1 sec
//     tfrsp(chirp([-2:0.001:15], 400, 10, 100, 'quadratic')',1:5001,128,'plot');
//    plot(chirp([0:1/8000:5], 200, 2, 500, "logarithmic"));
//
// // Shows linear sweep of 100 Hz/sec starting at zero for 5 sec
// // since the sample rate is 1000 Hz, this should be a diagonal
// // from bottom left to top right.
//  tfrsp(chirp([0:0.001:5])',1:5001,128,'plot'); // linear, 0-100Hz in 1 sec
//
// // Shows a quadratic chirp of 400 Hz at t=0 and 100 Hz at t=10
// // Time goes from -2 to 15 seconds.
// stacksize('max');
// tfrsp(chirp([-2:0.001:15], 400, 10, 100, 'quadratic')',1:17001,128,'plot');
//
// // Shows a logarithmic chirp of 200 Hz at t=0 and 500 Hz at t=2
// // Time goes from 0 to 5 seconds at 8000 Hz.
// stacksize('max');
// tfrsp(chirp([0:1/8000:5], 200, 2, 500, "logarithmic")',1:40001,128,'plot');
//  Authors
// 2001-08-31 Paul Kienzle pkienzle@users.sf.net

// * Fix documentation for quadratic case
// Copyright (C) 1999-2000 Paul Kienzle
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; If not, see <http://www.gnu.org/licenses/>.


  [nargout,nargin]=argn(0);
   fname="chirp";
  
  if nargin == 0 then
    error(msprintf(_("%s: Wrong number of input argument: %d to %d expected.\n"),fname,1,6));
  end;
  
  //phase
  if nargin==6 then
    if phase<>[] then
      if type(phase)<>1|size(phase,"*")<>1|~isreal(phase) then
        error(msprintf(_("%s: Wrong type for input argument #%d: A real scalar or an empty matrix expected.\n"),fname,6));
      end
    end 
  else
    phase = []
  end
  
  //form
  if nargin>=5 then
    if form<>[] then
      if type(form)<>10|size(form,"*")<>1 then
        error(msprintf(_("%s: Wrong type for input argument #%d: A character string  or an empty matrix expected.\n"),fname,5));
      end 
      form=convstr(form,"l")
      if and(form<>["linear","quadratic","logarithmic"]) then
        error(msprintf(_("%s: Wrong value for input argument #%d: Must be in the set {%s}.\n"),fname,5,"""linear"",""quadratic"",""logarithmic"""));
      end
    end
  else
    form=[]
  end
  
  

  //t1
  if nargin>=3 then
    if t1<>[] then
      if type(t1)<>1|size(t1,"*")<>1|~isreal(t1)then
        error(msprintf(_("%s: Wrong type for input argument #%d: A real scalar or an empty matrix expected.\n"),fname,3));
      end
      if t1<=0 then
        error(msprintf(_("%s: Wrong value for input argument #%d: A positive scalar expected.\n"),fname,3));
      end
    end
  else
    t1=[];
  end
  
  //f0
  if nargin>=2 then
    if f0<>[] then
      if type(f0)<>1|size(f0,"*")<>1|~isreal(f0) then
        error(msprintf(_("%s: Wrong type for input argument #%d: A real scalar or an empty matrix expected.\n"),fname,2));
      end
      if f0<=0 then
        error(msprintf(_("%s: Wrong value for input argument #%d: Must be > %d.\n"),fname,2,0));
      end 
    end
  else
    f0=[]
  end
  
  //f1
  if nargin>=4 then
    if f1<>[] then
      if type(f1)<>1|size(f1,"*")<>1|~isreal(f1) then
        error(msprintf(_("%s: Wrong type for input argument #%d: A real scalar or an empty matrix expected.\n"),fname,4));
      end
      if f1<0 then
        error(msprintf(_("%s: Wrong value for input argument #%d: Must be > %d.\n"),fname,4,0));
      end 
      
    end
  else
    f1=[]
  end
  //t
  if type(t)<>1|~isreal(t)|and(size(t)>1) then
    error(msprintf(_("%s: Wrong type for input argument #%d: A real vector expected.\n"),fname,3));
  end
  

  if isempty(f0) then f0 = 0; end
  if isempty(t1) then t1 = 1; end
  if isempty(f1) then f1 = 100; end
  if isempty(form) then form = "linear"; end
  if isempty(phase) then phase = 0; end

  phase = 2*%pi*phase/360;
  b = 2*%pi*f0;
  if form== "linear" then
    a = %pi*(f1 - f0)/t1;
    y = cos(a*t.^2 + b*t + phase);
  elseif form== "quadratic" then
    a = (2/3*%pi*(f1-f0)/t1/t1);  
    y = cos(a*t.^3 + b*t + phase);
  elseif form== "logarithmic" then
    a = 2*%pi*t1/log(f1-f0);
    x = (f1-f0)^(1/t1);
    y = cos(a*x.^t + b*t + phase);
  end

endfunction

