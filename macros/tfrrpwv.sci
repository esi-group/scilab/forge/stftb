function [tfr,rtfr,rhat] = tfrrpwv(x,t,N,h,ptrace,opt_plot);
// Reassigned  pseudo Wigner-Ville distribution.
// Calling Sequence
//	[TFR,RTFR,HAT] = tfrrpwv(X) 
//	[TFR,RTFR,HAT] = tfrrpwv(X,T) 
//	[TFR,RTFR,HAT] = tfrrpwv(X,T,N) 
//	[TFR,RTFR,HAT] = tfrrpwv(X,T,N,H) 
//	[TFR,RTFR,HAT] = tfrrpwv(X,T,N,H,TRACE) 
//	[TFR,RTFR,HAT] = tfrrpwv(...,'plot') 
// Parameters
//	X     : analysed signal,
//	T     : the time instant(s)      (default : 1:length(X)).
//	N     : number of frequency bins (default : length(X)).
//	H     : frequency smoothing window, H(0) being forced to 1     (default : Hamming(N/4)).
//	TRACE : if nonzero, the progression of the algorithm is shown       (default : 0).
//      'plot':	if one input parameter is 'plot',  tfrrpwv runs tfrqview. and TFR will be plotted
//	TFR,  : time-frequency representation and its reassigned
//	RTFR    version.
//	HAT   : Complex matrix of the reassignment vectors.
// Description
//	tfrrpwv computes the pseudo Wigner-Ville distribution
//	and its reassigned version.
//  Examples
//       sig=fmlin(128,0.1,0.4); t=1:2:128;
//       h=tftb_window(17,'Kaiser'); tfrrpwv(sig,t,64,h,1,'plot');
//     Authors
//      H. Nahrstaedt - Aug 2010
//	F. Auger, May-July 1994, July 1995.
//	Copyright (c) 1996 by CNRS (France).

//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  [nargout,nargin]=argn(0);
  fname="tfrrpwv";
 

  // handling optional opt_plot parameter
  in_par=['x','t','N','h','ptrace','opt_plot'];
  in_par_min=1;
  plotting=%f;
  if (nargin>in_par_min)
    if (type(evstr(in_par(nargin)))==10)
      if convstr(evstr(in_par(nargin)))=="plot" then
        plotting=%t; 
        nargin=nargin-1;
      end; 
    end;
  end;
  if nargin == 0 then
    error(msprintf(_("%s: Wrong number of input argument: %d to %d expected.\n"),fname,1,6));
  end;
  
  //x
  if type(x)<>1|and(size(x)<>1) then
    error(msprintf(_("%s: Wrong type for argument #%d: A vector of double expected.\n"),fname,1));
  end
  x=x(:)
  Nx=size(x,1);xcol=1;

  //ptrace
  if nargin==5 then 
    if and(type(ptrace)<>[1 4])|size(ptrace,"*")<>1 then
      error(msprintf(_("%s: Wrong type for argument #%d: A boolean or real scalar expected.\n"),fname,5));
    end
  else
    ptrace=%f;
  end
  
  //h
  if nargin==4 then 
    if type(h)<>1|and(size(h)>1)|~isreal(h) then 
      error(msprintf(_("%s: Wrong type for argument #%d: A real vector expected.\n"),fname,4));
    end
    hlength=size(h,"*");
    if (modulo(hlength,2)==0),
      error(msprintf(_("%s: Wrong size for argument #%d: A odd number of elements expected.\n"),fname,4));
    end;
  else
    hlength=floor(Nx/4);
    if rem(hlength,2)==0 then hlength=hlength+1;end;
    h = window("hm",hlength)
  end
  h=h(:);
  Lh=(hlength-1)/2; h=h/h(Lh+1);

  //N
  if nargin>=3 then
    if type(N)<>1|size(N,"*")>1|~isreal(N)|int(N)<>N then
      error(msprintf(_("%s: Wrong type for input argument #%d: A integer value expected.\n"),fname,3));
    end 
    if N<=0 then
      error(msprintf(_("%s: Wrong value for input argument #%d: A positive integer expected.\n"),fname,3));
    end
    if 2^nextpow2(N)~=N then
      //printf(msprintf(_("%s: For a faster computation, N should be a power of two\n"),fname,3));
    end
  else
    N=Nx
  end

  //T
  if nargin>=2 then
    if type(t)<>1|and(size(t)>1)|~isreal(t) then
      error(msprintf(_("%s: Wrong type for input argument #%d: A real vector expected.\n"),fname,2));
    end
    if max(t)>Nx|min(t)<1 then
      error(msprintf(_("%s: Wrong value for input argument #%d: The elements must be in the interval %s.\n"),fname,2,"[1 "+string(Nx)+"]"));
    end
    if size(t,"*")>1 then
      Deltat=diff(t);
      if min(Deltat)<>max(Deltat) then
        error(msprintf(_("%s: Wrong value for input argument #%d: Regularly spaced elements expected.\n"),fname,2));
      end
      t=matrix(t,1,-1);
    end
  else
    t=1:Nx;
  end
  Nt=size(t,2);

  tfr= zeros(N,Nt); tf2= zeros(N,Nt);
  if ptrace, disp('Pseudo Wigner-Ville distribution'); end;
  Dh=dwindow(h);
  for icol=1:Nt,
    ti= t(icol); 
    taumax=min([ti-1,Nx-ti,round(N/2)-1,Lh]);
    tau=-taumax:taumax; 
    indices= rem(N+tau,N)+1;
    if ptrace, disprog(icol,Nt,10); end;
    tfr(indices,icol)= h(Lh+1+tau).*x(ti+tau).*conj(x(ti-tau));
    tf2(indices,icol)=Dh(Lh+1+tau).*x(ti+tau).*conj(x(ti-tau));
    tau=round(N/2); 
    if (ti<=Nx-tau)&(ti>=tau+1)&(tau<=Lh) then
      tfr(tau+1,icol) = 0.5 * ( h(Lh+1+tau) * x(ti+tau,1) * conj(x(ti-tau,xcol))  + ...
                                h(Lh+1-tau) * x(ti-tau,1) * conj(x(ti+tau,xcol))) ;
      tf2(tau+1,icol) = 0.5 * (Dh(Lh+1+tau) * x(ti+tau,1) * conj(x(ti-tau,xcol))  + ...
                               Dh(Lh+1-tau) * x(ti-tau,1) * conj(x(ti+tau,xcol))) ;
    end;
  end ;
  tfr= real(fft(tfr,-1,1)); 
  tf2=imag(fft(tf2,-1,1));

  avoid_warn=find(tfr~=0);
  tf2(avoid_warn)=round(N*tf2(avoid_warn)./tfr(avoid_warn)/(2.0*%pi)); 
   if ptrace, printf ('\nreassignment: \n'); end;
 

  rtfr= zeros(N,Nt); 
  Ex=mean(abs(x(min(t):max(t))).^2); Threshold=1.0e-6*Ex;
  for icol=1:Nt,
    if ptrace, disprog(icol,Nt,10); end;
    for jcol=1:N,
      if abs(tfr(jcol,icol))>Threshold,
        jcolhat= jcol - tf2(jcol,icol);
        jcolhat=rem(rem(jcolhat-1,N)+N,N)+1;
        rtfr(jcolhat,icol)=rtfr(jcolhat,icol) + tfr(jcol,icol) ;
        tf2(jcol,icol)=jcolhat;
      else 
        tf2(jcol,icol)=%inf;
        rtfr(jcol,icol)=rtfr(jcol,icol) + tfr(jcol,icol) ;
      end;
    end;
  end;

  if ptrace, printf('\n'); end;
  if (plotting),
    TFTBcontinue=1;
    while (TFTBcontinue==1),
      choice=x_choose_modeless (['pseudo Wigner-Ville distribution',...
                    'reassigned pseudo Wigner-Ville distribution'],'Choose the representation:','stop');
      if (choice==0), TFTBcontinue=0;
      elseif (choice==1), 
        tfrqview(tfr,x,t,'tfrpwv',h);
      elseif (choice==2),
        tfrqview(rtfr,x,t,'tfrrpwv',h);
      end;
    end;
  end
  if (nargout>2),
    rhat=tf2;
  end;
endfunction

