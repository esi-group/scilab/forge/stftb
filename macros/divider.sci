function [N,M]=divider(N1);
// Find dividers of an integer.  
// Calling Sequence
//	[N,M]=divider(N1) 
// Description
//      find two integers N and M such that M*N=N1 and
//	M and N as close as possible from sqrt(N1).
//
// Examples
//	 N1=258; [N,M]=divider(N1)
// Authors
//      H. Nahrstaedt - Aug 2010
//	F. Auger - November 1995.
//	Copyright (c) 1996 by CNRS (France).

//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  if nargin <1 then
    error(msprintf(_("%s: Wrong number of input argument: %d  expected.\n"),fname,1));
  end;
  //N1 
  if type(N1)<>1|~isreal(N1)|int(N1)<>N1 then
    error(msprintf(_("%s: Wrong type for input argument #%d: An integer expected.\n"),fname,2));
  end
  if or(N1<=0) then
     error(msprintf(_("%s: Wrong value for input argument #%d : Must be > %d.\n"),fname,2,0));
  end
  sz=size(N1);
  N1=N1(:);
  N=floor(sqrt(N1));
  //TFTBcontinue=%t;
  sel=1:size(N,'*')
  while sel<>[],
    Nold(sel)=N(sel);
    M(sel)=ceil(N1(sel)./N(sel));
    N(sel)=floor(N1(sel)./M(sel));
    sel=find(N~=Nold);
  end;
  M=matrix(M,sz)
  N=matrix(N,sz)
endfunction
