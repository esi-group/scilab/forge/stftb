function g = gaussn(f0,n)
// generates the order n derivative of the  gaussian window, centered at frequency f0
// Calling Sequence
// function gn = gaussn(f0,n)
// Parameters
// alpha : std of the initial Gaussian 
// g_0(t) = sqrt(2) (alpha/2*pi)^(1/4) exp(-alpha t^2)
// Description
// The wavelet gn is real, but it is its analytic form that is 
// synthetized first. The real part is then normalized by the analytical
// value of its energy (so, L2 normalization!)
// Authors
// H. Nahrstaedt

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301
//  USA
  fname="gaussn";
  if nargin <>2 then
    error(msprintf(_("%s: Wrong number of input argument: %d expected.\n"),fname,2));
  end;
  
  //n
  
  if type(n)<>1|size(n,"*")<>1|~isreal(n)|int(n)<>n then
    error(msprintf(_("%s: Wrong type for input argument #%d: A integer value expected.\n"),fname,2));
  end 
  if n<=0 then
    error(msprintf(_("%s: Wrong value for input argument #%d: A positive integer expected.\n"),fname,2));
  end
 
  //f0
  if type(f0)<>1|size(f0,"*")>1|~isreal(f0) then
    error(msprintf(_("%s: Wrong type for input argument #%d: A real expected.\n"),fname,1));
  end 
  if f0<=0|f0>0.5 then
    error(msprintf(_("%s: Wrong value for input argument #%d: Must be in the interval %s.\n"),fname,1,"]0,0.5]"));
  end
  
 
  alpha = 2*%pi^2*f0^2 / n ;

  // N = number of points, depends on "n". The support of the wavelet 
  // at tolerance 10^(-3) is determined experimentaly and modeled by
  // a 4th order polynome (with reference frequency f0 is 0.1) 

  PolyCoef = [20.64720217778273   0.92639865727446  -0.00403466080616 ...
              0.00001199042535  -0.00000001420054];
  
  N = ceil((0.1/f0).*horner(poly(PolyCoef,"x","coeff"),n)) ;

  f = linspace(0,1,2*N+1) ; f = f(1:2*N) ;

  // Synthesis of the "analytic" wavelet Gn in the frequency domain 

  G = (2*%pi/alpha)^(1/4) * (2*%i*%pi*f).^n .* exp(-(%pi^2*f.^2)./alpha) ;

  // Calculus of the wavelet energy (theoretical expression, cf. Gratshteyn,
  // sec. 3.461, form. 2)

  p = (2*%pi^2)/alpha ;
  E = (2*%pi/alpha)^(1/2) * (2*%pi).^(2*n) * ...
      (gamma(2*n))/(2*(2*p)^n)*sqrt(%pi/p) ;
  if isinf(E) | isnan(E) | E == 0
    E = integ(abs(G).^2,f) ;
  end

  // L2 Normalized wavelet in time domain 
  g = real ( fftshift(ifft(G))./(sqrt(E/2)) ) ;
  g = g(2:2*N) ;
  t = -(N-1):(N-1) ;


endfunction


