function h=holder(tfr,f,n1,n2,t,opt_plot)
//Estimate the Holder exponent through an affine TFR.
// Calling Sequence
//	H=holder(TFR,F,N1,N2,T) 
// Parameters
//	TFR : affine time-frequency representation.  
//	F   : frequency values of the spectral analysis. 
//	N1  : indice of the  minimum frequency for the linear regression.    (default : 1).
//	N2  : indice of the  maximum frequency for the linear regression.   (default : length(F)).
//	T : time vector. If T is omitted, the function returns the global estimate of the Holder exponent. Otherwise, it  returns the local estimates H(T) at the instants specified  in T.  
//	H : output value (if T omitted) or vector (otherwise) containing   the Holder estimate(s).
// Description
//      holder estimates the Holder exponent of a function through an affine time-frequency representation of it, 
//	and plots the frequency marginal and the regression line.  
// Examples
//       S=altes(128); [TFR,T,F]=tfrscalo(S,1:128,8);
//       H=holder(TFR,F,1,length(F));
//   Authors
//      H. Nahrstaedt - Aug 2010
//	P. Goncalves, October 1995
//	Copyright (c) 1995 Rice University

//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  [nargout,nargin]=argn(0);
  fname="holder"
  
  in_par=['tfr','f','n1','n2','t','opt_plot'];
  in_par_min=2;
  plotting=%f;
  if (nargin>in_par_min)
    if (type(evstr(in_par(nargin)))==10)
      if convstr(evstr(in_par(nargin)))=="plot" then   plotting=%t; end; 
      clear evstr(in_par(nargin));
      nargin=nargin-1;
    end;
  end;
  clear in_par;
  if nargin <2 then
    error(msprintf(_("%s: Wrong number of input argument: %d to %d expected.\n"),fname,1,6));
  end;
  
  //tfr
  if type(tfr)<>1 then
    error(msprintf(_("%s: Wrong type for input argument #%d: A matrix of double expected.\n"),fname,1));
  end
  [tfrrow,tfrcol]=size(tfr);
  tfr = abs(tfr) ;

  //f
  if type(f)<>1|and(size(f)<>1)|~isreal(f)then
    error(msprintf(_("%s: Wrong type for input argument #%d: A real vector expected.\n"),fname,2));
  end
  if size(f,"*")<>tfrrow then
    error(msprintf(_("%s: Wrong size for input arguments #%d: %d expected.\n"),fname,2,tfrrow));
  end
  if or(f<0|f>0.5) then
    error(msprintf(_("%s: Wrong value for input argument #%d: The elements must be in %s\n"),fname,2,"]0,0.5]"))
  end
  
  //n1
  if nargin>=3
    if type(n1)<>1|size(n1,"*")>1|~isreal(n1)|int(n1)<>n1 then
      error(msprintf(_("%s: Wrong type for input argument #%d: A integer value expected.\n"),fname,3));
    end 
    if n1<=0|n1>tfrrow then
      error(msprintf(_("%s: Wrong value for input argument #%d: Must be in the interval %s.\n"),fname,3,"[1,"+string(tfrrow)+"]"));
    end
  else
    n1=1;
  end
  
  //n2
  if nargin>=4
    if type(n2)<>1|size(n2,"*")>1|~isreal(n2)|int(n2)<>n2 then
      error(msprintf(_("%s: Wrong type for input argument #%d: A integer value expected.\n"),fname,4));
    end 
    if n2<=0|n2>tfrrow then
      error(msprintf(_("%s: Wrong value for input argument #%d: Must be in the interval %s.\n"),fname,4,"[1,"+string(tfrrow)+"]"));
  else
    n2=length(f)
  end
  
  //t
  if nargin>=5 then
    if type(t)<>1|and(size(t)<>1)|~isreal(t)|or(int(t)<>t) then
      error(msprintf(_("%s: Wrong type for input argument #%d: A vector of integer values expected.\n"),fname,5));
    end
    if or(t<=0|t>tfrcol) then
      error(msprintf(_("%s: Wrong value for input argument #%d: The elements must be in the interval %s.\n"),fname,5,"[1,"+string(N)+"]"));
    end
  else
    t=[]
  end

  if plotting, clf; end;

  if t==[] then
    fmarg = mean(tfr,2) ;
    p = polyfit(log(f(n1:n2)),log(fmarg(n1:n2)),1) ;
    drt = p(1)*log(f)+p(2) ;
    if plotting,
      plot(log(f),log(fmarg)) ;
      plot(log(f),drt,'--g') ; 
      legend(_('Frequency marginal'),_('Regression line'));
      plot(log(f([n1 n2])),log(fmarg([n1 n2])),'+r') ;
      xticklabels = round(logspace(log10(min(f)),log10(max(f)),4)*1000)./1000 ;
      xlabel(_('frequency (logarithmically spaced)')) ;
    end
    h = (-p(1)-1)/2 ;
  elseif length(t) == 1 then
      p = polyfit(log(f(n1:n2)),log(tfr(n1:n2,t)),1) ;
      drt = p(1)*log(f)+p(2) ;
      if plotting then
        plot(log(f),log(tfr(:,t))) ;
        plot(log(f),drt,'--g') ;
        legend(_('Frequency marginal'),_('Regression line'));
        plot(log(f([n1 n2])),log(tfr([n1 n2],t)),'+r') ;
        xticklabels = round(logspace(log10(min(f)),log10(max(f)),4)*1000)./1000 ;
        xlabel(_('frequency (logarithmically spaced)')) ;      
      end
      h = (-p(1)-1)/2 ;
  elseif length(t) > 1 
      [yt,xt] = size(t) ; if yt>xt, t = t.' ; end ;
      j = 1 ;
      for k = t,
        p = polyfit(log(f(n1:n2)),log(tfr(n1:n2,k)),1) ;
        h(j) = (-p(1)-1)/2 ; j = j + 1 ;
      end
      if plotting then
        plot(t,h) ;xgrid
        title(_('Holder estimates at time instants T'));
      end
    end,
  end
endfunction

function p = polyfit(x, y, n, s)
// return coefficient vector or poly if fourth string argument given
  [lhs, rhs] = argn(0)
  x = x(:); y = y(:)
  m = length(x)
  if length(y) <> m, error('x and y must have same length'), end
  v = ones(m,n+1)
  for i=2:n+1, v(:,i) = x.*v(:,i-1), end
  p = (v\y)'
  p=p($:-1:1);
  if rhs > 3, p = poly(p, s, 'coeff'), end 
endfunction
