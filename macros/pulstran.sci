function y = pulstran(t, d, pulse, varargin)
// puls generation
//Calling Sequence
//y=pulstran(t,d,func,...)
//y=pulstran(t,d,p,Fs,'interp')
//Parameters
// func
//   'rectpuls' :  rectangular pulse, parameter are (w) (default w=1)
//    'tripuls' :   triangular pulse,  parameter are (w, skew) (default w=1,skew=0)
//    'gmonopuls':   gaussian monopulse, parameter are (fc) (default fc=1e3)
//    'gauspuls':    Gaussian modulated sinusoidal pulse, parameter are (fc, bw) (default fc=1e3, bw=0.5)
// Description
// Generate the signal y=sum(func(t+d,...)) for each d.  If d is a
// matrix of two columns, the first column is the delay d and the second
// column is the amplitude a, and y=sum(a*func(t+d)) for each d,a.
// Clearly, func must be a function which accepts a vector of times.
// Any extra arguments needed for the function must be tagged on the end.
//
// If instead of a function name you supply a pulse shape sampled at
// frequency Fs (default 1 Hz),  an interpolated version of the pulse
// is added at each delay d.  The interpolation stays within the the
// time range of the delayed pulse.  The interpolation method defaults
// to linear, but it can be any interpolation method accepted by the
// function interp1.
//
// Examples
//   fs = 11025;  // arbitrary sample rate
//   f0 = 100;    // pulse train sample rate
//   w = 0.001;   // pulse width of 1 millisecond
//   plot(pulstran(0:1/fs:0.1, 0:1/f0:0.1, 'rectpuls', w));
//
//   fs = 11025;  // arbitrary sample rate
//   f0 = 100;    // pulse train sample rate
//   w = ones(10,1);  // pulse width of 1 millisecond at 10 kHz
//   plot(pulstran(0:1/fs:0.1, 0:1/f0:0.1, w, 10000));
//Authors
// Copyright (C) 2000 Paul Kienzle


// Note that pulstran can be used for some pretty strange things such
// as simple band-limited interpolation:
//     xf = 0:0.05:10; yf = sin(2*pi*xf/5);
//     xp = 0:10; yp = sin(2*pi*xp/5); # .2 Hz sine sampled every second
//     s = pulstran(xf, [xp, yp],'sinc'); 
//     plot(f, yf, ";original;", xf, s, ";sinc;",xp,yp,"*;;");
// You wouldn't want to do this in practice since it is expensive, and
// since it works much better with a windowed sinc function, at least
// for short samples.
// Copyright (C) 2000 Paul Kienzle
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; If not, see <http://www.gnu.org/licenses/>.
  [nargout,nargin]=argn(0);
  fname="pulstran";
  if nargin < 3 then
    error(msprintf(_("%s: Wrong number of input argument: at least %d expected.\n"),fname,3));
  end
  
  //t
  szt=size(t)
  if type(t)<>1|and(szt>1)|~isreal(t) then
    error(msprintf(_("%s: Wrong type for input argument #%d: A real vector expected.\n"),fname,1));
  end
  t=t(:)
  if t==[] then
    y=[]
    return;
  end

 
  //d
  szd=size(d)
   if type(d)<>1|~isreal(d) then
    error(msprintf(_("%s: Wrong type for input argument #%d: A real array expected.\n"),fname,2));
   end
   if or(szd==1) then
     d=d(:)
   elseif szd(2)==2 then
     a=d(:,2)
   else
     error(msprintf(_("%s: Wrong size for input argument #%d: A vector or a two colulmns array expected.\n"),fname,2));
   end
   
  //pulse or Fs
  
  
  if nargin<3 | (~type(pulse)==10 & nargin>5)
    error("y=pulstran(t,d,''func'',...) or y==pulstran(t,d,p,Fs,''interp'')");
  end
 

  if type(pulse)==10 then
    if size(pulse,"*")<>1 then
       error(msprintf(_("%s: Wrong size for input argument #%d: A character string expected.\n"),fname,3));
    end
    pulse=convstr(pulse,"l")
    
    nv=size(varargin)
    for i=1:nv
      w=varargin(i)
      if type(w)<>1|or(size(w)<>1)|~isreal(w) then
        error(msprintf(_("%s: Wrong type for input argument #%d: A real scalar expected.\n"),fname,4));
      end
    end
    
    //check parameters
    select pulse
    case 'rectpuls'
      if nv==0 then
        w=1
      else
        w=varargin(1)
        if w<=0 then
          error(msprintf(_("%s: Wrong value for input argument #%d: Must be > %d.\n"),fname,4,0)); 
        end
      end
    case 'tripuls'  
      if nv<1 then
        w=1;
      else
        w=varargin(1)
        if w<=0 then
          error(msprintf(_("%s: Wrong value for input argument #%d: Must be > %d.\n"),fname,4,0)); 
        end
      end
      if nv<2 then
        skew=0
      else
        skew=varargin(2)
        if type(skew)<>1|or(size(skew)<>1)|~isreal(skew) then
          error(msprintf(_("%s: Wrong type for input argument #%d: A real scalar expected.\n"),fname,5));
        end
        if skew<=0 then
          error(msprintf(_("%s: Wrong value for input argument #%d: Must be >= %d.\n"),fname,5,0)); 
        end
      end
    case 'gmonopuls'
      if nv==0 then
        fc=1e3
      else
        fc=varargin(1)
        if fc<=0 then
          error(msprintf(_("%s: Wrong value for input argument #%d: Must be > %d.\n"),fname,4,0)); 
        end
      end
    case 'gauspuls'
      if nv<1 then
        fc=1e3;
      else
        fc=varargin(1)
        if fc<=0 then
          error(msprintf(_("%s: Wrong value for input argument #%d: Must be > %d.\n"),fname,4,0)); 
        end
      end
      if nv<2 then
        bw=0.5
      else
        bw=varargin(2)
        if type(bw)<>1|or(size(bw)<>1)|~isreal(bw) then
          error(msprintf(_("%s: Wrong type for input argument #%d: A real scalar expected.\n"),fname,5));
        end
        if bw<=0 then
          error(msprintf(_("%s: Wrong value for input argument #%d: Must be >= %d.\n"),fname,5,0)); 
        end
      end
    else
      error(msprintf(_("%s: Wrong value for input argument #%d: Must be in the set {%s}.\n"),fname,3,"""rectpuls"",""tripuls"",""gmonopuls"",""gauspuls"""))
    end

    // apply function t+d for all d
    for i=1:size(d,1)
      t_t=t-d(i,1);
      select pulse
      case 'rectpuls'
        y_t = zeros(size(t_t,1),1);
        idx = find(t_t>=-w/2 & t_t < w/2);
        y_t(idx) = ones(max(size(idx)),1);
      case 'tripuls'
        y_t = zeros(size(t_t,1),1);
        peak = skew*w/2;
        idx = find(t_t>=-w/2 & t_t <= peak);
        if (idx) y_t(idx) = ( t_t(idx) + w/2 ) / ( peak + w/2 ); end
        idx = find(t_t>peak & t_t < w/2);
        if (idx) y_t(idx) = ( t_t(idx) - w/2 ) / ( peak - w/2 ); end
      case  'gmonopuls'
        y_t = 2*sqrt(exp(1)) .* %pi.*t_t.*fc.*exp(-2 .* (%pi.*t_t.*fc).^2);
      case 'gauspuls'
        fv = -(bw.^2 .*fc.^2)/(8.*log(10.^(-6/20)));
        tv = 1/(4 .*%pi.^2 .*fv);
        y_t = exp(-t_t.*t_t/(2 .*tv)).*cos(2 .*%pi.*fc.*t_t);
      end
      y = y+a(i)*y_t;
    end
  else
    // interpolate each pulse at the specified times
    Fs = 1; method = 'linear';
    if (nargin==4),
      arg=varargin(1);
      if type(arg)==10, 
	method=arg;
      else
	Fs = arg;
      end
    elseif (nargin==5),
      Fs = varargin(1);
      method = varargin(2);
    end
    span = (length(pulse)-1)/Fs;
    t_pulse = (0:length(pulse)-1)/Fs;
    for i=1:size(d,1)
      dt = t-d(i,1);
      idx = find(dt>=0 & dt<=span);
      y(idx) = y(idx) + a(i)*interp1(t_pulse, pulse, dt(idx), method);
    end
  end
  if t_is_col then
    y=matrix(y,szt)
  end;
endfunction

//error pulstran
//error pulstran(1,2,3,4,5,6)

//// parameter size and shape checking
//shared t,d
// t = 0:0.01:1; d=0:0.1:1;
//assert (isempty(pulstran([], d, 'sin')));
//assert (pulstran(t, [], 'sin'), zeros(size(t)));
//assert (isempty(pulstran([], d, boxcar(5))));
//assert (pulstran(t, [], boxcar(5)), zeros(size(t)));
//assert (size(pulstran(t,d,'sin')), size(t));
//assert (size(pulstran(t,d','sin')), size(t));
//assert (size(pulstran(t',d,'sin')), size(t'));
//assert (size(pulstran(t,d','sin')), size(t));

//demo
// fs = 11025;                   # arbitrary sample rate
// f0 = 100;                     # pulse train sample rate
// w = 0.003;                    # pulse width of 3 milliseconds
// t = 0:1/fs:0.1; d=0:1/f0:0.1; # define sample times and pulse times 
// a = hanning(length(d));       # define pulse amplitudes
//
// subplot(221); title("rectpuls");
// auplot(pulstran(t', d', 'rectpuls', w), fs);
// hold on; plot(d*1000,ones(size(d)),'g*;pulse;'); hold off;
//
// subplot(223); title("sinc => band limited interpolation");
// auplot(pulstran(f0*t, [f0*d', a], 'sinc'), fs);
// hold on; plot(d*1000,a,'g*;pulse;'); hold off;
//
// subplot(222); title("interpolated boxcar");
// pulse = boxcar(30);  # pulse width of 3 ms at 10 kHz
// auplot(pulstran(t, d', pulse, 10000), fs);
// hold on; plot(d*1000,ones(size(d)),'g*;pulse;'); hold off;
//
// subplot(224); title("interpolated asymmetric sin");
// pulse = sin(2*pi*[0:0.0001:w]/w).*[w:-0.0001:0];
// auplot(pulstran(t', [d', a], pulse', 10000), fs);
// hold on; plot(d*1000,a*w,'g*;pulse;'); hold off; title("");
// oneplot();
// %----------------------------------------------------------
// % Should see (1) rectangular pulses centered on *,
// %            (2) rectangular pulses to the right of *,
// %            (3) smooth interpolation between the *'s, and
// %            (4) asymetric sines to the right of *
