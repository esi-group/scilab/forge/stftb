function  [tfr,t,f]=tfrdfla(X,time,fmin,fmax,N,ptrace,opt_plot);
// D-Flandrin time-frequency distribution.
// Calling Sequence
//	[TFR,T,F]=tfrdfla(X) 
//	[TFR,T,F]=tfrdfla(X,T) 
//	[TFR,T,F]=tfrdfla(X,T,FMIN,FMAX) 
//	[TFR,T,F]=tfrdfla(X,T,FMIN,FMAX,N) 
//	[TFR,T,F]=tfrdfla(X,T,FMIN,FMAX,N,TRACE) 
//	[TFR,T,F]=tfrdfla(...,'plot') 
//   Parameters
//	X : signal (in time) to be analyzed. If X=[X1 X2], tfrdfla    computes the cross-D-Flandrin distribution (Nx=length(X)).
//	T : time instant(s) on which the TFR is evaluated (default : 1:Nx).
//	FMIN,FMAX : respectively lower and upper frequency bounds of    the analyzed signal. These parameters fix the equivalent   frequency bandwidth (expressed in Hz). When unspecified, you	   have to enter them at the command line from the plot of the  spectrum. FMIN and FMAX must be >0 and <=0.5.	 
//	N : number of analyzed voices (default : automatically determined).
//	TRACE : if nonzero, the progression of the algorithm is shown	default : 0).
//      'plot':	if one input parameter is 'plot',  tfrdfla runs tfrqview. and TFR will be plotted
//	TFR : time-frequency matrix containing the coefficients of the  decomposition (abscissa correspond to uniformly sampled   time, and ordonates correspond to a geometrically sampled    frequency). First row of TFR corresponds to the lowest   frequency.
//	F : vector of normalized frequencies (geometrically sampled   from FMIN to FMAX).
//      Description
//         tfrdfla generates the auto- or cross- D-Flandrin distribution. 
//      Examples
//       sig=altes(64,0.1,0.45); tfrdfla(sig,'plot'); 
//     Authors
//      H. Nahrstaedt - Aug 2010
//	P. Goncalves, October 95 - O. Lemoine, June 1996. 
//	Copyright (c) 1995 Rice University - CNRS (France) 1996.

//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  [nargout,nargin]=argn(0);
  fname="tfrdfla"

  in_par=['X','time','fmin','fmax','N','ptrace','opt_plot'];
  in_par_min=1;
  plotting=%f;
  if (nargin>in_par_min)
    if (type(evstr(in_par(nargin)))==10)
      if convstr(evstr(in_par(nargin)))=="plot" then
        plotting=%t; 
        nargin=nargin-1;
      end; 
    end;
  end;
  clear in_par;

   if nargin == 0 then
    error(msprintf(_("%s: Wrong number of input argument: %d to %d expected.\n"),fname,1,7));
  end;
  
  //X
  if type(X)<>1 then
    error(msprintf(_("%s: Wrong type for input argument #%d: A matrix of double expected.\n"),fname,1));
  end
  if or(size(X)==1) then
    X=X(:);
  else
    if size(X,2)<>2 then
      error(msprintf(_("%s: Wrong size for input argument #%d: A  vector or a two columns array expected.\n"),fname,1));
    end
  end
  [xrow,xcol] = size(X);
 
  //ptrace
  if nargin>=6 then 
    if and(type(ptrace)<>[1 4])|size(ptrace,"*")<>1 then
      error(msprintf(_("%s: Wrong type for argument #%d: A boolean or real scalar expected.\n"),fname,6));
    end
  else
    ptrace=%f;
  end
  
  //N
  if nargin>=5 then
    if type(N)<>1|size(N,"*")>1|~isreal(N)|int(N)<>N then
      error(msprintf(_("%s: Wrong type for input argument #%d: A integer value expected.\n"),fname,5));
    end 
    if N<1 then
       error(msprintf(_("%s: Wrong value for input argument #%d: Must be >=%d.\n"),fname,5,1));
    end
  else
    N=[];
  end
  
  //fmax
  if nargin>=4 then
     if type(fmax)<>1|size(fmax,"*")>1|~isreal(fmax) then
       error(msprintf(_("%s: Wrong type for input argument #%d: A real expected.\n"),fname,4));
     end 
     if fmax<=0|fmax>0.5 then
       error(msprintf(_("%s: Wrong value for input argument #%d: Must be in the interval %s.\n"),fname,4,"]0,0.5]"));
     end
  else
    fmax=[]
  end
  
  //fmin
  if nargin>=3 then
     if type(fmin)<>1|size(fmin,"*")>1|~isreal(fmin) then
       error(msprintf(_("%s: Wrong type for input argument #%d: A real expected.\n"),fname,3));
     end 
     if fmin<=0|fmin>0.5 then
       error(msprintf(_("%s: Wrong value for input argument #%d: Must be in the interval %s.\n"),fname,3,"]0,0.5]"));
     end
     if fmin>fmax then
       error(msprintf(_("%s: Wrong value for input argument #%d: Must be < %g.\n"),fname,2,fmax));
     end
  else
    fmin=[]
  end

  //time
  if nargin>=2 then
    if type(time)<>1|and(size(time)<>1)|~isreal(time) then
      error(msprintf(_("%s: Wrong type for input argument #%d: A real vector expected.\n"),fname,2));
    end
     if max(time)>xrow|min(time)<1 then
      error(msprintf(_("%s: Wrong value for input argument #%d: The elements must be in the interval %s.\n"),fname,2,"[1 "+string(xrow)+"]"));
    end
    time=matrix(time,1,-1)
  else
     time=1:xrow; 
  end
  [trow,tcol] = size(time);
  
  
  
 

  
  Mt = length(X); 

  if ptrace, disp('D-Flandrin distribution'); end;

  if xcol==1,
    X1=X;
    X2=X; 
  else
    X1=X(:,1);
    X2=X(:,2);
  end
  s1 = real(X1);
  s2 = real(X2);
  M  = (Mt+rem(Mt,2))/2;

  t = (1:Mt)-M-1;
  T = xrow;

    if fmin==[]|fmax==[] then	        // fmin,fmax unspecified
    STF1 = fft(fftshift(s1(min(time):max(time)))); Nstf=length(STF1);
    sp1 = (abs(STF1(1:Nstf/2))).^2; Maxsp1=max(sp1);
    STF2 = fft(fftshift(s2(min(time):max(time)))); 
    sp2 = (abs(STF2(1:Nstf/2))).^2; Maxsp2=max(sp2);
    f = linspace(0,0.5,Nstf/2+1) ; f=f(1:Nstf/2);
    plot(f,sp1) ; xgrid;  plot(f,sp2) ; 
    xlabel(_('Normalized frequency'));
    title(_('Analyzed signal energy spectrum')); 
    a=gca();a.data_bounds=([0 1/2 0 1.2*max(Maxsp1,Maxsp2)]) ; 
    if fmin==[] then
      indmin=min(find(sp1>Maxsp1/100));
      fmindflt=max([0.01 0.05*fix(f(indmin)/0.05)]);
      txtmin=msprintf(_('Lower frequency bound [%s] :'),string(fmindflt));
      while  %t then
        fmin = input(txtmin); 
        if fmin==[] then 
          fmin=fmindflt; break
        elseif fmin>0&fmin<=0.5 then
          break
        else
          mprintf(_("%s: Wrong given value for lower frequency bound: Must be in the interval %s.\n"),fname,"]0,0.5]"); 
        end
      end
    end
    if fmax==[] then
      indmax=max(find(sp1>Maxsp1/100));
      fmaxdflt=0.05*ceil(f(indmax)/0.05);
      txtmax=msprintf(_('Upper frequency bound [%s] :'),string(fmaxdflt));
      while  %t then
        fmax = input(txtmax); 
        if fmax==[] then 
          fmax=fmaxdflt; break
        elseif fmax>0&fmax<=0.5&fmax>fmin then
          break
        else
          mprintf(_("%s: Wrong given value for upper frequency bound: Must be in the interval %s and greater than fmin\n"),fname,"]0,0.5]"); 
        end
      end  
    end
  end


  B = fmax-fmin ; 
  R = B/((fmin+fmax)/2) ; 
  ratio_f = fmax/fmin ; 
  //global ratio_f
  //umax = fzero('umaxdfla',0); 
  //There are 2 solutions take the one in ]-4 4[
  umax=[(4*(ratio_f+1+2*sqrt(ratio_f)))/(ratio_f-1);-(4*(-ratio_f-1+2*sqrt(ratio_f)))/(ratio_f-1)]
  i=find(umax>-4&umax<4,1)
  umax=umax(i);
  Teq = M/(fmax*umax);  
  if Teq<2*M,
    M0 = round((2*M^2)/Teq-M)+1;
    T  = 2*(M+M0)-1;
  else 
    M0 = 0;
  end;
  M1 = M+M0;

  Nq= ceil((B*T*(1+2/R)*log((1+R/2)/(1-R/2)))/2);
  Nmin = Nq-rem(Nq,2);
  Ndflt = 2^nextpow2(Nmin);
  if nargin<=3,
    Ntxt=['Number of frequency samples (>='+string(Nmin)+') ['+string(Ndflt)+'] : '];
    N = input(Ntxt);
  end
  if ~isempty(N),
    if (N<Nmin),
      dispstr=['Warning : the number of analyzed voices (N) should be > '+string(Nmin)];
      disp(dispstr);
    end
  else
    N=Ndflt; 
  end

  fmin_s = string(fmin) ; fmax_s = string(fmax) ; N_s = string(N) ;
  if ptrace,
    disp(['frequency runs from '+fmin_s+' to '+fmax_s+' with '+N_s+' points']);
  end


  // Geometric sampling of the analyzed spectrum
  k = 1:N;
  q = (fmax/fmin)^(1/(N-1));
  t = (1:Mt)-M-1;
  geo_f  = fmin*(exp((k-1).*log(q)));
  tfmatx = zeros(Mt,N);
  tfmatx = exp(-2*%i*t'*geo_f*%pi);
  S1 = s1'*tfmatx; 
  S2 = s2'*tfmatx; 
  clear tfmatx
  S1(N+1:2*N) = zeros(1,N);
  S2(N+1:2*N) = zeros(1,N);


  // Mellin transform computation of the analyzed signal
  p = 0:(2*N-1);
  Mellin1 = fftshift(ifft(S1));
  Mellin2 = fftshift(ifft(S2));
  umin = -umax;
  du = abs(umax-umin)/(2*M1);
  u(1:2*M1) = umin:du:umax-du;
  u(M1+1) = 0;
  Beta = (p/N-1)./(2*log(q));

  // Computation of the Lambda(+/- u) dilations/compressions 
  // of the analyzed signal
  waf = zeros(2*M1,N);
  for n = 1:2*M1,
    if ptrace, disprog(n,4*M1,10); end
    MX1 = exp(-(2*%i*%pi*Beta+0.5)*2*log((1-u(n)/4))).*Mellin1;
    MX2 = exp(-(2*%i*%pi*Beta+0.5)*2*log((1+u(n)/4))).*Mellin2;
    FX1 = fft(fftshift(MX1)) ;
    FX1 = FX1(1:N) ;
    FX2 = fft(fftshift(MX2)) ;
    FX2 = FX2(1:N) ;
    waf(n,:) = FX1.*conj(FX2);
  end
  waf = [waf(M1+1:2*M1,:) ; waf(1:M1,:)].*geo_f(ones(2*M1,1),:);
  tffr = fft(waf,1,1);  
  tffr = real(rot90([tffr(M1+1:2*M1,:) ; tffr(1:M1,:)],-1));


  // Conversion from [t.f,f] to [t,f] using a 1-D interpolation
  tfr  = zeros(N,tcol);
  Ts2  = (Mt-1)/2 ;
  tgamma = linspace(-geo_f(N)*Ts2,geo_f(N)*Ts2,2*M1) ;
  alpha = (0.6*N-1)/0.4;
  for n = 1:N,
    if ptrace, disprog(n+alpha,N+alpha,10); end
    ind = find(tgamma>=-geo_f(n)*Ts2 & tgamma<=geo_f(n)*Ts2);
    x = tgamma(ind);
    y = tffr(n,ind);
    xi = (time-Ts2-1)*geo_f(n);
    v=interp1(x,y,xi,'spline');
    [l,r]=size(v);
    if (r==1),
      v=v';
    end;
    tfr(n,:)=v;
    clear v 
  end 


  t = time;
  f = geo_f';

  // Normalization
  SP1 = fft(hilbert(s1)); 
  SP2 = fft(hilbert(s2)); 
  indmin = 1+round(fmin*(tcol-2));
  indmax = 1+round(fmax*(tcol-2));
  SP1ana=SP1(indmin:indmax);
  SP2ana=SP2(indmin:indmax);
  tfr=tfr*(SP1ana'*SP2ana)/integ2d(tfr,t,f)/N;

  clear ratio_f

  if plotting then
    tfrqview(real(tfr),hilbert(real(X)),t,'tfrdfla',N,f);
  end;

endfunction



