function y = rot90 (x, k)
// rotates the given matrix clockwise by 90 degrees. 
// Calling Sequence
// y = rot90 (x, k)
// Description
// Return a copy of x with the elements rotated counterclockwise in
// 90-degree increments.  The second argument is optional, and specifies
// how many 90-degree rotations are to be applied (the default value is 1).
// Negative values of @var{n} rotate the matrix in a clockwise direction.

// Copyright (C) 1993, 1994, 1995, 1996, 1997, 1999, 2000, 2004, 2005,
//               2006, 2007, 2008, 2009 John W. Eaton
//
// This file is part of Octave.
//
// Octave is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// Octave is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Octave; see the file COPYING.  If not, see
// <http://www.gnu.org/licenses/>.

// Author: jwe
  [nargout,nargin]=argn(0);
  fname="rot90";
  if nargin < 1 then
    error(msprintf(_("%s: Wrong number of input argument: %d to %d expected.\n"),fname,1, 2));
  end
  
  if nargin==2 then
    if type(k)<>1|~isreal(k)|or(size(k)<>1)|round(k)<>k then
      error(msprintf(_("%s: Wrong type for input argument #%d: An integer value expected.\n"),fname,2));
    end
  else
    k=1
  end
  
  if (ndims (x) > 2)
    error(msprintf(_("%s: Wrong size for input argument #%d: A 2-D array expected.\n"),fname,2));
  end


  k = pmodulo (k, 4);
  select k
  case 0 then
    y=x
  case 1 then
    y=x.'
    y=x($:-1:1,:)
  case 2 then
     y=x($:-1:1,$:-1:1)
  case 3 then
    y=x($:-1:1,:).'
  end
 

endfunction

//test
// x1 = [1, 2;
// 3, 4];
// x2 = [2, 4;
// 1, 3];
// x3 = [4, 3;
// 2, 1];
// x4 = [3, 1;
// 4, 2];
// 
// assert((rot90 (x1)== x2 && rot90 (x1, 2) == x3 && rot90 (x1, 3) == x4
// && rot90 (x1, 4) == x1 && rot90 (x1, 5) == x2 && rot90 (x1, -1) == x4));

//error rot90 ();

//error rot90 (1, 2, 3);

