function [tfr,rtfr,rhat] = tfrrgab(x,t,N,Nh,ptrace,K,opt_plot);
// Reassigned Gabor spectrogram time-frequency distribution.
// Calling Sequence
//	[TFR,RTFR,HAT] = tfrrgab(X) 
//	[TFR,RTFR,HAT] = tfrrgab(X,T) 
//	[TFR,RTFR,HAT] = tfrrgab(X,T,N) 
//	[TFR,RTFR,HAT] = tfrrgab(X,T,N,NH) 
//	[TFR,RTFR,HAT] = tfrrgab(X,T,N,NH,TRACE) 
//	[TFR,RTFR,HAT] = tfrrgab(X,T,N,NH,TRACE,K) 
//	[TFR,RTFR,HAT] = tfrrgab(...,'plot') 
//   Parameters
//	X     : analysed signal
//	T     : the time instant(s)           (default : 1:length(X))
//	N     : number of frequency bins      (default : length(X))
//	NH    : length of the gaussian window (default : N/4))
//	TRACE : if nonzero, the progression of the algorithm is shown      (default : 0).
//	K     : value at both extremities     (default 0.001)
//      'plot':	if one input parameter is 'plot',  tfrrgab runs tfrqview. and TFR will be plotted
//	TFR,  : time-frequency representation and its reassigned
//	RTFR    version.
//	HAT   : Complex matrix of the reassignment vectors.
// Description
//	tfrrgab computes the Gabor spectrogram and its reassigned version.
//	This particular window (a Gaussian window) allows a 20 % faster
//	algorithm than the tfrrsp function.
//    Examples
//        sig=fmlin(128,0.1,0.4); tfrrgab(sig,1:128,128,19,1,'plot');
//     Authors
//      H. Nahrstaedt - Aug 2010
//	F. Auger, May-July 1994, July 1995. 
//       Copyright (c) 1996 by CNRS(France). 

// 
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  [nargout,nargin]=argn(0);
  fname="tfrrgab"


  in_par=['x','t','N','Nh','ptrace','K','opt_plot'];
  in_par_min=1;
  plotting=%f;
  if (nargin>in_par_min)
    if (type(evstr(in_par(nargin)))==10)
      if convstr(evstr(in_par(nargin)))=="plot" then
        plotting=%t; 
        nargin=nargin-1;
      end; 
    end;
  end;
  clear in_par;

  if nargin == 0 then
    error(msprintf(_("%s: Wrong number of input argument: %d to %d expected.\n"),fname,1,7));
  end
  
  //x
  if type(x)<>1|and(size(x)<>1) then
    error(msprintf(_("%s: Wrong type for input argument #%d: A vector of double expected.\n"),fname,1));
  end
  x=x(:);
  xrow=size(x,1);
  
  //t
  if nargin>=2 then
    if type(t)<>1|and(size(t)>1)|~isreal(t) then
      error(msprintf(_("%s: Wrong type for input argument #%d: A real vector expected.\n"),fname,2));
    end
    if max(t)>xrow|min(t)<1 then
      error(msprintf(_("%s: Wrong value for input argument #%d: The elements must be in the interval %s.\n"),fname,2,"[1 "+string(xrow)+"]"));
    end
    t=matrix(t,1,-1)
  else
    t=1:xrow;
  end
  tcol=size(t,2);
  
  
  //N
  if nargin>=3 then
    if type(N)<>1|size(N,"*")>1|~isreal(N)|int(N)<>N then
      error(msprintf(_("%s: Wrong type for input argument #%d: A integer value expected.\n"),fname,3));
    end 
    if N<=0 then
      error(msprintf(_("%s: Wrong value for input argument #%d: A positive integer expected.\n"),fname,3));
    end
 
  else
    N=xrow
  end
  
 
  //Nh
  if nargin >=4 then
    if type(Nh)<>1|size(Nh,"*")>1|~isreal(Nh)|int(Nh)<>Nh then
      error(msprintf(_("%s: Wrong type for input argument #%d: A integer value expected.\n"),fname,4));
    end 
    if Nh<=0 then
      error(msprintf(_("%s: Wrong value for input argument #%d: A positive integer expected.\n"),fname,4));
    end
    if modulo(Nh,2)==0 then
      error(msprintf(_("%s: Wrong vale for argument #%d: A odd number expected.\n"),fname,4));
      end
  else
    Nh=floor(N/4);
    Nh=Nh+1-rem(Nh,2);
  end

 //ptrace
  if nargin>=5 then 
    if and(type(ptrace)<>[1 4])|size(ptrace,"*")<>1 then
      error(msprintf(_("%s: Wrong type for argument #%d: A boolean or real scalar expected.\n"),fname,5));
    end
  else
    ptrace=%f;
  end
  
  //K
  if nargin>=6 then 
    if type(K)<>1|size(K,"*")>1|~isreal(K) then
       error(msprintf(_("%s: Wrong type for input argument #%d: A real expected.\n"),fname,6));
     end 
  else
    K=0.001
  end
 

  Nh2=Nh-2;
  TFTBcontinue=%t;
  while TFTBcontinue,
    Nh2=Nh2+2;
    h=tftb_window(Nh2,'gauss',K^((Nh2-1)^2 /(Nh-1)^2)); 
    TFTBcontinue=h(Nh2)*(Nh2-1)>2*K;
  end;

  K=K^((Nh2-1)^2 /(Nh-1)^2); 
  Nh=Nh2; 
  Lh=(Nh-1)/2; 
  Th=h.*[-Lh:Lh]';

  if (tcol==1),
    Dt=1; 
  else
    Deltat=diff(t); 
    Mini=min(Deltat); Maxi=max(Deltat);
    if (Mini~=Maxi),
      error(msprintf(_("%s: Wrong value for input argument #%d: The elements must be regularly spaced.\n"),fname,2))
    else
      Dt=Mini;
    end;
    clear Deltat Mini Maxi;
  end;

  tfr= zeros(N,tcol); 
  tf2= zeros(N,tcol); 
  tf3= zeros(N,tcol);
  if ptrace, disp('Gabor spectrogram'); end;

  for icol=1:tcol,
    if ptrace, disprog(icol,tcol,10); end;
    ti= t(icol); 
    tau=-min([round(N/2)-1,Lh,ti-1]):min([round(N/2)-1,Lh,xrow-ti]);
    indices= rem(N+tau,N)+1;
    norm_h=norm(h(Lh+1+tau));
    tfr(indices,icol)=x(ti+tau).*conj( h(Lh+1+tau)) /norm_h;
    tf2(indices,icol)=x(ti+tau).*conj(Th(Lh+1+tau)) /norm_h;
  end ;
  tfr=fft(tfr,-1,1); tf2=fft(tf2,-1,1);


  tfr=tfr(:); tf2=tf2(:);  tf3=tf3(:);
  avoid_warn=find(tfr~=0.0);
  tf3(avoid_warn)=round(imag(2*log(K)*N*tf2(avoid_warn)./tfr(avoid_warn)/(2.0*%pi*Lh^2)));
  tf2(avoid_warn)=round(real(tf2(avoid_warn)./tfr(avoid_warn)/Dt));
  tfr=abs(tfr).^2;
  if ptrace, printf ('\nreassignment: \n'); end;
  tfr=matrix(tfr,N,tcol);
  tf2=matrix(tf2,N,tcol);
  tf3=matrix(tf3,N,tcol);

  rtfr= zeros(N,tcol); 
  Ex=mean(abs(x(min(t):max(t))).^2); Threshold=1.0e-6*Ex;
  for icol=1:tcol,
    if ptrace, disprog(icol,tcol,10); end;
    for jcol=1:N,
      if abs(tfr(jcol,icol))>Threshold,
        icolhat= real(icol + tf2(jcol,icol));
        icolhat=min(max(icolhat,1),tcol);
        jcolhat= jcol - tf3(jcol,icol);
       
        jcolhat=rem(rem(jcolhat-1,N)+N,N)+1;
        rtfr(jcolhat,icolhat)=rtfr(jcolhat,icolhat) + tfr(jcol,icol) ;
        tf2(jcol,icol)=jcolhat + %i * icolhat;
      else
        tf2(jcol,icol)=%inf*(1+%i);
        rtfr(jcol,icol)=rtfr(jcol,icol) + tfr(jcol,icol) ;
      end;
    end;
  end;

  if ptrace, printf('\n'); end;
  clear tf3;
  if (plotting),
    TFTBcontinue=%t;
    while TFTBcontinue
      choice=x_choose([ 'Gabor spectrogram',...
                    'reassigned Gabor spectrogram'],'Choose the representation:','stop');
      if (choice==0) then
        TFTBcontinue=%f;
      elseif (choice==1), 
        Q=round(tcol*N/xrow);
        tfrqview(tfr,x,t,'tfrgabor',tcol,Q,h);
      elseif (choice==2),
        tfrqview(rtfr,x,t,'type1',Nh);
      end;
    end;
  end
  if (nargout>2),
    rhat=tf2;
  end;

endfunction
