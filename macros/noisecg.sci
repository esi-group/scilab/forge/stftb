function noise=noisecg(N,a1,a2)
// Analytic complex gaussian noise.
// Calling Sequence
//     NOISE=noisecg(N)
//    NOISE=noisecg(N,A1)
//    NOISE=noisecg(N,A1,A2)
// Description
//	NOISE=noisecg(N,A1,A2) computes an analytic complex gaussian
//	noise of length N with mean 0.0 and variance 1.0. 
//
//	NOISE=noisecg(N) yields a complex white gaussian noise.
//
//	NOISE=noisecg(N,A1) yields a complex colored gaussian noise
//	obtained by filtering a white gaussian noise through a
//		sqrt(1-A1^2)/(1-A1*z^(-1)) 
//	first order filter.
//
//	NOISE=noisecg(N,A1,A2) yields a complex colored gaussian noise
//	obtained by filtering a white gaussian noise through a
//		sqrt(1-A1^2-A2^2)/(1-A1*z^(-1)-A2*z^(-2)) 
//	second order filter.
//
//  Examples
//        N=512;noise=noisecg(N);mean(noise),std(noise).^2
//       subplot(211); plot(real(noise)); a=gca();a.data_bounds=([1 N -3 3]);
//       subplot(212); f=linspace(-0.5,0.5,N); 
//       plot(f,abs(fftshift(fft(noise))).^2);
//   See also 
//      noisecu
//   Authors
//      H. Nahrstaedt - Aug 2010
//	O. Lemoine, June 95/May 96 - F. Auger, August 95.
//	Copyright (c) 1996 by CNRS (France).

//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  [nargout,nargin]=argn(0);
  fname="noisegc"
  
  if type(N)<>1|or(size(N)<>1)|(int(N)<>N|N <= 0),
    error (msprintf(_("%s: Wrong value for input argument #%d: A positive integer expected.\n"),fname,1));
  end;

  if (nargin==1) then
    if N<=2 then
      noise=(rand(N,1,'normal')+%i*rand(N,1,'normal'))/sqrt(2); 
    else
      noise=rand(2^nextpow2(N),1,'normal'); 
    end
  elseif (nargin==2) then
    if (~isreal(a1)|abs(a1)>=1.0) then
      error (msprintf(_("%s: Wrong value for input argument #%d: Must be in the interval %s.\n"),fname,2,"[-1 1]"));
    elseif (abs(a1)<=%eps) then
      if N<=2 then
        noise=(rand(N,1,'normal')+%i*rand(N,1,'normal'))/sqrt(2); 
      else
        noise=rand(N,1,'normal');
      end
    else
      if N<=2 then
        noise=(rand(N,1,'normal')+%i*rand(N,1,'normal'))/sqrt(2); 
      else
        Nnoise=ceil(N-2.0/log(a1));
        noise=rand(2^nextpow2(Nnoise),1,'normal');
      end
      //noise=mtlb_filter(sqrt(1.0-a1^2), [1 -a1],noise);
      noise=rtitr(sqrt(1.0-a1^2)*%z, -a1+%z,noise')';
      
    end;
  elseif nargin==3 then
    if or(abs(roots([1 -a1 -a2]))>1),
      error (msprintf(_("%s: unstable filter.\n"),fna));me
    else
      if N<=2,
        noise=(rand(N,1,'normal')+%i*rand(N,1,'normal'))/sqrt(2); 
      else
        Nnoise=ceil(N-2.0/log(max(real(roots([1 -a1 -a2])))));
        noise=rand(2^nextpow2(Nnoise),1,'normal');
      end
      //noise=mtlb_filter(sqrt(1.0-a1^2-a2^2), [1 -a1 -a2],noise);
      noise=rtitr(sqrt(1.0-a1^2-a2^2)*%z^2, -a2-a1*%z+%z^2,noise')';
    end;
  end;

  if N>2 then
    noise=hilbert(noise)/stdev(noise)/sqrt(2);
    noise=noise($-(N-1:-1:0));
  end

endfunction
