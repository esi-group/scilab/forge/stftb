function [y,iflaw]=fmodany(iflaw,t0);
// Signal with arbitrary frequency modulation.
// Calling Sequence
//	[Y,IFLAW]=fmodany(IFLAW) 
//	[Y,IFLAW]=fmodany(IFLAW,T0) 
//  Parameters
//	IFLAW : vector of the instantaneous frequency law samples.
//	T0    : time reference		(default: 1).
//	Y     : output signal
//    Description
//      fmodany generates a frequency modulated
//	signal whose instantaneous frequency law is approximately given by
//	the vector IFLAW (the integral is approximated by CUMSUM).
//	The phase of this modulation is such that y(t0)=1.
//    Examples
//       [y1,ifl1]=fmlin(100); [y2,ifl2]=fmsin(100);
//       iflaw=[ifl1;ifl2]; sig=fmodany(iflaw); 
//       subplot(211); plot(real(sig))
//       subplot(212); plot(iflaw); 
//   See also 
//      fmhyp
//      fmsin
//      fmpar
//      fmconst
//      fmlin
//      fmpower
//   Authors
//      H. Nahrstaedt - Aug 2010
//	F. Auger, August 1995.
//	Copyright (c) 1996 by CNRS (France).

//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  [nargout,nargin]=argn(0);
  fname="fmodany";
  
  if nargin < 1 then
    error(msprintf(_("%s: Wrong number of input argument: %d to %d expected.\n"),fname,1,2));
  end;
  
  //iflaw
   if type(iflaw)<>1|and(size(iflaw)>1)|~isreal(iflaw) then
    error(msprintf(_("%s: Wrong type for input argument #%d: A real vector expected.\n"),fname,1));
  end 
  if or(abs(iflaw)>0.5) then
    error(msprintf(_("%s: Wrong value for input argument #%d: Elements must be in %s.\n"),fname,1,"[-0.5 0.5]"));
  end
  iflaw=iflaw(:);
  N=size(iflaw,1)
  
  //t0
  if nargin==2 then
    if type(t0)<>1|size(t0,"*")>1|~isreal(t0)|int(t0)<>t0 then
      error(msprintf(_("%s: Wrong type for input argument #%d: A integer value expected.\n"),fname,2));
    end 
    if t0<1|t0>N then
      error(msprintf(_("%s: Wrong value for input argument #%d:  Must be in the interval %s.\n"),fname,2,"[1,"+string(N)+"]"));
    end    
  else
    t0 = 1;
  end
  
  y=exp(%i*2.0*%pi*cumsum(iflaw));
  y=y*conj(y(t0));
endfunction
