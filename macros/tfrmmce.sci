function [tfr,t,f] = tfrmmce(x,h,t,N,ptrace,opt_plot);
// Minimum mean cross-entropy combination of spectrograms.
// Calling Sequence
//	[TFR,T,F]=tfrmmce(X,H)
//	[TFR,T,F]=tfrmmce(X,H,T)
//	[TFR,T,F]=tfrmmce(X,H,T,N)
//	[TFR,T,F]=tfrmmce(X,H,T,N,TRACE)
//	[TFR,T,F]=tfrmmce(...,'plot')
//  Parameters
//	X     : signal.
//	H     : frequency smoothing windows, the H(:,i) being normalized   so as to be of unit energy. 
//	T     : time instant(s)          (default : 1:length(X)).
//	N     : number of frequency bins (default : length(X))
//	TRACE : if nonzero, the progression of the algorithm is shown         (default : 0).
//      'plot':	if one input parameter is 'plot',  tfrmh runs tfrqview. and TFR will be plotted
//	TFR   : time-frequency representation. 
//	F     : vector of normalized frequencies.
//  Description
//       tfrmmce  computes the minimum mean cross-entropy combination of spectrograms using as 
//	windows the columns of the matrix H.
//  Examples
//       sig=fmlin(128,0.1,0.4); h=zeros(19,3);
//       h(10+(-5:5),1)=tftb_window(11); h(10+(-7:7),2)=tftb_window(15);  
//       h(10+(-9:9),3)=tftb_window(19); tfrmmce(sig,h,'plot');
//     Authors
//      H. Nahrstaedt - Aug 2010
//	F. Auger, August 1995.
//	Copyright (c) 1996 by CNRS (France).

//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  [nargout,nargin]=argn(0);
  fname="tfrmmce";
  
  in_par=['x','h','t','N','ptrace','opt_plot'];
  in_par_min=2;
  plotting=%f;
  if (nargin>in_par_min)
    if (type(evstr(in_par(nargin)))==10)
      if convstr(evstr(in_par(nargin)))=="plot" then
        plotting=%t; 
        nargin=nargin-1;
      end; 
    end;
  end;  
  clear in_par;
  if nargin <2 then
    error(msprintf(_("%s: Wrong number of input argument: %d to %d expected.\n"),fname,2,6));
  end
 
  //x
  if type(x)<>1 then
    error(msprintf(_("%s: Wrong type for input argument #%d: A matrix of double expected.\n"),fname,1));
  end
  if or(size(x)==1) then
    x=x(:);
  else
    error(msprintf(_("%s: Wrong size for input argument #%d: A vector expected.\n"),fname,1));
  end
  Nx=size(x,1);

  //h
  if type(h)<>1|size(h,2)<2|~isreal(h) then 
    error(msprintf(_("%s: Wrong type for argument #%d: A real  array with at least two columns expected.\n"),fname,2));
  end
  [hlength,hcol]=size(h);
  if (modulo(hlength,2)==0),
    error(msprintf(_("%s: Wrong size for argument #%d: A odd number of rows expected.\n"),fname,2));
  end;
  Lh=(hlength-1)/2; 
  h=h*diag(1.0 ./ sqrt(sum(abs(h).^2,1)));

  //ptrace
  if nargin>=5 then 
    if and(type(ptrace)<>[1 4])|size(ptrace,"*")<>1 then
      error(msprintf(_("%s: Wrong type for argument #%d: A boolean or real scalar expected.\n"),fname,5));
    end
  else
    ptrace=%f;
  end
 
  //N
  if nargin>=4 then
    if type(N)<>1|size(N,"*")>1|~isreal(N)|int(N)<>N then
      error(msprintf(_("%s: Wrong type for input argument #%d: A integer value expected.\n"),fname,4));
    end 
    if N<=0 then
      error(msprintf(_("%s: Wrong value for input argument #%d: A positive integer expected.\n"),fname,4));
    end
  else
    N=Nx
  end
  
  //T
  if nargin>=3 then
    if type(t)<>1|and(size(t)>1)|~isreal(t) then
      error(msprintf(_("%s: Wrong type for input argument #%d: A real vector expected.\n"),fname,3));
    end
    if max(t)>Nx|min(t)<1 then
      error(msprintf(_("%s: Wrong value for input argument #%d: The elements must be in the interval %s.\n"),fname,3,"[1 "+string(Nx)+"]"));
    end
  else
    t=1:Nx;
  end
  Nt=size(t,2);
 
 
  tfr= zeros (N,Nt);
  slides= zeros(N,hcol);
  if ptrace, disp('MMCE Spectrogram'); end;

  for icol=1:Nt,
    ti= t(icol); 
    tau=-min([round(N/2)-1,Lh,ti-1]):min([round(N/2)-1,Lh,Nx-ti]);
    indices= rem(N+tau,N)+1;
    slides= zeros(N,hcol);
    if ptrace, disprog(icol,Nt,10); end;
    for ih=1:hcol,
      slides(indices,ih)=x(ti+tau).*h(Lh+1+tau,ih);
    end;
    slides=abs(fft(slides,-1,1)).^2;

    tfr(:,icol)=prod(slides,2) .^(1/hcol);
  end;

  tfr=tfr*(sum(abs(x(t)).^2)/sum(tfr));
  if ptrace, printf('\n'); end;

  if (plotting),
    tfrqview(tfr,x,t,'tfrmmce',h);
  end
  if (nargout==3),
    //f=fftshift((-(N/2):(N/2)-1)/N)';
    f=(0:N-1)/N;	
  end;

endfunction


