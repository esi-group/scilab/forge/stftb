function flag=istfraff(method);
// returns true is method is an affine time frequency representation.
// Calling Sequence
// flag=istfr2(method)
// See also 
//  istfr1
//  istfr2
//   Authors
//      H. Nahrstaedt - Aug 2010
//	F. Auger, may 98
//	Copyright (c) CNRS - France 1998. 

//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  fname="istfraff"
  if nargin == 0 then
    error(msprintf(_("%s: Wrong number of input argument: %d expected.\n"),fname,1));
  end;
  if type(method)<>10|or(size(method)<>1) then
    error(msprintf(_("%s: Wrong type for input argument #%d: A character string expected.\n"),fname,1));
  end

  method=convstr(method,"u")

  M=['TFRASPW' 'TFRSCALO' 'TFRDFLA'  'TFRSPAW' 'TFRUNTER' 'TFRBERT' ...
     'TFRSPBK'];
  if or(method==M) then
    flag=1;
  else
    flag=0;
  end;
endfunction
