function [tm,D2]=momftfr(tfr,tmin,tmax,time) 
//	[TM,D2]=momftfr(TFR,TMIN,TMAX) 
//	[TM,D2]=momftfr(TFR,TMIN,TMAX,TIME) 
//  Parameters
//	TFR    : time-frequency representation ([Nrow,Ncol]size(TFR)). 
//	TMIN   : smallest column element of TFR taken into account       (default : 1) 
//	TMAX   : highest column element of TFR taken into account       (default : Ncol)
//	TIME   : true time instants (default : 1:Ncol)
//	TM     : averaged time          (first order moment)
//	D2     : squared time duration  (second order moment)
//  Description
//      momftfr computes the frequeny moments of a time-frequency representation.
//  Examples
//       sig=fmlin(128,0.1,0.4); 
//       [tfr,t,f]=tfrwv(sig); [tm,D2]=momftfr(tfr); 
//       subplot(211); plot(f,tm); subplot(212); plot(f,D2);
//    See also 
//       momttfr
//   Authors
//      H. Nahrstaedt - Aug 2010
//	F. Auger, August 1995.
//	Copyright (c) 1996 by CNRS (France).

//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  [nargout,nargin]=argn(0);
  fname="momftfr" 
  if nargin == 0 then
    error(msprintf(_("%s: Wrong number of input argument: %d to %d expected.\n"),fname,1,4));
  end;
  
  //tfr
  if type(tfr)<>1 then
    error(msprintf(_("%s: Wrong type for input argument #%d: A matrix of double expected.\n"),fname,1));
  end
  [tfrrow,tfrcol]=size(tfr);
  
  //tmin
  if nargin>=2 then
     if type(tmin)<>1|~isreal(tmin)|or(size(tmin)<>1)|int(tmin)<>tmin then
       error(msprintf(_("%s: Wrong type for input argument #%d: An integer expected.\n"),fname,2));
     end
     if tmin<1|tmin>tfrcol then
       error(msprintf(_("%s: Wrong value for input argument #%d: Must be in the interval %s.\n"),fname,2,"[1 "+string(tfrcol)+"]"));
       
     end
  else
    tmin=1
  end
  
  //tmax
  if nargin>=3 then
    if type(tmax)<>1|~isreal(tmax)|or(size(tmax)<>1)|int(tmax)<>tmax then
       error(msprintf(_("%s: Wrong type for input argument #%d: An integer expected.\n"),fname,3));
     end
     if tmax<tmin|tmax>tfrcol then
       error(msprintf(_("%s: Wrong value for input argument #%d: Must be in the interval %s.\n"),fname,3,"["+string(tmin)+","+string(tfrcol)+"]"));
     end
  else
    tmax=tfrcol
  end

  //time
  if nargin==4 then
    if type(time)<>1|~isreal(time)|and(size(time)<>1)|or(int(time)<>time) then
      error(msprintf(_("%s: Wrong type for input argument #%d: A  vector of integer values expected.\n"),fname,4));
    end
    if size(time,"*")<>tmax-tmin+1 then
     error(msprintf(_( "%s: Wrong size for input argument #%d: A %d elements array expected.\n"),fname,4,tmax-tmin+1));
    end
  else
    time=tmin:tmax
  end
  tfr=tfr(:,tmin:tmax).'

  E  = sum(tfr,1);
  tm = (time    * tfr ./E).'; 
  D2 = (time.^2 * tfr ./E).' - tm.^2;

endfunction
