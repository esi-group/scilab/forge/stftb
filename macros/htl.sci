function [ht,rho,theta]=htl(IM,M,N,ptrace,opt_plot)
// 	Hough transform for detection of lines in images.
// Calling Sequence
//	[HT,RHO,THETA]=HTL(IM)
//	[HT,RHO,THETA]=HTL(IM,M)
//	[HT,RHO,THETA]=HTL(IM,M,N)
//	[HT,RHO,THETA]=HTL(IM,M,N,TRACE)
//	[HT,RHO,THETA]=HTL(...,'plot')
// Parameters
//	IM    : image to be analyzed (size Xmax x Ymax).
//	M     : desired number of samples along the radial axis (default : Xmax).
//	N     : desired number of samples along the azimutal (angle) axis (default : Ymax). 
//	TRACE : if nonzero, the progression of the algorithm is shown             (default : 0).
//	HT    : output matrix (MxN matrix). 
//      'plot':	when called with the additional string 'plot', htl displays HT using mesh.
//	RHO   : sequence of samples along the radial axis.
//	THETA : sequence of samples along the azimutal axis.
// Description
//	From an image IM, computes the integration of the values
//	of the image over all the lines. The lines are parametrized 
//	using polar coordinates. The origin of the coordinates is fixed
//	at the center of the image, and theta is the angle between the
//	VERTICAL axis and the perpendicular (to the line) passing through 
//	the origin. Only the values of IM exceeding 5 // of the maximum 
//	are taken into account (to speed up the algorithm). 
//   Examples
//       N=64; t=(1:N); y=fmlin(N,0.1,0.3); 
//       IM=tfrwv(y,t,N); grayplot(t,1:length(y),IM'); scf(); htl(IM,N,N,1,'plot'); 
//   Authors
//      H. Nahrstaedt - Aug 2010

//	Copyright (c) 1996 by CNRS (France).

//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  [nargout,nargin]=argn(0);
  fname="htl";
  in_par=['IM','M','N','ptrace','opt_plot'];
  in_par_min=1;
  plotting=%f;
  if (nargin>in_par_min)
    if (type(evstr(in_par(nargin)))==10)
      if convstr(evstr(in_par(nargin)))=="plot" then
        plotting=%t; 
        nargin=nargin-1;
      end; 
    end;
  end;
  clear in_par;
  
  if nargin == 0 then
    error(msprintf(_("%s: Wrong number of input argument: %d to %d expected.\n"),fname,1,5));
  end;
  
  //IM
  if type(IM)<>1|~isreal(IM) then
    error(msprintf(_("%s: Wrong type for input argument #%d: A real matrix expected.\n"),fname,1));
  end
  [Xmax,Ymax] = size(IM);

  //ptrace
  if nargin>=4 then 
    if and(type(ptrace)<>[1 4])|size(ptrace,"*")<>1 then
      error(msprintf(_("%s: Wrong type for argument #%d: A boolean or real scalar expected.\n"),fname,4));
    end
  else
    ptrace=%f;
  end
  
  //N
  if nargin>=3 then 
    if type(N)<>1|size(N,"*")>1|~isreal(N)|int(N)<>N then
      error(msprintf(_("%s: Wrong type for input argument #%d: A integer value expected.\n"),fname,3));
    end 
    if N<=0 then
      error(msprintf(_("%s: Wrong value for input argument #%d: A positive integer expected.\n"),fname,3));
    end
    
  else
    N=Ymax;
  end
  
  //M
  if nargin>=2 then 
    if type(M)<>1|size(M,"*")>1|~isreal(M)|int(M)<>M then
      error(msprintf(_("%s: Wrong type for input argument #%d: A integer value expected.\n"),fname,2));
    end 
    if M<=0 then
      error(msprintf(_("%s: Wrong value for input argument #%d: A positive integer expected.\n"),fname,2));
    end
   
  else
     M=Xmax;
  end
  
  rhomax=sqrt(Xmax^2+Ymax^2)/2;

  deltar=rhomax/(M-1);
  deltat=2*%pi/N;

  ht=zeros(M,N);
  Max=max(IM); 

  if modulo(Xmax,2)==1 then
    Xc=(Xmax+1)/2; X0=1-Xc; Xf=Xc-1;
  else
    Xc=Xmax/2; X0=1-Xc; Xf=Xc;
  end
  if  modulo(Ymax,2)==1 then
    Yc=(Ymax+1)/2; Y0=1-Yc; Yf=Yc-1;
  else
    Yc=Ymax/2; Y0=1-Yc; Yf=Yc;
  end

  if ptrace then disp('Hough transform - Detection of lines');end
  for x=X0:Xf
    if ptrace, disprog(x-X0+1,Xmax,10); end
    for y=Y0:Yf 
      
        for theta=0:deltat:(2*%pi-deltat),
          rho=x*cos(theta)-y*sin(theta);
          if ((rho>=0)&(rho<=rhomax)) then
            i=round(rho/deltar)+1;
            j=round(theta/deltat)+1;
            ht(i,j)=ht(i,j)+IM(x+Xc,y+Yc);
          end
        end

    end
  end

  rho=0:deltar:rhomax;
  theta=0:deltat:(2*%pi-deltat);
  if ptrace, disp(' '); end

  if plotting then
    mesh(theta,rho,ht)
    title(_("Hough transform - Detection of lines"));
    Min=min(abs(ht));
    Max=max(abs(ht));
    a=gca();a.data_bounds=[min(theta) max(theta) min(rho) max(rho) Min Max];
    xlabel('Theta');
    ylabel('Rho');
  end

endfunction

