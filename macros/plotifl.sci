function plotifl(t,iflaws,signal);
// Plot normalized instantaneous frequency laws.
// Calling Sequence
// 	plotifl(T,IFLAWS) 
// 	plotifl(T,IFLAWS,SIGNAL) 
//  Parameters
// 	T      : time instants,
// 	IFLAWS : (M,P)-matrix where each column corresponds to the instantaneous frequency law of an (M,1)-signal,   These P signals do not need to be present at the same   time instants.   The values of IFLAWS must be between -0.5 and 0.5.
//       SIGNAL : if the signal is precised, display it also 
//  Description
//      plotifl plot the normalized instantaneous frequency laws of each signal component.
//  Examples
//        N=140; t=0:N-1; [x1,if1]=fmlin(N,0.05,0.3); 
//        [x2,if2]=fmsin(70,0.35,0.45,60);x1(35+(1:70))=x1(35+(1:70))+2*x2;
//        if2=[zeros(35,1)*NaN;if2;zeros(N-70-35,1)*NaN];
//        figure(1); clf; plotifl(t,[if1 if2]);
//        figure(2); clf; plotifl(t,[if1 if2],x1);
//   See also 
//      tfrideal
//      plotsid
//   Authors
//      H. Nahrstaedt - Aug 2010
// 	F. Auger, August 94, August 95, May 1998.
//	Copyright (c) 1996 by CNRS (France).

//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  [nargout,nargin]=argn(0);

  fname="plotifl";
  if nargin < 2 then
    error(msprintf(_("%s: Wrong number of input argument: %d to %d expected.\n"),fname,2,3));
  end
  
  //t
  if type(t)<>1|and(size(t)>1)|~isreal(t) then
    error(msprintf(_("%s: Wrong type for input argument #%d: A real vector expected.\n"),fname,1));
  end
  t=matrix(t,1,-1)
  tcol = size(t,2);

  //iflaws
  if type(iflaws)<>1|~isreal(iflaws) then
    error(msprintf(_("%s: Wrong type for input argument #%d: A real matrix expected.\n"),fname,2));
  end
  [ifrow,ifcol]=size(iflaws); 
  if tcol~=ifrow then
    error(msprintf(_("%s: incompatible input arguments %d and %d\n"),fname,1,2));
  end

  maxif=max(iflaws); 
  minif=min(iflaws);
  if maxif > 0.5 | minif < -0.5 then 
    error(msprintf(_("%s: Wrong value for input argument #%d: Elements must be in the interval %s.\n"),fname,2,"[-0.5 0.5]"))
  end
  
  
  //signal
  if nargin==3 then
    if type(signal)<>1|and(size(signal)>1) then
      error(msprintf(_("%s: Wrong type for input argument #%d: A complex vector expected.\n"),fname,3));
    end
    signal=matrix(signal,-1,1)
    Nsig = size(signal,1);
    if Nsig<> tcol then
      error(msprintf(_("%s: Wrong value for input arguments #%d and #%d: Same sizes expected.\n"),fname,1,3));
    end
 
    clf; 
    axsig = newaxes();
    axsig.axes_bounds=[0.10 0.69 0.80 0.25];
    sca(axsig); 
    plot(t,real(signal)); 
    xgrid();
    axsig.data_bounds(:,1)=[min(t),max(t)]';
    xlabel(_('Time'));
    title(_('signal'));
    
    axtfr = newaxes();
    axtfr.axes_bounds=[0.10 0.12 0.80 0.45];
    sca(axtfr);
  else
    clf; 
  end;

  plot(t,iflaws); 
  if (minif>=0) then
    xgrid();
    a=gca(); a.data_bounds=[min(t) 0; max(t) 0.5];
  else
    xgrid();
    a=gca(); a.data_bounds=[min(t) -0.5; max(t) 0.5];
  end;

  xlabel(_('Time'));
  ylabel(_('Normalized frequency'));
  title(_('Instantaneous frequency law(s)'));
endfunction
