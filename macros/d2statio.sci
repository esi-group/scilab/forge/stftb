function [d,f]=d2statio(sig);
// Distance to stationarity
// Calling Sequence
//	[D,F]=D2STATIO(SIG) 
// Parameters
//	SIG : signal to be analyzed (real or complex).
//	D   : vector giving the distance to stationarity for each frequency.
//	F   : vector of frequency bins
// Description
//        d2statio evaluates the distance of the signal to stationarity, using the pseudo Wigner-Ville distribution.
// Examples
//      sig=noisecg(128); [d,f]=d2statio(sig);
//       scf; plot(f,d); xlabel('Frequency'); ylabel('Distance'); 
//
//       sig=fmconst(128); [d,f]=d2statio(sig);
//       scf;  plot(f,d); xlabel('Frequency'); ylabel('Distance'); 
// Authors
//      H. Nahrstaedt - Aug 2010
//	O. Lemoine - May 1996.
//	Copyright (c) by CNRS France, 1996.

//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301
//  USA
  [nargout,nargin]=argn(0);
  fname="d2statio";
  
  if nargin <1 then
    error(msprintf(_("%s: Wrong number of input argument: %d  expected.\n"),fname,1));
  end;

  //sig
  if type(sig)<>1|and(size(sig)>1) then
    error(msprintf(_("%s: Wrong type for input argument #%d: A real or complex vector expected.\n"),fname,1));
  end
  N=size(sig,"*");

  [tfr,t,f]=tfrspwv(sig);
  
  d2=((tfr-mean(tfr,2)*ones(1,N))/norm(sig)).^2;	

  d=mean(d2,2);
endfunction

