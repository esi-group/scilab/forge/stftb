function [naf,tau,xi]=ambifunb(x,tau,N,ptrace,opt_plot);
//  Narrow-band ambiguity function.
// Calling Sequence
//	[NAF,TAU,XI]=ambifunb(X) 
//	[NAF,TAU,XI]=ambifunb(X,TAU)
//	[NAF,TAU,XI]=ambifunb(X,TAU,N)  
//	[NAF,TAU,XI]=ambifunb(X,TAU,N,TRACE)
//	[NAF,TAU,XI]=ambifunb(...,'plot')  
// Parameters
//	X     : signal if auto-AF, or [X1,X2] if cross-AF (length(X)=Nx).
//	TAU   : vector of lag values     (default is -Nx/2:Nx/2).
//	N     : number of frequency bins (default is length(X)).
//	TRACE : if 1,              (default is 0)  the progression of the algorithm is shown.
//	'plot'      :  if input contains the string 'plot', the output values will be plotted
//	NAF   : doppler-lag representation, with the doppler bins stored in the rows and the time-lags stored in the columns. When called without output arguments, AMBIFUNB displays  the squared modulus of the ambiguity function by means of contour.
//	XI    : vector of doppler values.
// Description
//      ambifunb computes the narrow-band ambiguity function of a signal X, or the cross-ambiguity 
//	function between two signals.
//  Examples
//       sig=anabpsk(256,8);
//       ambifunb(sig,'plot'); 
//  See also
//    ambifuwb
// Authors
//   H. Nahrstaedt - Aug 2010
//   O. Lemoine
//   F. Auger - August 1995.

//	Copyright (c) 1996 by CNRS (France).
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  [nargout,nargin]=argn(0);
  fname="ambifunb";
  
 
  in_par=['x','tau','N','ptrace','opt_plot'];
  in_par_min=1;
  plotting=%f;
  if (nargin>in_par_min)
    if (type(evstr(in_par(nargin)))==10)
      if convstr(evstr(in_par(nargin)))=="plot" then
        plotting=%t; 
        nargin=nargin-1;
      end; 
    end;
  end;
  clear in_par;
  
  if nargin == 0 then
    error(msprintf(_("%s: Wrong number of input argument: %d to %d expected.\n"),fname,1,5));
  end;
  
  //x
  if type(x)<>1 then
    error(msprintf(_("%s: Wrong type for input argument #%d: A matrix of double expected.\n"),fname,1));
  end
  if or(size(x)==1) then
    x=x(:);
  end
  if size(x,2)>2 then
    error(msprintf(_("%s: Wrong size for input argument #%d: A a vector or a two columns array expected.\n"),fname,1));
  end
  [Nx,xcol]=size(x);
 
  //ptrace
  if nargin>=4 then 
    if and(type(ptrace)<>[1 4])|size(ptrace,"*")<>1 then
      error(msprintf(_("%s: Wrong type for argument #%d: A boolean or real scalar expected.\n"),fname,4));
    end
  else
    ptrace=%f;
  end
  
  //N
  if nargin>=3 then
    if type(N)<>1|size(N,"*")>1|~isreal(N)|int(N)<>N then
      error(msprintf(_("%s: Wrong type for input argument #%d: A integer value expected.\n"),fname,3));
    end 
    if N<1|N>Nx then
       error(msprintf(_("%s: Wrong value for input argument #%d: Must be in the interval %s.\n"),fname,3,"[1,"+string(Nx)+"]"));
    end
  else
    N=Nx
  end
  
  //tau
  taumax=floor((Nx-1)/2);
  taumin=-taumax

  if nargin>=2 then
    if type(tau)<>1|~isreal(tau)|and(size(tau)>1)|or(int(tau)<>tau) then
      error(msprintf(_("%s: Wrong type for input argument #%d: A  vector of positive integers expected.\n"),fname,2));
    end
     
    if or(diff(tau,2)<>0) then
      error(msprintf(_("%s: Wrong value for input argument #%d: regularily spaced vector expected.\n"), "ambifunb", 2))
    end
    if min(tau)<taumin|max(tau)>taumax then
      error(msprintf(_("%s: Wrong value for input argument #%d: Must be in [%d %d].\n"), "ambifunb", 2,taumin,taumax))
    end
    tau=matrix(tau,1,-1)
  else
    tau=taumin:taumax
  end
  Ntau=size(tau,2)
 
  naf=zeros(Nx,Ntau); 
  if ptrace, disp(_('Narrow-band ambiguity function')); end;

  for icol=1:Ntau,
    if ptrace then disprog(icol,Ntau,10); end;
    taui=tau(icol);
    t=(1+abs(taui)):(Nx-abs(taui));
    naf(t,icol)=x(t+taui,1).* conj(x(t-taui,xcol));
  end;
  r=modulo(N,2)
  naf=fftshift(fft(naf,-1,1),1);

  naf=naf((Nx-N)/2+(1:N),:);

  //xi=-0.5+((0:N-1)+r/2)/N
  xi=(-(N-r)/2:(N+r)/2-1)/N;

  if plotting then
    xset("fpf"," ");
    contour2d(2*tau,xi,(abs(naf).^2)',16); 
    xgrid
    xlabel(_('Delay')); ylabel(_('Doppler'))
    title(_('Narrow-band ambiguity function'));
  end;
endfunction
