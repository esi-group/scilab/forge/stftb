function som=integ2d(mat,x,y);
// Approximate 2-D integral.
// Calling Sequence
//	SOM=integ2d(MAT)
//	SOM=integ2d(MAT,X)
//	SOM=integ2d(MAT,X,Y)
// Parameters
//	MAT : MxN matrix to be integrated
//	X   : N-row-vector indicating the abscissa integration path 	(default : 1:N)
//	Y   : M-column-vector indicating the ordinate integration path 	(default : 1:M)	
//	SOM : result of integration
//  Description
//     integ2d approximates the 2-D integral of  matrix MAT according to abscissa X and ordinate Y.
//  Examples     
//       S = altes(256,0.1,0.45,10000) ;
//       [TFR,T,F] = tfrscalo(S,21:190,8,'auto') ;
//       E = integ2d(TFR,T,F)
//  See also 
//     integ
//   Authors
//      H. Nahrstaedt - Aug 2010
//	P. Goncalves, October 95
//	Copyright (c) 1995 Rice University

//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  [nargout,nargin]=argn(0);
  fname="integ2d";
  if nargin <1 then
    error(msprintf(_("%s: Wrong number of input argument: %d to %d expected.\n"),fname,1,3));
  end;
  
  //mat
  if type(mat)<>1 then
    error(msprintf(_("%s: Wrong type for input argument #%d: A matrix of double expected.\n"),fname,1));
  end
  [M,N]=size(mat);
  
  //y
  if nargin==3 then
    if type(y)<>1|and(size(y)<>1) then
      error(msprintf(_("%s: Wrong type for input argument #%d: A vector of double expected.\n"),fname,3));
    end
    if size(y,"*")<>M then
      error(msprintf(_("%s: Wrong size for input argument %d: A vector of size %d expected.\n"),fname,3,M));
    end
    dy=matrix(diff(y),1,-1);
  else
    dy=ones(1,M-1);
  end
  
  //x
  if nargin>=2 then
    if type(x)<>1|and(size(x)<>1) then
      error(msprintf(_("%s: Wrong type for input argument #%d: A vector of double expected.\n"),fname,2));
    end
    if size(x,"*")<>N then
      error(msprintf(_("%s: Wrong size for input argument %d: A vector of size %d expected.\n"),fname,2,N));
    end
    dx=matrix(diff(x),-1,1);
  else
    dx=ones(N-1,1)
  end
  s=(mat(:,1:N-1) + mat(:,2:N))*dx;
  som=dy*(s(1:M-1)+s(2:M))/4
  
endfunction
