function [tfr,t,f] = tfrridbn(x,t,N,g,h,ptrace,opt_plot);
// Reduced Interference Distribution with a binomial kernel.
// Calling Sequence
//       [TFR,T,F]=tfrridbn(X)
//       [TFR,T,F]=tfrridbn(X,T)
//       [TFR,T,F]=tfrridbn(X,T,N)
//       [TFR,T,F]=tfrridbn(X,T,N,G)
//       [TFR,T,F]=tfrridbn(X,T,N,G,H)
//       [TFR,T,F]=tfrridbn(X,T,N,G,H,TRACE)
//       [TFR,T,F]=tfrridbn(...,'plot')
//  Parameters
//       X     : signal if auto-RIDBN, or [X1,X2] if cross-RIDBN.
//       T     : time instant(s)          (default : 1:length(X)).
//       N     : number of frequency bins (default : length(X)).
//       G     : time smoothing window, G(0) being forced to 1.    (default : Hamming(N/10)). 
//       H     : frequency smoothing window, H(0) being forced to 1.     (default : Hamming(N/4)). 
//       TRACE : if nonzero, the progression of the algorithm is shown        (default : 0).
//      'plot':	if one input parameter is 'plot',  tfrridbn runs tfrqview. and TFR will be plotted
//       TFR   : time-frequency representation.
//       F     : vector of normalized frequencies.
// Description
//       tfrridbn Reduced Interference
//       Distribution with a kernel based on the binomial coefficients.
//       tfrridbn computes either the distribution of a discrete-time 
//       signal X, or the cross representation between two signals. 
//    Examples
//        sig=[fmlin(128,.05,.3)+fmlin(128,.15,.4)]; tfrridbn(sig,'plot'); 
//     Authors
//      H. Nahrstaedt - Aug 2010
//       F. Auger, June 1996.
//       Copyright (c) 1996 by CNRS (France).

//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  [nargout,nargin]=argn(0);
  fname="tfrridbn"

  in_par=['x','t','N','g','h','ptrace','opt_plot'];
  in_par_min=1;
  plotting=%f;
  if (nargin>in_par_min)
    if (type(evstr(in_par(nargin)))==10)
      if convstr(evstr(in_par(nargin)))=="plot" then
        plotting=%t; 
        nargin=nargin-1;
      end; 
    end;
  end;
  clear in_par;
  if nargin == 0 then
    error(msprintf(_("%s: Wrong number of input argument: %d to %d expected.\n"),fname,1,7));
  end
  
  //x
  if type(x)<>1 then
    error(msprintf(_("%s: Wrong type for input argument #%d: A matrix of double expected.\n"),fname,1));
  end
  if or(size(x)==1) then
    x=x(:);
  end
  if size(x,2)>2 then
    error(msprintf(_("%s: Wrong size for input argument #%d: A  vector or a two columns array expected.\n"),fname,1));
  end
  [Nx,xcol]=size(x);

  //ptrace
  if nargin==6 then 
    if and(type(ptrace)<>[1 4])|size(ptrace,"*")<>1 then
      error(msprintf(_("%s: Wrong type for argument #%d: A boolean or real scalar expected.\n"),fname,6));
    end
  else
    ptrace=%f;
  end
  
  //h
  if nargin>=5 then 
    if type(h)<>1|and(size(h)>1)|~isreal(h) then 
      error(msprintf(_("%s: Wrong type for argument #%d: A real vector expected.\n"),fname,5));
    end
    hlength=size(h,"*");
    if (modulo(hlength,2)==0),
      error(msprintf(_("%s: Wrong size for argument #%d: A odd number of elements expected.\n"),fname,5));
    end;
  else
    hlength=floor(Nx/4);
    if rem(hlength,2)==0 then hlength=hlength+1;end;
    h = window("hm",hlength)
  end
  h=h(:);
  Lh=(hlength-1)/2; h=h/h(Lh+1);
  
  //g
  if nargin>=4 then 
    if type(g)<>1|and(size(g)>1)|~isreal(g) then 
      error(msprintf(_("%s: Wrong type for argument #%d: A real vector expected.\n"),fname,4));
    end
    glength=size(g,"*");
    if (modulo(glength,2)==0),
      error(msprintf(_("%s: Wrong size for argument #%d: A odd number of elements expected.\n"),fname,4));
    end;
  else
    glength=floor(Nx/10);
    if rem(glength,2)==0 then glength=glength+1;end;
    g = window("hm",glength)
  end
  g=g(:);
  Lg=(glength-1)/2; g=g/g(Lg+1);
  
  //N
  if nargin>=3 then
    if type(N)<>1|size(N,"*")>1|~isreal(N)|int(N)<>N then
      error(msprintf(_("%s: Wrong type for input argument #%d: A integer value expected.\n"),fname,3));
    end 
    if N<=0 then
      error(msprintf(_("%s: Wrong value for input argument #%d: A positive integer expected.\n"),fname,3));
    end
    if 2^nextpow2(N)~=N then
      //printf(msprintf(_("%s: For a faster computation, N should be a power of two\n"),fname,3));
    end
  else
    N=Nx
  end
  
  //T
  if nargin>=2 then
    if type(t)<>1|and(size(t)>1)|~isreal(t) then
      error(msprintf(_("%s: Wrong type for input argument #%d: A real vector expected.\n"),fname,2));
    end
    if max(t)>Nx|min(t)<1 then
      error(msprintf(_("%s: Wrong value for input argument #%d: The elements must be in the interval %s.\n"),fname,2,"[1 "+string(Nx)+"]"));
    end
  else
    t=1:Nx;
  end
  Nt=size(t,2);
  

  taumax = min([round(N/2)-1,Lh]);

  tfr= zeros (N,Nt) ;  
  if ptrace, disp('Binomial RID distribution'); end;
  for icol=1:Nt,
    ti= t(icol); taumax=min([ti+Lg-1,Nx-ti+Lg,round(N/2)-1,Lh]);
    if ptrace, disprog(icol,Nt,10); end;
    tfr(1,icol)= g(Lg+1) * x(ti,1) .* conj(x(ti,xcol));

    Ker=1;
    for tau=1:taumax,
      points= -min([tau,Lg,Nx-ti-tau]):min([tau,Lg,ti-tau-1]);
      Ker= ([Ker; 0; 0]+2*[0; Ker; 0]+[0; 0; Ker])/4.0; 
      g2 = g(Lg+1+points) .* Ker(tau+points+1); 
      if (sum(g2)>%eps), g2=g2/sum(g2); end;
      R=sum(g2 .* x(ti+tau-points,1) .* conj(x(ti-tau-points,xcol)));
      tfr(  1+tau,icol)=h(Lh+tau+1)*R;
      R=sum(g2 .* x(ti-tau-points,1) .* conj(x(ti+tau-points,xcol)));
      tfr(N+1-tau,icol)=h(Lh-tau+1)*R;
    end;

    tau=round(N/2); 
    if (ti<=Nx-tau)&(ti>=tau+1)&(tau<=Lh),
      Ker=ones(2*tau+1,1);
      for p=1:2*tau,
        Ker(p+1)=Ker(p)*(2*tau-p+1)/p;
      end;
      Ker=Ker/sum(Ker);
      points= -min([tau,Lg,Nx-ti-tau]):min([tau,Lg,ti-tau-1]);
      g2 = g(Lg+1+points) .* Ker(tau+points+1); g2=g2/sum(g2);
      tfr(tau+1,icol) = 0.5 * ...
          (h(Lh+tau+1)*sum(g2 .* x(ti+tau-points,1) .* conj(x(ti-tau-points,xcol)))+...
           h(Lh-tau+1)*sum(g2 .* x(ti-tau-points,1) .* conj(x(ti+tau-points,xcol))));
    end;
  end; 

  if ptrace, printf('\n'); end;
  tfr= fft(tfr,-1,1); 
  if (xcol==1), tfr=real(tfr); end ;

  if (plotting),
    tfrqview(tfr,x,t,'tfrridbn',g,h);
  end
  if (nargout==3),
    f=(0.5*(0:N-1)/N)';
  end;

endfunction
