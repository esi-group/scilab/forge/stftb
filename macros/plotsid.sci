function plotsid(t,iflaws,k); 
// Schematic interference diagram of FM signals.  
// Calling Sequence
//	plotsid(T,IFLAWS)  
//	plotsid(T,IFLAWS,K)  
//  Parameters
//	T : time instants, 
//	IFLAWS : matrix of instantaneous frequencies, with as may columns as signal components.  
//	K : distribution		(default : 2): 
//	  K = 2     : Wigner-Ville 
//	  K = 1/2   : D-Flandrin
//	  K = 0     : Bertrand (unitary) 
//	  K = -1    : Unterberger (active)
//	  K = inf   : Margenhau-Hill-Rihaczek
//   Description
//        plotsid plots the schematic interference diagram of (analytic) FM signals. 
//   Examples
//       Nt=90; [y,iflaw]=fmlin(Nt,0.05,0.25); 
//       [y2,iflaw2]=fmconst(50,0.4); 
//       iflaw(:,2)=[%nan*ones(10,1);iflaw2;%nan*ones(Nt-60,1)]; 
//       plotsid(1:Nt,iflaw,0); 
//    See also
//      plotifl
//      midpoint
//   Authors
//      H. Nahrstaedt - Aug 2010
//	P. Flandrin, September 1995.
//	Copyright (c) 1996 by CNRS (France).

//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  [nargout,nargin]=argn(0);

  fname="plotsid";
  if nargin < 2 then
    error(msprintf(_("%s: Wrong number of input argument: %d to %d expected.\n"),fname,2,3));
  end
  
  //t
  if type(t)<>1|and(size(t)>1)|~isreal(t) then
    error(msprintf(_("%s: Wrong type for input argument #%d: A real vector expected.\n"),fname,1));
  end
  t=matrix(t,1,-1)
  tcol = size(t,2);

  //iflaws
  if type(iflaws)<>1|~isreal(iflaws) then
    error(msprintf(_("%s: Wrong type for input argument #%d: A real matrix expected.\n"),fname,2));
  end
  [iflawrow,iflawcol]=size(iflaws);
  if tcol~=iflawrow then
    error(msprintf(_("%s: incompatible input arguments %d and %d\n"),fname,1,2));
  end

  maxif=max(iflaws); 
  minif=min(iflaws);
  if maxif > 0.5 | minif < -0.5 then 
    error(msprintf(_("%s: Wrong value for input argument #%d: Elements must be in the interval %s.\n"),fname,2,"[-0.5 0.5]"))
  end
  
  // k
  if nargin==3 then
    if type(k)<>1|~isreal(k)|or(size(k)<>1) then
      error(msprintf(_("%s: Wrong type for input argument #%d: A real scalar expected.\n"),fname,3));
    end
    if and(k<>[-1 0 1/2 2 %inf]) then
      error(msprintf(_("%s: Wrong value for input argument #%d: Must be in the set {%s}.\n"),fname,3,"-1,0,0.5,2,%inf"));
    end
  else
    k=2 // Wigner-Ville
  end
  
  clf; 
  plotifl(t,iflaws); 
  col=[7,6,4,5];

  // auto-terms
  for j=1:iflawcol,
    indices=find(1-isnan(iflaws(:,j)));
    Nbpoints=length(indices);
    for i=1:Nbpoints-1,
      ta=       t(indices(i))  *ones(1,Nbpoints-i); 
      fa=  iflaws(indices(i),j)*ones(1,Nbpoints-i);
      tb= t(indices(i+1:Nbpoints));
      fb=iflaws(indices(i+1:Nbpoints),j)';
      [ti,fi]=midpoint(ta,fa,tb,fb,k);  
      plot2d(ti,fi,style=0);
      p=get("hdl");
      p.children.mark_foreground=col(rem(j-1,4)+1); 
    end;
  end;

  // cross-terms
  for j1=1:iflawcol,
    indices1=find(1-isnan(iflaws(:,j1)));
    Nbpoints1=length(indices1);
    for j2=j1+1:iflawcol,
      indices2=find(1-isnan(iflaws(:,j2)));
      Nbpoints2=length(indices2);
      for i=1:Nbpoints1,
        ta=       t(indices1(i))   *ones(1,Nbpoints2); 
        fa=  iflaws(indices1(i),j1)*ones(1,Nbpoints2);
        tb= t(indices2);
        fb=iflaws(indices2,j2)'; 
        [ti,fi]=midpoint(ta,fa,tb,fb,k);
        plot2d(ti,fi,style=0);
        p=get("hdl");
        p.children.mark_foreground=3;
      end;
    end;
  end;

  a=gca();a.data_bounds=([t(1) t(tcol) 0 0.5]);
  xgrid;
  if k==2,
    dist=' of the Wigner-Ville distribution';
  elseif k==1/2,
    dist=' of the D-Flandrin distribution';
  elseif k==0,
    dist=' of the (unitary) Bertrand distribution';
  elseif k==-1,
    dist=' of the (active) Unterberger distribution';
  elseif k>1/sqrt(%eps),
    dist=' of the Margenhau-Hill-Rihaczek distribution';
  else
    dist='';
  end

  title(['Interference diagram'+dist+' (k = '+string(k)+')']);
endfunction
