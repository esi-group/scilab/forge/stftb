function tfrview(tfr,sig,t,method,param,p1,p2,p3,p4,p5);
//  Visualization of time-frequency representations.
// Calling Sequence
//        tfrview(TFR,SIG,T,METHOD,PARAM)
//        tfrview(TFR,SIG,T,METHOD,PARAM,P1)
//        tfrview(TFR,SIG,T,METHOD,PARAM,P1,P2)
//        tfrview(TFR,SIG,T,METHOD,PARAM,P1,P2,P3)
//        tfrview(TFR,SIG,T,METHOD,PARAM,P1,P2,P3,P4)
//        tfrview(TFR,SIG,T,METHOD,PARAM,P1,P2,P3,P4,P5)
//  Parameters
//        TFR    : time-frequency representation.
//        SIG    : signal in the time-domain.  
//        T      : time instants.
//        METHOD : chosen representation (name of the corresponding sci-file)
//        PARAM  : visualization parameter vector :  PARAM  = [DISPLAY LINLOG THRESHOLD LEVNUMB NF2 LAYOUT FS ISGRID fmin fmax] where
//         DISPLAY : 1..5 for contour, imagesc, pcolor, surf or mesh
//         LINLOG  :0/1 for linearly/logarithmically spaced levels
//         THRESHOLD : is the visualization threshold, in % 
//         LEVELNUMB : is the number of levels used with contour
//         NF2       : is the number of frequency bins displayed
//         LAYOUT    : determines the layout of the figure : TFR alone (1),  TFR and SIG (2), TFR and spectrum (3), TFR and SIG and    spectrum (4), add 4 if you want a colorbar
//         FS    :  is the sampling frequency (may be set to 1.0)
//         ISGRID : depends on the grids' presence :  isgrid=isgridsig+2*isgridspec+4*isgridtfr        where isgridsig=1 if a grid is present on the signal and =0 if not, and so on   
//        fmin :  smallest normalized frequency  
//        fmax :  highest  normalized frequency
//        P1..P5: parameters of the representation. Run the file tfrparam(METHOD) to know the meaning of P1..P5.  		 
// Description
//        tfrview allows to visualize a time-frequency representation.
//        tfrview is called through tfrqview from any tfr* function.
//  See also 
//        tfrqview
//        tfrparam
// Authors
//      H. Nahrstaedt, Aug 2010
//	F. Auger, July 1994, July 1995 - 
//	O. Lemoine, October-November 1995, May-June 1996. 
//       F. Auger, May 1998.
//	Copyright (c) CNRS - France 1996. 

//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 [nargout,nargin]=argn(0);

 TickLabelStr='Ticklabel';
 
if ( nargin < 5 ),
 error ('at least 5 parameters are required');
end;

[tfrrow,tfrcol] = size(tfr); // the size of tfr
[trow,tcol]     = size(t);   // the size of t
[Nsig,Ncol]     = size(sig); // the size of sig


if (and(abs(imag(tfr))<2*%eps))
  tfr=real(tfr);
end;
maxx = max(tfr);
//mtlb_max has a bug
// if (and(imag(tfr)==0))
//     maxx=max(real(tfr));
// else
//     maxx=mtlb_max(tfr);
//     if (and(imag(maxx)==0))
//        maxx=max(real(maxx));
//     else
//        maxx=mtlb_max(maxx); 
//     end
// end;
if (length(param)<10)
  error("size of param is smaller then 10!");
end;

// Extraction of the elements of param
display     = param( 1);      // contour, imagesc, pcolor, surf, or mesh
linlog      = param( 2);      // linear or logarithmic scale
threshold   = param( 3);      // visualization threshold
levelnumb   = param( 4);      // number of levels
Nf2         = param( 5);      // number of frequency points
layout      = param( 6);      // figure layout
fs          = param( 7);      // sampling frequency
isgrid      = param( 8);      // grid(s)
fmin        = param( 9);      // smallest displayed frequency 
fmax        = param(10);      // highest displayed frequency

if (fmin>=fmax),
 error('fmin should be lower than fmax');
elseif (fmax>0.5),
 error('fmax is a normalized frequency, and should be lower than 0.5');
end

if fs<1000, unitHz=1;                            // display in  s and  Hz
elseif (fs>=1e3 & fs<1e6), fs=fs/1e3; unitHz=2;  // display in ms and kHz
elseif (fs>=1e6), fs=fs/1e6; unitHz=3;           // display in us and MHz
end

linlogtfr=rem(linlog,2);
linlogspec=rem(linlog-linlogtfr,4)/2;
sigenveloppe=rem(linlog-linlogtfr-linlogspec*2,8)/4;

issig=rem(layout-1,2);
isspec=rem(layout-1-issig,4)/2;
iscolorbar=rem(layout-1-issig-isspec*2,8)/4;
if isempty(sig),                        // I can't make miracles
 issig=0; isspec=0; layout=issig+isspec*2+1;
else  
 layout=layout-4*iscolorbar;
end;

isgridsig =rem(isgrid,2);
isgridspec=rem(isgrid-isgridsig,4)/2;
isgridtfr =rem(isgrid-isgridsig-isgridspec*2,8)/4;

// Computation of isaffine and freq (vector of frequency samples) 
if istfraff(method),
 frq=evstr(["p"+string(nargin-5)]);   	// last input argument is freqs.
 isaffine=1;
 //if display==2,	                        // imagesc not allowed
 // display=3;                            // non linear scales for the axes.
 // disp('Imagesc does not support non-linear scales for axes. We use pcolor instead');
 //end
else
 isaffine=0;                            // weyl-heisenberg group of distributions
 frq=(0.5*(0:Nf2-1)/Nf2);              // dispaly only the positive frequencies
end
freqr=frq*fs; ts=t/fs;                 // real time and frequency


// Update variables minn, levels, LinLogStr according to linlog 
//mtlb_min has a bug
// if (and(imag(tfr)==0))
//     minn=min(real(tfr));
// else
//     minn=mtlb_min(tfr);
//     if (and(imag(minn)==0))
//        minn=min(real(minn));
//     else
//        minn=mtlb_min(minn); 
//     end
// end;

if ~linlogtfr,
 if (display==4)|(display==5),          // surf or mesh
  minn=min(tfr);
 else
   minn=max(min(tfr),maxx*threshold/100.0);
 end
 levels=linspace(minn,maxx,levelnumb+1); 
 LinLogStr=', lin. scale';
else
 minn=max(min(tfr),maxx*threshold/100.0);
 levels=logspace(log10(minn),log10(maxx),levelnumb+1);
 LinLogStr=', log. scale';
end;

// Initialization of the current figure
 clf; 
//set(gcf,'Resize','On','NextPlot','Add');

// Initialization of the axes
if iscolorbar,
 axcb   = newaxes(); //axes('Units','normal','Visible','off','Box','On');
  axcb.margins=[0.005, 0.005, 0.005, 0.005];

 //set(gcf,'UserData',[get(gcf,'UserData') axcb]);
  axcb.tight_limits ='on';
end;

if issig,
 axsig  = newaxes();//axes('Units','normal','Visible','off','Box','On');
 //if comp(1:2)=='PC', set(axsig ,'fontsize',10); end
 //set(gcf,'UserData',[get(gcf,'UserData') axsig]);
 //axsig.margins=[0.005, 0.005, 0.005, 0.005];
axsig.margins=[0, 0, 0.015, 0.015];
 axsig.tight_limits ='on';
end;

if isspec,
 axspec = newaxes();//axes('Units','normal','Visible','off','Box','On');
 //if comp(1:2)=='PC', set(axspec,'fontsize',10); end;
 //set(gcf,'UserData',[get(gcf,'UserData') axspec]);
 //axspec.margins=[0.005, 0.005, 0.005, 0.005];
axspec.margins=[0, 0.1, 0, 0];
 axspec.tight_limits ='on';
end;

axtfr  = newaxes();//axes('Units','normal','Visible','off','Box','On');
// axtfr.margins=[0.005, 0.005, 0.005, 0.005];
axtfr.margins=[0.1, 0, 0, 0.1];
axtfr.tight_limits ='on';
//if comp(1:2)=='PC', set(axtfr ,'fontsize',10); end
//set(gcf,'UserData',[get(gcf,'UserData') axtfr]);

 // Test of analycity and computation of spec
 if ~isempty(sig),
  for k=1:Ncol,
   isana=1; alpha=2; Lt=max(t)-min(t)+1;   	
   if 2*Nf2>=Lt,
    spc(:,k)=abs(tftb_fft(sig(min(t):max(t),k),2*Nf2)).^2; 
   else
    // modifications :  F. Auger (fog), 30/11/97
    Nb_tranches_fog = floor(Lt/(2*Nf2));
    // printf('%f \n',Nb_tranches_fog);
    spc(:,k)=zeros(2*Nf2,1);
    for Num_tranche_fog=0:Nb_tranches_fog-1,
     // printf('%f \n',Num_tranche_fog);
     spc(:,k)=spc(:,k)+abs(fft(sig(min(t)+2*Nf2*Num_tranche_fog+(0:2*Nf2-1),k))).^2;
    end;

    if (Lt>Nb_tranches_fog*2*Nf2),
     spectre_fog=tftb_fft(sig(min(t)+tfrrow*Nb_tranches_fog:max(t),k),2*Nf2);
     spectre_fog=spectre_fog(:);
     spc(:,k)=spc(:,k)+abs(spectre_fog).^2; 
    end;    
    // sp=abs(fft(sig(min(t):max(t),k))).^2; 
    // fr1=(0.5*(0:Lt-1)/Lt)*fs;
    // fr2=(0.5*(0:2*Nf2-1)/2/Nf2)*fs;
    // spc(:,k)=interp1(fr1,sp,fr2);
   end
   spec1=sum(spc(1:Nf2,k));
   spec2=sum(spc(Nf2+1:2*Nf2,k));
   if spec2>0.1*spec1,
    isana=0;
    if ~isreal(sig(min(t):max(t),k)),
     alpha=1;
    end
   end
  end
 end

 if layout==1,                          // Time-Frequency Representation only
  axtfr.axes_bounds=([0.10 0.10 0.80 0.80]);
  
 elseif layout==2,			// TFR + Signal
  axtfr.axes_bounds=[0.10 0.10 0.80 0.55]; //set(axtfr,'Position',[0.10 0.10 0.80 0.55]);
  axsig.axes_bounds=[0.10 0.73 0.80 0.20];//set(axsig,'Position',[0.10 0.73 0.80 0.20]); 
   sca(axsig); //axes(axsig);  
  if sigenveloppe,
   plot((min(t):max(t))/fs,real(sig(min(t):max(t),:)),...
        (min(t):max(t))/fs, abs(sig(min(t):max(t),:)));
  else
   plot((min(t):max(t))/fs,real(sig(min(t):max(t),:)));
  end;
  //set(gca,['X' TickLabelStr],[]);
  ylabel('Real part');
  title('Signal in time');
  Min=min(real(sig)); Max=max(real(sig));
  a=gca();a.data_bounds=([min(ts) max(ts) Min Max]);

 elseif layout==3,			// TFR + spectrum
  axspec.axes_bounds=[0.10 0.10 0.15 0.80];//set(axspec,'Position',[0.10 0.10 0.15 0.80]);
   axtfr.axes_bounds=[0.35 0.10 0.55 0.80];//set(axtfr ,'Position',[0.35 0.10 0.55 0.80]);
  sca(axspec);//axes(axspec); 
  if isaffine, 
   f1=freqr(1); f2=freqr(Nf2); df=f2-f1; 
   Nf4=round((Nf2-1)*fs/(2*df))+1;
   for k=1:Ncol,
    spc(1:alpha*Nf4,k)=abs(fft(sig(min(t):max(t),k),alpha*Nf4)).^2; 
   end
   spc=spc((round(f1*2*(Nf4-1)/fs)+1):(round(f1*2*(Nf4-1)/fs)+Nf2),:);
   freqs=linspace(f1,f2,Nf2);
  else
   freqs=freqr;
   spc=spc(1:Nf2,:);
  end
  Maxsp=max(spc);
  if linlogspec==0,
   plot(freqs,spc);
   title('Linear scale'); 
   // set(axspec,'ytick',[0 Maxsp*max(eps,threshold)*0.01 Maxsp]);
   //set(axspec,'YTickMode', 'auto');
   axspec.data_bounds(:,2)=[Maxsp*threshold*0.01 Maxsp*1.2]';//set(axspec,'Ylim', [Maxsp*threshold*0.01 Maxsp*1.2]);
   axspec.data_bounds(:,1)=[fmin fmax]';//set(axspec,'Xlim', [fmin fmax]);
  else
   plot(freqs,10*log10(spc/Maxsp)); 
   title('Log. scale [dB]');
   //set(axspec,'YTickMode', 'auto');
   axspec.data_bounds(:,2)=[10*log10(threshold*0.01) 0]';//set(axspec,'Ylim',[10*log10(threshold*0.01) 0]);
   axspec.data_bounds(:,1)=[fmin fmax]';//set(axspec,'Xlim', [fmin fmax]);
  end
  xlabel('Energy spectral density');
  Nsp=length(spc); 
  //set(gca, ['X' TickLabelStr],[],'view',[-90 90]);
  axspec.rotation_angles=[0, 180];
  axspec.x_label.font_angle=-90;
    if linlogspec==0,
      axspec.x_label.position=[(fmin*fs+fmax*fs)/3, Maxsp*1.3];
    else
      axspec.x_label.position=[(fmin*fs+fmax*fs)/3, 1];
    end
 elseif layout==4,			// TFR + signal + spectrum

  axspec.axes_bounds=[0.10 0.10 0.15 0.55];//set(axspec,'Position',[0.10 0.10 0.15 0.55]);
  axsig.axes_bounds=[0.35 0.73 0.55 0.20];//set(axsig ,'Position',[0.35 0.73 0.55 0.20]);
  axtfr.axes_bounds=[0.35 0.10 0.55 0.55];//set(axtfr ,'Position',[0.35 0.10 0.55 0.55]);

  sca(axsig);//axes(axsig); 
  if sigenveloppe,
   plot((min(t):max(t))/fs,real(sig(min(t):max(t),:)),...
        (min(t):max(t))/fs, abs(sig(min(t):max(t),:)));
  else
   plot((min(t):max(t))/fs,real(sig(min(t):max(t),:)));
  end;
  ylabel('Real part');
  title('Signal in time');
  //set(gca,['X' TickLabelStr],[]);

  Min=min(real(sig)); Max=max(real(sig));
  a=gca();a.data_bounds=([min(ts) max(ts) Min Max]);

  sca(axspec);//axes(axspec); 
  if isaffine, 
// IF YOU UNDERSTAND SOMETHING TO THAT, PLEASE EXPLAIN ME (f. auger)
//   f1=freqr(1); f2=freqr(Nf2); df=f2-f1; 
//   Nf4=round((Nf2-1)*fs/(2*df))+1;
//   for k=1:Ncol,
//    spec(1:alpha*Nf4,k)=abs(fft(sig(min(t):max(t),k),alpha*Nf4)).^2; 
//   end
//   spec=spec((round(f1*2*(Nf4-1)/fs)+1):(round(f1*2*(Nf4-1)/fs)+Nf2),:);
//   freqs=linspace(f1,f2,Nf2);
   for k=1:Ncol,
    freqs=linspace(freqr(1),freqr(Nf2),Nf2); 
    spc=interp1(0.5*fs*(0:Nf2-1)/Nf2,spc(1:Nf2,k),freqs);
   end;
  else
   freqs=freqr;
   spc=spc(1:Nf2,:);
  end
  Maxsp=max(max(spc));
  if linlogspec==0,
   plot(freqs,spc);
   title('Linear scale');  
   //set(axspec,'YTickMode', 'auto');
   axspec.data_bounds(:,2)=[Maxsp*threshold*0.01 Maxsp*1.2]';//set(axspec,'Ylim', [Maxsp*threshold*0.01 Maxsp*1.2]);
   axspec.data_bounds(:,1)=[fmin*fs fmax*fs]';//set(axspec,'Xlim', [fmin*fs fmax*fs]);
  else
   plot(freqs,10*log10(spc/Maxsp)); 
   title('Log. scale [dB]');
   //set(axspec,'Ytickmode','auto');
   axspec.data_bounds(:,2)=[10*log10(threshold*0.01) 0]';//set(axspec,'Ylim',[10*log10(threshold*0.01) 0]);
   axspec.data_bounds(:,1)=[fmin*fs fmax*fs]';//set(axspec,'Xlim', [fmin*fs fmax*fs]);
  end
  xlabel('Energy spectral density');
  Nsp=length(spc); 
  axspec.rotation_angles=[0, 180];
  //set(gca,['X' TickLabelStr],[],'view',[-90 90]);
    axspec.x_label.font_angle=-90;
    if linlogspec==0,
      axspec.x_label.position=[(fmin*fs+fmax*fs)/3, Maxsp*1.3];
    else
      axspec.x_label.position=[(fmin*fs+fmax*fs)/3, 1];
    end
 end;

  if iscolorbar,                     // Is there a color bar ?
   PositionTfr=axtfr.axes_bounds;//get(axtfr,'Position');
   axtfr.axes_bounds=PositionTfr-[0 0 0.03 0];//set(axtfr,'Position',PositionTfr-[0 0 0.03 0]);
   axcb.axes_bounds= [PositionTfr(1)+PositionTfr(3)-0.15 PositionTfr(2) 0.15 PositionTfr(4)];
    //set(axcb, 'Position',[PositionTfr(1)+PositionTfr(3)-0.01,...
                      //   PositionTfr(2) 0.01 PositionTfr(4)]);
   sca(axcb); //axes(axcb); 
  // Ncolors=length(get(sdf(),"color_map"));//length(colormap); 
   //[cmin,cmax]=_caxis; 
   //colorvect=linspace(minn,maxx,Ncolors);
    if linlogtfr==0,
      colorbar(min(tfr),max(tfr));
    else
      colorbar(min(log10(tfr)),max(log10(tfr)));
    end
   //grayplot(colorvect); //axis('off'); 
    if issig,                          // there is a signal
     PositionSig=axsig.axes_bounds;//get(axsig,'Position');
     axsig.axes_bounds=PositionSig-[0 0 0.03 0];//set(axsig,'Position',PositionSig-[0 0 0.03 0]);
    end 
  end

 sca(axtfr); //axes(axtfr);                         // Display the tfr
 if (tcol==1),
  plot(freqr,tfr(1:Nf2));
   axtfr.data_bounds(:,1)=[fmin*fs fmax*fs]'; // set(axtfr,'Xlim', [fmin*fs fmax*fs]);

 else
   tfr=tfr(1:Nf2,:);
 
  indmin=find(tfr<minn);
  tfr(indmin)=minn*ones(1,length(indmin));
 
  indmax=find(tfr>maxx);
  tfr(indmax)=maxx*ones(1,length(indmax));
  //if isaffine & (display==2),
  // printf('grayplot does not support non-linear scales for axes. Replaced by Sgraycolor.\n');
  // display=3; 
  //end;

  if display==1,                  // contour
   xset("fpf"," ");
   if linlogtfr==0,
     contour2d(ts,freqr,tfr',levels);  // contour(tfr,levels,ts,freqr);
   else
    contour2d(ts,freqr,log10(tfr)',levels);  // contour(tfr,levels,ts,freqr);
   end
   axtfr.data_bounds(:,2)= [fmin*fs fmax*fs]'; //set(axtfr,'Ylim', [fmin*fs fmax*fs]);

   DisplayStr=', contour';
  elseif display==2,              // imagesc
   if linlogtfr==0,
    //_imagesc(ts,freqr,tfr); _axis('xy');
    grayplot(ts,freqr,tfr');
   else
    //_imagesc(ts,freqr,log10(tfr));_axis('xy');
      grayplot(ts,freqr,log10(tfr)');
   end
   axtfr.data_bounds(:,2)= [fmin*fs fmax*fs]'; //set(axtfr,'Ylim', [fmin*fs fmax*fs]);
   DisplayStr=', grayplot';

  elseif display==3,              // pcolor
   if linlogtfr==0,
    //_pcolor(ts,freqr,tfr); _shading interp;
   Sgrayplot(ts,freqr,tfr');
   else
    //_pcolor(ts,freqr,log10(tfr)); _shading interp;
    Sgrayplot(ts,freqr,log10(tfr)');
   end
   axtfr.data_bounds(:,2)= [fmin*fs fmax*fs]'; //set(axtfr,'Ylim', [fmin*fs fmax*fs]);
   DisplayStr=', Sgrayplot';

  elseif display==4,              // surf
   if linlogtfr==0,
    surf(ts,freqr,tfr); //_shading interp;
    zlabel('Amplitude');
    a=gca();a.data_bounds=([ts(1) ts(tcol) fmin*fs fmax*fs minn maxx]);
   else
    surf(ts,freqr,log10(tfr)); //_shading interp;
    zlabel('Positive values');
    a=gca();a.data_bounds=([ts(1) ts(tcol) fmin fmax log10(minn) log10(maxx)]);
   end
   DisplayStr=', surf';

  elseif display==5,              // mesh
   if linlogtfr==0,
    mesh(ts,freqr,tfr); //_shading interp;
    zlabel('Amplitude');
    a=gca();a.data_bounds=([ts(1) ts(tcol) fmin*fs fmax*fs minn maxx]);
   else
    mesh(ts,freqr,log10(tfr)); //_shading interp;
    zlabel('Positive values');
    a=gca();a.data_bounds=([ts(1) ts(tcol) fmin*fs fmax*fs log10(minn) log10(maxx)]);
   end
   DisplayStr=', mesh';
  end
 
 // Define the title and check the input arguments depending on 'method'

 method=part(method,4:length(method));

 if nargin==5, // if there is no additional parameters, do the best.
  title([method, LinLogStr,DisplayStr,...
         ', Threshold=',string(threshold),'%']);

 elseif (method=='WV'  ) | (method=='MH') | ...
        (method=='PAGE'), // no parameters
  title([method,', Nf=',string(Nf2), LinLogStr,DisplayStr,...
        ', Threshold=',string(threshold),'%']);

 elseif (method=='PWV'  )|(method=='PMH'  )| ...
        (method=='SP'   )|(method=='PPAGE')| ...
        (method=='RSP'  )|(method=='RPPAG')| ...
        (method=='RPWV' )|(method=='RPMH' ),
  h=p1;[hrow,hcol]=size(h); Lh=(hrow-1)/2; // one parameter
  if (hcol~=1)|(rem(hrow,2)==0),
   error('h must be a smoothing window with odd length'); end;
  title([method, ', Lh=',string(Lh), ', Nf=',string(Nf2),...
        LinLogStr, DisplayStr,', Threshold=',string(threshold),'%']);
 
 elseif (method=='STFT'), // short-time fourier transform case
  h=p1;[hrow,hcol]=size(h); Lh=(hrow-1)/2;
  if (hcol~=1)|(rem(hrow,2)==0),
   error('h must be a smoothing window with odd length'); end;
  title(['|',method, '|^2, Lh=',string(Lh),...
        ', Nf=',string(Nf2), LinLogStr, DisplayStr,', Thld=',...
        string(threshold),'%']);
 
 elseif (method=='SPWV' ) | (method=='MHS'  )| ...
        (method=='RSPWV') | (method=='RMHS' )| ...
        (method=='ZAM'  ) | (method=='RIDBN')|...
        (method=='BJ'   ) | (method=='RIDB' )| ...
        (method=='RIDH' ) | (method=='RIDT' ),
  g=p1; [grow,gcol]=size(g); Lg=(grow-1)/2;
  if (gcol~=1)|(rem(grow,2)==0),
   error('g must be a smoothing window with odd length'); end;
  h=p2; [hrow,hcol]=size(h); Lh=(hrow-1)/2; 
  if (hcol~=1)|(rem(hrow,2)==0),
   error('h must be a smoothing window with odd length'); end;
  title([method,', Lg=',string(Lg),', Lh=',string(Lh),...
         ', Nf=',string(Nf2),LinLogStr, DisplayStr,...
         ', Threshold=',string(threshold),'%']);
 
 elseif (method=='MMCE'),
  h=p1;[hrow,hcol]=size(h); Lh=(hrow-1)/2;
  if (rem(hrow,2)==0),
   error('h must be a smoothing window with odd length'); end;
  title([method, ', Lh=',string(Lh), ', Nf=',string(Nf2),...
        LinLogStr, DisplayStr,', Threshold=',string(threshold),'%']);
 
 elseif (method=='CW' ) | (method=='BUD'),
  g=p1; [grow,gcol]=size(g); Lg=(grow-1)/2;
  if (gcol~=1)|(rem(grow,2)==0),
   error('g must be a smoothing window with odd length'); end;
  h=p2; [hrow,hcol]=size(h); Lh=(hrow-1)/2; 
  if (hcol~=1)|(rem(hrow,2)==0),
   error('h must be a smoothing window with odd length'); end;
  sigma=p3;
  title([method,', Lg=',string(Lg),', Lh=',string(Lh),...
         ' sigma=',string(sigma),', Nf=',string(Nf2),...
         LinLogStr, DisplayStr, ', Threshold=',string(threshold),'%']);
 
 elseif (method=='GRD')
  g=p1; [grow,gcol]=size(g); Lg=(grow-1)/2;
  if (gcol~=1)|(rem(grow,2)==0),
   error('g must be a smoothing window with odd length'); end;
  h=p2; [hrow,hcol]=size(h); Lh=(hrow-1)/2; 
  if (hcol~=1)|(rem(hrow,2)==0),
   error('h must be a smoothing window with odd length'); end;
  title([method,', Lg=',string(Lg),', Lh=',string(Lh),...
         ', rs =',string(p3), ', M/N =',string(p4), ...
         ', Nf =',string(Nf2), ...
         LinLogStr, DisplayStr, ', Threshold=',string(threshold),'%']);
 
 elseif (method=='MSC' ) | (method=='RMSC' )
  f0T=p1; if (f0T<=0), error('f0T must be positive'); end;
  title([method, ', f0T=',string(f0T), ', Nf=',string(Nf2),...
        LinLogStr, DisplayStr, ', Threshold=',string(threshold),'%']);
 
 elseif (method=='RGAB' )
  Nh=p1; if (Nh<=0), error('Nh must be positive'); end;
  title([method, ', Nh=',string(Nh), ', Nf=',string(Nf2),...
        LinLogStr, DisplayStr, ', Threshold=',string(threshold),'%']);
 
 
 elseif (method=='DFLA' ) | (method=='UNTER' )| ...
        (method=='BERT' ),
  N=p1; 
  if (N<=0),                error('N must be positive'); end;
  title([method, ', N=',string(N), LinLogStr, DisplayStr, ', Threshold=',...
         string(threshold), '%']);  
 
 elseif (method=='SCALO'),
  Nh0=p1; N=p2; 
  if (Nh0<0),               error('Nh0 must be positive'); end;
  if (N<=0),                error('N must be positive'); end;
  if (Nh0>0), 
   title([method, ', Morlet wavelet, Nh0=', string(Nh0), ...
         ', N=',string(N), LinLogStr, DisplayStr, ', Thld=',...
         string(threshold), '%']);  
  else 
   title([method, ', Mexican hat, N=',string(N), LinLogStr, DisplayStr, ...
          ', Thld=', string(threshold), '%']);  
  end
 
 elseif (method=='ASPW'),
  Nh0=p1; Ng0=p2; N=p3; 
  if (Nh0<0),               error('Nh0 must be positive'); end;
  if (Ng0<0),               error('Ng0 must be positive'); end;
  if (N<=0),                error('N must be positive'); end;
  if (Nh0>0), 
   title([method, ', Morlet wlt, Nh0=', string(Nh0), ', Ng0=',...
         string(Ng0), ', N=',string(N), LinLogStr, DisplayStr, ', Thld=',...
         string(threshold), '%']);  
  else 
   title([method, ', Mexican hat, Ng0=',string(Ng0),...  
         ', N=',string(N), LinLogStr, DisplayStr, ', Thld=',... 
         string(threshold), '%']);  
  end
 
 elseif (method=='SPAW'),
  K=p1; Nh0=p2; Ng0=p3; N=p4; 
  if (Nh0<0),               error('Nh0 must be positive'); end;
  if (Ng0<0),               error('Ng0 must be positive'); end;
  if (N<=0),                error('N must be positive'); end;
  if (Nh0>0), 
   title([method, ', K=', string(K), ', Morlet wlt, Nh0=',...
         string(Nh0), ', Ng0=',...
         string(Ng0), ', N=',string(N), LinLogStr, DisplayStr, ', Thld=',...
         string(threshold), '%']);  
  else 
   title([method, ', K=', string(K), ', Mexican hat, Ng0=',...
         string(Ng0),', N=',string(N), LinLogStr, DisplayStr, ', Thld=',...
         string(threshold), '%']);  
  end
 
 
 elseif (method=='GABOR'),
  N=p1; Q=p2; h=p3; [hrow,hcol]=size(h); Lh=(hrow-1)/2;
  if (hcol~=1)|(rem(hrow,2)==0),
   error('h must be a smoothing window with odd length'); end;
  title([method, ', Lh=',string(Lh), ', Nf=',...
         string(Nf2),', N=',string(N),', Q=',string(Q),...
         LinLogStr, DisplayStr, ', Thld=',string(threshold),'%']);
 
 end;
end

// add the correct legend on the axes
if unitHz==1,
 xlabel('Time [s]'); ylabel('Frequency [Hz]');
elseif unitHz==2,
 xlabel('Time [ms]'); ylabel('Frequency [kHz]');
elseif unitHz==3,
 xlabel('Time [µs]'); ylabel('Frequency [MHz]');
end


if (isgridsig & issig),		// Updating of the grids
 sca(axsig); xgrid;
elseif (~isgridsig & issig),
 sca(axsig); xgrid(-1);
end 
if (isgridspec & isspec),
 sca(axspec); xgrid;
elseif (~isgridspec & isspec),
 sca(axspec); xgrid(-1);
end 

if (isgridtfr),			// upating of the grid on the tfr
 sca(axtfr); xgrid;
elseif (~isgridtfr),
 sca(axtfr); xgrid(-1);
end 
endfunction


