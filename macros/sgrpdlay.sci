function [gd,fnorm]=sgrpdlay(x,fnorm);
// Group delay estimation of a signal.
// Calling Sequence
//	[GD,FNORM]=sgrpdlay(X) 
//	[GD,FNORM]=sgrpdlay(X,FNORM) 
// Parameters
//	X     : signal in the time-domain.
//	FNORM : normalized frequency. By default, FNORM is a  linearly spaced vector between -0.5 and 0.5 with length(X) elements.
//	GD    : computed group delay. When GD equals zero, it means that the estimation of the group delay for this frequency was outside the interval [1 xrow], and therefore meaningless.
//  Description
//      sgrpdlay estimates the group delay ofsignal X at the normalized frequency(ies) FNORM.
//  Examples
//       N=128; x=amgauss(N,64,30).*fmlin(N,0.1,0.4);
//       fnorm=0.1:0.04:0.38; gd=sgrpdlay(x,fnorm); 
//       t=2:N-1; instf=instfreq(x,t);
//       plot(t,instf,gd,fnorm); a=gca();a.data_bounds=([1 N 0 0.5]); 
//   Authors
//      H. Nahrstaedt - Aug 2010
//	F. Auger, March 1994, July 1995.
//	Copyright (c) 1996 by CNRS (France).

//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  [nargout,nargin]=argn(0);
  fname="sdrgpdlay";
  if nargin == 0 then
    error(msprintf(_("%s: Wrong number of input argument: %d to %d expected.\n"),fname,1,2));
  end;
  //x
  if type(x)<>1|and(size(x)<>1) then
    error(msprintf(_("%s: Wrong type for input argument #%d: A real or complex vector expected.\n"),fname,1));
  end
  x=x(:);
  xrow=size(x,1);
  
  //fnorm
  if nargin>=2 then
    if type(fnorm)<>1|~isreal(fnorm)|and(size(fnorm)<>1) then
      error(msprintf(_("%s: Wrong type for input argument #%d: A real vector expected.\n"),fname,2));
    end
    if or(abs(fnorm)>0.5) then
      error(msprintf(_("%s: Wrong value for input argument #%d: Must be in the interval %s.\n"),fname,2,"[-0.5,0.5]"));
    end
    fnorm=matrix(fnorm,1,-1)
  else
    if (modulo(xrow,2)==0),
      L=xrow/2;
      fnorm=(-L:L-1)/xrow;
    else
      L=(xrow-1)/2;
      fnorm=(-L:L)/xrow;
    end;
  end
  
  
 
  expo=exp(-%i*2.0*%pi*fnorm'*(0:xrow-1));
  
  Num=expo*(x .* (1:xrow)');
  Den=expo*x;
  H=real(Num./Den)
  gd=H .* bool2s(H>=1&H<=xrow+3);
 
endfunction

