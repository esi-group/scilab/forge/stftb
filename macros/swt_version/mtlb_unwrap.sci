function Q=mtlb_unwrap(P,tol)

[nargout,nargin]=argn(0);

if (nargin==1),
tol = 3.1415926;
end

mo = 0;
Q=P;
t=diff(P);
for i=2:length(P),
if (t(i-1)>tol),
mo=mo - 2*tol;
end
if (t(i-1)<(-1*tol)),
mo=mo + 2*tol;
end
Q(i)=Q(i)+mo;
end


endfunction
