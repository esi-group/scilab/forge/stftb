function P=mtlb_angle(Z)

P=atan(imag(Z)/real(Z));

endfunction
