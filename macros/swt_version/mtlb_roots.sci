function r=mtlb_roots(c)

p=poly(c,'x');
r=roots(p);

endfunction
