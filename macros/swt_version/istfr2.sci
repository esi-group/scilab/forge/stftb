function flag=istfr2(method); 
// flag=istfr2(method) returns true is method is a 
// time frequency representation of type 2 (only positive frequencies). 
//	See also istfr1, istfraff. 
 
//	F. Auger, may 98 
//	Copyright (c) CNRS - France 1998.  
// 
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 
method=mtlb_upper(method); 
if mtlb_strcmp(method,'TFRWV'   ) | mtlb_strcmp(method,'TFRPWV'  ) | ... 
   mtlb_strcmp(method,'TFRSPWV' ) | mtlb_strcmp(method,'TFRCW'   ) | ... 
   mtlb_strcmp(method,'TFRZAM'  ) | mtlb_strcmp(method,'TFRBJ'   ) | ... 
   mtlb_strcmp(method,'TFRBUD'  ) | mtlb_strcmp(method,'TFRGRD'  ) | ... 
   mtlb_strcmp(method,'TFRRSPWV') | mtlb_strcmp(method,'TFRRPWV' ) | ... 
   mtlb_strcmp(method,'TFRRIDB' ) | mtlb_strcmp(method,'TFRRIDH' ) | ... 
   mtlb_strcmp(method,'TFRRIDT' ) | mtlb_strcmp(method,'TFRASPW' ) | ... 
   mtlb_strcmp(method,'TFRDFLA' ) | mtlb_strcmp(method,'TFRSPAW' ) | ... 
   mtlb_strcmp(method,'TFRRIDBN') | mtlb_strcmp(method,'TFRUNTER') | ... 
   mtlb_strcmp(method,'TFRBERT' ) | mtlb_strcmp(method,'TFRSCALO') | ... 
   mtlb_strcmp(method,'TFRSPBK' ) | mtlb_strcmp(method,'TYPE2'   ), 
 flag=1; 
else 
 flag=0; 
end; 
 
endfunction
