function [pointst,pointsf]=ridges(tfr,mhat,t,method,ptrace,opt_plot);
//  Extraction of ridges.
// Calling Sequence
//	[POINTST,POINTSF]=ridges(TFR,HAT) 
//	[POINTST,POINTSF]=ridges(TFR,HAT,T) 
//	[POINTST,POINTSF]=ridges(TFR,HAT,T,METHOD) 
//	[POINTST,POINTSF]=ridges(TFR,HAT,T,METHOD,TRACE) 
//	[POINTST,POINTSF]=ridges(...,'plot') 
// Parameters
//	TFR    : time-frequency representation
//	HAT    : complex matrix of the reassignment vectors.
//	T      : the time instant(s).
//	METHOD : the chosen representation (default: 'tfrrsp'). 
//	TRACE  : if nonzero, the progression of the algorithm is shown   (default : 0).
//      'plot':	when called with the additional string 'plot', the output values will be plotted
//	POINTST,POINTSF are two vectors for the time and frequency coordinates of the stationary points of the reassignment. Therefore, PLOT(POINTST,POINTSF,'.') shows the squeleton of the representation.
//  Description
//      ridges xtracts the ridges of a time-frequency distribution. These ridges are some
//	particular sets of curves deduced from the stationary points of
//	their  reassignment operators.
//  Examples
//	 sig=fmlin(128,0.1,0.4); g=tftb_window(21,'kaiser'); 
//	 h=tftb_window(47,'Kaiser'); t=1:2:127; 
//	 figure(1), [tfr,rtfr,hat]=tfrrspwv(sig,t,128,g,h); 
//	 ridges(tfr,hat,t,'tfrrspwv',1,'plot');
//	 figure(2), [tfr,rtfr,hat]=  tfrrsp(sig,t,128,h);   
//	 ridges(tfr,hat,t,'tfrrsp',1,'plot');
//  See also 
//   friedman
//   Authors
//      H. Nahrstaedt - Aug 2010
//	F. Auger, August 1994, December 1995.
//	Copyright (c) 1996 by CNRS (France).

//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  [nargout,nargin]=argn(0);
  fname="ridges";
  in_par=['tfr','mhat','t','method','ptrace','opt_plot'];
  in_par_min=2;
  plotting=%f;
  if (nargin>in_par_min)
    if (type(evstr(in_par(nargin)))==10)
      if convstr(evstr(in_par(nargin)))=="plot" then
        plotting=%t; 
        nargin=nargin-1;
      end; 
    end;
  end;
  clear in_par;
  
  if nargin <3 then
    error(msprintf(_("%s: Wrong number of input argument: %d to %d expected.\n"),fname,2,6));
  end;

  //tfr
  if type(tfr)<>1 then
    error(msprintf(_("%s: Wrong type for input argument #%d: A matrix of double expected.\n"),fname,1));
  end
  [tfrrow,tfrcol]=size(tfr);
  
   //mhat
  if type(mhat)<>1 then
    error(msprintf(_("%s: Wrong type for input argument #%d: A matrix of double expected.\n"),fname,2));
  end
  [hatrow,hatcol]=size(mhat);
  
  if (tfrrow~=hatrow)|(tfrcol~=hatcol) then
    error(msprintf(_("%s: Arguments #%d and #%d must have the same sizes.\n"),fname,1,2));
  end;

    //ptrace
  if nargin==5 then 
    if and(type(ptrace)<>[1 4])|size(ptrace,"*")<>1 then
      error(msprintf(_("%s: Wrong type for argument #%d: A boolean or real scalar expected.\n"),fname,5));
    end
  else
    ptrace=%f;
  end
  
  //method
  if nargin>=4 then
    if type(method)<>10|size(method,"*")<>1 then
      error(msprintf(_("%s: Wrong type for input argument #%d: A character string expected.\n"),fname,4));
    end
    method=convstr(method,"U")
    M=['TFRRPWV' 'TFRRPMH' 'TFRRSPWV' 'TFRRSP' 'TYPE1'];
    if and(method<>M) then
      error(msprintf(_("%s: Wrong value for input argument #%d: Must be in the set {%s}.\n"),fname,3,...
                     """TFRRPWV"",""TFRRPMH"",""TFRRSPWV"",""TFRRSP"",""TYPE1"""));
    end
  else
    method='TFRRSP'
  end

  //t
  if nargin>=3 then
    if type(t)<>1|and(size(t)>1)|~isreal(t) then
      error(msprintf(_("%s: Wrong type for input argument #%d: A real vector expected.\n"),fname,3));
    end
    if min(t)<1 then
      error(msprintf(_("%s: Wrong value for input argument #%d: The elements must be >=.\n"),fname,3,1));
    end
    t=matrix(t,1,-1);
  else
    t=1:tfrcol;
  end
  tcol = size(t,2);



  Nf=tfrrow; frequencies=(1:Nf)'; 
  threshold=sum(tfr)*0.5/(tfrrow*tfrcol);

  pointst=[]; pointsf=[];

  if ptrace, printf ('\nRidge extraction: \n'); end;

 
  if (method=='TFRRPWV') | (method=='TFRRPMH'),
    for icol=1:tfrcol, ti=t(icol); 
      if ptrace, disprog(icol,tfrcol,10); end;
      indices=find((tfr(:,icol)>threshold)&(mhat(:,icol)-frequencies==0))'; 
      nbindices=length(indices);
      if (nbindices>0), 
        pointst=[pointst;ones(nbindices,1)*ti];
        pointsf=[pointsf;indices/(2.0*Nf)]; 
      end;
    end;
  elseif (method=='TFRRSPWV'),
    for icol=1:tfrcol, ti=t(icol); 
      if ptrace, disprog(icol,tfrcol,10); end;
      indices=find((real(mhat(:,icol))-frequencies==0)&...
                   (imag(mhat(:,icol))-icol==0)&...
                   (tfr(:,icol)>threshold))'; 
      nbindices=length(indices);
      if (nbindices>0), 
       
        pointst=[pointst;ones(nbindices,1)*ti];
        pointsf=[pointsf;indices/(2.0*Nf)]; 
      end;
    end;
  elseif (method=='TFRRSP')|strcmp(method,'TYPE1')
    for icol=1:tfrcol, ti=t(icol); 
      if ptrace, disprog(icol,tfrcol,10); end;
      indices=find((real(mhat(:,icol))-frequencies==0)&...
                   (imag(mhat(:,icol))-icol==0)&...
                   (tfr(:,icol)>threshold))'; 
      nbindices=length(indices);
      if (nbindices>0), 
        pointst=[pointst;ones(nbindices,1)*ti];
        pointsf=[pointsf;indices/Nf]; 
      end;
    end;

  end;

  if plotting then
    clf;
    plot(pointst,pointsf,'.')  
    a=gca();a.data_bounds=[min(t) max(t) 0 0.5];
  end;
endfunction
