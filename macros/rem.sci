function r = rem(x,y)
//Return the remainder of the division x/y
// Calling Sequence
// r = rem(x,y)

// Copyright (C) 1993, 1994, 1995, 1996, 1997, 1999, 2000, 2002, 2004,
//               2005, 2006, 2007, 2008, 2009 John W. Eaton
//
// This file is part of Octave.
//
// Octave is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// Octave is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Octave; see the file COPYING.  If not, see
// <http://www.gnu.org/licenses/>.

// -*- texinfo -*-
// @deftypefn {Mapping Function} {} rem (@var{x}, @var{y})
// Return the remainder of the division @code{@var{x} / @var{y}}, computed
// using the expression
//
// @example
// x - y .* fix (x ./ y)
// @end example
//
// An error message is printed if the dimensions of the arguments do not
// agree, or if either of the arguments is complex.
// @seealso{mod, fmod}
// @end deftypefn

// Author: jwe


  [nargout,nargin]=argn(0);
  fname="rem";
  if nargin <>2 then
    error(msprintf(_("%s: Wrong number of input argument: %d expected.\n"),fname,2));
  end
  
  //x
  if type(x)==1 then
    if ~isreal(x) then
      error(msprintf(_("%s: Wrong type for input argument #%d: A real array expected.\n"),fname,1));
    end
  elseif type(x)<>8 then
    error(msprintf(_("%s: Wrong type for input argument #%d: An integet type array expected.\n"),fname,1));
  end 

  //y
  if type(y)==1 then
    if ~isreal(y) then
      error(msprintf(_("%s: Wrong type for input argument #%d: A real array expected.\n"),fname,2));
    end
  elseif type(y)<>8 then
    error(msprintf(_("%s: Wrong type for input argument #%d: An integer type array expected.\n"),fname,2));
    
  end 
  if size(x,"*")<>1& size(y,"*")<>1 then
    if or(size(x)<>size(y)) then
      error(msprintf(_("%s: Arguments #%d and #%d must have the same sizes.\n"),fname,1,2));
    end
  end
  y=iconvert(y,inttype(x))
  r = x - y .* fix (x ./ y);
endfunction
//assert(rem ([1, 2, 3; -1, -2, -3], 2), [1, 0, 1; -1, 0, -1]);
//assert(rem ([1, 2, 3; -1, -2, -3], 2 * ones (2, 3)),[1, 0, 1; -1, 0, -1]);
//error rem ();
//error rem (1, 2, 3);
//error rem ([1, 2], [3, 4, 5]);
//error rem (i, 1);
//assert(rem (uint8([1, 2, 3; -1, -2, -3]), uint8 (2)), uint8([1, 0, 1; -1, 0, -1]));
//assert(uint8(rem ([1, 2, 3; -1, -2, -3], 2 * ones (2, 3))),uint8([1, 0, 1; -1, 0, -1]));
//error rem (uint(8),int8(5));
//error rem (uint8([1, 2]), uint8([3, 4, 5]));
