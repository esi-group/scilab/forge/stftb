function [tfr,rtfr,rhat] = tfrrspwv(x,t,N,g,h,ptrace,opt_plot);
// Reassigned smoothed pseudo Wigner-Ville distribution.
// Calling Sequence
//	[TFR,RTFR,HAT] = tfrrspwv(X)
//	[TFR,RTFR,HAT] = tfrrspwv(X,T)
//	[TFR,RTFR,HAT] = tfrrspwv(X,T,N)
//	[TFR,RTFR,HAT] = tfrrspwv(X,T,N,G)
//	[TFR,RTFR,HAT] = tfrrspwv(X,T,N,G,H)
//	[TFR,RTFR,HAT] = tfrrspwv(X,T,N,G,H,TRACE)
//	[TFR,RTFR,HAT] = tfrrspwv(...,'plot')
//  Parameters
//	X     : analysed signal.
//	T     : the time instant(s)      (default : 1:length(X)).
//	N     : number of frequency bins (default : length(X)).
//	G     : time smoothing window, G(0) being forced to 1.   (default : Hamming(N/10)). 
//	H     : frequency smoothing window, H(0) being forced to 1     (default : Hamming(N/4)).
//	TRACE : if nonzero, the progression of the algorithm is shown      (default : 0).
//      'plot':	if one input parameter is 'plot',  tfrrspwv runs tfrqview. and TFR will be plotted
//	TFR,  : time-frequency representation and its reassigned
//	RTFR  : version.
//	HAT   : Complex matrix of the reassignment vectors.
//  Description
//      tfrrspwv computes the smoothed pseudo Wigner-Ville distribution and its
//	reassigned version.
// Examples
//       sig=fmlin(128,0.05,0.15)+fmlin(128,0.3,0.4); t=1:2:128; 
//       g=tftb_window(15,'Kaiser'); h=tftb_window(63,'Kaiser');  
//       tfrrspwv(sig,t,64,g,h,1,'plot');
//     Authors
//      H. Nahrstaedt - Aug 2010
//	F. Auger, May-July 1994, July 1995.
//	Copyright (c) 1996 by CNRS (France).

//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  [nargout,nargin]=argn(0);
  fname="tfrrspwv"
  in_par=['x','t','N','g','h','ptrace','opt_plot'];
  in_par_min=1;
  plotting=%f;
  if (nargin>in_par_min)
    if (type(evstr(in_par(nargin)))==10)
      if convstr(evstr(in_par(nargin)))=="plot" then
        plotting=%t; 
        nargin=nargin-1;
      end; 
    end;
  end;
  clear in_par;

  if nargin == 0 then
    error(msprintf(_("%s: Wrong number of input argument: %d to %d expected.\n"),fname,1,7));
  end;

  //x
  if type(x)<>1|and(size(x)<>1) then
    error(msprintf(_("%s: Wrong type for argument #%d: A vector of double expected.\n"),fname,1));
  end
  x=x(:)
  Nx=size(x,1)

  //ptrace
  if nargin>=6 then 
    if and(type(ptrace)<>[1 4])|size(ptrace,"*")<>1 then
      error(msprintf(_("%s: Wrong type for argument #%d: A boolean or real scalar expected.\n"),fname,6));
    end
  else
    ptrace=%f;
  end
  
  //h
  if nargin>=5 then 
    if type(h)<>1|and(size(h)>1)|~isreal(h) then 
      error(msprintf(_("%s: Wrong type for argument #%d: A real vector expected.\n"),fname,5));
    end
    hlength=size(h,"*");
    if (modulo(hlength,2)==0),
      error(msprintf(_("%s: Wrong size for argument #%d: A odd number of elements expected.\n"),fname,5));
    end;
  else
    hlength=floor(Nx/4);
    if rem(hlength,2)==0 then hlength=hlength+1;end;
    h = window("hm",hlength)
  end
  h=h(:);
  Lh=(hlength-1)/2; h=h/h(Lh+1);

  //g
  if nargin>=4 then 
    if type(h)<>1|and(size(h)>1)|~isreal(h) then 
      error(msprintf(_("%s: Wrong type for argument #%d: A real vector expected.\n"),fname,4));
    end
    glength=size(g,"*");
    if (modulo(glength,2)==0),
      error(msprintf(_("%s: Wrong size for argument #%d: A odd number of elements expected.\n"),fname,4));
    end;
  else
    glength=floor(Nx/10);
    if rem(glength,2)==0 then glength=glength+1;end;
    g = window("hm",glength)
  end
  g=g(:);
  Lg=(glength-1)/2; ;

  //N
  if nargin>=3 then
    if type(N)<>1|size(N,"*")>1|~isreal(N)|int(N)<>N then
      error(msprintf(_("%s: Wrong type for input argument #%d: A integer value expected.\n"),fname,3));
    end 
    if N<=0 then
      error(msprintf(_("%s: Wrong value for input argument #%d: A positive integer expected.\n"),fname,3));
    end
    
  else
    N=Nx
  end

  //T
  if nargin>=2 then
    if type(t)<>1|and(size(t)>1)|~isreal(t) then
      error(msprintf(_("%s: Wrong type for input argument #%d: A real vector expected.\n"),fname,2));
    end
    if max(t)>Nx|min(t)<1 then
      error(msprintf(_("%s: Wrong value for input argument #%d: The elements must be in the interval %s.\n"),fname,2,"[1 "+string(Nx)+"]"));
    end
    if size(t,"*")>1 then
      Deltat=diff(t);
      if min(Deltat)<>max(Deltat) then
        error(msprintf(_("%s: Wrong value for input argument #%d: Regularly spaced elements expected.\n"),fname,2));
      end
      t=matrix(t,1,-1);
    end
  else
    t=1:Nx;
  end
  Nt=size(t,2);


  if (Nt==1),
    Dt=1; 
  else
    Deltat=diff(t); 
    Mini=min(Deltat); Maxi=max(Deltat);
    if (Mini~=Maxi),
      error(msprintf(_("%s: Wrong value for input argument #%d: Regularly spaced elements expected.\n"),fname,2));
    else
      Dt=Mini;
    end;
    clear Deltat Mini Maxi;
  end;

  tfr= zeros(N,Nt); 
  tf2= zeros(N,Nt); 
  tf3= zeros(N,Nt);
  if ptrace, disp('Smoothed pseudo Wigner-Ville distribution'); end;
  Dh=dwindow(h); // Tg=g.*[-Lg:Lg]'; 
  for icol=1:Nt,
    ti= t(icol);
    taumax=min([ti+Lg-1,Nx-ti+Lg,round(N/2)-1,Lh]);
    if ptrace, disprog(icol,Nt,10); end;

    // tau=0
    points= -min([Lg,Nx-ti]):min([Lg,ti-1]);
    g2=g(Lg+1+points); g2=g2/sum(g2); Tg2= g2 .* points.' ;
    xx= x(ti-points) .* conj(x(ti-points));
    tfr(1,icol)= sum( g2 .* xx) ; 
    tf2(1,icol)= sum( Tg2 .* xx) ;
    tf3(1,icol)= Dh(Lh+1) * tfr(1,icol) ;

    for tau=1:taumax,
      points= -min([Lg,Nx-ti-tau]):min([Lg,ti-tau-1]);
      g2=g(Lg+1+points); g2=g2/sum(g2); Tg2= g2 .* points.' ;
      xx=x(ti+tau-points,1) .* conj(x(ti-tau-points));
      tfr(  1+tau,icol)= sum( g2 .* xx);
      tf3(  1+tau,icol)=Dh(Lh+tau+1) * tfr(  1+tau,icol) ;
      tfr(  1+tau,icol)= h(Lh+tau+1) * tfr(  1+tau,icol) ;
      tf2(  1+tau,icol)= h(Lh+tau+1) * sum(Tg2 .* xx);

      tfr(N+1-tau,icol)= sum( g2 .* conj(xx));
      tf3(N+1-tau,icol)=Dh(Lh-tau+1) * tfr(N+1-tau,icol);
      tfr(N+1-tau,icol)= h(Lh-tau+1) * tfr(N+1-tau,icol);
      tf2(N+1-tau,icol)= h(Lh-tau+1) * sum(Tg2 .* conj(xx));
    end;
  end;

  tfr=real(fft(tfr,-1,1));
  tf2=real(fft(tf2,-1,1));
  tf3=imag(fft(tf3,-1,1));


  tfr=tfr(:); tf2=tf2(:); tf3=tf3(:);
  avoid_warn=find(tfr~=0);
  tf2(avoid_warn)=round(tf2(avoid_warn)./tfr(avoid_warn)/Dt);
  tf3(avoid_warn)=round(N*tf3(avoid_warn)./tfr(avoid_warn)/(2.0*%pi));
  if ptrace, printf ('\nreassignment: \n'); end;
  tfr=matrix(tfr,N,Nt);
  tf2=matrix(tf2,N,Nt);
  tf3=matrix(tf3,N,Nt);


  rtfr= zeros(N,Nt); 
  Ex=mean(abs(x(min(t):max(t))).^2); Threshold=1.0e-6*Ex;
  for icol=1:Nt,
    if ptrace, disprog(icol,Nt,10); end;
    for jcol=1:N,
      if abs(tfr(jcol,icol))>Threshold,
        icolhat= icol - tf2(jcol,icol);
        icolhat=min(max(real(icolhat),1),Nt);
        jcolhat= jcol - tf3(jcol,icol);
        jcolhat=real(rem(rem(jcolhat-1,N)+N,N)+1);
        rtfr(jcolhat,icolhat)= rtfr(jcolhat,icolhat) + tfr(jcol,icol);
        tf2(jcol,icol)= jcolhat + %i * icolhat;
      else 
        tf2(jcol,icol)=%inf*(1+%i);
        rtfr(jcol,icol)=rtfr(jcol,icol) + tfr(jcol,icol) ;
      end;
    end;
  end;

  if ptrace, printf('\n'); end;
  clear tf3; 

  if (plotting),
    TFTBcontinue=%t;
    while TFTBcontinue
      choice=x_choose(['smoothed pseudo Wigner-Ville distribution',...
                       'reassigned smoothed pseudo Wigner-Ville distribution'],'Choose the representation:','stop');
      if (choice==0), TFTBcontinue=%f;
      elseif (choice==1), 
        tfrqview(tfr,x,t,'tfrspwv',g,h);
      elseif (choice==2),
        tfrqview(rtfr,x,t,'tfrrspwv',g,h);
      end;
    end;
  end
  if (nargout>2),
    rhat=tf2;
  end;
endfunction
