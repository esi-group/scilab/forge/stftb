function [tfr,t,f]=tfrideal(iflaws,t,N,ptrace,opt_plot)
// Ideal TFR for given instantaneous frequency laws.
// Calling Sequence
//	[TFR,T,F]=tfrideal(IFLAWS) 
//	[TFR,T,F]=tfrideal(IFLAWS,T,) 
//	[TFR,T,F]=tfrideal(IFLAWS,T,N) 
//	[TFR,T,F]=tfrideal(IFLAWS,T,N,TRACE) 
//	[TFR,T,F]=tfrideal(...,'plot') 
// Parameters
//	IFLAWS : (M,P)-matrix where each column corresponds to the instantaneous frequency law of an (M,1)-signal, These P signals do not need to be present at the same time. The values of IFLAWS must be between 0 and 0.5.
//	T      : the time instant(s)      (default : 1:M).
//	N      : number of frequency bins (default : M).
//	TRACE  : if nonzero, the progression of the algorithm is shown   (default : 0).
//      'plot':	if one input parameter is 'plot',  tfrideal runs tfrqview. and TFR will be plotted
//	TFR    : output time-frequency matrix, of size (N,length(t)).
//	F      : vector of normalized frequencies.
//  Description
//    tfridealgenerates the ideal time-frequency representation corresponding to the
//	instantaneous frequency laws of the components of a signal. 
// Examples
//         N=140; t=0:N-1; [x1,if1]=fmlin(N,0.05,0.3); 
//         [x2,if2]=fmsin(70,0.35,0.45,60);
//         if2=[zeros(35,1)*%nan;if2;zeros(35,1)*%nan];
//         tfrideal([if1 if2],'plot'); 
//     Authors
//      H. Nahrstaedt - Aug 2010
//	O. Lemoine, F. Auger - March, April 1996.
//	Copyright (c) 1996 by CNRS (France).

//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  [nargout,nargin]=argn(0);
  fname="tfrideal";

  in_par=['iflaws','t','N','ptrace','opt_plot'];
  in_par_min=1;
  plotting=%f;
  if (nargin>in_par_min)
    if (type(evstr(in_par(nargin)))==10)
      if convstr(evstr(in_par(nargin)))=="plot" then
        plotting=%t; 
        nargin=nargin-1;
      end; 
    end;
  end;
  clear in_par;
  
  if nargin == 0 then
    error(msprintf(_("%s: Wrong number of input arguments: %d to %d expected.\n"),fname,1,5));
  end;

  //iflaw
  if type(iflaws)<>1 then 
    error(msprintf(_("%s: Wrong type for input argument #%d: A matrix of double expected.\n"),fname,1));
  end
   if or(iflaws>0.5) | or(iflaws<0),
     error(msprintf(_("%s: Wrong value for input argument #%d: The elements must be in the interval %s.\n"),fname,1,"[1 0.5]"));
  end

  [ifrow,ifcol]=size(iflaws);
  
  //t
  if nargin>=2 then
    if type(t)<>1|and(size(t)<>1)|~isreal(t)|or(int(t)<>t) then 
      error(msprintf(_("%s: Wrong type for input argument #%d: A vector of integer values expected.\n"),fname,2));
      t=matrix(t,1,-1);
    end
  else
    t=1:ifrow;
  end
  tcol=size(t,2);
  
  //N
  if nargin>=3 then
    if type(N)<>1|size(N,"*")>1|~isreal(N)|int(N)<>N then
      error(msprintf(_("%s: Wrong type for input argument #%d: A integer value expected.\n"),fname,3));
    end 
  else
    N=ifrow;
  end
  
  //ptrace
  if nargin>=4 then 
    if and(type(ptrace)<>[1 4])|size(ptrace,"*")<>1 then
      error(msprintf(_("%s: Wrong type for argument #%d: A boolean or real scalar expected.\n"),fname,4));
    end
  else
    ptrace=%f;
  end

  tfr=zeros(N,tcol);

 
  if ptrace, disp('Ideal time-frequency distribution'); end;

  for icol=1:tcol,
    if ptrace, disprog(icol,tcol,10); end;
    ti= t(icol); 
    for fi=1:ifcol,
      if isnan(iflaws(ti,fi)),
        tfr(fi,icol)=%nan;
      else
        tfr(round(iflaws(ti,fi)*2*(N-1))+1,icol)=1;
      end
    end
  end;

  if plotting then
    f=(0:N-1)/(2*N); 
    xset("fpf"," ");
    contour(t,f,tfr',1,'y');
    xlabel(_('Time')); ylabel(_('Normalized frequency'));
    title(_('Ideal time-frequency representation'));
    xgrid;
  end
  if (nargout==3),
    f=(0.5*(0:N-1)/N)';
  end;

endfunction
