function [scalo,f,T,a,wt,wavescaled] = contwtgnmir(x,fmin,fmax,N,wave);
// Continuous wavelet transform of mirrored 1-D signals
// Calling Sequence
// [scalo,f,T,a,wt,wavescaled] = contwtgnmir(x,fmin,fmax,N,wave);
// Parameters
//   x      : signal (in time) to be analyzed          
//fmin,fmax : respectively lower and upper frequency bounds of the analysis (in cycles/sec).
//     N    : number of analyzed voices
//     wave : specifies the analyzing wavelet    An order "wave" derivative of the Gaussian is chosen 
//    scalo : scalogram (squared magnitude of WT)
//    f     : frequency samples (geometrically sampled between FMAX 	and FMIN).
//    T     : time samples
//    a     : scale vector (geometrically sampled between 1 and FMAX/FMIN)
  
//    wt    : coefficient of the wavelet transform. X-axis corresponds to time
//            (uniformly sampled), Y-axis corresponds to frequency (or
//            scale) samples  (geometrically sampled between Fmin
//           (resp. Fmax/Fmin aand Fmax (resp. 1) First row of WT corresponds to
//            the lowest analyzed frequency.
  
// wavescaled : when the analyzing wavelet is Morlet or Mexican hat, wavescaled = wave. 
//               For an aritrary band-pass analyzing function, wavescaled
//              contains columnwise 
//              the (N) scaled version of it
  
// Description
// If x = [a b c e f] is the signal to analyzed, contwtmir runs contwt
// on the mirrored version XxX = [c b [a  b  c d e f] e d]. The number of
// mirrored samples depends on the analyzed scale and the wavelet length.
// USE AN ORDER "wave" DERIVATIVE OF THE GAUSSIAN


//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  [nargout,nargin]=argn(0);
  fname="contwtgnmir";
  
  if nargin == 0 then
    error(msprintf(_("%s: Wrong number of input argument: %d to %d expected.\n"),fname,1,5));
  end;

  //x
  if type(x)<>1|~isreal(x)|and(size(x)>1) then
    error(msprintf(_("%s: Wrong type for input argument #%d: A real vector expected.\n"),fname,1));
  end
  x=matrix(x,1,-1);
  nx=size(x,2)
  T = 1 : nx;
  
  //wave
  if nargin==5 then
  else
    wave = 2 ;
  end
  
  
  //N
  if nargin>=4 then
    if type(N)<>1|size(N,"*")>1|~isreal(N)|int(N)<>N then
      error(msprintf(_("%s: Wrong type for input argument #%d: A integer value expected.\n"),fname,4));
    end 
    if N<=0 then
      error(msprintf(_("%s: Wrong value for input argument #%d: A positive integer expected.\n"),fname,4));
    end
  else
    N=[]
  end
  
  //fmax
  if nargin>=3 then
     if type(fmax)<>1|size(fmax,"*")>1|~isreal(fmax) then
       error(msprintf(_("%s: Wrong type for input argument #%d: A real expected.\n"),fname,3));
     end 
     if fmax<=0|fmax>0.5 then
       error(msprintf(_("%s: Wrong value for input argument #%d: Must be in the interval %s.\n"),fname,3,"]0,0.5]"));
     end
  else
    fmax=[]
  end
  
  //fmin
  if nargin>=2 then
     if type(fmin)<>1|size(fmin,"*")>1|~isreal(fmin) then
       error(msprintf(_("%s: Wrong type for input argument #%d: A real expected.\n"),fname,2));
     end 
     if fmin<=0|fmin>0.5 then
       error(msprintf(_("%s: Wrong value for input argument #%d: Must be in the interval %s.\n"),fname,2,"]0,0.5]"));
     end
     if fmin>fmax then
       error(msprintf(_("%s: Wrong value for input argument #%d: Must be < %g.\n"),fname,2,fmax));
     end
  else
    fmin=[]
  end
  
  if fmin==[]|fmax==[] then
    XTF = fft(fftshift(x)) ;
    sp = (abs(XTF(1:nx/2))).^2 ;
    f = linspace(0,0.5,nx/2+1) ; f = f(1:nx/2) ;
    plot(f,sp) ; xgrid ;
    xlabel(_('Frequency'));
    title(_('Analyzed Signal Spectrum')) ;

    if fmin==[] then
      while %t
        fmin = input(_('lower frequency bound = ')) ;
        if fmin<>[]&fmin>0&fmin<=0.5 then break,end
      end
    end
    if fmax==[] then
      while %t
        fmax = input(_('upper frequency bound = ')) ;
        if fmax<>[]&fmax>fmin&fmax<=0.5 then break,end
      end
    end
  end
  if N==[] then
    while %t
      N = input(_('Frequency samples = ')) ;
      if N<>[]&N>1 then break,end
    end
  end
 

  f = logspace(log10(fmax),log10(fmin),N) ;
  a = logspace(log10(1),log10(fmax/fmin),N) ;
  amax = max(a) ;
  for ptr = 1:N
    ha = gaussn(f(ptr),wave) ; 
    nha = (length(ha)-1)/2 ;
    nbmir = min(nx,nha) ;
    x_mir = [x(nbmir:-1:2) x x(nx-1:-1:nx-nbmir+1)] ;
    detail = clean(convol(x_mir,ha)) ;
    wt(ptr,1:nx) = detail(nha + nbmir  : nha + nbmir + nx -1 ) ;
  end  
  wavescaled = wave ;

  //////// pour etre compatible avec le format de donnees de TFTB //////////
  wt=wt($:-1:1,:);
  a=a($:-1:1).';
  f=f($:-1:1).';
  scalo = (wt.*conj(wt)) ;

endfunction
