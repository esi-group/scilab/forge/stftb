function [fm,B]=locfreq(sig);
// Frequency localization caracteristics.
// Calling Sequence
//	[FM,B]=locfreq(SIG)
//  Parameters
//	SIG   is the signal.
//	FM    is the averaged normalized frequency center.
//	B     is the frequency spreading.
//  Description
//    locfreq  computes the frequency localization caracteristics of signal SIG.
//  Examples
//     z=amgauss(160,80,50);[tm,T]=loctime(z),[fm,B]=locfreq(z),B*T
//    See also 
//       loctime
//   Authors
//      H. Nahrstaedt - Aug 2010
//	F. Auger, July 1995.
//	Copyright (c) 1996 by CNRS (France).

//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  [nargout,nargin]=argn(0);
  fname="locfreq"
  if nargin <>1 then
    error(msprintf(_("%s: Wrong number of input argument: %d expected.\n"),fname,1));
  end;
  
  //sig
  if type(sig)<>1|and(size(sig)<>1) then
    error(msprintf(_("%s: Wrong type for input argument #%d: A real vector expected.\n"),fname,1));
  end
  sig=sig(:);
  N=size(sig,1)
  if  modulo(N,2)<>0 then
    sig=sig(1:$-1);
    N=N-1
  end
  Sig2=abs(fft(sig)).^2;
  Sig2=Sig2/mean(Sig2);
  Ns2=N/2;
  freqs=[0:Ns2-1 -Ns2:-1]'/N;
  fm=mean(freqs.*Sig2);
  B=2*sqrt(%pi*mean(((freqs-fm).^2).*Sig2));
endfunction
