function [tfr,rtfr,rhat] = tfrrsp(x,t,N,h,ptrace,opt_plot);
// Reassigned Spectrogram.
// Calling Sequence
//	[TFR,RTFR,HAT] = TFRRSP(X) 
//	[TFR,RTFR,HAT] = TFRRSP(X,T) 
//	[TFR,RTFR,HAT] = TFRRSP(X,T,N) 
//	[TFR,RTFR,HAT] = TFRRSP(X,T,N,H) 
//	[TFR,RTFR,HAT] = TFRRSP(X,T,N,H,TRACE) 
//	[TFR,RTFR,HAT] = TFRRSP(...,'plot') 
//  Parameters
//	X     : analysed signal.
//	T     : the time instant(s)      (default : 1:length(X)).
//	N     : number of frequency bins (default : length(X)).
//	H     : frequency smoothing window, H(0) being forced to 1     (default : Hamming(N/4)).
//	TRACE : if nonzero, the progression of the algorithm is shown       (default : 0).
//      'plot':	if one input parameter is 'plot',  tfrrsp runs tfrqview. and TFR will be plotted
//	TFR,  : time-frequency representation and its reassigned
//	RTFR    version. 
//	HAT   : Complex matrix of the reassignment vectors.
// Description
//     tfrrsp computes the spectrogram and its reassigned version.
// Examples
//       sig=fmlin(128,0.1,0.
//	X     : analysed signal.
//	T     : the time instant(s)      (default : 1:length(X)).
//	N     : number of frequency bins (default : length(X)).
//	H     : frequency smoothing window, H(0) being forced to 1     (default : Hamming(N/4)).
//	TRACE : if nonzero, the progression of the algorithm is shown       (default : 0).
//      'plot':	if one input parameter is 'plot',  tfrrsp runs tfrqview. and TFR will be plotted
//	TFR,  : time-frequency representation and its reassigned
//	RTFR    version. 
//	HAT   : Complex matrix of the reassignment vectors.
// Description
//     tfrrsp computes the spectrogram and its reassigned version.
// Examples
//       sig=fmlin(128,0.1,0.4); t=1:2:128;
//       h=tftb_window(17,'Kaiser'); tfrrsp(sig,t,64,h,1,'plot');
//     Authors
//      H. Nahrstaedt - Aug 2010
//	F. Auger, May-July 1994, July 1995.
  //       h=tftb_window(17,'Kaiser'); tfrrsp(sig,t,64,h,1,'plot');
  //     Authors
  //      H. Nahrstaedt - Aug 2010
  //	F. Auger, May-July 1994, July 1995.
  //	Copyright (c) 1996 by CNRS (France).

  //
  //  This program is free software; you can redistribute it and/or modify
  //  it under the terms of the GNU General Public License as published by
  //  the Free Software Foundation; either version 2 of the License, or
  //  (at your option) any later version.
  //
  //  This program is distributed in the hope that it will be useful,
  //  but WITHOUT ANY WARRANTY; without even the implied warranty of
  //  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  //  GNU General Public License for more details.
  //
  //  You should have received a copy of the GNU General Public License
  //  along with this program; if not, write to the Free Software
  //  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  [nargout,nargin]=argn(0);
  fname="tfrrsp"

  in_par=['x','t','N','h','ptrace','opt_plot'];
  in_par_min=1;
  plotting=%f;
  if (nargin>in_par_min)
    if (type(evstr(in_par(nargin)))==10)
      if convstr(evstr(in_par(nargin)))=="plot" then
        plotting=%t; 
        nargin=nargin-1;
      end; 
    end;
  end;
  clear in_par;

  if nargin == 0 then
    error(msprintf(_("%s: Wrong number of input argument: %d to %d expected.\n"),fname,1,6));
  end;

  //x
  if type(x)<>1 then
    error(msprintf(_("%s: Wrong type for input argument #%d: A matrix of double expected.\n"),fname,1));
  end
  x=x(:);
  xrow = size(x,1);
  Nx=xrow

  //ptrace
  if nargin>=5 then 
    if and(type(ptrace)<>[1 4])|size(ptrace,"*")<>1 then
      error(msprintf(_("%s: Wrong type for argument #%d: A boolean or real scalar expected.\n"),fname,5));
    end
  else
    ptrace=%f;
  end  
  
  //h
  if nargin>=4 then 
    if type(h)<>1|and(size(h)>1)|~isreal(h) then 
      error(msprintf(_("%s: Wrong type for argument #%d: A real vector expected.\n"),fname,4));
    end
    hlength=size(h,"*");
    if (modulo(hlength,2)==0),
      error(msprintf(_("%s: Wrong size for argument #%d: A odd number of elements expected.\n"),fname,4));
    end;
  else
    hlength=floor(xrow/4);
    if rem(hlength,2)==0 then hlength=hlength+1;end;
    h = window("hm",hlength)
  end
  h=h(:);
  Lh=(hlength-1)/2; //h=h/h(Lh+1);
  

  //N
  if nargin>=3 then
    if type(N)<>1|size(N,"*")>1|~isreal(N)|int(N)<>N then
      error(msprintf(_("%s: Wrong type for input argument #%d: A integer value expected.\n"),fname,3));
    end 
    if N<=0 then
      error(msprintf(_("%s: Wrong value for input argument #%d: A positive integer expected.\n"),fname,3));
    end
    
  else
    N=xrow
  end

  //T
  if nargin>=2 then
    if type(t)<>1|and(size(t)>1)|~isreal(t) then
      error(msprintf(_("%s: Wrong type for input argument #%d: A real vector expected.\n"),fname,2));
    end
    if max(t)>xrow|min(t)<1 then
      error(msprintf(_("%s: Wrong value for input argument #%d: The elements must be in the interval %s.\n"),fname,2,"[1 "+string(xrow)+"]"));
    end
  else
    t=1:xrow;
  end
  tcol=size(t,2);
  

  if (tcol==1),
    Dt=1; 
  else
    Deltat=diff(t); 
    Mini=min(Deltat); Maxi=max(Deltat);
    if (Mini~=Maxi),
      error(msprintf(_("%s: Wrong value for input argument #%d: The elements must be regularly spaced.\n"),fname,2))
    else
      Dt=Mini;
    end;
    clear Deltat Mini Maxi;
  end;

  tfr= zeros(N,tcol); 
  tf2= zeros(N,tcol); 
  tf3= zeros (N,tcol);
  if ptrace, printf ('Spectrogram\n'); end;
  Th=h.*[-Lh:Lh]'; 
  Dh=dwindow(h);
  //Calcul des STFT
  norm_h=zeros(tcol)
  for icol=1:tcol,
    ti= t(icol); 
    tau=-min([round(N/2)-1,Lh,ti-1]):min([round(N/2)-1,Lh,xrow-ti]);
    indices= rem(N+tau,N)+1;
    if ptrace, disprog(icol,tcol,10); end;
    norm_h(icol)=norm(h(Lh+1+tau));
    tfr(indices,icol)=x(ti+tau).* h(Lh+1+tau)
    tf2(indices,icol)=x(ti+tau).*Th(Lh+1+tau);
    tf3(indices,icol)=x(ti+tau).*Dh(Lh+1+tau);
  end ;
  tfr=fft(tfr,-1,1);//sft_h
  tf2=fft(tf2,-1,1);//sft_th /* 
  tf3=fft(tf3,-1,1);//sft_dh
  Ex=mean(abs(x(min(t):max(t))).^2); 
  Threshold=1.0e-6*Ex;

  nz=find(abs(tfr)>Threshold);
  tf2(nz)= round(real(tf2(nz)./tfr(nz))/Dt);//field_time REAL(STFT_TH / STFT_H)
  tf3(nz)=-round(0.5*N*imag(tf3(nz)./tfr(nz))/%pi);//field_freq -IMAG(STFT_DH / STFT_H)
  z=find(abs(tfr)<=sqrt(Threshold));
  tf2(z)= 0;
  tf3(z)=0;
  tf2=real(tf2);
  tf3=real(tf3);
  tfr=(abs(tfr)/diag(norm_h)).^2;

  if ptrace, printf ('reassignment: \n'); end;

  rtfr= zeros(N,tcol); 

  for icol=1:tcol,
    if ptrace, disprog(icol,tcol,10); end;
    for jcol=1:N,
      if abs(tfr(jcol,icol))>Threshold,
        //in time, the pixels are reassigned along the edges
        icolhat=min(max(icol+ real(tf2(jcol,icol)),1),tcol);
        //in frequency, the pixels are reassigned  according to a rotation 
        jcolhat=pmodulo(jcol + real(tf3(jcol,icol))-1,N)+1;
        rtfr(jcolhat,icolhat)=rtfr(jcolhat,icolhat) + tfr(jcol,icol) ;
        tf2(jcol,icol)=jcolhat + %i * icolhat;
      else
        tf2(jcol,icol)=%inf*(1+%i);
        rtfr(jcol,icol)=rtfr(jcol,icol) + tfr(jcol,icol) ;
      end;
    end;
  end;
 
 
  if ptrace, printf('\n'); end;
  clear tf3;
  if (plotting),
    
    while %t
      choice=x_choose(['spectrogram',...
                       'reassigned spectrogram'],'Choose the representation:','stop');
      if (choice==0) then
        break
      elseif (choice==1) then 
        tfrqview(tfr,x,t,'tfrsp',h);
      elseif (choice==2) then
        tfrqview(rtfr,x,t,'tfrrsp',h);
      end;
    end;
  end
  if (nargout>2),
    rhat=tf2;
  end;

endfunction
