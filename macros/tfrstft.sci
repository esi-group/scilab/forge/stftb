function [tfr,t,f] = tfrstft(x,t,N,h,ptrace,opt_plot);
// Short time Fourier transform.
// Calling Sequence
//	[TFR,T,F]=tfrstft(X)
//	[TFR,T,F]=tfrstft(X,T)
//	[TFR,T,F]=tfrstft(X,T,N)
//	[TFR,T,F]=tfrstft(X,T,N,H
//	[TFR,T,F]=tfrstft(X,T,N,H,TRACE)
//	[TFR,T,F]=tfrstft(...,'plot')
//    Parameters
//	X     : signal.
//	T     : time instant(s)          (default : 1:length(X)).
//	N     : number of frequency bins (default : length(X)).
//	H     : frequency smoothing window, H being normalized so as to   be  of unit energy.      (default : Hamming(N/4)). 
//	TRACE : if nonzero, the progression of the algorithm is shown   (default : 0).
//      'plot':	if one input parameter is 'plot',  tfrstft runs tfrqview. and TFR will be plotted
//	TFR   : time-frequency decomposition (complex values). The frequency axis is graduated from -0.5 to 0.5.
//	F     : vector of normalized frequencies.
// Desciption
//    tfrstft computes the short-time Fourier transform of a discrete-time signal X. 
//  Examples 
//       sig=[fmconst(128,0.2);fmconst(128,0.4)]; [tfr,t,f]=tfrstft(sig);
//       subplot(211); grayplot(t,f,abs(tfr)');
//       subplot(212); grayplot(t,f,atan(imag(tfr),real(tfr))');
// Authors
//      H. Nahrstaedt - Aug 2010
//	F. Auger, May-August 1994, July 1995.
//	Copyright (c) 1996 by CNRS (France).

//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  [nargout,nargin]=argn(0);
  fname="tfrstft"
 
  in_par=['x','t','N','h','ptrace','opt_plot'];
  in_par_min=1;
  plotting=%f;
  if (nargin>in_par_min)
    if (type(evstr(in_par(nargin)))==10)
      if convstr(evstr(in_par(nargin)))=="plot" then
        plotting=%t; 
        nargin=nargin-1;
      end; 
    end;
  end;
  clear in_par;
  if nargin == 0 then
    error(msprintf(_("%s: Wrong number of input argument: %d to %d expected.\n"),fname,1,6));
  end;
 //x
  if type(x)<>1 then
    error(msprintf(_("%s: Wrong type for input argument #%d: A matrix of double expected.\n"),fname,1));
  end
  if or(size(x)==1) then
    x=x(:);
  else
    error(msprintf(_("%s: Wrong size for input argument #%d: A  vector  expected.\n"),fname,1));
  end
  Nx=size(x,1);

  //ptrace
  if nargin>=5 then 
    if and(type(ptrace)<>[1 4])|size(ptrace,"*")<>1 then
      error(msprintf(_("%s: Wrong type for argument #%d: A boolean or real scalar expected.\n"),fname,5));
    end
  else
    ptrace=%f;
  end

   //h
  if nargin>=4 then 
    if type(h)<>1|and(size(h)>1)|~isreal(h) then 
      error(msprintf(_("%s: Wrong type for argument #%d: A real vector expected.\n"),fname,4));
    end
    hlength=size(h,"*");
    if (modulo(hlength,2)==0),
      error(msprintf(_("%s: Wrong size for argument #%d: A odd number of elements expected.\n"),fname,4));
    end;
  else
    hlength=floor(Nx/4);
    if rem(hlength,2)==0 then hlength=hlength+1;end;
    h = window("hm",hlength)
  end
  h=h(:);
  Lh=(hlength-1)/2;
  h=h/norm(h);
  
  //N
  if nargin>=3 then
    if type(N)<>1|size(N,"*")>1|~isreal(N)|int(N)<>N then
      error(msprintf(_("%s: Wrong type for input argument #%d: A integer value expected.\n"),fname,3));
    end 
    if N<=0 then
      error(msprintf(_("%s: Wrong value for input argument #%d: A positive integer expected.\n"),fname,3));
    end
  else
    N=Nx
  end

//T
  if nargin>=2 then
    if type(t)<>1|and(size(t)>1)|~isreal(t) then
      error(msprintf(_("%s: Wrong type for input argument #%d: A real vector expected.\n"),fname,2));
    end
    if max(t)>Nx|min(t)<1 then
      error(msprintf(_("%s: Wrong value for input argument #%d: The elements must be in the interval %s.\n"),fname,2,"[1 "+string(Nx)+"]"));
    end
  else
    t=1:Nx;
  end
  Nt=size(t,2);

 

  tfr= zeros (N,Nt) ;  
  if ptrace, disp('Short-time Fourier transform'); end;
  for icol=1:Nt,
    ti= t(icol); 
    tau=-min([round(N/2)-1,Lh,ti-1]):min([round(N/2)-1,Lh,Nx-ti]);
    indices= rem(N+tau,N)+1; 
    if ptrace, disprog(icol,Nt,10); end;
    tfr(indices,icol)=x(ti+tau,1).*h(Lh+1+tau);
  end;

  tfr=fft(tfr,-1,1); 

  if ptrace, printf('\n'); end;

  if (plotting),
    tfrqview(abs(tfr).^2,x,t,'tfrstft',h);
  end
  if (nargout==3),
    f=(0:N-1)/N;
  end;
endfunction

