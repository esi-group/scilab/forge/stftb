function x=anasing(N,t0,h) ;
// Lipschitz singularity.
// Calling Sequence
//	X=anasing(N)
//	X=anasing(N,T0)
//	X=anasing(N,T0,H)
//  Parameters
//	N  : number of points in time
//	T0 : time localization of the singularity  (default : N/2)
//	H  : strenght of the Lipschitz singularity (positive or negative)  (default : 0.0)
//	X  : the time row vector containing the signal samples
// Description
// anasing generates the N-points Lipschitz singularity 
//	centered around T=T0 : X(T) = |T-T0|^H. 
//  Examples  
//    x=anasing(128); scf(); plot(real(x));
//  See also 
//        anastep
//        anapulse
//        anabpsk
//        doppler
//  Authors
//  H. Nahrstaedt - Aug 2010
//	P. Goncalves - September 1995
//	Copyright (c) 1995 Rice University

//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  [nargout,nargin]=argn(0);

  
  fname="anasing";
  if nargin == 0 then
    error(msprintf(_("%s: Wrong number of input argument: %d to %d expected.\n"),fname,1,3));
  end;
  
  //N
  if type(N)<>1|size(N,"*")>1|~isreal(N)|int(N)<>N then
    error(msprintf(_("%s: Wrong type for input argument #%d: A integer value expected.\n"),fname,1));
  end 
  if N<=0 then
    error(msprintf(_("%s: Wrong value for input argument #%d: Must be >= %d.\n"),fname,1,0))
  end
  
  //h
  if nargin==3 then
    if type(h)<>1|size(h,"*")>1|~isreal(h) then
      error(msprintf(_("%s: Wrong type for input argument #%d: A real expected.\n"),fname,3));
    end 
  else
    h=0;
  end
  
  //t0
  if nargin>=2 then
    if type(t0)<>1|size(t0,"*")>1|~isreal(t0)|int(t0)<>t0 then
      error(msprintf(_("%s: Wrong type for input argument #%d: A integer value expected.\n"),fname,2));
    end 
    if t0<=0|t0>N then
      error(msprintf(_("%s: Wrong value for input argument #%d: Must be in %s.\n"),fname,2,"]0,"+string(N)+"]"))
    end
  else
    t0=round(N/2);
  end

  if h <= 0 
    f = (1/N:1/N:0.5-1/N) ;
    y = zeros(1,N/2);
    y(2:N/2) = (f.^(-1-h)).*exp(-%i*2*%pi*f.*(t0-1));
    x = real(tftb_ifft(y,N)) ;
    x = x./max(x); 
    x = x.' - sign(min(x))*abs(min(x)) ;
  else
    t = 1:N;
    x = abs(t-t0).^h;
    x = max(x)-x.' ;
  end

  x=hilbert(x);
endfunction
