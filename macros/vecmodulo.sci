function y=vecmodulo(x,N);
//	Congruence of a vector.
// Calling Sequence
//	Y=vecmodulo(X,N) 
// Description
//      vecmodulo gives the congruence of each element of the
//	vector X modulo N. These values are strictly positive and 
//	lower equal than N.
//   See also
//       rem
//   Authors
//      H. Nahrstaedt - Aug 2010
//	O. Lemoine - February 1996.
//	Copyright (c) 1996 by CNRS (France).

//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  if isreal(x)
    y=pmodulo(x,N);
    y(y==0)=N
  else
    y=pmodulo(real(x),N)+%i*pmodulo(imag(x),N);
  end;
endfunction
