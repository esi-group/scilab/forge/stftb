function [fnorm,t,rejratio]=ifestar2(x,t);
// Instantaneous frequency estimation using AR2 modelisation. 
// Calling Sequence
//	[FNORM,T2,RATIO]=ifestar2(X,T)
//	[FNORM,T2,RATIO]=ifestar2(X)
//  Parameters
//	X     : real signal to be analyzed.
//	T     : Time instants (must be greater than 4) (default : 4:length(X)).
//	FNORM : Output (normalized) instantaneous frequency.
//	T2    : Time instants coresponding to FNORM. Since the	algorithm can not always give a value, T2 is different of T. 
//       RATIO : proportion of instants where the algorithm yields an estimation
//  Description
//       ifestar2 computes an estimate of the
//       instantaneous frequency of the real signal X at time
//	instant(s) T. The result FNORM lies between 0.0 and 0.5. This
//	estimate is based only on the 4 last signal points, and has
//	therefore an approximate delay of 2.5 points. 
//  Examples
//        [x,if]=fmlin(50,0.05,0.3,5); x=real(x); [if2,t]=ifestar2(x);
//        plot(t,if(t),t,if2);
//         N=1100; [deter,if]=fmconst(N,0.05); deter=real(deter);
//        noise=rand(N,1,'normal'); NbSNR=101; SNR=linspace(0,100,NbSNR);
//        for iSNR=1:NbSNR,
//         sig=sigmerge(deter,noise,SNR(iSNR));
//         [if2,t,ratio(iSNR)]=ifestar2(sig); 
//         EQM(iSNR)=norm(if(t)-if2)^2 / length(t) ;
//        end;
//        clf(); subplot(211); plot(SNR,-10.0 * log10(EQM)); xgrid;
//        xlabel('SNR'); ylabel('-10 log10(EQM)');
//        subplot(212); plot(SNR,ratio); xgrid;
//        xlabel('SNR'); ylabel('ratio');
//    See also
//       instfreq
//       kaytth
//       sgrpdlay
//   Authors
//      H. Nahrstaedt - Aug 2010
//	F. Auger, April 1996.
//	Copyright (c) 1996 by CNRS (France).

//       This estimator is the causal version of the estimator called
//       "4 points Prony estimator" in the article "Instantaneous
//	frequency estimation using linear prediction with comparisons
//	to the dESAs", IEEE Signal Processing Letters, Vo 3, No 2, p
//	54-56, February 1996. 


//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  fname="ifestar";
  nargin=argn(2)
  if  nargin== 0 then
    error(msprintf(_("%s: Wrong number of input argument: %d to %d expected.\n"),fname,1,2));
  end;
  if type(x)<>1|~isreal(x)|and(size(x)<>1) then
    error(msprintf(_("%s: Wrong type for input argument #%d: A real vector expected.\n"),fname,1));
  end
  x=x(:);
 

  if nargin==2 then
    if type(t)<>1|~isreal(t)|or(int(t)<>t)|and(size(t)<>1) then
      error(msprintf(_("%s: Wrong type for input argument #%d: A vector of integer elements expected.\n"),fname,2));
    end
    if min(t)<4 then
      error(msprintf(_("%s: Wrong value for input argument #%d: Must be >= %d.\n"),fname,2,4));
    end
    t=matrix(t,1,-1);
  else
    t=4:size(x,1); 
  end;


  Kappa = x(t-1) .* x(t-2) - x(t  ) .* x(t-3) ;
  psi1  = x(t-1) .* x(t-1) - x(t  ) .* x(t-2) ;
  psi2  = x(t-2) .* x(t-2) - x(t-1) .* x(t-3) ;
  den   = psi1 .* psi2 ;
  indices = find(den>0);
  arg=0.5*Kappa(indices)./sqrt(den(indices));
  indarg=find(abs(arg)>1);
  arg(indarg)=sign(arg(indarg));
  fnorm = acos(arg)/(2.0*%pi);
  rejratio = length(indices)/length(t);
  t = matrix(t(indices),-1,1);
endfunction
