function x=klauder(N,lambda,f0)
// Klauder wavelet in time domain.
// Calling Sequence
//	X=klauder(N,LAMBDA,F0)
// Parameters
//	N      : number of points in time   
//	LAMBDA : attenuation factor or the envelope (default : 10)
//	F0     : central frequency of the wavelet (default : 0.2)
//	X      : time row vector containing the klauder samples.
//  Description
//    klauder generates the KLAUDER wavelet in the time domain
//	K(f) = e^{-2.pi.LAMBA.f} f^{2.pi.LAMBDA.F0-1/2} 
//  Examples
//       x=klauder(128); plot(x);
//   See also  
//     altes
//     anasing
//     doppler
//     anafsk
//     anastep
//   Authors
//      H. Nahrstaedt - Aug 2010
//	P. Goncalves 9-95
//	Copyright (c) 1995 Rice University

//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  [nargout,nargin]=argn(0);
  fname="klauder"
  if nargin == 0 then
    error(msprintf(_("%s: Wrong number of input argument: %d to %d expected.\n"),fname,1,3));
  end;
  
  //N
  if type(N)<>1|size(N,"*")<>1|~isreal(N)|int(N)<>N then
    error(msprintf(_("%s: Wrong type for input argument #%d: An integer value expected.\n"),fname,1));
  end
  if N<=0 then
    error(msprintf(_("%s: Wrong value for input argument #%d: A value >= %d expected.\n"),fname,1,1));
  end
  
  //lambda
  if nargin>=2 then
    if type(lambda)<>1|size(lambda,"*")<>1|~isreal(lambda) then
       error(msprintf(_("%s: Wrong type for input argument #%d: A real expected.\n"),fname,2));
    end 
    if lambda<0 then 
      error(msprintf(_("%s: Wrong value for input argument #%d: A value >= %d expected.\n"),fname,2,0));
      
    end
  else
    lambda=10
  end
  
  //f0
  if nargin==3 then
    if type(f0)<>1|size(f0,"*")<>1|~isreal(f0) then
       error(msprintf(_("%s: Wrong type for input argument #%d: A real expected.\n"),fname,3));
     end 
     if f0<0|f0>0.5 then
       error(msprintf(_("%s: Wrong value for input argument #%d: Must be in the interval %s.\n"),fname,3,"[0,0.5]"));
     end
  else
    f0=0.2
  end

  f = linspace(0,0.5,int(N/2)+1) ;
  wave = exp(-2*%pi*lambda*f).*f.^(2*%pi*lambda*f0-0.5) ;
  wave(1)=0 ; 
  wave = fftshift(ifft([wave(1:N/2) wave(floor(N/2)+1:-1:2)])) ;
  x = real(wave).'/norm(wave) ; 
endfunction

