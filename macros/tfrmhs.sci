function [tfr,t,f] = tfrmhs(x,t,N,g,h,ptrace,opt_plot);
//	Margenau-Hill-Spectrogram time-frequency distribution.
// Calling Sequence
//	[TFR,T,F]=tfrmhs(X) 
//	[TFR,T,F]=tfrmhs(X,T) 
//	[TFR,T,F]=tfrmhs(X,T,N) 
//	[TFR,T,F]=tfrmhs(X,T,N,G) 
//	[TFR,T,F]=tfrmhs(X,T,N,G,H) 
//	[TFR,T,F]=tfrmhs(X,T,N,G,H,TRACE) 
//	[TFR,T,F]=tfrmhs(...,'plot') 
//  Parameters
//	X     : Signal if auto-MHS, or [X1,X2] if cross-MHS.
//	T     : time instant(s)          (default : 1:length(X)).
//	N     : number of frequency bins (default : length(X)).
//	G,H   : analysis windows, normalized so that the representation    preserves the signal energy.(default : Hamming(N/10) and Hamming(N/4)). 
//	TRACE : if nonzero, the progression of the algorithm is shown      (default : 0).
//      'plot':	if one input parameter is 'plot',  tfrmhs runs tfrqview. and TFR will be plotted
//	TFR   : time-frequency representation. 
//	F     : vector of normalized frequencies.
// Description
// tfrmhs computes the Margenau-Hill-Spectrogram 
//	distribution of a discrete-time signal X, or the cross
//	Margenau-Hill-Spectrogram representation between two signals. 
//   Examples
//       sig=fmlin(128,0.1,0.4); g=tftb_window(21,'Kaiser'); 
//       h=tftb_window(63,'Kaiser'); tfrmhs(sig,1:128,64,g,h,1,'plot');
//    Authors
//      H. Nahrstaedt - Aug 2010
//	F. Auger, May-August 1994, July 1995.
//	Copyright (c) 1996 by CNRS (France).

//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  [nargout,nargin]=argn(0);
  fname="tfrmhs";

  // handling optional opt_plot parameter
  in_par=['x','t','N','g','h','ptrace','opt_plot'];
  in_par_min=1;
  plotting=%f;
  if (nargin>in_par_min)
    if (type(evstr(in_par(nargin)))==10)
      if convstr(evstr(in_par(nargin)))=="plot" then
        plotting=%t; 
        nargin=nargin-1;
      end; 
    end;
  end;
  clear in_par;
  if nargin == 0 then
    error(msprintf(_("%s: Wrong number of input argument: %d to %d expected.\n"),fname,1,7));
  end
  
  //x
  if type(x)<>1 then
    error(msprintf(_("%s: Wrong type for input argument #%d: A matrix of double expected.\n"),fname,1));
  end
  if or(size(x)==1) then
    x=x(:);
  end
  if size(x,2)>2 then
     error(msprintf(_("%s: Wrong size for input argument #%d: A  vector or a two columns array expected.\n"),fname,1));
  end
  [Nx,xcol]=size(x);

  //ptrace
  if nargin==6 then 
    if and(type(ptrace)<>[1 4])|size(ptrace,"*")<>1 then
      error(msprintf(_("%s: Wrong type for argument #%d: A boolean or real scalar expected.\n"),fname,6));
    end
  else
    ptrace=%f;
  end
  
  //h
  if nargin>=5 then 
    if type(h)<>1|and(size(h)>1)|~isreal(h) then 
      error(msprintf(_("%s: Wrong type for argument #%d: A real vector expected.\n"),fname,5));
    end
    hlength=size(h,"*");
    if (modulo(hlength,2)==0),
      error(msprintf(_("%s: Wrong size for argument #%d: A odd number of elements expected.\n"),fname,5));
    end;
  else
    hlength=floor(Nx/4);
    if rem(hlength,2)==0 then hlength=hlength+1;end;
    h = window("hm",hlength)
  end
  h=h(:);
  Lh=(hlength-1)/2; h=h/h(Lh+1);
  
  //g
  if nargin>=4 then 
    if type(g)<>1|and(size(g)>1)|~isreal(g) then 
      error(msprintf(_("%s: Wrong type for argument #%d: A real vector expected.\n"),fname,4));
    end
    glength=size(g,"*");
    if (modulo(glength,2)==0),
      error(msprintf(_("%s: Wrong size for argument #%d: A odd number of elements expected.\n"),fname,4));
    end;
  else
    glength=floor(Nx/10);
    if rem(glength,2)==0 then glength=glength+1;end;
    g = window("hm",glength)
  end
  g=g(:);
  Lg=(glength-1)/2; 
  
  //N
  if nargin>=3 then
    if type(N)<>1|size(N,"*")>1|~isreal(N)|int(N)<>N then
      error(msprintf(_("%s: Wrong type for input argument #%d: A integer value expected.\n"),fname,3));
    end 
    if N<=0 then
      error(msprintf(_("%s: Wrong value for input argument #%d: A positive integer expected.\n"),fname,3));
    end
    if 2^nextpow2(N)~=N then
      //printf(msprintf(_("%s: For a faster computation, N should be a power of two\n"),fname,3));
    end
  else
    N=Nx
  end
  
  //T
  if nargin>=2 then
    if type(t)<>1|and(size(t)>1)|~isreal(t) then
      error(msprintf(_("%s: Wrong type for input argument #%d: A real vector expected.\n"),fname,2));
    end
    if max(t)>Nx|min(t)<1 then
      error(msprintf(_("%s: Wrong value for input argument #%d: The elements must be in the interval %s.\n"),fname,2,"[1 "+string(Nx)+"]"));
    end
  else
    t=1:Nx;
  end
  Nt=size(t,2);
 

  Lgh=min(Lg,Lh); points=-Lgh:Lgh; 
  Kgh=sum(h(Lh+1+points).*g(Lg+1+points)); 
  h=h/Kgh;

  tfr= zeros (N,Nt); tfr2= zeros(N,Nt);
  if ptrace, disp('Pseudo Margenau-Hill distribution'); end;
  Nfs2=round(N/2)
  for icol=1:Nt,
    ti= t(icol);
    if ptrace, disprog(icol,Nt,10); end;
    tau=-min([Nfs2,Lg,ti-1]):min([Nfs2-1,Lg,Nx-ti]);
    indices= rem(N+tau,N)+1;
    tfr(indices,icol)=x(ti+tau,1).*g(Lg+1+tau);
    tau=-min([Nfs2,Lh,ti-1]):min([Nfs2-1,Lh,Nx-ti]);
    indices= rem(N+tau,N)+1;
    tfr2(indices,icol)=x(ti+tau,xcol).*conj(h(Lh+1+tau));
  end; 
  if ptrace, printf('\n'); end;

  tfr=real(fft(tfr,-1,1).*conj(fft(tfr2,-1,1))); 

  if plotting then
    tfrqview(tfr,x,t,'tfrmhs',g,h);
  end
  if (nargout==3),
    f=(0:N-1)/N;
  end;

endfunction

