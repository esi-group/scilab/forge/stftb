function  [dsp,f]=frpowerspec(x,N,h,opt_plot);
// Computes the power spectrum of the signal
// Calling Sequence
//	[SP,F]=frpowerspec(x) 
//	[SP,F]=frpowerspec(x,N) 
//	[SP,F]=frpowerspec(x,N,H) 
//	[SP,F]=frpowerspec(...,'plot') 
//   Parameters
//	x : signal (in time) to be analyzed.
//	N : number of frequency bins (default : length(x)).
//	H     : analysis window,   (default : Rect(length(x)). 
//      'plot':	if one input parameter is 'plot', the window will be plotted
//      SP:  power spectrum of the signal
//      F  : Normalized frequency vector
//     Examples
//       sig=fmlin(128,0.1,0.4);
//       frpowerspec(sig,'plot');
//    Authors
//      H. Nahrstaedt - Aug 2010	
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  [nargout,nargin]=argn(0);
  fname="frpowerspec";
  
 
  in_par=['x','N','h','opt_plot'];
  in_par_min=1;
  plotting=%f;
  if (nargin>in_par_min)
    if (type(evstr(in_par(nargin)))==10)
      if convstr(evstr(in_par(nargin)))=="plot" then
        plotting=%t; 
        nargin=nargin-1;
      end; 
    end;
  end;
  clear in_par;

   
 if nargin == 0 then
    error(msprintf(_("%s: Wrong number of input argument: %d to %d expected.\n"),fname,1,4));
  end;

  //x
  if type(x)<>1|and(size(x)>1) then
    error(msprintf(_("%s: Wrong type for input argument #%d: A vector of double expected.\n"),fname,1));
  end
  x=x(:);
  xrow=size(x,1);

  //h
  if nargin>=3 then 
    if type(h)<>1|and(size(h)>1)|~isreal(h) then 
      error(msprintf(_("%s: Wrong type for argument #%d: A real vector expected.\n"),fname,3));
    end
    if size(h,"*")<>xrow then
      error(msprintf(_("%s: Arguments #%d and #%d: Same numbers of elements expected.\n"),fname,3,1));
    end
  else
    h = window("re",xrow)
  end
  h=h(:);
  Lh=(xrow-1)/2; 
  
  //N
  if nargin>=2 then
    if type(N)<>1|size(N,"*")>1|~isreal(N)|int(N)<>N then
      error(msprintf(_("%s: Wrong type for input argument #%d: A integer value expected.\n"),fname,2));
    end 
    if N<=0 then
      error(msprintf(_("%s: Wrong value for input argument #%d: A positive integer expected.\n"),fname,2));
    end
  else
    N=xrow
  end
    
  time=1:xrow; 

  x=x.*h;
  

  if isreal(x)
    dsp=(abs(tftb_fft(fftshift(x),N))).^2; 
    N=length(dsp);
    dsp = ((dsp(1:N/2))); 
    Maxsp=max(dsp);
    f = linspace(0,0.5,N/2+1) ; 
    f=f(1:N/2);
  else
    dsp=fftshift(abs(tftb_fft(x,N))).^2; 
    N=length(dsp);
    Maxsp=max(abs(dsp));
    if rem(N,2)==0
      f=[-N/2+1:-1,0:N/2].'/N;
    else
      f=[-(N-1)/2:-1,0:(N-1)/2].'/N;
    end;
  end



  if plotting then
    plot(f,dsp) ; xgrid; 
    xlabel(_('Normalized frequency'));
    ylabel(_('Squared modulus'));
    title(_('Energie spectrum')); 
    a=gca();a.data_bounds=([f(1) f($) 0 1.2*Maxsp]) ; 
  end;
endfunction
