function [y,iflaw]=anafsk(N,Ncomp,Nbf);
// Frequency Shift Keying (FSK) signal.
// Calling Sequence
//	[Y,IFLAW]=ANAFSK(N,NCOMP,NBF) 
// Description
//simulates a phase coherent
//	Frequency Shift Keying (FSK) signal. This signal is a succession
//	of complex sinusoids of NCOMP points each and with a normalized
//	frequency uniformly chosen between NBF distinct values between
//	0.0 and 0.5.  
// 	Such signal is only 'quasi'-analytic.
//   Parameters
//	N     : number of points
//	NCOMP : number of points of each component (default: N/5)
//	NBF   : number of distinct frequencies     (default: 4  )
// 	Y     : signal
// 	IFLAW : instantaneous frequency law  (optional).
//
//     Examples
//	 [signal,ifl]=anafsk(512,64,5); clf; figure(gcf);
//  	 subplot(211); plot(real(signal)); subplot(212); plot(ifl);
//
//   See also 
//     anabpsk
//     anaqpsk
//     anaask
//    Authors
//	O. Lemoine - June 1995, F. Auger - August 1995.
//	Copyright (c) 1996 by CNRS (France).
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  [nargout,nargin]=argn(0);
  fname="anafsk"
  if nargin == 0 then
    error(msprintf(_("%s: Wrong number of input argument: %d to %d expected.\n"),fname,1,3));
  end;
     
  //N
  if type(N)<>1|size(N,"*")>1|~isreal(N)|int(N)<>N then
    error(msprintf(_("%s: Wrong type for input argument #%d: An integer value expected.\n"),fname,1));
  end 
  if N<=0 then
    error(msprintf(_("%s: Wrong value for input argument #%d: Must be > %d.\n"),fname,1,0))
  end
  
  //Nbf
  if nargin==3 then
      if type(Nbf)<>1|size(Nbf,"*")>1|~isreal(Nbf)|int(Nbf)<>Nbf then
       error(msprintf(_("%s: Wrong type for input argument #%d: An integer value expected.\n"),fname,3));
     end 
     if Nbf<=0 then
        error(msprintf(_("%s: Wrong value for input argument #%d: Must be > %d.\n"),fname,3,0))
     end
  else
    Nbf=4;
  end
  
  if nargin>=2 then
     if type(Ncomp)<>1|size(Ncomp,"*")>1|~isreal(Ncomp)|int(Ncomp)<>Ncomp then
    error(msprintf(_("%s: Wrong type for input argument #%d: A integer value expected.\n"),fname,2));
  end 
  if Ncomp<=0 then
    error(msprintf(_("%s: Wrong value for input argument #%d: Must be > %d.\n"),fname,2,0))
  end
  else
    Ncomp=round(N/5);
  end

m=ceil(N/Ncomp);
freqs=0.25+0.25*(floor(Nbf*rand(m,1))/Nbf-(Nbf-1)/(2*Nbf));
iflaw=freqs .*. ones(Ncomp,1); iflaw=iflaw(1:N,1);
y=exp(%i*2*%pi*cumsum(iflaw));
endfunction
