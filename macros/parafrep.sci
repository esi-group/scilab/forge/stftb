function [fspec,freqs,A]=parafrep(Rx,N,method,q,opt_plot);
// parametric frequency representation of a signal.
// Calling Sequence
// [spec,freqs]=parafrep(Rx) 
// [spec,freqs]=parafrep(Rx,N) 
// [spec,freqs]=parafrep(Rx,N,method) 
// [spec,freqs]=parafrep(Rx,N,method,q) 
// [spec,freqs]=parafrep(...,'plot') 
// Parameters
// Rx     : correlation matrix of size (p+1)x(p+1)
// N      : number of frequency bins between 0 and 0.5
// method : can be either 'ar', 'periodogram', 'capon', 'capnorm', 'lagunas',  or 'genlag'.
// q      : parameter for the generalized Lagunas method.
// 'plot':  when called with the additional string 'plot', the output values will be plotted
//  Examples
// noise=rand(1000,1); signal=filter([1 0 0],[1 1 1],noise);
// figure(1);parafrep(correlmx(signal,2,'hermitian'),128,'AR');title('AR (2)');
// figure(2);parafrep(correlmx(signal,4,'hermitian'),128,'Capon');title('Capon (4)');
// figure(3);parafrep(correlmx(signal,2,'hermitian'),128,'lagunas');title('Lagunas (2)');
// figure(4);parafrep(correlmx(signal,40,'hermitian'),128,'periodogram');title('periodogram (40)');
//   Authors
//      H. Nahrstaedt - Aug 2010
//      F. Auger, july 1998, april 99.


//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  [nargout,nargin]=argn(0);
  fname="parafrep"

  in_par=['Rx','N','method','q','opt_plot'];
  in_par_min=1;
  plotting=%f;
  if (nargin>in_par_min)
    if (type(evstr(in_par(nargin)))==10)
      if convstr(evstr(in_par(nargin)))=="plot" then
        plotting=%t; 
        nargin=nargin-1;
      end; 
    end;
  end;
  clear in_par;
  
  if nargin == 0 then
    error(msprintf(_("%s: Wrong number of input argument: %d to %d expected.\n"),fname,1,5));
  end

  //Rx
  if type(Rx)<>1 then
    error(msprintf(_("%s: Wrong type for input argument #%d: A matrix of double expected.\n"),fname,1));
  end
  [Rxrow,Rxcol]=size(Rx);
  if (Rxrow ~= Rxcol),
    error(msprintf(_("%s: Wrong size for input argument #%d: A square matrix expected.\n"),fname,1));
  end;
  
  //q ??
   if nargin>=4 then
   
   else

   end
   
  //method

  if nargin>=3 then
    if type(method)<>10|or(size(method)<>1) then
      error(msprintf(_("%s: Wrong type for input argument #%d: A character string expected.\n"),fname,3));
    end
    
    method=convstr(method,"u");
    M=["AR" "PERIODOGRAM" "CAPON" "CAPNORM" "LAGUNAS" "GENLAG"]
    if and(method<>M) then
      error(msprintf(_("%s: Wrong value for input argument #%d: Must be in the set  {%s}.\n"),fname,3,"""AR"",""PERIODOGRAM"",""CAPON"",""CAPNORM"",""LAGUNAS"",""GENLAG"""));
    end
  else
    method="CAPON";
  end
  
  //N
  if nargin>=2 then
    if type(N)<>1|size(N,"*")>1|~isreal(N)|int(N)<>N then
      error(msprintf(_("%s: Wrong type for input argument #%d: A integer value expected.\n"),fname,2));
    end 
    if N<=0 then
      error(msprintf(_("%s: Wrong value for input argument #%d: A positive integer expected.\n"),fname,2));
    end
  else
    N=128
  end
  
  

  p=Rxrow-1;
  freqs=linspace(0,0.5,N);
  fspec=zeros(N,1);
  if (method=="AR"),
    Un=ones(Rxrow,1); Rxm1Un= (Rx\Un); 
    P1=real(Un'*Rxm1Un); A=Rxm1Un/P1; 
    for ifreq=1:N, 
      Z=exp(2*%i*%pi*freqs(ifreq)*(0:Rxrow-1)'); 
      fspec(ifreq)=P1 ./ abs(Z' * A)^2 ;
    end;
  elseif (method=="PERIODOGRAM"),
    for ifreq=1:N, 
      Z=exp(2*%i*%pi*freqs(ifreq)*(0:Rxrow-1)'); 
      fspec(ifreq)=real(Z' * Rx *Z)/(p+1)^2;
    end; 
  elseif (method=="CAPON"),
    for ifreq=1:N, 
      Z=exp(2*%i*%pi*freqs(ifreq)*(0:Rxrow-1)'); 
      fspec(ifreq)=1.0 / real(Z' * (Rx\Z));
    end; 
  elseif (method=="CAPNORM"),
    for ifreq=1:N, 
      Z=exp(2*%i*%pi*freqs(ifreq)*(0:Rxrow-1)'); 
      fspec(ifreq)=(p+1) / real(Z' * (Rx\Z));
    end; 
  elseif (method=="LAGUNAS"),
    for ifreq=1:N, 
      Z=exp(2*%i*%pi*freqs(ifreq)*(0:Rxrow-1)'); 
      Rxm1Z=Rx\Z; fspec(ifreq)=real(Z' * Rxm1Z)/real(Z' * (Rx\Rxm1Z));
    end; 
  elseif (method=="GENLAG"),
    for ifreq=1:N, 
      Z=exp(2*%i*%pi*freqs(ifreq)*(0:Rxrow-1)'); 
      Rxqm1Z=(Rx)^q \Z; fspec(ifreq)=real(Z' * Rx * Rxqm1Z)/real(Z' * (Rx\Rxqm1Z));
    end; 
 
  end;

  if plotting then
    plot(freqs.',10.0*log10(fspec)); xgrid;
    xlabel(_('normalized frequency'));
    ylabel('DSP  (dB)');
  end;
endfunction
