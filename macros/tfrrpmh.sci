function [tfr,rtfr,rhat] = tfrrpmh(x,t,N,h,ptrace,opt_plot);
// Reassigned pseudo Margenau-Hill time-frequency distribution.
// Calling Sequence
//	[TFR,RTFR,HAT] = tfrrpmh(X) 
//	[TFR,RTFR,HAT] = tfrrpmh(X,T) 
//	[TFR,RTFR,HAT] = tfrrpmh(X,T,N) 
//	[TFR,RTFR,HAT] = tfrrpmh(X,T,N,H) 
//	[TFR,RTFR,HAT] = tfrrpmh(X,T,N,H,TRACE) 
//	[TFR,RTFR,HAT] = tfrrpmh(...,'plot') 
// Parameters
//	X     : analysed signal,
//	T     : time instant(s)          (default : 1:length(X)).
//	N     : number of frequency bins (default : length(X)).
//	H     : frequency smoothing window, H(0) being forced to 1      (default : Hamming(N/4)).
//	TRACE : if nonzero, the progression of the algorithm is shown   (default : 0).
//      'plot':	if one input parameter is 'plot',  tfrrpmh runs tfrqview. and TFR will be plotted
//	TFR,  : time-frequency representation and its reassigned
//	RTFR    version. 
//	HAT   : Complex matrix of the reassignment vectors.
// Description
//	tfrrpmh computes the pseudo Margenau-Hill distribution
//	and its reassigned version.
//  Examples
//       sig=fmlin(128,0.1,0.4); t=1:2:128; 
//       h=tftb_window(17,'Kaiser'); tfrrpmh(sig,t,64,h,1,'plot');
//     Authors
//      H. Nahrstaedt - Aug 2010
//	F. Auger, May-July 1994, July 1995.
//	Copyright (c) 1996 by CNRS (France).

//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  [nargout,nargin]=argn(0);
  fname="tfrrpmh"


  in_par=['x','t','N','h','ptrace','opt_plot'];
  in_par_min=1;
  plotting=%f;
  if (nargin>in_par_min)
    if (type(evstr(in_par(nargin)))==10)
      if convstr(evstr(in_par(nargin)))=="plot" then
        plotting=%t; 
        nargin=nargin-1;
      end; 
    end;
  end;
  clear in_par;

  if nargin == 0 then
    error(msprintf(_("%s: Wrong number of input argument: %d to %d expected.\n"),fname,1,6));
  end;

  //x
  if type(x)<>1 then
    error(msprintf(_("%s: Wrong type for input argument #%d: A matrix of double expected.\n"),fname,1));
  end
  x=x(:);
  xrow = size(x,1);
  Nx=xrow

  //ptrace
  if nargin>=5 then 
    if and(type(ptrace)<>[1 4])|size(ptrace,"*")<>1 then
      error(msprintf(_("%s: Wrong type for argument #%d: A boolean or real scalar expected.\n"),fname,5));
    end
  else
    ptrace=%f;
  end  
  
  //h
  if nargin>=4 then 
    if type(h)<>1|and(size(h)>1)|~isreal(h) then 
      error(msprintf(_("%s: Wrong type for argument #%d: A real vector expected.\n"),fname,4));
    end
    hlength=size(h,"*");
    if (modulo(hlength,2)==0),
      error(msprintf(_("%s: Wrong size for argument #%d: A odd number of elements expected.\n"),fname,4));
    end;
  else
    hlength=floor(xrow/4);
    if rem(hlength,2)==0 then hlength=hlength+1;end;
    h = window("hm",hlength)
  end
  h=h(:);
  Lh=(hlength-1)/2; h=h/h(Lh+1);
  

  //N
  if nargin>=3 then
    if type(N)<>1|size(N,"*")>1|~isreal(N)|int(N)<>N then
      error(msprintf(_("%s: Wrong type for input argument #%d: A integer value expected.\n"),fname,3));
    end 
    if N<=0 then
      error(msprintf(_("%s: Wrong value for input argument #%d: A positive integer expected.\n"),fname,3));
    end
    
  else
    N=xrow
  end

  //T
  if nargin>=2 then
    if type(t)<>1|and(size(t)>1)|~isreal(t) then
      error(msprintf(_("%s: Wrong type for input argument #%d: A real vector expected.\n"),fname,2));
    end
    if max(t)>xrow|min(t)<1 then
      error(msprintf(_("%s: Wrong value for input argument #%d: The elements must be in the interval %s.\n"),fname,2,"[1 "+string(xrow)+"]"));
    end
  else
    t=1:xrow;
  end
  tcol=size(t,2);
  

  if (tcol==1),
    Dt=1; 
  else
    Deltat=diff(t); 
    Mini=min(Deltat); Maxi=max(Deltat);
    if (Mini~=Maxi),
      error(msprintf(_("%s: Wrong value for input argument #%d: The elements must be regularly spaced.\n"),fname,2))
    else
      Dt=Mini;
    end;
    clear Deltat Mini Maxi;
  end;

  tfr= zeros(N,tcol); 
  tf2= zeros(N,tcol);
  if ptrace, disp('Pseudo Margenau-Hill distribution'); end;
  Dh=dwindow(h);
  for icol=1:tcol,
    ti= t(icol); tau=-min([round(N/2)-1,Lh,xrow-ti]):min([round(N/2)-1,Lh,ti-1]);
    indices= rem(N+tau,N)+1;
    if ptrace, disprog(icol,tcol,10); end;
    tfr(indices,icol)= h(Lh+1+tau).*x(ti).*conj(x(ti-tau));
    tf2(indices,icol)=Dh(Lh+1+tau).*x(ti).*conj(x(ti-tau));
  end ;

  tfr= fft(tfr,-1,1); tf2=fft(tf2,-1,1);
  tfr=tfr(:); tf2=tf2(:);

  avoid_warn=find(tfr~=0); 
  tf2(avoid_warn)= round(N*tf2(avoid_warn)./tfr(avoid_warn)/(2.0*%pi)); 
  tfr=real(tfr); tf2=imag(tf2);
  if ptrace, printf ('\nreassignment: \n'); end;
  tfr=matrix(tfr,N,tcol);
  tf2=matrix(tf2,N,tcol);

  rtfr= zeros(N,tcol); 
  Ex=mean(abs(x(min(t):max(t))).^2); Threshold=1.0e-6*Ex;
  for icol=1:tcol,
    if ptrace, disprog(icol,tcol,10); end;
    for jcol=1:N,
      if abs(tfr(jcol,icol))>Threshold,
        jcolhat= real(jcol - tf2(jcol,icol));
        jcolhat=rem(rem(jcolhat-1,N)+N,N)+1;
        rtfr(jcolhat,icol)=rtfr(jcolhat,icol) + tfr(jcol,icol) ;
        tf2(jcol,icol)=jcolhat;
      else 
        tf2(jcol,icol)=%inf;
        rtfr(jcol,icol)=rtfr(jcol,icol) + tfr(jcol,icol) ;
      end;
    end;
  end;

  if ptrace, printf('\n'); end;
  if (plotting),
    TFTBcontinue=%t;
    while TFTBcontinue
      choice=x_choose(['pseudo Margenau-Hill distribution',...
                       'reassigned pseudo Margenau-Hill distribution'],'Choose the representation:','stop');
      if (choice==0), TFTBcontinue=%f;
      elseif (choice==1), 
        tfrqview(tfr,x,t,'tfrpmh',h);
      elseif (choice==2),
        tfrqview(rtfr,x,t,'tfrrpmh',h);
      end;
    end;
  end
  if (nargout>2),
    rhat=tf2;
  end;
endfunction
