function [fm,B2]=momttfr(tfr,method,fbmin,fbmax,freqs);
// Time moments of a time-frequency representation.
// Calling Sequence
//	[FM,B2]=momttfr(TFR)
//	[FM,B2]=momttfr(TFR,METHOD)
//	[FM,B2]=momttfr(TFR,METHOD,FBMIN)
//	[FM,B2]=momttfr(TFR,METHOD,FBMIN,FBMAX)
//	[FM,B2]=momttfr(TFR,METHOD,FBMIN,FBMAX,FREQS)
//  Parameters
//	TFR   : time-frequency representation ([Nrow,Ncol]=size(TFR)).
//	METHOD: chosen representation (name of the corresponding M-file).  
//	FBMIN : smallest frequency bin (default : 1)
//	FBMAX : highest  frequency bin (default : Nrow)
//	FREQS : true frequency of each frequency bin. FREQS must be of length FBMAX-FBMIN+1.       (default : 0:step:(0.5-step) or -0.5:step:(0.5-step)         depending on METHOD) 
//	FM    : averaged frequency     (first order moment)
//	B2    : frequency band         (second order moment)
//  Description
//        momttfr computes the time moments of a time-frequency representation.
//  Examples
//       sig=fmlin(128,0.1,0.4); tfr=tfrwv(sig);
//        [fm,B2]=momttfr(tfr,'tfrwv'); 
//        subplot(211); plot(fm); subplot(212); plot(B2);
//        freqs=linspace(0,63/128,64); tfr=tfrsp(sig); 
//        [fm,B2]=momttfr(tfr,'tfrsp',1,64,freqs); 
//        subplot(211); plot(fm); subplot(212); plot(B2);
//    See also 
//       momftfr
//   Authors
//      H. Nahrstaedt - Aug 2010
//	F. Auger, August 1995.
//	Copyright (c) 1996 by CNRS (France).

//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 [nargout,nargin]=argn(0);
[tfrrow,tfrcol]=size(tfr);
if (nargin<=1),
 error('At least two input arguments');
elseif (nargin==2),
 fbmin=1; fbmax=tfrrow; 
elseif (nargin==3),
 fbmax=tfrrow; 
end
if (fbmin>fbmax)|(fbmin==0)|(fbmax>tfrrow),
 error('1<=FBMIN<=FBMAX<=Nrow');
elseif nargin==5,
 if length(freqs)~=(fbmax-fbmin+1),
  error('FREQS must have FBMAX-FBMIN+1 elements');
 end
end;


method=convstr(method,"u");
if (method=='TFRWV'   ) | (method=='TFRPWV'  ) | ...
   (method=='TFRSPWV' ) | (method=='TFRCW'   ) | ...
   (method=='TFRZAM'  ) | (method=='TFRBJ'   ) | ...
   (method=='TFRBUD'  ) | (method=='TFRGRD'  ) | ...
   (method=='TFRRSPWV') | (method=='TFRRPWV' ) | ...
   (method=='TFRRIDB' ) | (method=='TFRRIDH' ) | ...
   (method=='TFRRIDT' ) | (method=='TFRASPW' ) | ...
   (method=='TFRDFLA' ) | (method=='TFRSPBK' ) | ...
   (method=='TFRUNTAC') | (method=='TFRUNTPA') | ...
   (method=='TFRBERT' ) | (method=='TFRSCALO') | ...
   (method=='TYPE2' ),
 typertf='TYPE2';
elseif (method=='TFRPMH'  )| (method=='TFRRPMH' )| ...
       (method=='TFRSP'   )| (method=='TFRRSP'  )| ...
       (method=='TFRPPAGE')| (method=='TFRRPPAG')| ...
       (method=='TFRMHS'  )| (method=='TFRRGAB' )| ...
       (method=='TFRMH'   )| (method=='TFRMMCE' )| ...
       (method=='TFRMSC'  )| (method=='TFRRMSC' )| ...
       (method=='TFRPAGE' )| (method=='TFRGABOR')| ...
       (method=='TFRRI'   )| (method=='TYPE1'   ),
 typertf='TYPE1';
else
 error('Unknown representation.');
end;

if nargin<=4, 
 if (typertf=='TYPE1'),
  freqs=rem((fbmin-1:fbmax-1)/tfrrow + 0.5, 1.0) - 0.5;
 elseif (typertf=='TYPE2'),
  freqs=0.5*(fbmin-1:fbmax-1)/tfrrow;
 end;
end

E  = mtlb_sum(tfr(fbmin:fbmax,:));
fm = (freqs * tfr(fbmin:fbmax,:) ./E).'; 
B2 = (freqs.^2 * tfr(fbmin:fbmax,:) ./E).' - fm.^2;


endfunction

