function [tfr,t,f,wt]=tfrscalo(X,time,wave,fmin,fmax,N,ptrace,opt_plot);
// Scalogram, for Morlet or Mexican hat wavelet.
// Calling Sequence
//	[TFR,T,F,WT]=tfrscalo(X)
//	[TFR,T,F,WT]=tfrscalo(X,T)
//	[TFR,T,F,WT]=tfrscalo(X,T,WAVE,FMIN, FMAX)
//	[TFR,T,F,WT]=tfrscalo(X,T,WAVE,FMIN,FMAX,N)
//	[TFR,T,F,WT]=tfrscalo(X,T,WAVE,FMIN,FMAX,N,TRACE)
//	[TFR,T,F,WT]=tfrscalo(...,'plot')
//  Description
//    tfrscalo computes the scalogram (squared magnitude of a continuous wavelet transform). 
//  Parameters
//	X : signal (in time) to be analyzed (Nx=length(X)). Its  analytic version is used (z=hilbert(real(X))).  
//	T : time instant(s) on which the TFR is evaluated  (default : 1:Nx).
//	WAVE : half length of the Morlet analyzing wavelet at coarsest  scale. If WAVE = 0, the Mexican hat is used. WAVE can also be    a vector containing the time samples of any bandpass     function, at any scale.   (default : sqrt(Nx)). 
//	FMIN,FMAX : respectively lower and upper frequency bounds of the analyzed signal. These parameters fix the equivalent frequency bandwidth (expressed in Hz). When unspecified, you   have to enter them at the command line from the plot of the   spectrum. FMIN and FMAX must be >0 and <=0.5.
//      N : number of analyzed voices.
//	TRACE : if nonzero, the progression of the algorithm is shown (default : 0).
//      'plot':	if one input parameter is 'plot',  tfrscalo runs tfrqview. and TFR will be plotted
//	TFR : time-frequency matrix containing the coefficients of the   decomposition (abscissa correspond to uniformly sampled time,   and ordinates correspond to a geometrically sampled    frequency). First row of TFR corresponds to the lowest frequency.
//	F : vector of normalized frequencies (geometrically sampled from FMIN to FMAX).
//	WT : Complex matrix containing the corresponding wavelet  transform. The scalogram TFR is the square modulus of WT.
//  Examples
//       sig=altes(64,0.1,0.45); tfrscalo(sig,'plot');  
//  Authors
//      H. Nahrstaedt - Aug 2010
//	P. Goncalves, October 1995 - O. Lemoine, June 1996. 
//	Copyright (c) 1995 Rice University - CNRS 1996.

//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  [nargout,nargin]=argn(0);
  fname="tfrscalo"


  in_par=['X','time','wave','fmin','fmax','N','ptrace','opt_plot'];
  in_par_min=1;
  plotting=%f;
  if (nargin>in_par_min)
    if (type(evstr(in_par(nargin)))==10)
      if convstr(evstr(in_par(nargin)))=="plot" then
        plotting=%t; 
        nargin=nargin-1;
      end; 
    end;
  end;
  clear in_par;

  if nargin == 0 then
    error(msprintf(_("%s: Wrong number of input argument: %d to %d expected.\n"),fname,1,8));
  end
  
  //X
  if type(X)<>1 then
    error(msprintf(_("%s: Wrong type for input argument #%d: A matrix of double expected.\n"),fname,1));
  end
  if or(size(X)==1) then
    X=X(:);
  else
    error(msprintf(_("%s: Wrong size for input argument #%d: A  vector  expected.\n"),fname,1));
  end
  Nx=size(X,1);
  
  //ptrace
  if nargin==7 then 
    if and(type(ptrace)<>[1 4])|size(ptrace,"*")<>1 then
      error(msprintf(_("%s: Wrong type for argument #%d: A boolean or real scalar expected.\n"),fname,6));
    end
  else
    ptrace=%f;
  end
  
  //N
  if nargin>=6 then
    if type(N)<>1|size(N,"*")>1|~isreal(N)|int(N)<>N then
      error(msprintf(_("%s: Wrong type for input argument #%d: A integer value expected.\n"),fname,6));
    end 
    if N<=0 then
      error(msprintf(_("%s: Wrong value for input argument #%d: A positive integer expected.\n"),fname,6));
    end
  else
    N=-1
  end
  
  //fmax
  if nargin>=5 then
     if type(fmax)<>1|size(fmax,"*")>1|~isreal(fmax) then
       error(msprintf(_("%s: Wrong type for input argument #%d: A real expected.\n"),fname,5));
     end 
     if fmax<=0|fmax>0.5 then
       error(msprintf(_("%s: Wrong value for input argument #%d: Must be in the interval %s.\n"),fname,5,"]0,0.5]"));
     end
  else
    fmax=-1
  end
  
  //fmin
  if nargin>=4 then
     if type(fmin)<>1|size(fmin,"*")>1|~isreal(fmin) then
       error(msprintf(_("%s: Wrong type for input argument #%d: A real expected.\n"),fname,4));
     end 
     if fmin<=0|fmin>0.5 then
       error(msprintf(_("%s: Wrong value for input argument #%d: Must be in the interval %s.\n"),fname,5,"]0,0.5]"));
     end
  else
    fmin=-1
  end
  
  //wave
  if nargin>=3 then
     if type(wave)<>1|and(size(wave)>1)|~isreal(wave) then
       error(msprintf(_("%s: Wrong type for input argument #%d: A real vector or scalar expected.\n"),fname,3));
     end 
     if size(wave,'*')==1&wave<0 then
       error(msprintf(_("%s: Wrong value for input argument #%d: Must be >= %d.\n"),fname,3,0));
     end
  else
    wave=sqrt(Nx)
  end
  
  //time
  if nargin>=2 then
    if type(time)<>1|and(size(time)>1)|~isreal(time) then
      error(msprintf(_("%s: Wrong type for input argument #%d: A real vector expected.\n"),fname,2));
    end
    if max(time)>Nx|min(time)<1 then
      error(msprintf(_("%s: Wrong value for input argument #%d: The elements must be in the interval %s.\n"),fname,2,"[1 "+string(Nx)+"]"));
    end
  else
    time=1:Nx;
  end
  Nt=size(time,2);
 

  s = (real(X)-mean(real(X)))';  
  z = hilbert(s) ;

  if ptrace, disp('Scalogram distribution'); end;
  if fmin==-1|fmax==-1 then
    STF = fft(fftshift(z(min(time):max(time)))); 
    Nstf=length(STF);
    sp = (abs(STF(1:round(Nstf/2)))).^2; 
    Maxsp=max(sp);
    f = linspace(0,0.5,round(Nstf/2)+1) ; 
    f = f(1:round(Nstf/2));
    plot(f,sp) ; xgrid;
    xlabel('Normalized frequency');
    title('Analyzed signal energy spectrum');
   
    if fmin==-1 then
      indmin=min(find(sp>Maxsp/100));
      fmindflt=max([0.01 0.05*fix(f(indmin)/0.05)]);
      txtmin=['Lower frequency bound ['+string(fmindflt)+'] : '];
      while  %t then
        fmin = input(txtmin); 
        if fmin==[] then 
          fmin=fmindflt; break
        elseif fmin>0&fmin<=0.5 then
          break
        else
          mprintf(_("%s: Wrong given value for lower frequency bound: Must be in the interval %s.\n"),fname,"]0,0.5]"); 
        end
        
      end
    end
    if fmax==-1 then
      indmax=max(find(sp>Maxsp/100));
      fmaxdflt=0.05*ceil(f(indmax)/0.05);
      txtmax=['Upper frequency bound ['+string(fmaxdflt)+'] : '];
      while  %t then
        fmax = input(txtmax); 
        if fmax==[] then 
          fmax=fmaxdflt; break
        elseif fmax>0&fmax<=0.5 then
          break
        else
          mprintf(_("%s: Wrong given value for upper frequency bound: Must be in the interval %s.\n"),fname,"]0,0.5]"); 
        end
      end
    end
  end
  if N==-1 then
    txt=['Number of frequency samples ['+string(2^nextpow2(Nx))+'] : ']; 
    while  %t then
      N=input(txt); 
      if N==[] then 
        N=2^nextpow2(Nx);break;
      elseif N>0&int(N)==N then
        break
      else
        mprintf(_("%s: Wrong given value for number of frequency samples: a positive integer expected.\n"),fname);  
      end
    end
  end
  
    
  fmin_s=string(fmin); fmax_s=string(fmax); 
  N_s=string(N);

  if ptrace,
    disp(['Frequency runs from '+fmin_s+' to '+fmax_s+' with '+N_s+' points']);
  end

  f = logspace(log10(fmin),log10(fmax),N);
  a = logspace(log10(fmax/fmin),log10(1),N); 


  wt =zeros(N,Nt);
  tfr=zeros(N,Nt);

  if wave > 0
    if ptrace, disp(['using a Morlet wavelet']) ; end
    for ptr=1:N,
      if ptrace, disprog(ptr,N,10); end
      nha = wave*a(ptr);
      tha = -round(nha) : round(nha);
      ha  = exp(-(2*log(10)/nha^2)*tha.^2).*exp(%i*2*%pi*f(ptr)*tha); 
      detail = convol(z,ha)./sqrt(a(ptr));
      detail = detail(round(nha)+1:length(detail)-round(nha)) ;
      wt(ptr,:)  = detail(time) ;
      tfr(ptr,:) = detail(time).*conj(detail(time)) ;
    end
  elseif wave == 0
    if ptrace, disp(['using a Mexican hat wavelet']) ; end
    for ptr = 1:N
      if ptrace, disprog(ptr,N,10); end
      ha  = mexhat(f(ptr)) ;
      nha = (length(ha)-1)/2 ;
      detail = convol(z,ha)./sqrt(a(ptr));
      detail = detail(round(nha)+1:length(detail)-round(nha)) ;
      wt(ptr,:)  = detail(time);
      tfr(ptr,:) = detail(time).*conj(detail(time)) ;
    end  
  elseif length(wave) > 1
    [rwav,cwav]=size(wave);
    if cwav>rwav, wave=wave.'; end
    wavef = fft(wave) ;
    nwave = length(wave) ;
    f0 = find(abs(wavef(1:nwave/2)) == max(abs(wavef(1:nwave/2))));
    f0 = mean((f0-1).*(1/nwave));
    if ptrace, disp(['mother wavelet centered at f0 = '+string(f0)]); end
    a = logspace(log10(f0/fmin),log10(f0/fmax),N);
    B = 0.99;
    R = B/((1.001)/2); 
    nscale = max(128,round((B*nwave*(1+2/R)*log((1+R/2)/(1-R/2)))/2));
    if ptrace, disp('Scale computation :'); end
    wts = scale(wave,a,fmin,fmax,nscale,ptrace);
    for ptr = 1:N, 
      clear detail
      if ptrace, disprog(ptr,N,10); end
      ha = wts(ptr,:);
      nha = length(ha)/2;
      detail = convol(z,ha)./sqrt(a(ptr));
      detail = detail(fix(nha):length(detail)-round(nha));
      wt(ptr,:) = detail(time);
      tfr(ptr,:) = detail(time).*conj(detail(time));
    end
  end


  t = time;
  f = f';

  // Normalization
  SP = fft(z); 
  indmin = 1+round(fmin*(Nx-2));
  indmax = 1+round(fmax*(Nx-2));
  SPana=SP(indmin:indmax);
  //disp(norm(SPana)^2/integ2d(tfr,t,f)/N)
  tfr=real(tfr*norm(SPana)^2/integ2d(tfr,t,f)/N);

  if (plotting),
    tfrqview(tfr,hilbert(real(X)),t,'tfrscalo',wave,N,f);
  end;

endfunction
