function [tfr,dgr,gam]=tfrgabor(sig,N,q,h,ptrace,opt_plot)
// Gabor representation of a signal.
// Calling Sequence
//       [TFR,DGR,GAM]=tfrgabor(SIG,N,Q) 
//       [TFR,DGR,GAM]=tfrgabor(SIG,N,Q,H) 
//       [TFR,DGR,GAM]=tfrgabor(SIG,N,Q,H,TRACE) 
//       [TFR,DGR,GAM]=tfrgabor(...,'plot') 
// Parameters
//	SIG : signal to be analyzed (length(SIG)=N1).
//	N   : number of Gabor coefficients in time (N1 must be a multiple  of N)       (default : divider(N1)). 
//	Q   : degree of oversampling ; must be a divider of N     (default : Q=divider(N)).
//	H   : synthesis window, which was originally chosen as a Gaussian    window by Gabor. Length(H) should be as closed as possible    from N, and must be >=N (default : Gauss(N+1)).  H must be of unit energy, and CENTERED. 
//	TRACE : if nonzero, the progression of the algorithm is shown          (default : 0).
//      'plot':	if one input parameter is 'plot',  tfrgabor runs tfrqview. and TFR will be plotted
//	TFR : Square modulus of the Gabor coefficients. 
//	DGR : Gabor coefficients (complex values). 
//	GAM : biorthogonal (dual frame) window associated to H.
//  Description
//       tfrgabor computes the Gabor	representation  of signal X, for a given synthesis window H, on a
//	rectangular grid of size (N,M) in the time-frequency plane. M and N
//	must be such that 
//			N1 = M * N / Q 
//	where N1=length(X) and Q is an integer corresponding to the 
//	degree of oversampling.
//       If Q=1, the time-frequency plane (TFP) is critically
//	 sampled, so there is no redundancy in the TFP.
//	If Q>1, the TFP is oversampled, allowing a greater
//	 numerical stability of the algorithm.
// Examples
//       sig=fmlin(128); 
//       tfrgabor(sig,64,32,'plot'); 
//     Authors
//      H. Nahrstaedt - Aug 2010
//	O. Lemoine, October 1995 - February 1996.
//	Copyright (c) 1996 by CNRS (France).

//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
//
//	References : 
//	  o Zibulski, Zeevi "Oversampling in the Gabor Scheme"
//	    IEEE Trans. on Signal Processing, Vol 41, No 8, August 1993
//	    PP 2679-87
//	  o Wexler, Raz "Discrete Gabor Expansions"
//	    Signal Processing, Vol 21, No 3, pp. 207-221, Nov 1990		
  [nargout,nargin]=argn(0);
  fname="tfrgabor";
  
  in_par=['sig','N','q','h','ptrace','opt_plot'];
  in_par_min=1;
  plotting=%f;
  if (nargin>in_par_min)
    if (type(evstr(in_par(nargin)))==10)
      if convstr(evstr(in_par(nargin)))=="plot" then
        plotting=%t; 
        nargin=nargin-1;
      end; 
    end;
  end;
  clear in_par;

 if nargin == 0 then
    error(msprintf(_("%s: Wrong number of input arguments: %d to %d expected.\n"),fname,1,6));
  end;
 
  //sig
  if type(sig)<>1|and(size(sig)<>1) then
    error(msprintf(_("%s: Wrong type for input argument #%d: A vector of double expected.\n"),fname,1));
  end
  sig=matrix(sig,1,-1);
  N1=size(sig,2)
  if N1<=2 then
    error(msprintf(_("%s: Wrong size for input argument #%d: Must be > %d.\n"),fname,1,2));
  end
  
  
  //N
  if nargin>=2 then
    if type(N)<>1|size(N,"*")>1|~isreal(N)|int(N)<>N then
      error(msprintf(_("%s: Wrong type for input argument #%d: A integer value expected.\n"),fname,2));
    end 
    if N<2|N>=N1 then
       error(msprintf(_("%s: Wrong value for input argument #%d: Must be in the interval %s.\n"),fname,2,"[2,"+string(N1-1)+"]"));
    end
    if modulo(N1,N)<>0 then
       error(msprintf(_("%s: Wrong value for input argument #%d: Must be a divisor of the length of argument %d.\n"),fname,2,1));
    end
  else
    N=divider(N1);
  end
  
  //q
  if nargin>=3 then
    if type(q)<>1|size(q,"*")>1|~isreal(q)|int(q)<>q then
      error(msprintf(_("%s: Wrong type for input argument #%d: A integer value expected.\n"),fname,3));
    end 
    if modulo(N,q)<>0 then
       error(msprintf(_("%s: Wrong value for input argument #%d: Must be a divisor of the  argument %d.\n"),fname,3,2));
    end
  else
    q=divider(N);
  end
  
  //h
  if nargin>=4 then
    if type(h)<>1|and(size(h)<>1)|~isreal(h) then
      error(msprintf(_("%s: Wrong type for input argument #%d: A real vector expected.\n"),fname,4));
    end
    Nh=size(h,"*")
    if Nh<=N then
      error(msprintf(_("%s: Wrong size for input argument #%d: Must be >= %d.\n"),fname,4,N));
    end
    if modulo(Nh,2)==0 then
      error(msprintf(_("%s: Wrong size for argument #%d: A odd number of elements expected.\n"),fname,4));
    end
  else
    Nh=odd(N)
    h=tftb_window(Nh,'Gauss');
  end
  h=matrix(h,1,-1);
  h=h/norm(h);
   
  //ptrace
  if nargin>=5 then 
    if and(type(ptrace)<>[1 4])|size(ptrace,"*")<>1 then
      error(msprintf(_("%s: Wrong type for argument #%d: A boolean or real scalar expected.\n"),fname,5));
    end
  else
    ptrace=%f;
  end
  

  // Initializations
  if ptrace, disp('Gabor representation'); end;
 

  M=q*N1/N;
  Mb=N1/N;        // frequency-shift between two Gabor coefficients
  Nb=N1/M;        // time-shift between two Gabor coefficients

  // Zak transform of h
 
  // Time shift to be correctly localized in time :
  alpha=round((2*N1/N-1-Nh)/(2*q));
  hN1=zeros(N1,1); 
  hN1(round((N1-(Nh-1))/2)-alpha:round((N1+Nh-1)/2)-alpha)=h;	

  DZTh=zeros(M,Nb);
  Msig=matrix(hN1,Nb,M);
  DZTh=fft(Msig.',-1,1)/sqrt(M);

  // Sum of elements of h-zak transform

  Mzh=zeros(M,Nb);
  x=(1:M);

  for l=0:q-1,
    mod=vecmodulo(x-l*M/q,M);
    Mzh=Mzh+abs(DZTh(mod,:)).^2;
  end

  [ind1,ind2]=find(Mzh<%eps);
  for l=1:length(ind1),
    Mzh(ind1(l),ind2(l))=1;
  end
  
  // Zak transform of the biorthogonal (dual frame) window gam

  DZTgam=zeros(M,Nb);
  DZTgam=DZTh./Mzh;
  gam=real(izak(DZTgam))/N1;

  // Computation of the Gabor coefficients from the dual frame window

  dgr=zeros(M,N);
  tfr=zeros(M,N);
  k=1:N1;
  dgrN1=zeros(N1,N);
  
  for n=1:N, 
    if ptrace, disprog(n,N,10); end;
    indice=vecmodulo(k-n*M/q,N1);
    dgrN1(:,n)=fft(sig.*fftshift(gam(indice)')).';
  end

  dgr=dgrN1(1:Nb:N1,:);
  tfr=abs(dgr).^2;
  t=1:M/q:N1;

  if plotting then
    tfrqview(tfr,sig.',t,'tfrgabor',N,q,h(:));
  end;
endfunction

