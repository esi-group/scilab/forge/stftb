function [ti,fi]=midpoint(t1,f1,t2,f2,k)
// Mid-point construction used in the interference diagram. 
// Calling Sequence
//	[TI,FI]=midpoint(T1,F1,T2,F2,K)
// Parameters
//	T1 : time-coordinate of the first point
//	F1 : frequency-coordinate of the first point (>0)
//	T2 : time-coordinate of the second point
//	F2 : frequency-coordinate of the second point (>0)
//	K  : power of the group-delay law
//	  K = 2    : Wigner-Ville 
//	  K = 1/2  : D-Flandrin
//	  K = 0    : Bertrand (unitary) 
//	  K = -1   : Unterberger (active)
//	  K = inf  : Margenau-Hill-Rihaczek
//	TI : time-coordinate of the interference term
//	FI : frequency-coordinate of the interference term
//   Description
//       midpoint  gives the coordinates in the
//	time-frequency plane of the interference-term corresponding to
//	the points (T1,F1) and (T2,F2), for a distribution in the
//	affine class perfectly localized on power-law group-delays of 
//	the form : tx(nu)=t0+c nu^(K-1).
// See also 
//        plotsid
//   Authors
//      H. Nahrstaedt - Aug 2010
//	P. Flandrin, September 1995 - F. Auger, April 1996.
//	Copyright (c) 1996 by CNRS (France).

//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  [nargout,nargin]=argn(0);
  fname="midpoint"  
  if nargin <>5 then
    error(msprintf(_("%s: Wrong number of input argument: %d expected.\n"),fname,1,5));
  end
  
  //t1
  if type(t1)<>1|~isreal(t1)|and(size(t1)<>1) then
    error(msprintf(_("%s: Wrong type for input argument #%d: A real vector expected.\n"),fname,1));
  end
  t1=matrix(t1,1,-1)
  N=size(t1,2)
  
  if type(f1)<>1|~isreal(f1)|and(size(f1)<>1) then
    error(msprintf(_("%s: Wrong type for input argument #%d: A real vector expected.\n"),fname,2));
  end
  if or(f1<0) then
     error(msprintf(_("%s: Wrong value for input argument #%d: The elements must be > %d\n"),fname,2,0))
  end
  f1=matrix(f1,1,-1)
  if size(f1,2)<>N then
    error(msprintf(_("%s: Incompatible input arguments #%d and #%d: Same element number expected.\n"),fname,1,2));
  end
  
  if type(t2)<>1|~isreal(t2)|and(size(t2)<>1) then
    error(msprintf(_("%s: Wrong type for input argument #%d: A real vector expected.\n"),fname,3));
  end  
  t2=matrix(t2,1,-1)
  if size(t1,2)<>N then
    error(msprintf(_("%s: Incompatible input arguments #%d and #%d: Same element number expected.\n"),fname,1,3));
  end
  
  if type(f2)<>1|~isreal(f2)|and(size(f2)<>1) then
    error(msprintf(_("%s: Wrong type for input argument #%d: A real vector expected.\n"),fname,4));
  end
  if or(f2<0) then
     error(msprintf(_("%s: Wrong value for input argument #%d: The elements must be > %d\n"),fname,4,0))
  end
  f2=matrix(f2,1,-1)
  if size(f2,2)<>N then
    error(msprintf(_("%s: Incompatible input arguments #%d and #%d: Same element number expected.\n"),fname,1,4));
  end

  if type(k)<>1|~isreal(k)|or(size(k)<>1) then
    error(msprintf(_("%s: Wrong type for input argument #%d: A real scalar expected.\n"),fname,5));
  end
  
  if (k==2) then
    fi=(f1+f2)/2;
    ti=(t1+t2)/2;
  elseif (k==%inf) then
    ti=[t1;t2];
    fi=[f2;f1];
  else
    I=find(abs(f1-f2)>sqrt(%eps));
    fi=zeros(f1)
    ti=zeros(t1)
    if length(I)~=0, 
      if (k==1),
        fi(I)=exp( (f1(I).*(log(f1(I))-1)-f2(I).*(log(f2(I))-1)) ./ ...
                   (f1(I)-f2(I))); 
        ti(I)=(t1(I).*f1(I)-t2(I).*f2(I)) ./ (f1(I)-f2(I)) - ...
              (t1(I)-t2(I)) ./ (log(f1(I))-log(f2(I)));
      elseif (k==0),
        fi(I)=(f1(I)-f2(I))./(log(f1(I))-log(f2(I)));
        ti(I)=(t1(I).*f1(I)-t2(I).*f2(I)) ./ (f1(I)-f2(I)) + ...
              f1(I) .* f2(I) .* (t2(I)-t1(I)) .* ...
              (log(f1(I))-log(f2(I))) ./ (f2(I)-f1(I)).^2; 
      else
        t0(I)=(t1(I).*f2(I).^(k-1)-t2(I).*f1(I).^(k-1)) ./ ...
              (f2(I).^(k-1)-f1(I).^(k-1));
        fi(I)=((f1(I).^k-f2(I).^k) ./ (f1(I)-f2(I))/k).^(1/(k-1));
        ti(I)=t0(I)+(t2(I)-t1(I)) ./ (f2(I).^(k-1)-f1(I).^(k-1)) .*fi(I).^(k-1);
      end
    end
  end
endfunction
