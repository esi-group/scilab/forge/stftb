function [waf,tau,theta] = ambifuwb(x,fmin,fmax,N,ptrace,opt_plot);
// Wide-band ambiguity function.
// Calling Sequence
//[WAF,TAU,THETA]=ambifuwb(X) 
//[WAF,TAU,THETA]=ambifuwb(X,FMIN) 
//[WAF,TAU,THETA]=ambifuwb(X,FMIN,FMAX) 
//[WAF,TAU,THETA]=ambifuwb(X,FMIN,FMAX,N) 
//[WAF,TAU,THETA]=ambifuwb(X,FMIN,FMAX,N,TRACE) 
//[WAF,TAU,THETA]=ambifuwb(...,'plot') 
//   Parameters
//	X     : signal (in time) to be analyzed (the analytic associated signal is considered), of length Nx.
//       FMIN,FMAX : respectively lower and upper frequency bounds of  the analyzed signal. When specified, these parameters fix  the equivalent frequency bandwidth (in Hz). When unspecified,  you have to enter them at the command line from the plot of the   spectrum. FMIN and FMAX must be >0 and <=0.5.
//       N     : number of Mellin points (default : automatically determined).
//	TRACE : if nonzero, the progression of the algorithm is shown (default : 0).
//	AF    : matrix containing the coefficients of the ambiguity  function. X-coordinate corresponds to the dual variable of  scale parameter ; Y-coordinate corresponds to time delay,  dual variable of frequency.  
//     'plot' : When called with the additional argument 'plot', ambifuwb displays  the squared modulus of the ambiguity function by means of  contour.
//       TAU   : X-coordinate corresponding to time delay
//       THETA : Y-coordinate corresponding to the log(scale) variable
//  Description
//      ambifuwb calculates the asymetric wide-band ambiguity function.
//  Examples
//       sig=altes(128,0.1,0.45); ambifuwb(sig,'plot');
//  See also 
//        ambifunb
//   Authors
//      H. Nahrstaedt - Aug 2010
//	P. Goncalvez - December 1995, O. Lemoine - August 1996.

//	Copyright (c) 1995 Rice University, CNRS (France)
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  [nargout,nargin]=argn(0);
   fname="ambifuwb";
 

  in_par=['x','fmin','fmax','N','ptrace','opt_plot'];
  in_par_min=1;
  plotting=%f;
  if (nargin>in_par_min)
    if (type(evstr(in_par(nargin)))==10)
      if convstr(evstr(in_par(nargin)))=="plot" then
        plotting=%t; 
        nargin=nargin-1;
      end; 
    end;
  end;
  clear in_par;
  
  if nargin == 0 then
    error(msprintf(_("%s: Wrong number of input argument: %d to %d expected.\n"),fname,1,5));
  end;
  
  //x
  if type(x)<>1 then
    error(msprintf(_("%s: Wrong type for input argument #%d: A matrix of double expected.\n"),fname,1));
  end
  if or(size(x)==1) then
    x=x(:);
  else
    error(msprintf(_("%s: Wrong size for input argument #%d: A a vector  expected.\n"),fname,1)); 
  end
  Nx=size(x,1);
 
  //ptrace
  if nargin>=5 then 
    if and(type(ptrace)<>[1 4])|size(ptrace,"*")<>1 then
      error(msprintf(_("%s: Wrong type for argument #%d: A boolean or real scalar expected.\n"),fname,5));
    end
  else
    ptrace=%f;
  end
  
  //N
  if nargin>=4 then
    if type(N)<>1|size(N,"*")>1|~isreal(N)|int(N)<>N then
      error(msprintf(_("%s: Wrong type for input argument #%d: A integer value expected.\n"),fname,4));
    end 

  else
    N=[];
  end
  
  //fmax
  if nargin>=3 then
     if type(fmax)<>1|size(fmax,"*")>1|~isreal(fmax) then
       error(msprintf(_("%s: Wrong type for input argument #%d: A real expected.\n"),fname,3));
     end 
     if fmax<=0|fmax>0.5 then
       error(msprintf(_("%s: Wrong value for input argument #%d: Must be in the interval %s.\n"),fname,3,"]0,0.5]"));
     end
  else
    fmax=[]
  end
  
  //fmin
  if nargin>=2 then
     if type(fmin)<>1|size(fmin,"*")>1|~isreal(fmin) then
       error(msprintf(_("%s: Wrong type for input argument #%d: A real expected.\n"),fname,2));
     end 
     if fmin<=0|fmin>0.5 then
       error(msprintf(_("%s: Wrong value for input argument #%d: Must be in the interval %s.\n"),fname,2,"]0,0.5]"));
     end
     if fmin>fmax then
       error(msprintf(_("%s: Wrong value for input argument #%d: Must be < %g.\n"),fname,2,fmax));
     end
  else
    fmin=[]
  end



  s = hilbert(real(x)); 
  M = round(Nx/2);

  t = (1:Nx)-M-1;
  Tmin = 1;
  Tmax = Nx;
  T = Tmax-Tmin;

  if fmin==[]|fmax==[] then
    STF = fft(fftshift(s)); 
    sp  = (abs(STF(1:M))).^2; Maxsp=max(sp);
    f   = linspace(0,0.5,M+1) ; f=f(1:M);
    plot(f,sp) ; xgrid;
    xlabel(_('Normalized frequency'));
    title(_('Analyzed signal energy spectrum'));
    a=gca();a.data_bounds=[0 0; 1/2  1.2*Maxsp];
    if fmin==[] then
      indmin=min(find(sp>Maxsp/100));
      fmindflt=max([0.01 0.05*fix(f(indmin)/0.05)]);
      txtmin=msprintf(_('Lower frequency bound [%s] :'),string(fmindflt));
      while  %t then
        fmin = input(txtmin); 
        if fmin==[] then 
          fmin=fmindflt; break
        elseif fmin>0&fmin<=0.5 then
          break
        else
          mprintf(_("%s: Wrong given value for lower frequency bound: Must be in the interval %s.\n"),fname,"]0,0.5]"); 
        end
      end
    end
    if fmax==[] then
      indmax=max(find(sp>Maxsp/100));
      fmaxdflt=0.05*ceil(f(indmax)/0.05);
      txtmax=msprintf(_('Upper frequency bound [%s] :'),string(fmaxdflt));
      while  %t then
        fmax = input(txtmax); 
        if fmax==[] then 
          fmax=fmaxdflt; break
        elseif fmax>0&fmax<=0.5&fmax>fmin then
          break
        else
          mprintf(_("%s: Wrong given value for upper frequency bound: Must be in the interval %s and greater than fmin\n"),fname,"]0,0.5]"); 
        end
      end  
    end
  end
 

  B = fmax-fmin ; 
  R = B/((fmin+fmax)/2) ; 

  Nq= ceil((B*T*(1+2/R)*log((1+R/2)/(1-R/2)))/2);
  Nmin = Nq-rem(Nq,2);
  if N<Nmin then
    error(msprintf(_("%s: Wrong value for input argument #%d: Must be > %d.\n"),fname,4,Nmin));
  end
  if N==[] then
    Ndflt = 2^nextpow2(Nmin);
    Ntxt=msprintf(_('Number of frequency samples (>=%d) [%d] : '),Nmin,Ndflt);
    while  %t then
      N=input(Ntxt); 
      if N==[] then 
        N=Ndflt;break;
      elseif N>=Nmin&int(N)==N then
        break
      else
        mprintf(_("%s: Wrong given value for number of frequency samples: a positive integer expected.\n"),fname);  
      end
    end
  end
 
  fmin_s = string(fmin) ; fmax_s = string(fmax) ; N_s = string(N) ;
  if ptrace,
    disp(['frequency runs from ',fmin_s,' to ',fmax_s,' with ',N_s,' points']);
  end


  // Geometric sampling of the analyzed spectrum
  k = 1:N;
  q = (fmax/fmin)^(1/(N-1));
  geo_f  = fmin*(exp((k-1).*log(q)));
  tfmatx = zeros(Nx,N);
  tfmatx = exp(-2*%i*t'*geo_f*%pi);
  S = s.'*tfmatx; 
  S = S(ones(1,Nx),:);
  Sb = S.*tfmatx ;

  tau = t;
  S(:,N+1:2*N) = zeros(Nx,N);  S = S.';
  Sb(:,N+1:2*N) = zeros(Nx,N); Sb = Sb.';

  // Mellin transform computation of the analyzed signal
  p=0:(2*N-1);
  coef = exp(p/2.*log(q))'; 
  MellinS = fftshift(ifft(S(:,1).*coef)).';
  MellinS = MellinS(ones(1,Nx),:) ; MellinS = MellinS.';
  for b=1:Nx,
    if ptrace, disprog(b,Nx,10); end
    MellinSb(:,b) = fftshift(ifft(Sb(:,b).*coef)) ;
  end

  k = 1:2*N;
  beta0 = (p/N-1)/(2*log(q));

  Scale = logspace(log10(fmin/fmax),log10(fmax/fmin),N) ;
  waf = zeros(N,Nx) ;
  MellinSSb = MellinS.*conj(MellinSb) ;

  waf = tftb_ifft(MellinSSb,N);
  No2=(N+rem(N,2))/2;
  waf = [waf(No2+1:N,:) ; waf(1:No2,:)]; 

  // Normalization
  s=real(s);
  SP = fft(hilbert(s)); 
  indmin = 1+round(fmin*(Nx-2));
  indmax = 1+round(fmax*(Nx-2));
  SPana = SP(indmin:indmax);

  waf=waf*norm(SPana)^2/waf(No2,M)/N;


  theta = log(Scale) ;


  if plotting then
    clf();
    xset("fpf"," ");
    contour2d(tau,theta,abs(waf.^2)',16); 
    xgrid;
    xlabel(_('Delay')); ylabel(_('Log(scale)'));
    title(_('Wide-band ambiguity function'));
  end;
endfunction
