function r=renyi(tfr,t,f,alpha);
//	Measure Renyi information.
// Calling Sequence
//	R=renyi(TFR) 
//	R=renyi(TFR,T) 
//	R=renyi(TFR,T,F) 
//	R=renyi(TFR,T,F,ALPHA) 
// Parameters
//	TFR : (M,N) 2-D density function (or mass function). Eventually    TFR can be a time-frequency representation, in which case     its first row must correspond to the lower frequencies
//	T : abscissa vector parametrizing the TFR matrix. T can be a    non-uniform sampled vector (eventually a time vector)(default : (1:N)).	
//	F : ordinate vector parametrizing the TFR matrix. F can be a    non-uniform sampled vector (eventually a frequency vector)	(default : (1:M)).	
//	ALPHA : rank of the Renyi measure	(default : 3).
//	R : the alpha-rank Renyi measure (in bits if TFR is a time-frequency matrix) :   R=log2[Sum[TFR(Fi,Ti)^ALPHA dFi.dTi]/(1-ALPHA)]  
//    Description
//      renyi measures the Renyi information relative 
//	to a 2-D density function TFR (which can be eventually a TF
//	representation).
//  Examples
//       s=atoms(64,[32,.3,16,1]); [TFR,T,F]=tfrsp(s); R=renyi(TFR,T,F,3) 
//       s=atoms(64,[16,.2,10,1;40,.4,12,1]); [TFR,T,F]=tfrsp(s); 
//       R=renyi(TFR,T,F,3) 
//   Authors
//      H. Nahrstaedt - Aug 2010
//	P. Goncalves, October 95
//	Copyright (c) 1995 Rice University.

//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  [nargout,nargin]=argn(0);
  fname="renyi";
  if nargin < 1 then
    error(msprintf(_("%s: Wrong number of input argument: %d to %d expected.\n"),fname,1,4));
  end

   //tfr
  if type(tfr)<>1 then
    error(msprintf(_("%s: Wrong type for input argument #%d: A matrix of double expected.\n"),fname,1));
  end
  [M,N] = size(tfr) ;

  //t
  if nargin>=2 then
    if type(t)<>1|and(size(t)<>1)|~isreal(t)then
      error(msprintf(_("%s: Wrong type for input argument #%d: A real vector expected.\n"),fname,2));
    end
    if size(t,"*")<>N then
      error(msprintf(_("%s: Wrong size for input arguments #%d: %d expected.\n"),fname,2,N));
    end
  else
    t = (1:N);
  end
  
  //f
  if nargin>=3 then  
    if type(f)<>1|and(size(f)<>1)|~isreal(f)then
      error(msprintf(_("%s: Wrong type for input argument #%d: A real vector expected.\n"),fname,3));
    end
    if size(f,"*")<>M then
      error(msprintf(_("%s: Wrong size for input arguments #%d: %d expected.\n"),fname,3,M));
    end
    if or(f<0) then
      error(msprintf(_("%s: Wrong value for input argument #%d: The elements must be >= %d\n"),fname,2,0))
    end
    f=f(:);
  else
    f = (1:M)';
  end
  
  //alpha
  if nargin>=4 then  
    if type(alpha)<>1|or(size(alpha)<>1)|~isreal(alpha)|alpha<0 then
      error(msprintf(_("%s: Wrong type for input argument #%d: A  positive scalar expected.\n"),fname,4));
    end
    if alpha == 1&min(tfr)<0 then
      error(msprintf(_("%s: Wrong value for input argument #%d: distribution with negative values => alpha=1 not allowed.\n"),fname,4));
    end
  else
    alpha=3
  end
 

  [f,ind]=gsort(f,"g","i" );
  tfr=tfr(ind,:)
  
  tfr = tfr./integ2d(tfr,t,f);
  if alpha == 1 then
    r=-integ2d(tfr.*log2(tfr+%eps),t,f);
  else
    r=log2(integ2d(tfr.^alpha,t,f)+%eps)/(1-alpha) ;
  end
endfunction
