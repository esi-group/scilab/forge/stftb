function y = amexpo1s(N,t0,T);
// Generate one-sided exponential amplitude modulation.
// Calling Sequence
//	Y = amexpo1s(N) 
//	Y = amexpo1s(N,T0) 
//	Y = amexpo1s(N,T0,T) 
//   Parameters
//	N  : number of points.
//	T0 : arrival time of the exponential	(default : N/2).
//	T  : time spreading			(default : 2*sqrt(N)).
//	Y  : signal.
// Description
//    amexpo1s generates a one-sided exponential
//	  amplitude modulation centered on a time T0, and with a 
//	  spread proportional to T.
//	  This modulation is scaled such that Y(T0)=1.
//  Examples
//    z=amexpo1s(160);plot(z);
//    z=amexpo1s(160,20,40);plot(z);
// See also 
//    amexpo2s
//    amgauss
//    amrect
//    amtriang
// Authors
//      H. Nahrstaedt - Aug 2010
//      F. Auger, July 1995.
//	Copyright (c) 1996 by CNRS (France).

//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  [nargout,nargin]=argn(0);
  fname="amexpo1s";
  if nargin == 0 then
    error(msprintf(_("%s: Wrong number of input argument: %d to %d expected.\n"),fname,1,3));
  end;
  
  //N
  if type(N)<>1|size(N,"*")>1|~isreal(N)|int(N)<>N then
    error(msprintf(_("%s: Wrong type for input argument #%d: A integer value expected.\n"),fname,1));
  end 
  if N<1
    error(msprintf(_("%s: Wrong value for input argument #%d: Must be >= %d.\n"),fname,1,1))
  end
 
  
  //T
  if nargin==3 then
     if type(T)<>1|size(T,"*")>1|~isreal(T) then
       error(msprintf(_("%s: Wrong type for input argument #%d: A real expected.\n"),fname,3));
     end 
     if T<=0 then
       error(msprintf(_("%s: Wrong value for input argument #%d: Must be > %d.\n"),fname,3,0))
     end
  else
    T=2*sqrt(N)
  end
  
  //t0
  if nargin>=2 then
    if type(t0)<>1|size(t0,"*")>1|~isreal(t0) then
       error(msprintf(_("%s: Wrong type for input argument #%d: A real expected.\n"),fname,2));
     end 
     if t0<=0 then
       error(msprintf(_("%s: Wrong value for input argument #%d: Must be > %d.\n"),fname,2,0))
     end
  else
     t0=N/2;
  end
 
  tmt0=(1:N)'-t0;
  y = exp(-sqrt(%pi)*tmt0/T).*bool2s(tmt0>=0.0);

endfunction
