function [x,iflaw]=fmpower(N,k,p1,p2)
// Signal with power-law frequency modulation.
// Calling Sequence
//	[X,IFLAW]=fmpower(N,K,P1) 
//	[X,IFLAW]=fmpower(N,K,P1,P2) 
//  Parameters
//	N  : number of points in time
//	K  : degree of the power-law (K~=1)
//	P1 : if the number of input arguments (NARGIN) is 3, P1 is a  vector containing the two coefficients [F0 C] for a  power-law instantaneous frequency (sampling frequency is set to 1). If NARGIN=4, P1 (as P2) is a time-frequency point of the  form [Ti Fi]. Ti is in seconds and Fi is a normalized frequency  (between 0 and 0.5). The coefficients F0    and C are then deduced such that the frequency modulation   law fits the points P1 and P2.
//	P2 : same as P1 if NARGIN=4         (optional)
//	X  : time row vector containing the modulated signal samples
//	IFLAW : instantaneous frequency law
//    Description
//    fmpower generates a signal with a
//	power-law frequency modulation.
//	X(t) = exp(j*2*pi(F0*t + C/(1-K)*abs(t).^(1-K))) 
//    Examples
//       [X,IFLAW]=fmpower(128,0.5,[1 0.5],[100 0.1]);
//       subplot(211);plot(real(X));subplot(212);plot(IFLAW);
//   See also 
//      fmhyp
//      fmsin
//      fmodany
//      fmconst
//      fmlin
//      fmpar
//   Authors
//      H. Nahrstaedt - Aug 2010
//	P. Goncalves - October 1995, O. Lemoine - April 1996.
//	Copyright (c) 1995 Rice University, 1996 CNRS (France).

//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  [nargout,nargin]=argn(0);
  fname="fmpower"
  
  if nargin <=2 then
    error(msprintf(_("%s: Wrong number of input argument: %d or %d expected.\n"),fname,3,4));
  end;
  
  //N
  if type(N)<>1|size(N,"*")>1|~isreal(N)|int(N)<>N then
    error(msprintf(_("%s: Wrong type for input argument #%d: A integer value expected.\n"),fname,1));
  end 
  if N<1 then
    error(msprintf(_("%s: Wrong value for input argument #%d: Must be >= %d.\n"),fname,1,1));
  end
  
  //k
  if type(k)<>1|size(k,"*")>1|~isreal(k) then
    error(msprintf(_("%s: Wrong type for input argument #%d: A real value expected.\n"),fname,2));
  end 
  if abs(k-1)<2*%eps then
    error(msprintf(_("%s: Wrong value for input argument #%d: Must be <> %d.\n"),fname,2,1));
  end
  
  //p1
  if nargin==3 then
    if type(p1)<>1|size(p1,"*")<>2|~isreal(p1) then
      error(msprintf(_("%s: Wrong type for input argument #%d: A real vector of size %d expected.\n"),fname,3,2));
    end
    f0=p1(1);
    c=p1(2);
    if f0<=0|f0>0.5 then
      error(msprintf(_("%s: Wrong value for input argument #%d: The element #%d must be in the interval %s.\n"),fname,3,1,"[-0.5,0.5]"));
    end 
  end
  if nargin==4 then
    if type(p1)<>1|size(p1,"*")<>2|~isreal(p1) then
      error(msprintf(_("%s: Wrong type for input argument #%d: A real vector of size %d expected.\n"),fname,2,2));
    end
    if p1(1)>N | p1(1)<1 then
      error(msprintf(_("%s: Wrong value for input argument #%d: The element #%d must be in the interval %s.\n"),fname,2,1,"[1,"+string(N)+"]"));
    end
    if p1(2)<0 then
      error(msprintf(_("%s: Wrong value for input argument #%d: The element #%d must be >= %d.\n"),fname,2,2,0));
    end
      
    if type(p2)<>1|size(p2,"*")<>2|~isreal(p2) then
      error(msprintf(_("%s: Wrong type for input argument #%d: A real vector of size %d expected.\n"),fname,3,2));
    end
      
    if p2(1)>N | p2(1)<1 then
      error(msprintf(_("%s: Wrong value for input argument #%d: The element #%d must be in the interval %s.\n"),fname,3,1,"[1,"+string(N)+"]"));
    end
    if p2(2)<0 then
      error(msprintf(_("%s: Wrong value for input argument #%d: The element #%d must be >= %d.\n"),fname,3,2,0));
    end
      
    c  = (p2(2) - p1(2))/(1/p2(1)^k - 1/p1(1)^k) ;
    f0 = p1(2) - c/p1(1)^k ;
  end 
 
  t=1:N;

  phi = 2*%pi*(f0*t + c/(1-k)*abs(t).^(1-k)) ;
  iflaw = (f0 + c*abs(t).^(-k)).' ;

  aliasing = find(iflaw<0 | iflaw>0.5) ;
  if isempty(aliasing) == 0
     warning(msprintf(_("%s: signal is undersampled or has negative frequencies\n"),fname))
  end

  x = exp(%i*phi).';
endfunction
