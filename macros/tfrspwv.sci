function [tfr,t,f] = tfrspwv(x,t,N,g,h,ptrace,opt_plot);
// Smoothed Pseudo Wigner-Ville time-frequency distribution.
// Calling Sequence
//	[TFR,T,F]=tfrspwv(X) 
//	[TFR,T,F]=tfrspwv(X,T) 
//	[TFR,T,F]=tfrspwv(X,T,N) 
//	[TFR,T,F]=tfrspwv(X,T,N,G 
//	[TFR,T,F]=tfrspwv(X,T,N,G,H) 
//	[TFR,T,F]=tfrspwv(X,T,N,G,H,TRACE) 
//	[TFR,T,F]=tfrspwv(...,'plot') 
//   Parameters
//	X     : signal if auto-SPWV, or [X1,X2] if cross-SPWV.
//	T     : time instant(s)          (default : 1:length(X))		.
//	N     : number of frequency bins (default : length(X)).
//	G     : time smoothing window, G(0) being forced to 1.  default : Hamming(N/10)). 
//	H     : frequency smoothing window in the time-domain, H(0) being forced to 1   (default : Hamming(N/4)). 
//	TRACE : if nonzero, the progression of the algorithm is shown  (default : 0).
//      'plot':	if one input parameter is 'plot',  tfrspwv runs tfrqview. and TFR will be plotted
//	TFR   : time-frequency representation.
//	F     : vector of normalized frequencies.
//   Description
//      computes the Smoothed Pseudo Wigner-Ville distribution of a discrete-time signal X, or the 
//	cross Smoothed Pseudo Wigner-Ville representation between two 
//	signals. 
//    Examples
//       sig=fmlin(128,0.05,0.15)+fmlin(128,0.3,0.4);   
//       g=tftb_window(15,'Kaiser'); h=tftb_window(63,'Kaiser'); 
//       tfrspwv(sig,1:128,64,g,h,1,'plot');
//    Authors
//      H. Nahrstaedt - Aug 2010
//	F. Auger, May-August 1994, July 1995.
//	Copyright (c) 1996 by CNRS (France).

//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
  [nargout,nargin]=argn(0);
  fname="tfrspwv"
 

  in_par=['x','t','N','g','h','ptrace','opt_plot'];
  in_par_min=1;
  plotting=%f;
  if (nargin>in_par_min)
    if (type(evstr(in_par(nargin)))==10)
      if convstr(evstr(in_par(nargin)))=="plot" then
        plotting=%t; 
        nargin=nargin-1;
      end; 
    end;
  end;
  clear in_par;

    if nargin == 0 then
    error(msprintf(_("%s: Wrong number of input argument: %d to %d expected.\n"),fname,1,7));
  end
  
  //x
  if type(x)<>1 then
    error(msprintf(_("%s: Wrong type for input argument #%d: A matrix of double expected.\n"),fname,1));
  end
  if or(size(x)==1) then
    x=x(:);
  end
  if size(x,2)>2 then
     error(msprintf(_("%s: Wrong size for input argument #%d: A  vector or a two columns array expected.\n"),fname,1));
  end
  [Nx,xcol]=size(x);

  //ptrace
  if nargin==6 then 
    if and(type(ptrace)<>[1 4])|size(ptrace,"*")<>1 then
      error(msprintf(_("%s: Wrong type for argument #%d: A boolean or real scalar expected.\n"),fname,6));
    end
  else
    ptrace=%f;
  end
  
  //h
  if nargin>=5 then 
    if type(h)<>1|and(size(h)>1)|~isreal(h) then 
      error(msprintf(_("%s: Wrong type for argument #%d: A real vector expected.\n"),fname,5));
    end
    hlength=size(h,"*");
    if (modulo(hlength,2)==0),
      error(msprintf(_("%s: Wrong size for argument #%d: A odd number of elements expected.\n"),fname,5));
    end;
  else
    hlength=floor(Nx/4);
    if rem(hlength,2)==0 then hlength=hlength+1;end;
    h = window("hm",hlength)
  end
  h=h(:);
  Lh=(hlength-1)/2; h=h/h(Lh+1);
  //g
  if nargin>=4 then 
    if type(g)<>1|and(size(g)>1)|~isreal(g) then 
      error(msprintf(_("%s: Wrong type for argument #%d: A real vector expected.\n"),fname,4));
    end
    glength=size(g,"*");
    if (modulo(glength,2)==0),
      error(msprintf(_("%s: Wrong size for argument #%d: A odd number of elements expected.\n"),fname,4));
    end;
  else
    glength=floor(Nx/10);
    if rem(glength,2)==0 then glength=glength+1;end;
    g = window("hm",glength)
  end
  g=g(:);
  Lg=(glength-1)/2; //g=g/g(Lg+1);
 
  //N
  if nargin>=3 then
    if type(N)<>1|size(N,"*")>1|~isreal(N)|int(N)<>N then
      error(msprintf(_("%s: Wrong type for input argument #%d: A integer value expected.\n"),fname,3));
    end 
    if N<=0 then
      error(msprintf(_("%s: Wrong value for input argument #%d: A positive integer expected.\n"),fname,3));
    end
  else
    N=Nx
  end
  
  //T
  if nargin>=2 then
    if type(t)<>1|and(size(t)>1)|~isreal(t) then
      error(msprintf(_("%s: Wrong type for input argument #%d: A real vector expected.\n"),fname,2));
    end
    if max(t)>Nx|min(t)<1 then
      error(msprintf(_("%s: Wrong value for input argument #%d: The elements must be in the interval %s.\n"),fname,2,"[1 "+string(Nx)+"]"));
    end
  else
    t=1:Nx;
  end
  Nt=size(t,2);


  tfr= zeros (N,Nt) ;  
  if ptrace, disp('Smoothed pseudo Wigner-Ville distribution'); end;
  for icol=1:Nt,
    ti= t(icol); 
    taumax=min([ti+Lg-1,Nx-ti+Lg,round(N/2)-1,Lh]);
    if ptrace, disprog(icol,Nt,10); end;
    points= -min([Lg,Nx-ti]):min([Lg,ti-1]); 
    g2=g(Lg+1+points); g2=g2/sum(g2);
    tfr(1,icol)= sum(g2 .* x(ti-points,1) .* conj(x(ti-points,xcol)));
    for tau=1:taumax,
      mumin=-min([Lg,Nx-ti-tau]);mumax=min([Lg,ti-tau-1]);
      points= mumin:mumax; 
      g2=g(Lg+1+points); 
      normT=sum(g2)
      g2=g2/sum(g2);
      R1=sum(g2 .* x(ti+tau-points,1) .* conj(x(ti-tau-points,xcol)));
      tfr(  1+tau,icol)=h(Lh+tau+1)*R1;
      R2=sum(g2 .* x(ti-tau-points,1) .* conj(x(ti+tau-points,xcol)));
      tfr(N+1-tau,icol)=h(Lh-tau+1)*R2;
    end;
    
    tau=round(N/2); 
    if (ti<=Nx-tau)&(ti>=tau+1)&(tau<=Lh),
      points= -min([Lg,Nx-ti-tau]):min([Lg,ti-tau-1]); 
      g2=g(Lg+1+points); g2=g2/sum(g2);
      tfr(tau+1,icol) = 0.5 * ...
          (h(Lh+tau+1)*sum(g2 .* x(ti+tau-points,1) .* conj(x(ti-tau-points,xcol)))+...
           h(Lh-tau+1)*sum(g2 .* x(ti-tau-points,1) .* conj(x(ti+tau-points,xcol))));
    end;
  end; 
  if ptrace, printf('\n'); end;

  tfr= real(fft(tfr,-1,1)); 

  if (plotting),
    tfrqview(tfr,x,t,'tfrspwv',g,h);
  end;
  if (nargout==3),
    f=(0.5*(0:N-1)/N)';
  end;


endfunction
