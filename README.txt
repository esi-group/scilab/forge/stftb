                Scilab Time Frequency Toolbox(STFTB)
                ==========================

The Time-Frequency Toolbox (STFTB) is a collection of about 100 scripts
for Scilab developed for the analysis of
non-stationary signals using time-frequency distributions. It is
primary intended for researchers and engineers with some basic
knowledge in signal processing.

The toolbox contains numerous algorithms performing time-frequency
analysis with a special emphasis on quadratic energy distributions of
the Cohen and affine classes, and their version enhanced by the
reassignment method. The toolbox also includes signal generation
procedures and processing/post-processing routines (with display
utilities).

STFTB (version 2.0 For Scilab 6)
======================


AUTHORS
=======

Francois Auger
Olivier Lemoine
Paulo Gonvalves
Patrick Flandrin
Holger Nahrstaedt
Serge Steer (Scilab 6 port)
